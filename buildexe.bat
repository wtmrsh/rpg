@echo off
IF "%1" == "" GOTO bad_args
IF "%2" == "" GOTO bad_args

rmdir /s /q dist
rmdir /s /q build

set NAME=%1
set ENCRYPTKEY=%2
set DISTDIR=dist
set GAMEDIR=%DISTDIR%\games\%NAME%

pyinstaller main.py ^
            --noconfirm ^
            --clean ^
            --name %NAME% ^
            --onefile ^
            --noconsole ^
            --key=%ENCRYPTKEY% ^
            --hidden-import=engine.traits.body ^
            --hidden-import=engine.traits.collective ^
            --hidden-import=engine.traits.intelligence ^
            --hidden-import=engine.traits.inventory ^
            --hidden-import=engine.traits.key ^
            --hidden-import=engine.traits.life ^
            --hidden-import=engine.traits.lockable ^
            --hidden-import=engine.traits.logicstate ^
            --hidden-import=engine.traits.openable ^
            --hidden-import=engine.traits.useable ^
            --hidden-import=engine.traits.vision ^
            --hidden-import=engine.traits.wearable ^
            --hidden-import=engine.views.infoview ^
            --hidden-import=engine.views.inventoryview ^
            --hidden-import=engine.views.mapview ^
            --hidden-import=engine.views.messageview ^
            --hidden-import=engine.views.pickupview ^
            --hidden-import=engine.views.windowview ^
            --hidden-import=games.%NAME%.traits.guardintelligence ^
            --hidden-import=games.%NAME%.traits.hasloot ^
            --hidden-import=games.%NAME%.traits.pickable ^
            --hidden-import=games.%NAME%.init
            
IF %ERRORLEVEL% NEQ 0 echo pyinstaller failed. && exit /b %ERRORLEVEL%
            
copy libtcod.dll %DISTDIR%
copy openal32.dll %DISTDIR%
copy SDL2.dll %DISTDIR%
copy bearlibterminal\BearLibTerminal.dll %DISTDIR%

copy rpg.ini %DISTDIR%

md %GAMEDIR%
md %GAMEDIR%\entities
md %GAMEDIR%\maps
md %GAMEDIR%\resources

xcopy /E games\%NAME%\entities %GAMEDIR%\entities\
xcopy /E games\%NAME%\maps %GAMEDIR%\maps\
xcopy /E games\%NAME%\resources %GAMEDIR%\resources\

copy games\%NAME%\entities.json %GAMEDIR%\
copy games\%NAME%\yoink.ini %GAMEDIR%\
copy games\%NAME%\facilitator.py %GAMEDIR%\
copy games\%NAME%\factions.py %GAMEDIR%\
copy games\%NAME%\mapobjects.py %GAMEDIR%\
copy games\%NAME%\prereqs.py %GAMEDIR%\
copy games\%NAME%\tiles.json %GAMEDIR%\
copy games\%NAME%\views.json %GAMEDIR%\
copy games\%NAME%\oryx.tsx %GAMEDIR%\
copy games\%NAME%\oryx-entities.tsx %GAMEDIR%\

exit /b 0

:bad_args
echo buildexe.bat :game: :encryption-key:
exit /b 1