"""Event system"""

from collections import defaultdict
import inspect
import bisect

# TODO:
# Debug mode should:
# - Log call-stack for when on and off are called
# - Log number of times a handler is called, and for each its arguments and call-stack

# Dictionary mapping event source to another dictionary, which in turn maps event names to handlers
# TODO: see if it's quicker to store as a dictionary mapping (event source, event name) pair to handlers
events = {}

# Dictionary mapping event names to handlers, for when we want to fire for all sources
allSources = {}

#
# EventHandlerNotFound
#
# Exception for if a handler is not found in an event sink.
#
class EventHandlerNotFound(Exception):
    """Raised if a handler wasn't found"""

    def __init__(self, event, handler):
        self.event = event
        self.handler = handler

    def __str__(self):
        return "Handler {} wasn't found for event {}".format(self.handler, self.event)

#
# EventNotFound
#
# Exception for if an event is not found in an event sink.
#
class EventNotFound(Exception):
    """Raised if an event wasn't found"""

    def __init__(self, event):
        self.event = event

    def __str__(self):
        return "Event {} wasn't found".format(self.event)

class EventHandler:

	def __init__(self, source, event, priority, maxTriggers, handler):
		self.source = source
		self.event = event
		self.priority = priority
		self.maxTriggers = maxTriggers
		self.numTriggers = 0
		self.handler = handler
		
	def __cmp__(self, other):
		return self.priority < other.priority
		
	def __eq__(self, other):
		if self.source != other.source:
			return False
		if self.events != other.event:
			return False
		if self.handler != other.handler:
			return False
			
		return True
		
	def execute(self, *args, **kwargs):
	
		if self.maxTriggers != 0:
			self.handler(self.source, *args, **kwargs)
			self.maxTriggers -= 1
			self.numTriggers += 1

		if self.maxTriggers == 0:
			pass
			#off(self) Is this safe to do if it's within a loop?  Might invalidate iterator
		
#
# on()
#
# Adds handlers for a given source and event.  If source is None,
# then the handler will be called regardless of which source triggers it.
#
def on(source, event, handler, priority=0, maxTriggers=-1):  # pylint: disable=invalid-name
	"""Register a handler to a specified event"""

	def _on_wrapper(handler):
		"""wrapper for on decorator"""
		eh = EventHandler(source, event, priority, maxTriggers, handler)

		if source:
			# Just add the handlers to this specific source
			if source not in events:
				events[source] = defaultdict(list)

			sink = events[source]
			
			bisect.insort(sink[event], eh)
		else:
			# Add the handlers to a separate lookup
			if event not in allSources:
				allSources[event] = list()

			bisect.insort(allSources[event], eh)

		return eh

	if handler:
		return _on_wrapper(handler)
	return _on_wrapper

#
# off()
#
# Removes handlers for a given source and event.
#
def off(handler):
	"""Unregister an event or handler from an event"""

	source = handler.source
	event = handler.event
	
	# If we've given a source, remove handlers listening to it
	if source:
		sink = events[source]
		
		# If we haven't given an event, remove all of the handlers for this source
		# by removing all of the events
		if not event:
			sink.clear()
			return

		if not event in sink:
			raise EventNotFound(event)

		# If we've given a specific list of handlers, check each one is in the given sink
		if not handler in sink[event]:
			raise EventHandlerNotFound(event, handler)
		sink[event].remove(handler)
	else:
		if event not in allSources:
			raise EventNotFound(event)
			
		if not eh in allSources[event]:
			raise EventHandlerNotFound(event, handler)
		allSources[event].remove(eh)
	
#
# trigger()
#
# Fires event handlers that listen for a given source/event.
#
def trigger(source, event, *args, **kw):
	"""Trigger all functions which are subscribed to an event"""

	# Go through events from a specific sender
	if source in events:
		sink = events[source]
		handlers = sink.get(event) or []
	else:
		handlers = []

	# Go through events from any sender
	if event in allSources:
		handlers.extend(allSources.get(event))
			
	for eh in handlers:
		# TODO: if we encounter a state exception, store it and continue with the other events
		try:
			#print(inspect.stack())
			eh.execute(*args, **kw)
		except Exception as e:
			raise e

#
# print_sink()
#
# Prints out the contents of an event sink
#
def print_sink(sink, source, event):
	"""Print contents of an event sink"""
	if event:
		functions = sink[event]
		for func in functions:
			print(source, event, func)
	else:
		for event,functions in sink.items():
			for func in functions:
				print(source, event, func)

			
#
# print_handlers()
#
# Prints out the active handlers
#
def print_handlers(source=None, event=None):
	"""Shows all handlers"""
	if source:
		print_sink(events[source], source, event)
	else:
		for source,sink in events.items():
			print_sink(sink, source, event)
