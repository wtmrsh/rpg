import random

def get_movement_chains(graph):
    vertices = set(graph.keys())

    cycles = []
    while len(vertices) > 0:
        source = vertices.pop()
        cycle = [source]

        target = graph[source]
        while target in graph:

            if target == source:
                # Found a cycle
                cycles.append(cycle)
                break
            elif target not in vertices:
                # Found a chain
                break

            cycle.append(target)
            vertices.remove(target)
            target = graph[target]

    return cycles

graph1 = {
    'stanmore': 'canary wharf',
    'canary wharf': 'stratford',

    'bank': 'waterloo',
    'waterloo': 'bank',

    'kings cross': 'edinburgh',

    'victoria': 'aldgate',
    'aldgate': 'farringdon',
    'farringdon': 'edgware road',
    'edgware road': 'victoria',

    'tufnell park': 'euston',
    'euston': 'angel',
    'angel': 'kennington',
    'kennington': 'clapham common',
    'clapham common': 'morden',
}

chains = get_movement_chains(graph1)