"""
Main module.

Formatted 17/09
"""

#
# Imports
#
import sys
import time
import logging
import code

import engine.ini
import engine.audio
import engine.gamemodule
import engine.entitydefinitions
from bearlibterminal import terminal
from common import *
from engine.exceptions import *
from engine.settings import GameSettings
from engine.statemanager import StateManager
from engine.viewmanager import ViewManager


#
# main()
#
def main(argv=None):
    """
    Entry point.
    
    :param argv: command line arguments
    :return: 0 on successful completion, non-zero otherwise
    """
    try:
        settings, args = engine.ini.process_command_line(argv)

        # Load settings by merging game-specific settings into default ones
        mainini = engine.ini.parse_ini_file(ENGINE_INI_FILE)

        gamename = settings.game
        if not gamename:
            try:
                gamename = mainini['Game']['Game']
            except KeyError:
                pass

        if not gamename:
            print(f'No game specified, either in {ENGINE_INI_FILE} or on the command-line via -g')
            return 1

        GameSettings.game = gamename

        gameinifile = './{0}/{1}/{1}.ini'.format(GAMES_DIR, gamename)
        gameini = engine.ini.parse_ini_file(gameinifile)
        mainini.update(gameini)

        # Load in game
        try:
            gamemodulename = f'games.{GameSettings.game}.init'
            gamemodule = __import__(gamemodulename, fromlist=['*'])
        except Exception as e:
            raise Exception('Could not load Game initialiser.') from e

        # Set up terminal
        terminal.open()
        
        # http://foo.wyrd.name/en:bearlibterminal:reference:configuration
        windowtitle = GameSettings.programName + ' v' + str(GameSettings.programVersion)
        configstrings = [
            # Set window options, including cell size (currently square cells)
            'window: size={0}x{1}, cellsize={2}x{2}, fullwindow=false, title={3}'.format(GameSettings.windowWidth, GameSettings.windowHeight, CELL_SIZE, windowtitle),
            
            # Set default font
            # 'font: {0}, size={1}'.format(FONT_NAME, FONT_SIZE),
            
            # Both keyboard and mouse (listening to mouse releases as well), and trigger all mouse move events
            'input: filter=[keyboard, mouse+], precise-mouse=true, mouse-cursor=false',
            
            # Allow format tags in text
            'output: postformatting=true, vsync={0}, tab-width={1}'.format('true' if ENABLE_VSYNC else 'false', TAB_WIDTH),
            
            # Set log details
            'log: file={0}, level={1}, mode=truncate'.format(BLT_LOGFILE, BLT_LOGLEVEL)
        ]
        
        for configString in configstrings:
            terminal.set(configString)

        # Allow tiles to be layered on top of each other
        terminal.composition(terminal.TK_ON)

        engine.ini.create_settings_from_ini(mainini)

        engine.entitydefinitions.definitions = engine.ini.parse_entity_definitions(f'./{GAMES_DIR}/{GameSettings.game}/entities.json')
        ViewManager.viewDefinitions = engine.ini.parse_view_definitions(f'./{GAMES_DIR}/{GameSettings.game}/views.json')

        # Load audio
        engine.audio.load_sounds(f'./{GAMES_DIR}/{GameSettings.game}/resources/sounds')

        # Setup
        gameuserfunction = getattr(gamemodule, 'get_user_function_module')
        engine.gamemodule.set_function_modules(gameuserfunction('facilitators'),
                                               gameuserfunction('prerequisites'),
                                               gameuserfunction('aptitudes'),
                                               gameuserfunction('factions'),
                                               gameuserfunction('dynamictrans'),
                                               gameuserfunction('intelligencetasks'))

        # Intelligence tasks are registered by decorator, so load those in explicitly
        engine.gamemodule.get_game_intelligence_tasks_module()

        gameinitialiser = getattr(gamemodule, 'get_initial_state')
        statemanager = StateManager(gameinitialiser()())
        statemanager.join()

        # Initial refresh, required to consume close window event
        terminal.refresh()

        # Main loop
        curtime = time.perf_counter()
        while True:
            oldtime = curtime
            curtime = time.perf_counter()
            frametime = curtime - oldtime

            # Process input
            statemanager.handle_input()

            # Update
            statemanager.update(frametime)

            # Audio
            engine.audio.update_sounds(frametime)

            # Render
            statemanager.render()

            # Blit to window
            terminal.refresh()

    except ExitException as e:
        exitCode = e.exitCode
    except Exception:
        type, value, traceback = sys.exc_info()
        logging.exception(' an exception was thrown.')
        exitCode = 1
    else:
        exitCode = 2

    # Shutdown
    engine.audio.unload_sounds()
    terminal.close()

    return exitCode


#
# Non-module entry
#    
if __name__ == '__main__':
    sys.exit(main())
