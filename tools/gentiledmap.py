"""
Create an empty tiled map for editing.
"""

#
# Imports
#
import sys
import os
import json

#
# createTileLayer()
#
def createTileLayer(name, width, height, **kwargs):
    """
    Create a tile layer.

    :param name: name of layer
    :param width: width of layer in tiles
    :param height: height of layer in tiles
    :param kwargs: extra properties
    :return: layer as a dictionary
    """
    layer = {
        'name': name,
        'type': 'tilelayer',
        'data': [0] * (width * height),
        'width': width,
        'height': height,
        'x': 0,
        'y': 0,
        'opacity': 1,
        'visible': True
    }

    layer.update(kwargs)
    return layer

#
# createObjectLayer()
#
def createObjectLayer(name, **kwargs):
    """
    Create an object layer.

    :param name: name of layer
    :param kwargs: extra properties
    :return: layer as a dictionary
    """
    layer = {
        'name': name,
        'type': 'objectgroup',
        'objects': [],
        'x': 0,
        'y': 0,
        'opacity': 1,
        'visible': True,
        'draworder': 'topdown'
    }

    layer.update(kwargs)
    return layer

#
# main()
#
def main(argv=[]):
    """
    Entry point.

    :param argv: command line arguments
    :return: 0 on successful completion, non-zero otherwise
    """
    if len(argv) < 4:
        print('Syntax: gentiledmap.py <name> <width> <height> <filename>')
        return 1

    mapName = argv[0]
    width = int(argv[1])
    height = int(argv[2])
    mapPath = argv[3]

    scriptName = mapName.lower().replace(' ', '')

    # Layers
    layers = []

    layers.append(createTileLayer('Ground', width, height))
    layers.append(createTileLayer('Overlay', width, height))
    layers.append(createTileLayer('Decoration', width, height))

    layers.append(createObjectLayer('World Entities'))
    layers.append(createObjectLayer('Entities'))

    areaLayer = createObjectLayer('Areas', color='#ffff00')

    # Start area
    startArea = {
        'name': '',
        'id': 1,
        'type': 'StartArea',
        'width': 24,
        'height': 24,
        'x': 0,
        'y': (height - 1) * 24,
        'rotation': 0,
        'visible': True,
        'properties': {
            'Direction': 'NORTH'
        },
        'propertytypes': {
            'Direction': 'string'
        }
    }
    areaLayer['objects'].append(startArea)
    layers.append(areaLayer)

    layers.append(createObjectLayer('Lights'))
    layers.append(createObjectLayer('Paths'))

    # Properties
    properties = {
        'Name': mapName,
        'Script': scriptName
    }

    propertyTypes = {
        'Name': 'string',
        'Script': 'string'
    }

    # Main data
    jsonData = {
        'height': height,
        'width': width,
        'layers': layers,
        'tileheight': 24,
        'tilewidth': 24,
        'nextobjectid': 1,
        'orientation': 'orthogonal',
        'renderorder': 'right-up',
        'tiledversion': '1.0.3',
        'tilesets': [],
        'type': 'map',
        'version': 1,
        'properties': properties,
        'propertytypes': propertyTypes
    }

    # Write data
    with open(mapPath, 'wt') as mapFile:
        fileData = json.dumps(jsonData, indent=4, sort_keys=True)
        mapFile.write(fileData)

    # Create script
    with open('script.py.template', 'rt') as scriptInFile:
        scriptData = scriptInFile.read()
        scriptData = scriptData.replace('!!NAME!!', mapName)

        scriptFileName = scriptName + '.py'
        scriptPath = os.path.join(os.path.dirname(mapPath), scriptFileName)
        with open(scriptPath, 'wt') as scriptOutFile:
            scriptOutFile.write(scriptData)

#
# Non-module entry
#
if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))