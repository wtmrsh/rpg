# Entity Definitions
Entities are currently defined in JSON, loaded in, and converted to nested dictionaries/lists, and then passed in to each `Map` instance.  This structure is keyed on the entity class name.

## Inherits

This defines an inheritance hierarchy which lets you compose entity definitions.  It should refer to another entity definition in the same file, which does not necessarily have to come before it.

## CodeClass

This specifies the name of the class in code which should be instantiated.  If not set, it uses the first name in the definition hierarchy that it can find an `Entity` subclass for, or Entity itself is nothing is found.
For instance, the definition inheritance hierarchy (as defined with the *Inherits* key) of Entity -> Human -> Guard would normally first look to see if a subclass of `Entity` named `Guard` exists, and if not then look for `Human`, etc.  Specifying **CodeClass** ignores this and directly uses the class specified.

## Properties

This is a dictionary, containing properties describing the nature of the Entity.

| Key               | `Entity` member | Type         |Required | Default |
| ----------------- | --------------- | ------------ | ------- | ------- |
| names             | names           | `list[2]`    | Yes     |         |
| size              | size            | `EntitySize` | Yes     |         |
| attached-to-world | attachedToWorld | boolean      | Yes     |         |
| colour            | rgba            | colour       | No      | White   |
| inventory-type    | inventoryType   | string       | Yes     |         |
| factions          | factions        | `set`        | No      | *empty* |

### names

*names* is a list of two string, which define the printable names of a single instance of this entity, and of multiple instances.

### size

*size* is the name of an entry in the `EntitySize` enum, which is currently one of:
- NONE
- SMALL
- LARGE

### attached-to-world

This is a boolean value, which determines such things as whether an Entity can be picked up, etc.

### colour

This is string which can be in various format, as long as it is convertible to a `Colour` instance.  For example:
- White # Named
- 0xFFAA0808 # 32-bit hex
- #FF00FF # 24-bit web code
- 255 128 255 # 24-bit tuple
- 255 128 255 128 # 32-bit tuple

### block-sight

This is a boolean value, which determines such things as whether an Entity blocks another's vision, etc.

### inventory-type

This is a string, which should map to a pre-defined value in the game.  It is mainly used for ordering inventory in views, etc.

### factions

This is a list of strings, which should map to the name of a faction, as defined in the `Factions` enum.

## Arguments

This is a dictionary

## Actors

This is a dictionary, keyed on the action name, of actor declarations.  A actor declaration consists of a dictionary, which includes a list of prerequisite functions, a facilitator and an aptitude function.
None except a facilitator is required, so unless it inherits one from a base class there will be an error.

## Targets

This is a dictionary, keyed on the action name, of target declarations.  A target declaration consists of a dictionary, which includes a list of prerequisite functions.

## Traits

This is a dictionary, keyed on the trait name, of trait declarations.  A trait declaration can have the following members:

### Actors

This is a dictionary, keyed on the action name, of actor declarations.  A actor declaration consists of a dictionary, which includes a list of prerequisite functions and a facilitator.
Neither have to be specified, but a facilitator is required, so unless it inherits one from a base class there will be an error.  Unlike the main Actors declaration, this is used for actions exposed by the trait itself.

### Targets

This is a dictionary, keyed on the action name, of target declarations.  A target declaration consists of a dictionary, which includes a list of prerequisite functions.
Unlike the main Targets declaration, this is used for actions exposed by the trait itself.

### Parameters

This is a dictionary of named parameters which gets passed into the trait constructor.

### Specialisation

This is a string, and is used when we want to override an inherited trait with a subclass of that trait.  For instance, to give a Guard entity - which has inherited the Intelligence trait from Human - the GuardIntelligence trait.

## State Management

### Handlers

This is a dictionary, mapping states to a list of trait handlers to be called when that state is set (or unset, if marked as -state)

### Blockers

This is a dictionary, holding different things which can be blocked - pathing, movement, audio and sight - with a set of states that specify whether it is true or not.

### Visuals

This is a list of rules to apply to the current stateset, to determine which visual to use.  All bar the final string in the list are evaluated against the state set, and if they match, the last entry in the list is used as the visual.

## Stats

This is a dictionary of entity stats.  The value is evaluated, so you can use operators and engine constants.