# .ini file settings
Each game has an ini file, which should be in the game root directory, and be named *game*.ini.

The sections are as follows:

## Logging

File: the name of the log file, base directory being the working directory of the executable process.
FileLevel: one of DEBUG, INFO, WARNING, ERROR, CRITICAL, NONE.
StdoutLevel: one of DEBUG, INFO, WARNING, ERROR, CRITICAL, NONE.

## Display

Font: font file, must be placed in /resources/fonts within the game directory.
FontSize: size to generate fonts at.

## Game

Game: name of the game, corresponds to a directory in the working directory of the executable process.

## UserInterface

This is a list of user interface declarations.  The format is:
*UserInterfaceName*: *image*, *cell size*, *width in cells*, *height in cells*, *code offset*
- image: this is an image file, which must be placed in /resources/images within the game directory.
- cell size: this is the pixel size of a cell, in multiples of `CELL_SIZE` (defined in common.py)
- width in cells: the number of horizontal cells in the image
- height in cells: the number of vertical cells in the image
- code offset: the point in the codepage where you want to insert these as glyphs/code points.  You should make sure not to overwrite any glyphs that you actually need. 0xE500 is a common starting point, then increase in multiplies of 0x100.

## *UserInterfaceX*

For each user interface name defined in the *UserInterface* section, you will need a section named after it.  Each entry here defines a named tile which can be referenced via its name and put directly to the terminal.
For instance:

    [UserInterface1]
    Bar = 0, 0

## ViewDecorators

This is a list of view decorator declarations.  The format is:
*UserInterfaceName*: *image*, *cell size*, *width in cells*, *height in cells*, *code offset*
- image: this is an image file, which must be placed in /resources/images within the game directory.
- cell size: this is the pixel size of a cell, in multiples of `CELL_SIZE` (defined in common.py)
- width in cells: the number of horizontal cells in the image
- height in cells: the number of vertical cells in the image
- code offset: the point in the codepage where you want to insert these as glyphs/code points.  You should make sure not to overwrite any glyphs that you actually need. 0xE500 is a common starting point, then increase in multiplies of 0x100.

## *ViewDecoratorX*

For each view decorator name defined in the *ViewDecorator* section, you will need a section named after it.  Each entry here defines an element of the image system which will be used for views.  For instance:

    [ViewDecorator1]
    TopLeft = 0, 0

These names are predefined, and expected:
- TopLeft
- TopRight
- BottomLeft
- BottomRight
- Top
- Left
- Bottom
- Right
- Centre
- Button-Left
- Button-Centre
- Button-Right
- Button-TopLeft
- Button-Top
- Button-TopRight
- Button-BottomLeft
- Button-Bottom
- Button-BottomRight
- Tooltip-Left
- Tooltip-Centre
- Tooltip-Right

## Cursors

This is a list of cursor image declarations.  The format is:
*CursorName*: *image*, *cell size*, *width in cells*, *height in cells*, *code offset*
- image: this is an image file, which must be placed in /resources/images within the game directory.
- cell size: this is the pixel size of a cell, in multiples of `CELL_SIZE` (defined in common.py)
- width in cells: the number of horizontal cells in the image
- height in cells: the number of vertical cells in the image
- code offset: the point in the codepage where you want to insert these as glyphs/code points.  You should make sure not to overwrite any glyphs that you actually need. 0xE500 is a common starting point, then increase in multiplies of 0x100.

## *CursorX*

For each user interface name defined in the *Cursors* section, you will need a section named after it.  Each entry here defines a named cursor which can be referenced via its name and put directly to the terminal.
For instance:

    [Cursor1]
    Pointer = 0, 0, 0, 0
    Hand = 1, 0, -6, 0

The second two values define the hotspot in pixels, taken from the top-left (so here, -6 means 6 to the right of the origin)