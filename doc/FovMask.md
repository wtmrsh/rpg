# FOV Masks

FOV masks are used by entities with the Vision trait.  The base class is `FovMask` and there are two subclasses:
- A mask loaded from a file.
- A mask defined by a radius and a field of view.

They are created by the helper functions:
- `FovMaskCacheget_mask_from_file()`
- `FovMaskCache.get_mask_from_fov_and_radius()`

## MaskFovMask

These are created via `FovMaskCacheget_mask_from_file()`, which takes a raw image file with 3x3 orientations (matching the compass directions), with the following colours:
- White is in the mask
- Red is the origin of the mask
- Magenta is the origin of the mask, but is not included in the mask itself 
- Any other colour is not in the mask.

### Specifying in Entity Definitions

Set the Vision trait as follows:

    "Vision": {
      "Parameters": {
        "mask": ["\"path/to/file.raw\"", width, height]
      }
    }

## RadialFovMask

These are created via `FovMaskCache.get_mask_from_fov_and_radius()`, and take a radius and an arc size.

### Specifying in Entity Definitions

Set the Vision trait as follows:

    "Vision": {
      "Parameters": {
        "fov": 90,
        "viewdistance": 3
      }
    }