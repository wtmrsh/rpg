# Triggers

Triggers are map areas which fire an event, or use a Useable, when the player moves into them.  They only work with the player as it would be too expensive to listen for all entities in the map.
They are defined in the codebase by the TriggerArea class.

## Tiled definition

Triggers are rectangular areas placed on the **Areas** layer and given the Type property of 'Trigger'.  They then have one of several custom properties, which define what type of action they perform:

### Core.Event

This custom property should be the name of an event which is triggered.  It takes the following parameters:

- Parameters.N, where N is from 1 upwards.  The parameters to be passed into the event handler.

### Core.UseTarget.N

- UseTarget definitions, in JSON format.

If you trigger an actions via a usetarget, these are run with the map as actor, therefore you need to create facilitators, and specify the following custom map property for each:

- Facilitator.<action>

This should be the name of the facilitator function in facilitators.py




