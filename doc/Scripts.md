# Scripts

Map scripts are set via a Map's 'Script' property.  This should be the name of the script file (minus the extension)
relative to the Map file's directory.

The general pattern for scripts is to supply a set of event handlers using the
`mapobjects.map_script_handler` decorator.  This should be used instead of the
standard `observable` functionality, as they will then be unregistered on map exit,
unlike Observables which need explicit de-registration.

The current map, player and model are passed into `Map.Enter`, and can be captured
as global variables in the script to be used in other callbacks.

Useful events to hook include:
- Map.Enter
- Map.Exit: this should raise an appropriate exception, one of:
  - TransitionToStateException
  - SpawnChildStateException 
  - ExitStateException
  - ExitException
