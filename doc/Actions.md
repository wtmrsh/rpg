# Action System

Each entity chooses an action.  There are generally 3 cases here:
- Intelligent entity (ie has the 'Intelligence' trait)
- Non-intelligent entity
- Player

Intelligent entities select their action based on their task-based logic.  Non-intelligent entities always choose `Pass`.  The player gets to choose their own action.

## Requesting an action

This is done with `ActionParticipant.request_action`.  Prerequisites are run on it, and if passed, priority is set and an `EntityActionDefinition` is created for this entity.

## Resolution

The player always gets to go first - any actions interfering with it are set to `Pass`.  The moving entities go, and finally entities performing other actions.  Moving entities
are always guaranteed to be able to move, unless they interfere with the player.  If there are any 'non-moving' entities in their way, those non-moving entities are moved out of
the way and are unable to perform their action.  Finally, any entities which have chosen `Pass` or `Wait` as their action are liable to be moved by other entities.

### Priority

When two entities want to perform actions on the same target, or same tile, then their priority is considered.  The priority is first determined by the type of action -
whether it's the player, whether it's a 'move' action, or whether it's a regular action.  For instance, an entity moving to an open doorway will have priority over one
wanting to close that door.  Then a priority modifier is added, which can be set at an entity definition level, or an entity instance level.

### Aptitude

When two competing entities have the same priority, aptitude functions are considered.  These are functions specified in entity definitions which return a value in [0,1] to act
as a 'tie-breaker'.  They allow custom logic to be used.  For instance, if two entities are trying to pick up the same object, then the aptitude function could check to see which
has the highest 'agility' score.

## Executing an action

Once all the actions have been resolved, the move actions are run in a two-step process.  Then the regular actions are done.  The prerequisites are checked again, because it could
in theory still fail at this point, and then the facilitator is run.  Finally a post- step is run where events are triggered.