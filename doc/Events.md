# Events

## Entity events

### Entity.AddedToMap
Source: added entity

Arguments:
- entity: added entity
- map: map the entity was added to

### Entity.RemovedFromMap
Source: Removed entity

Arguments:
- entity: removed entity
- map: map the entity was removed from

### Entity.SetPlayer
Source: entity in question

Arguments:
- entity: entity in question
- isplayer: whether or not it is the new player entity (bool)

### Entity.Intelligence.TaskComplete
Source: entity in question

Arguments:
- task: name of Intelligence task
- *args: additional list of arguments

### Entity.Intelligence.TaskAborted
Source: entity in question

Arguments:
- task: name of Intelligence task
- *args: additional list of arguments

## Action events

### Entity.Actor.*
Source: actor entity

Arguments:
- Move:
  - x: new tile x-position
  - y: new tile y-position
 
### Entity.Target.*
Source: target entity
Arguments:

### Entity.Target.*.IncompleteSet

## Map events

### Map.Enter
Source: map in question

Arguments:
- map: map entered
- player: player entity
- model: model instance

### Map.Exit
Source: map in question

Arguments:
- map: map entered
- player: player entity
- model: model instance
	
### Map.SightChanged
Source:	entity causing the sight change

Arguments:
- entity: entity causing the sight change
- fromsight: whether the entity was previously blocking sight
- tosight: whether the entity is now blocking sight

## UI events

### UI.RefreshView
Source: view in question

Arguments:
- view: view in question

### UI.RefreshViews
Source: None

Arguments: none

### UI.ClearViews
Source: None

Arguments: none

### UI.View.MouseEntered 
Source: view entered

Arguments:
- view: view entered
- cellx: x-cell in game
- celly: y-cell in game
- pixelx: x-position in window
- pixely: y-position in window
	
### UI.View.MouseExited
Source: view exited

Arguments:
- view: view exited
- cellx: x-cell in game
- celly: y-cell in game
- pixelx: x-position in window
- pixely: y-position in window
	
### UI.View.MouseMoved
Source: view moved in

Arguments:
- view: view moved in
- cellx: new x-cell in game
- celly: new y-cell in game
- pixelx: new x-position in window
- pixely: new y-position in window

### UI.InventoryView.ActionCountChanged
Source: view in question

Arguments:
- actioncount: new action count
  
### UI.InventoryView.ItemHovered
Source: view in question

Arguments:
- view: view in question
- item: item hovered
  
### UI.InventoryView.ItemUnhovered
Source: view in question

Arguments:
- view: view in question
- item: item unhovered
 
## Inventory events

### Inventory.CapacityChanged
Source: inventory in question

Arguments:
- inventory: inventory in question

### Inventory.ItemAdded
Source: inventory in question

Arguments:
- inventory: inventory in question
- entity: entity added
- count: number added

### Inventory.ItemRemoved
Source: inventory in question

Arguments:
- inventory: inventory in question
- entity: entity removed
- count: number removed

## Game events

### Game.Message
Source: None

Arguments:
- msg: message
- entity: entity sending the message

### Game.TurnTaken
Source: None

Arguments:
- turn: current turn
