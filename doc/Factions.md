# Factions

Factions define the relationship between entities, and are used by Intelligence traits to determine how an entity should react to another.  They can be defined in an Entity Definition.

Factions are defined in a file called `factions.py` within the root game directory.  This is loaded via reflection in the engine.  The engine expects to find the following within this file:
- An enum called *Factions* defined as follows:

    class Faction(IntFlag):
      FACTION1 = auto()
      FACTION2 = auto()
  
- A function called *get_faction_relationship* defined as follows:

`def get_faction_relationship(ent1, ent2):`

This should return a `FactionRelation`