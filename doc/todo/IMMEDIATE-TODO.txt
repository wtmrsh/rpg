Bug:
- We don't want items in inventories to perform actions, as the turn resolution system doesn't work if more than one entity on the same tile performs an action.
  - Need a way to determine if an entity is a) and item and b) in an inventory

Immediate plan:

0. Create a method somewhere to see if an action is a movement action or not, so we can use it in actionparticipant.py:129 and elsewhere [DONE]
   - Current one isn't quite right as owning_tile type being Door might mean an action on an item in an open doorway. [DONE]
1. Change priority tuple in EntityActionDefinition into a class/dataclass with property for priority value so we don't have to put *100 everywhere [DONE]
   - Check priority modifier is in [-99, 99] [DONE]
2. Map should have an debug option to not scroll to keep player fixed, or should be global debug option [DONE]
3. Make sure player actions get set as ActionPriority.Player, and that these are done first. [DONE]
   - Set any contending actions to Pass [DONE]
   - Set up a new map to test that players don't get bumped when waiting, and that they can walk into entities [DONE]
     - Make sure player's move action works with other entities' [DONE]
4. Implement priority modifiers for entities (in entitydef and maps) for relative modification of non-move actions [DONE]
5. Finish movement
   - Keep the current system, but we have to assume that if the player enters into a movement graph, then crushing may occur [DONE]
   - Rename ActionPriority.Lowest to ActionPriority.Pass [DONE]
   - Move graph stuff and anything else which doesn't need Map (ie static) out of Map [DONE]   
   - Handle crushing (see CRUSHED-ENTITIES.txt) [DONE]
   - Wait should become a non-move action, and elevatable.  Think of it as a non-move action where the actor does nothing. [DONE]
6. Do contention of non-move actions (separately for elevated and not) grouping by modified priority, and then for each
   group using aptitudes.
   - New test map [DONE]
     - Have a patroller press a button to open a door, which can be interrupted by movers. [DONE]
	   - Set so that only the patroller can use the switch, to demonstrate that. [DONE]
	   - Action needs to lock/unlock doors, how do we deal with doors being open? [DONE]
	   - Has trouble pathing to the use location when the player in the way [DONE]
   - Check entities can path in all situations:
     - Player standing on a tile, not moving [DONE]
	 - NPC standing on a tile, not moving [DONE]
     - Player blocking a doorway [DONE]
	 - NPC blocking a doorway [DONE]
   - If we only have one move task in an intelligence definition it gets stuck when it reaches the end of a cycle because it tries to move to the place
     it's already at.  We should catch this case (cyclic, one move task) and throw an error.  The correct fix is to make it non-cyclic, and then we need logic
	 to make it Pass forever after that, ie at the end of the task list it doesn't return an action. [DONE]
   - Contention between entities trying to open and lock a door at the same time [DONE]
     - Add in aptitude functions [DONE]
   - Contention between an entity pressing a button to open a door, and another trying to lock the door [DONE]
7. Static transitions in taskdefs should take a list of tasks and choose randomly [DONE]
	- Document [DONE]
8. Document [DONE]
   - Action resolution [DONE]
     - Priority [DONE]
	 - Aptitude [DONE]
9. Dynamic transitions
   - Where do we define the functions?  Need to be loaded in before entities are created, ie on startup [DONE]
   - Check them in the game module instead of registering with decorator [DONE]
   - Supertasks need some kind of args, eg for ChaseHostile we need to set the target entity
     - The dynamic trans function needs to return a payload as well as True/False [DONE]
	 - Intelligence.set_task should take supertask params which are then accessible to subtasks [DONE]
	 - Need to set params appropriately for all supertask transitions (static, dynamic, initial, SetIntelligenceTask action)
	   - For initial, the InitialTask string should change to a dict [DONE]
	   - For static, params are there too [DONE]
	   - For dynamic, dynamic-trans function returns params to pass into next supertask, if the supertask has params defined then these are overridden [DONE]
	   - SetIntelligenceTask should be params [DONE]
   - Document dynamic trans functions in intelligence docs and new schema with params etc [DONE]
10. For task def, if no x/y param given, take the previous task's.  (Needed for move, and first task of course) [DONE]
    - Params entry should then be optional [DONE]
	- Document [DONE]
11. Error (rather than hanging) when pathfinding can't compute a path [DONE]
12. Vision fixes [DONE]
	- Currently, rendering NPC visible tiles.  Need to:
	  - Way to toggle this on/off, either make it a feature of the base Map class, or via subclass, or as some kind of extension [DONE]
	    - Specify whether to show player, NPC, or all [DONE]
	- Implement the show hostiles/non-hostiles functionality in HostileTileVisibleMapViewPlugin [DONE]
13. What is the Map's acceleration grid used for? [DELETED]
14. Make sure Map.add_entity() and Map.add_entity_to_map() are used properly [DONE]
15. Dynamic task change to chase player	
    - Per-entity type overrides for intelligencetask, so each entity type could implement 'EngageTarget' in a different way. [DONE]
	  - These are classes so we need to add them to the list of things like dynamic-trans functions, prereqs, etc [DONE]
	    - Document [DONE]
    - Implement a simple 'chase-and-tag' intelligencetask for Guards' 'EngageTarget' task, and use in test map. [DONE]
    - Better vision mask for guards in test map	[DONE]
16. Implement hearing trait [DONE]
17. Look at the enable trait action, document etc. [DONE]
18. Destructible trait, eg for doors, windows, which affects pathing, vision, movement, etc.
    - Rework Trait's blocks_ functionality [DONE]
	- Visual for destroyed door [DONE]
	- Add test destroyable door [DONE]
	- Vision not updating when open/close/destroy door [DONE]
	- How do we handle multiple status setters (eg Openable and Destroyable, what if we have a closed/destroyed door?)
	  - Here, the entity's status should be a comma-separated list - eg "Closed,Destroyed" - and we need to call all the visualstatesetters
      - Rework trait states:
	    - Need to do blocks_pathing and blocks_movement: but check how these are actually used, first
		  - For blocks_pathing, traits probably need a function they can call, eg Lockable checks to see if the actor has the correct key.
		    - So define functions for each of vision/audio/movement/pathing in traits and call them
		  - check that the movement test maps still work as expected
		    - can an entity with a key path through a locked door?
			  - at the moment we're breaking the task down for them, but maybe we have to get the path, and then go through it and see if there are
			    any doors and break the task down like that.
		  - pathing needs entity logic, eg an entity can path through a locked door if it has the key
        - Unify visualstatesetters with this: eg if something starts destroyed, we may want to call functions in multiple traits.
        - Remove the check_blocks_ functions from Entity
	  - Update traits properly in Entity.turn()
    - Write up traits, states, properties and how they interact
19. All function args with default values of list, dict, etc should be None, and set manually [DONE]
20. PriorityModifierEntityProperty doesn't seem to be implemented
21. Create new maps for each of the different aspects of AI behaviour
    - Task failure handlers
	- Task events
	- Task static transitions
	- Task dynamic transitions
	  - Proper 'chase hostile' action, which we definitely will need in the future.
22. Look into some kind of automated testing, eg play each test map and get it to record the state of everything to disk, then replay the
    map and check the state on each turn against the recorded state.
	- Don't need a window, just start the game up and run each turn in a loop.
23. Better loading of EntityDefinitions, more checking, and unify mechanism with tiled loader	
24. Update documentation

---

- Look at Entity.register_events() / unregister_events() and see where it should be called:
  - On creation, or on added to map?
  - Make sure to give traits a similar on_added/removed callback where hearing can register itself
  - Do the on-added setup calls in the appropriate place, eg map/entity/trait

- Validate entitydef, and once merged together, convert the nested dictionary structure into
  dataclasses or namedtuple as appropriate

- Create audio system
  - Rework current Entity.emit_sound to put that functionality into
    audio system, and register entities with hearing with it.
  - Come up with spec for when text should be printed for the player
  - Unify the Handlers/Visual/Audio/Blockers spec, so we can parse it
    once for each of them, eg have specific keys to look for, to make
    it flexible enough for all.
    - Need a triggering action (eg open, or close), optional additional
      state, etc.
  - Need to hook an entity's hearing callback into its AI somehow

Spec:
- We need an action which can trigger it, eg a door opening or closing
- We need optional additional states which need to be matched after the
  action is done.  Eg if we have action '-Open' and additional states
  '-Locked' and '-Destroyed', then we do the -Open action and check to see
  if all the Open, Locked and Destroyed states are unset


Idea for game with RPG
----------------------
Bit like aliens/stealth game but you have to escape
- Start in a room with supplies, etc.  Room is secure for now
- If you do nothing, eventually aliens will break in
- You have to come up with a plan for escape, based on studying
  CCTV, blueprints, etc
- Tension arises because the longer you stay in the room, the
  more likely you will be attacked, but you have to leave at some
  point.
- Can have premade scenarios and maybe some generated
  - Maybe expand it so you have to survive until rescue arrives, and essentially
    scavenge.  Could be contacted by someone who explains what you need to do,
    eg get computer systems online, etc.


---
New entitydef parsing:

Entity definition specification:
- Dictionary[str:Dictionary]                                               # Maps names of entities to entity definitions
  - [OPTIONAL] str 'Inherits'                                              # Name of entity to use as a base definition.
  - [REQUIRED] Dictionary['Properties':Dictionary] 'Properties'            # Entity base properties
    - [REQUIRED] List[str,2] 'names'                                       # List of two strings: the printable names of one entity and multiple entities, respectively
	- [REQUIRED] str 'category'                                            # Name of category, one of: 'NONE', 'STANDARD', 'ABSTRACT', 'WORLD', 'WALL', 'ITEM'
	- [REQUIRED] str 'inventory-type'                                      # User-defined type for sorting/filtering in inventory.  Needs custom validation, if any
	- [OPTIONAL] str 'colour'                                              # Colour to render entity's name in: defaults to white.  Needs to be parseable by Colour.from_string()
    - [OPTIONAL] List[str] 'factions'                                      # List of factions to which the entity belongs: defaults to none.  Needs to be defined in factions module
	- [OPTIONAL] int 'priority-modifier'                                   # Value by which to modify entity's action priority: defaults to zero.  Must be in [-99, 99]
  - [OPTIONAL] Dictionary['Actors':Dictionary] 'Actors'                    # Action definitions for the Entity as an actor
    - Dictionary[str:Dictionary]                                           # Maps name of action to action definition
	  - [OPTIONAL] str 'Facilitator'                                       # Name of facilitator function: defaults to none.  Expected to be defined in facilitator module.
	  - [OPTIONAL] List[str] 'Prerequisites'                               # List of prerequisite functions: defaults to empty list.  Can remove a prerequisite by prepending '-'.  Expected to be defined in prerequisite module.
	  - [OPTIONAL] str 'Aptitude'                                          # Name of aptitude function: defaults to none.  Expected to be defined in aptitude module.
  - [OPTIONAL] Dictionary['Targets':Dictionary] 'Targets'                  # Action definitions for the Entity as an target
    - Dictionary[str:Dictionary]                                           # Maps name of action to target definition
	  - [OPTIONAL] List[str] 'Prerequisites'                               # List of prerequisite functions: defaults to empty list.  Can remove a prerequisite by prepending '-'.  Expected to be defined in prerequisite module.
  - [OPTIONAL] Dictionary['Traits':Dictionary] 'Traits'                    # Trait definitions for the entity
    - Dictionary[str:Dictionary]                                           # Maps name of trait to trait definition
	  - [OPTIONAL] Dictionary['Actors':Dictionary] 'Actors'                # Actor action definitions inherited by having this trait
	    - See main 'Actors' definition                                     # Cannot have facilitator or aptitude specified at both trait and actor level
	  - [OPTIONAL] Dictionary['Targets':Dictionary] 'Targets'              # Target action definitions inherited by having this trait
	    - See main 'Targets' definition
	  - [OPTIONAL] Dictionary['Parameters':Dictionary] 'Parameters'        # Keyword arguments passed by name to trait initialiser.  Expected to be an expression, a list of expressions, or a dictionary of expressions.  Should match named initialiser args.
  - [OPTIONAL] Dictionary['Stats':Dictionary] 'Stats'                      
      - Dictionary[str:value]                                              # Entity statistics.  Value is expected to be an expression, a list of expressions, or a dictionary of expressions.
  - [OPTIONAL] Dictionary['StateManagement':Dictionary] 'StateManagement'