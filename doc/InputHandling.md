# Input handling

The base `State` class has a member called `playerAction`.  This is initialised to a list of `[None, False]`, which represents:
- An action name
- Whether it should be executed immediately
- Action arguments

It also has a member called `action`, which is actually the result of the player action, and holds:
- Whether the action resulted in a turn being taken
- Whether a message should be printed
- A message for the player, if they performed the action
- A message for the player, when a non-player entity performs the action
- The function to call, if this was delayed

We go through the following stack to get to the player-input handling code:

    mainloop
      StateManager::handle_input()
        State::handle_input()
          ViewManager::handle_input()

At this point, input is read from the terminal, and if it's mouse input, then this is handled via firing the relevant events.
It then checks whether there is a `View` in focus and if so, looks for a handler function with its name, for instance `handle_input_map()` for the `MapView`.
It then calls this function, which returns:
- An action function
- Whether it should be executed immediately
- Action arguments

And this is stored in `State.playerAction`.

Then, when it comes to the update, we check to see if we have a player action to execute:

1. In `State.update()`, check if `self.playerAction` is set.
2. If it is, then create an inline function that:
   1. Extracts the elements and resets `self.playerAction`
   2. If we're executing now, then call the action function, otherwise set the action to be delayed
   3. If we're executing now, the action function returns the action result in `self.action`


