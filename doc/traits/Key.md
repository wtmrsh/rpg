# Key

This trait complements the Lockable trait, and is simply a way for an Entity to
provide key codes.  As an obvious example, a key entity will have this trait,
however so may a switch entity which opens a locked door.

## Actors

This trait exposes no actors.

## Targets

This trait exposes no targets.

## Parameters

| Name             | Type         | Default                          | Description |
|------------------| ------------ | -------------------------------- | ----------- |
| codes            | list[int]    | `None`                           | List of integer codes which can lock/unlock this. |

## Visuals

No default visuals are expected to be defined, as keys are abstract.

## Defining in Tiled
