# Destroyable

This trait is given to Entities which can be destroyed.

## Actors

This trait exposes no actors.

## Targets

This trait exposes no targets.

## States

This trait has the following possible states:
- Destroyed
- NotDestroyed

## Parameters

| Name             | Type         | Default                          | Description |
|------------------| ------------ | -------------------------------- | ----------- |
| startdestroyed   | boolean      | False                            | Whether or not to start destroyed. |
| blocksmovement   | dictionary   | '': None                         | Mapping of state to whether movement is blocked. |
| blockspathing    | dictionary   | '': None                         | Mapping of state to whether pathing is blocked. |
| blocksvision     | dictionary   | '': None                         | Mapping of state to whether vision is blocked. |
| blocksaudio      | dictionary   | '': None                         | Mapping of state to whether audio is blocked. |
| autoclose        | boolean      | False                            | Whether or not this closes automatically. |

## Defining in Tiled
