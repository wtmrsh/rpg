# Vision

The Vision trait is applied to Entities which can see.  These entities maintain
a list of visible tiles, and can receive events about the world.

## Actors

This trait exposes no actors.

## Targets

This trait exposes no targets.

## Parameters

| Name             | Type         | Default                          | Description |
|------------------| ------------ | -------------------------------- | ----------- |
| fov              | float        | `None`                           | Field of view. |
| viewdistance     | float        | `None`                           | Field of view. |
| mask             | str,int,int  | `None`                           | Filename, width, height |

## Visuals

No default visuals are expected to be defined.

## Defining in Tiled
