# Intelligence
    
The Intelligence trait enables an entity to choose an action to perform on its turn.
It does this by specifying a set of Tasks for the entity to do, which have the corresponding
logic to choose the action.

## Actors

This trait exposes no actors.

## Targets

This trait exposes no targets.

## Parameters

| Name      | Type                        | Default                          | Description |
|-----------| --------------------------- | -------------------------------- | ----------- |
| tasks     | list[IntelligenceTask]      | None                             | List of tasks to be instantiated, in the format given below. |

## Tasks

Tasks are organised as follows:

- Supertasks contain a list of sequential subtasks.
- Subtasks have an action, a target of that action (optionally, as required by the task), and parameters which the action takes.  All tasks take an 'x' and a 'y' parameter,
  in addition to their normal ones, which is the location the task (action) must be performed on.  This is required because some actions (for instance,
  opening a door) can have multiple locations where they can be performed (ie on either side of the door).  If 'x' and 'y' are not suuplied, then the previous task's values
  are used.  If there was no previous task (ie it's the first task) then None is used as a default value.
- Subtasks have handlers for when the action requested is either rejected (ie by prerequisite checks) or fails on execution.  These handlers work as follows:
  - RetryCount specifies how many times to retry the action.  Negative means forever.
  - After RetryCount attempts have failed, the tasks is either skipped (by setting Continue to true) or we move to a different supertask, by specifying ChangeTask.
- Supertasks can trigger events in certain situations:
  - When the supertask is complete.
  - When the supertask has been aborted (eg by a subtask failing and specifying a ChangeTask)
  In this situation, the Entity.Intelligence.TaskComplete / Entity.Intelligence.TaskAborted event is called, with the target, and any additional arguments specified in
  the definition.
- Supertasks have a LoopCount, which is the number of times to repeat the loop before checking Static Transitions.  Negative means forever.
- Supertasks have a set of Transitions - Static and Dynamic - which determine how to move to the next supertask.  Static are checked after a supertask completes.  Dynamic
  are checked every turn.
  
### Static transitions

Currently only 'OnFinished' is implemented.  A transition is chosen at random from the list.  The specified parameters are passed into the supertask.

### Dynamic transitions

This is list of functions to call - see dynamic transition functions - which return a tuple of whether the task should transition, and the parameters to use
for the supertask it's transitioning to.  For instance, you might have a dynamic transition to check if an enemy can see the player, and move to the 'Chase'
supertask if so.  The function would check if the player is in the enemy's sight, and if so return True, plus the entity (location) to chase as parameters.

There can be multiple dynamic checks run each turn.  The first one listed to succeed wins.

### Defining in Tiled

The entity should have a custom property *Traits.Intelligence.Parameters.tasks*, with the following JSON definition:
```
{
  "InitialTask": {
    "Task": "TaskName1",
	"Params": {
	  "genericvalue": 1
	}
  },
  "Tasks": {
    "TaskName1": {
	  "Tasks": [
	    {
	      "Type": "<task-type>",
		  "Target": "<entity-name>",    # Optional, not required for move, for instance
		  "Params": {
            "param1": value1
		  },
          "OnRequestFailure": {         # Called if a request fails (due to prerequisites, etc)
            "RetryCount": 0,            # -1 means forever, 0 means don't retry, >0 is the number of times to retry this action
            "ChangeTask": "<new-task>", # Task to change to if we fail RetryCount times
			"Continue": true|false      # If true, skip to next subtask if we fail RetryCount times
          },
          "OnExecutionFailure": {       # Called if an execution fails (bumped in precendence, etc)
            "RetryCount": 0,
			"ChangeTask": "<new-task>",
            "Continue": true|false
          }
		},
	    {
	      "Type": "<task-type>",
		  "Params": {
            "param1": value1
		  }
		},
      ],
	  "Events": {
	    "Complete": [ # If supplied, fires the Entity.Intelligence.TaskComplete event with the given args when the 'supertask' completes
		  arg1,
		  arg2
		],
	    "Aborted": [ # If supplied, fires the Entity.Intelligence.TaskComplete event with the given args when the 'supertask' is aborted due to failures
		  arg1,
		  arg2
		],
	  }
	  "LoopCount": -1, # -1 for infinite, >0 for a set number of times
	  "Transitions": {
	    "Static": {
		  "OnFinished": [
		    {
			  "Task": "<task-to-move-to>",
			  "Params": {
			    "myparam": "Hello, world!"
			  }
			}
		  ]
		},
		"Dynamic": [
		  {
		    "Function": "<function-to-call>",
			"Task": "<task-to-move-to>"
		  }
		]
	  }
    }
  }
}
```

You may also define the custom property *Traits.Intelligence.Parameters.taskhandlers*.  This is a JSON map of task name (as specified in task definition) to an IntelligenceTask implementation.
The idea of this is to be able to create a generic behaviour as a set of tasks, and allow the implementation of the task to be overridden at the entity class or entity instance level.

For instance, you might have a task definition as follows:
```
{
  "InitialTask": {
    "Task": "Default",
  },
  "Tasks": {
    "Default": {
	  "Tasks": [
	    {
	      "Type": "RangedAttack",
        }
      ],
	}
  }
}
```

Here, all entities with this task definition will look for an IntelligenceTask named 'RangedAttack'.  If, however, we give a particular Entity the following taskhandler:

```
{
  "RangedAttack": "RangedAttackWithCrossbow"
}
```

Then the Entity in question will actually use the RangedAttackWithCrossbow IntelligenceTask.  

