# Inventory

## Actors

This trait exposes no actors.

## Targets

This trait exposes no targets.

## Parameters

| Name             | Type         | Default                          | Description |
|------------------| ------------ | -------------------------------- | ----------- |
| capacity         | int          | 1                                | Capacity of inventory in weight units. |
| searchable       | boolean      | False                            | *Not implemented*. |

## Defining in Tiled
