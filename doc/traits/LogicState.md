# LogicState
    
The LogicState trait enables an entity to store a set of values which can be
operated on by standard logical operators, such as AND, XOR, NOT, etc.  It
holds a set of callbacks to run for when these values match requested criteria.  
A typical example is to use an abstract entity to allow an event or action to be 
triggered based on multiple inputs.  For instance, only open a door once multiple 
inputs from other entities are received.

## Actors

This trait exposes no actors.

## Targets

This trait exposes the **ModifyLogicState** target on its owning entity.

## Parameters

| Name      | Type         | Default                          | Description |
|-----------| ------------ | -------------------------------- | ----------- |
| spoofuser | boolean      | True                             | Whether or not the actor that performs the delegated actions should be the LogicState entity itself.  If not, then the entity that uses the LogicState entity is the actor. |


## Visuals

No default visuals are expected to be defined.

## LogicStateCallback definition

LogicState executes one or more **LogicStateCallback**.  These are defined as 
follows:
- State to check.
- Verifying function that checks the state's value and see if it passes.
- A list of **UseTargets** to execute if the verifier passes.

## Defining in Tiled

Give the entity a **Name** if required, and then one or more 
**Core.LogicStateCallback** properties (eg *Core.LogicStateCallback.0*, 
*Core.LogicStateCallback.1*, etc), defined as JSON as follows:

    {
      "state": "<status-name>",
      "verifier": "value == 1",
      "usetargets":  [
        {
        "target": "<target-entity-name>",
          "type": "cycle|once",
          "pass": "next|break",
          "actions": [
            {
              "name": <action-name>",
              "args": {
                "arg1": "<value>",
                "argN": "<value>"
              }
            }
          ]
        }
      ]
    }

Finally, add any property prerequisites that the targets might require.