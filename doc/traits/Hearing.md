# Hearing

The Hearing trait is applied to Entities which can hear.  Entities (and the map) can make noises, by calling `Entity.emit_sound()` or `Entity.emit_speech()`.


## Actors

This trait exposes no actors.

## Targets

This trait exposes no targets.

## Parameters
No parameters are defined.

## Defining in Tiled
