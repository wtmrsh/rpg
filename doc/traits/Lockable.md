# Lockable

This trait is given to Entities which can be locked, such as doors, or chests.

## Actors

This trait exposes no actors.

## Targets

This trait exposes the **Lock** and **Unlock** targets on its owning entity.

## States

This trait has the following possible states:
- Locked
- Unlocked

## Parameters

| Name             | Type         | Default                          | Description |
|------------------| ------------ | -------------------------------- | ----------- |
| startlocked      | boolean      | False                            | Whether or not to start locked. |
| codes            | list[int]    | `None`                           | List of integer codes which can lock/unlock this. |

## Visuals

Visuals for **Locked** and **Unlocked** are expected to be defined.

## Defining in Tiled
