# Useable

Useable is implemented slightly differently from most other actions, due to the
fact that you are performing an action on an entity, which then performs further
actions on other entities.

An entity with this trait can be thought of as a "middle man", that performs
actions on other entities, "on behalf of" the entity which used it.  A typical
example of this would be a switch object, which when pressed (via the **Use** 
action), performs an **Open** action on a nearby door.  It is important to bear 
in mind that the full set of prerequisites will be run, so if the target of a 
delegated action requires something, the Useable entity must provide that 
functionality (eg codes for unlocking a door).

Trigger areas also have this trait, in order to allow actions to be run once
they are triggered.  See the TriggerArea documentation on how to implement this.

## Actors

This trait exposes no actors.

## Targets

This trait exposes the **Use** target on its owning entity.

## Parameters

| Name             | Type         | Default                          | Description |
|------------------| ------------ | -------------------------------- | ----------- |
| spoofuser        | boolean      | True                             | Whether or not the actor that performs the delegated actions should be the Useable entity itself.  If not, then the entity that uses the Useable entity is the actor. |
| suppressmsg      | boolean      | False                            | Whether or not to display the action status message.  Can be useful if you want a 'silent' action triggered by a trigger. |
| playerusemsg     | string       | *Check engine/traits/useable.py* | A custom message to print when the player successfully performs the action.
| playerfailmsg    | string       | *Check engine/traits/useable.py* | A custom message to print when the player unsuccessfully performs the action.
| otherusemsg      | string       | *Check engine/traits/useable.py* | A custom message to print when a non-player entity successfully performs the action.  Requires a '%s' placeholder for the entity name. |
| otherfailmsg     | string       | *Check engine/traits/useable.py* | A custom message to print when a non-player entity unsuccessfully performs the action.  Requires a '%s' placeholder for the entity name. |
| allowedusernames | list[string] | `None`                           | a list of names of entities which can interact with the Useable entity.  If `None`, then no restrictions on users. |
| numusersneeded   | int          | 1                                | the number of uses needed (by unique users) for this to trigger. |
| userfilterfunc   | string       | `None`                           | a lambda which returns whether the user should be allowed to trigger the Useable.  An implicit `lambda entity: ` is prepended to the string.  If `None`, then not run.
| visualcycle      | list[string] | `None`                           | A list of visual statuses (the keys in the Useable trait's *Visuals* definition, through which to cycle, each time the Useable is successfully used.  If `None`, then the default visual is always used. |

## Visuals

No default visuals are expected to be defined.  To specify a list of visuals to
cycle through (on each successful use of the Useable), use the **visualcycle**
parameter.

## UseTarget definition

Useables execute one or more **UseTargets**.  These are defined as follows:
- Target entity.  If this is defined, then we expect a list of actions to be performed
  on the target.  If it is not defined, then we expected a list of states to be set
  or unset on the owning entity.
- Whether or not the UseTarget should be executed only once ("once"), or multiple
  times ("cycle") - currently there is no limit defined, and it will cycle
  indefinitely.
- What to do if the next action in the list is already done (eg a door you want to
  open is already open).  Either move onto the next action and try that ("next"), 
  or return success ("break").  (Returning success indicates that a turn will pass:
  after all, the user did spend time using the Useable, even if it didn't do 
  anything).
- Whether the visual status should be updated if the UseTarget action fails  
- A list of actions, each defined as follows:
    - Name of action
    - A dictionary of keyword arguments
- A list of state settings, which contain one or more of:
    - Set states
    - Unset states
    - Toggled states

## Defining in Tiled

Give the entity a **Name** if required, and then one or more **Core.UseTarget** 
properties (eg *Core.UseTarget.0*, *Core.UseTarget.1*, etc), defined as JSON as 
follows:

    {
      "target": "<target-entity-name>",
      "type": "cycle|once",
      "pass": "next|break",
	  "updateonfailure": true|false,
      "actions": [
        {
          "name": "<action-name>",
          "args": {
            "arg1": "<value>",
            "argN": "<value>"
          }
        }
      ]
    }

Alternatively, for state-setting:

    {
      "type": "cycle|once",
      "pass": "next|break",
	  "updateonfailure": true|false,
      "states": [
        {
          "set": "<state-name>",
          "unset": "<state-name>",
          "toggle": "<state-name>",
        }
      ]
    }

Then if required, specify Visuals via **Traits.Useable.Parameters.visualcycle**
as a comma-separated list.  Finally, add any property prerequisites that the
targets might require.