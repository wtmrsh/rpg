# Openable

This trait is given to Entities which can be opened, such as doors, or chests.

## Actors

This trait exposes no actors.

## Targets

This trait exposes the **Open** and **Close** targets on its owning entity.

## States

This trait has the following possible states:
- Open
- Closed

## Parameters

| Name             | Type         | Default                          | Description |
|------------------| ------------ | -------------------------------- | ----------- |
| startopen        | boolean      | False                            | Whether or not to start open. |
| blocksmovement   | dictionary   | '': None                         | Mapping of state to whether movement is blocked. |
| blockspathing    | dictionary   | '': None                         | Mapping of state to whether pathing is blocked. |
| blocksvision     | dictionary   | '': None                         | Mapping of state to whether vision is blocked. |
| blocksaudio      | dictionary   | '': None                         | Mapping of state to whether audio is blocked. |
| autoclose        | boolean      | False                            | Whether or not this closes automatically. |

## Visuals

Visuals for **Open** and **Closed** are expected to be defined.

## Defining in Tiled
