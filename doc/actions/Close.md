# Close

Close is used on objects such as doors and chests, ie either World Objects or
items with inventory.

## Actor

The Actor is the Entity which is doing the closing.

## Target

The Target is either a World Entity such as a door or window, or an Entity with
an inventory, such as a chest.

## Prerequisites

This should check that the target has the `Openable` trait, and that it is
currently not already closed.

## Facilitator

The facilitator takes no arguments, and should just call
`Entity.openable.close()`.

## Requesting in code

This takes no parameters.  Eg:
```
entity.request_action('Close', target)
```

## Tiled UseTarget definition

```
{
  "name": "Close",
  "args": {
  }
}
```