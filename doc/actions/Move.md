# Move

The Move action alters the tile that an Entity is on.  This does not
necessarily have to be an adjacent tile, but can also "teleport" the Entity.
This functionality is implemented slightly differently to other actions as when
multiple entities are moving at the same time, a degree of coordination is
required.  Therefore, this is not executed via
`ActionParticipant.execute_action()` but via specialised functions within `Map`.

## Actor

The Actor is the Entity which is moving.

## Target

The Target should be the Map which the moving Entity is on.  This is so that
the Target can register a callback, for instance if you want to check for
particular Entities moving, you can put that check in the Map.  However, the
target is currently unused, so if you do not need a callback then it is fine
to pass `None` as the Target.

## Prerequisites

There are no built-in prerequisites for moving.

## Facilitator

Uniquely, there is no facilitator for the Move action, and it is handled by the
Map.

## Requesting in code

This takes two parameters, the target x and y positions.  Eg:
```
entity.request_action('Move', entity.map, entity.x + dx, entity.y + dy)
```

## Tiled UseTarget definition

Generally you will not need to specify this action in Tiled, unless you want a
way of teleporting something around the Map.  To do so, use the following:
```
{
  "name": "Move",
  "args": {
	"x": 1,
	"y": 2
  }
}
```