# Unlock

Unlock is used on objects such as doors and chests, ie either World Objects or
items with inventory.

## Actor

The Actor is the Entity which is doing the unlocking.

## Target

The Target is either a World Entity such as a door or window, or an Entity with
an inventory, such as a chest.

## Prerequisites

This should check that the target has the `Lockable` trait, and that it is
currently not already unlocked.  It may also need to check if it has the
`Openable` trait and is closed, although this depends on the target.  It
should also check that the unlocking Entity has the correct key.  The
unlocking Entity may either have the `Key` trait, or have the `Inventory`
trait (from which it can check for keys that it carries).

## Facilitator

The facilitator takes no arguments, and should just call
`Entity.lockable.unlock()`.

## Requesting in code

This takes no parameters.  Eg:
```
entity.request_action('Unlock', target)
```

## Tiled UseTarget definition

```
{
  "name": "Unlock",
  "args": {
  }
}
```