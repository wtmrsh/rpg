# SetIntelligenceTask

SetIntelligenceTask is used on an Entity with the Intelligence trait, to change its current task.  It is designed to be used by 'facilitating' entities
which can orchestrate complex AI sequences.

## Actor

The Actor is the Entity which is issuing the action.

## Target

The Target is an Entity with the Intelligence trait.

## Prerequisites

This should check that the target has the `Intelligence` trait, and that it has the requested task.

## Facilitator

The facilitator takes one argument - the name of the task, and should just call
`Entity.intelligence.set_curtask(<taskname>)`.

## Requesting in code

This takes one parameter - the name of the task.  Eg:
```
entity.request_action('SetIntelligenceTask', 'MoveToDoor')
```

## Tiled UseTarget definition

```
{
  "name": "SetIntelligenceTask",
  "args": {
    "taskname": "..."
  }
}
```