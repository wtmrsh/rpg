# Wait

Wait does nothing, it essentially skips a turn.

## Prerequisites

There are no prerequisites to waiting.  There is nothing that should hinder it.

## Facilitator

The Wait facilitator just return success without doing anything.

## Requesting in code

This takes no parameters.  Eg:
```
entity.request_action('Wait', entity.map)
```

## Tiled UseTarget definition

Generally you will not need to specify this action in Tiled, but to do so, use
the following:
```
{
  "name": "Wait",
  "args": {
  }
}
```