# Use

Use is used to activate some aspect of a target entity.  This is done via
`UseTarget`s which can perform actions on other targets.

## Actor

The Actor is the Entity which is doing the using.

## Target

The Target is the Entity which is being used.  It requires the `Useable`
trait.  This fires off other actions on other Entities.  Thus the Target is
really just an intermediary.  A typical example might be a switch wall object.

## Prerequisites

There are typically no prerequisites to this action.

## Facilitator

The facilitator takes no arguments, and should just call `Useable.use()`

## Requesting in code

This takes no parameters.  Eg:
```
entity.request_action('Use', target)
```

## Tiled UseTarget definition

```
{
  "name": "Use",
  "args": {
  }
}
```