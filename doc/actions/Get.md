# Get

Get is used to transfer Items from the Map to an Entity - it is essentially
'pick up'.

## Actor

The Actor is the Entity which is doing the getting.

## Target

The Target is an Entity of type 'Item' which can be placed in the Actor's
inventory.

## Prerequisites

The Actor should check that there is enough space in its inventory, and
possibly that it is strong enough to pick up the item.

## Facilitator

The facilitator takes no arguments, and should just call
`inventory.transfer_entity(target, actor.inventory, count)` on the Actor's
target inventory, which will probably be a `Tile`'s.

## Requesting in code

This takes no parameters.  Eg:
```
entity.request_action('Get', target)
```

## Tiled UseTarget definition

```
{
  "name": "Get",
  "args": {
  }
}
```