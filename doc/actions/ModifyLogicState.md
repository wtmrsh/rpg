# ModifyLogicState

ModifyLogicState is used to change the `LogicState` of a target Entity.

## Actor

The Actor is the Entity which is doing the modify.

## Target

The Target is the Entity whose `LogicState` trait is being modified.

## Prerequisites

There are typically no prerequisites to this action.

## Facilitator

The facilitator takes three arguments:
- op
- state
- value

It should call the corresponding method in `LogicState` based on *op*, passing
in *state* and *value*, eg: `target.logicState.set_state(actor, state, value)`.

## Requesting in code

```
entity.request_action('ModifyLogicState', target, op, state, value)
```

## Tiled UseTarget definition

```
{
  "name": "Use",
  "args": {
    "op": "set|add|and|or|xor|not",
    "state": "mystate",
    "value": 10
  }
}
```