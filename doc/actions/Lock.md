# Lock

Lock is used on objects such as doors and chests, ie either World Objects or
items with inventory.

## Actor

The Actor is the Entity which is doing the locking.

## Target

The Target is either a World Entity such as a door or window, or an Entity with
an inventory, such as a chest.

## Prerequisites

This should check that the target has the `Lockable` trait, and that it is
currently not already locked.  It may also need to check if it has the
`Openable` trait and is closed, although this depends on the target.  It
should also check that the locking Entity has the correct key.  The
locking Entity may either have the `Key` trait, or have the `Inventory`
trait (from which it can check for keys that it carries).

## Facilitator

The facilitator takes no arguments, and should just call
`Entity.lockable.lock()`.

## Requesting in code

This takes no parameters.  Eg:
```
entity.request_action('Lock', target)
```

## Tiled UseTarget definition

```
{
  "name": "Lock",
  "args": {
  }
}
```