# Pick

Pick is used on objects such as doors and chests, ie items with a lock.

## Actor

The Actor is the Entity which is doing the picking.

## Target

The Target is either a World Entity such as a door or window, or an Entity with
an inventory, such as a chest.

## Prerequisites

This should check that the target has the `Pickable` trait, and that it is
currently locked.  This is not part of the core engine.

## Facilitator

The facilitator takes no arguments, and should just call
`Entity.pickable.pick()`.

## Requesting in code

This takes no parameters.  Eg:
```
entity.request_action('Pick', target)
```

## Tiled UseTarget definition

```
{
  "name": "Pick",
  "args": {
  }
}
```