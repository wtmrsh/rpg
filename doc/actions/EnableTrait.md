# EnableTrait

This action simply either enables or disables a trait on a target entity.

## Actor

The Actor is the Entity which is doing the enabling/disabling.

## Target

The Target is a regular entity.

## Prerequisites

This should check that the target has the required trait, and that it is not in the target state.

## Facilitator

The facilitator takes the trait and whether to enable or not, for instance:
```
def exec_enabletrait_actor_target(actor, target, traitname, enabled):
    trait = target.get_trait(traitname)
    trait.enabled = enabled
    return FacilitatorResult(success=True, actionperformed=True, playermsg=None, othermsg=None)
```

## Requesting in code

This takes no the trait name, and whether to enable.  Eg:
```
entity.request_action('EnableTrait', target, 'Vision', True)
```

## Tiled UseTarget definition

```
{
  "name": "EnableTrait",
  "args": {
    "trait": "Vision",
	"enabled": true
  }
}
```