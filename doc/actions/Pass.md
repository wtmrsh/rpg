# Pass

Pass does nothing, similar to Wait.  Pass is also given as a default action for
entities which take part in movement resolution, but which aren't actually
intelligent.  For example, a barrel may be moved out of the way by another, 
moving entity.  To facilitate this, the barrel is given the Pass action, as 
entities without actions do not take part in turn-resolution.

If an entity does not have the Pass action, it is given it, with a default 
facilitator.

## Prerequisites

There are no prerequisites to passing.  There is nothing that should hinder it.

## Facilitator

The facilitator takes no arguments.  It should just return success without doing 
anything.  This is provided by default for all entities, and does not have to be 
specified.

## Requesting in code

This takes no parameters.  Eg:
```
entity.request_action('Pass', entity.map)
```

## Tiled UseTarget definition

Generally you will not need to specify this action in Tiled, but to do so,
use the following:
```
{
  "name": "Pass",
  "args": {
  }
}
```