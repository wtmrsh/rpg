

# Entities

Entities are defined in a file called *entities.json*, know as the Definition File.  From this JSON definition, they are instantiated and added to a map.

## Inheritance

`ActionParticipant` is a base class, which stores a dictionary of prerequisite functions for when it is the actor in an action (keyed on the action name), and a dictionary of prerequisite functions for when it is the target.
It also stores a queue of the history of its actions, both as an actor and a target.  This allows us to perform specific sequences of actions, for instance, unlocking a door, opening it, going through it, closing it and then locking it again.

`Visual` is a base class which stores information relating to an Entity's appearance.  There are two differentiators here: **variant** and **status**.
- **variant** is used to to provide different visuals with a purely cosmetic change, eg different coloured doors, or floors with some wear and tear.
- **status** is used to indicate the state that the Entity is in, and acts as a link between the `Entity` and the `Visual`, so that we can update the Visual based on a status name.  For instance, we might have *Open* and *Closed* statuses for a door, and when we call `Visual.set_visual()` , we'd pass in the required status, and it would update the visual for the Entity appropriately.

If no variants or statuses are specified, then a single tile will use *_* for these.  If a Visual has multiple tiles, then they must be differentiated somehow by providing explicit  variants/statuses, otherwise later entries will overwrite earlier ones.

## Built-in Members

- Entities store *visual state setters*, which are functions which are designed to set the state of an entity based on the name of the state.  For instance, calling the visual state setter for a Door's *open* state, will actually open the door (and we are required to set the visual state within `Door.open()`).  This is actually a way of setting an entity's state via reflection.
- Entities also store a list of events which they are registered to listen to.
- Entities have an *owner*.  This will be either a map tile, another entity if carried by, or contained by them, or the map itself if the entity is abstract
- Entities store the *map* on which they are exist.
- Entities have a *scriptName*, which is used to refer to entities from within map scripts, via `Map.get_scripted_entity()`.
- Entities store a list of hostile targets, which is set by the *Vision* trait, based on factions, and can be used by other traits, such as Intelligence traits.
- Entities have a task status, which is designed for Intelligence-based traits, but is currently unused.
- Entities store the last turn number on which they were updated.
- Entities store whether or not they should print messages when they are used by another entity with the *Useable* trait.

### Entity names

Entities have several different names.
- The **name** property is the "formal" name of an entity.  For instance, it might be a human entity's real name.  The attribute (actually a property) first checks to see if the entity is a collective entity, and if so, returns its `Collective.name` property, for instance "a pair of boots".  Otherwise it returns its **scriptName** if set, and finally its **_name**.
- The **namePlural** property is essentially the same as **name**, except that it defaults to **_namePlural**, and does not check the **scriptName** (because we have multiple entities so they must have different script names, if set).
- The **def_name** and **indef_name** properties are used for printing messages, and wrap the names with the correct definite/indefinite articles.

## Configurable Members

The following members are set in the Definition File
- **type**: a string, required.  This is generally used by the inventory, and by views, for example to group entities together, or to return entities of a particular type.
- **direction**: an enum, optional.  Maps to `CompassDirection`
    - Default: no direction.
- **size**: an enum, required.  Maps to one of the entries in the `EntitySize` enum.  There are some engine-defined differences: for instance, you can only drop `SMALL` entities onto a tile,
- whereby they become part of the tile's inventory.  `LARGE` entities are considered to be the tile's main entity.
- **names**: a list of two strings, which are then accessed from the properties:
    - **name()**: the printable name of one of the entity.
    - **name_plural()**: the printable name of more than one of the entity.
- **colour**, a string, optional, which can be the name of a colour, or a different representation of it, for instance a 24-bit hex code.
    - Default: white
- **attachedToWorld**: a bool, required.  This determines things such as whether you can pick up an entity up, whether it blocks movement, and so on.
- **factions**: a set of strings.  This is used by entities with the *Vision* trait to determine how to react to other entities.

TODO:
- @actor
- @target

## Actions

Entities interact with each other via **actions**.  These take an *actor* and a *target*.  Actions are executed by calling `ActionParticipant.perform_action()`.

Each action requires that types be registered for it, in order for entities to use it.  These are subclassed from `Actor` and `Target` , and are then marked up with `@action_actor` and `@action_target` decorators.

### Prerequisites

The first thing that `ActionParticipant.perform_action()` does it to run all the prerequisite checks on the action.  There are two sets here: prerequisites for the actor, and prerequisites for the target.

#### Actor

These are functions marked up with the `@actor_prereq` decorator, which take the following form:
- Params: actor entity, target entity
- Returns: tuple of (did the check pass, failure message for player, failure message for non-player entity)

#### Target

These are functions marked up with the `@target_prereq` decorator, which take the following form:
- Params: target entity, actor entity
- Returns: tuple of (did the check pass, failure message for player, failure message for non-player entity)

TODO:
- @action_actor
- @action_target
- @forbidden_member

## Traits

Traits are composable blocks of functionality, which are instances of a subclass of the `Trait` class, and do not define any specific methods to be implemented, but expect that the functionality is used appropriately by other parts of the system.  Traits are designed to specify what an entity can do, and then implement that.  Thus, when updating an entity, a common paradigm is to check if an entity has a trait with `Entity.has_trait()`, before updating the relevant part.
Traits are added in the Definition File, and can specify extra target prerequisites for particular actions, when that trait would impact the action.  For instance, a special subclass of Door might require extra prerequisites to open (other doors nearby are closed, for instance), and this would be specified as a target prerequisite for the *Openable* trait.

TODO: 
- @required_trait
- @required_trait
- @forbidden_trait

## Stats

Entities can have specific stats, which can be used in prerequisites, facilitators, traits and so on.  They can be marked as requiring specific stats by using the `@required_stat` decorator on particular actions.
For instance, for an entity to be able to perform the *push* action, it requires the *strength* stat, and for an entity to be pushed, it requires the *weight* stat.  These can be done by adding the decorators to the relevant `Actor` and `Target` subclasses.

## StateManagement

### Handlers

TODO

### Visuals

Entities are displayed by one or more tiles, which are distinguished by class, variant, status and frame.  At the top of this hierarchy, the **class** of the tiles should match that of the entity.  At the next level, there are a set of **variants**, which are functionally-identical, but visually different, intended to provide variety.  Next up, for a particular variant-set of a class, there are a set of **statuses**, which refer to the state that the entity is currently in.  For instance, a door may be open or closed.  Finally, for this status, there are one or more **frames**, which are used for animation.
If no variant names are provided, then all status-sets are assumed to be in a default "\_" variant.  If no status names are provided, then all frame-sets are assumed to be in a default "\_" status.

### Changing the visual

This information is stored in the `Visual` class, which is a base class of `Entity`.  To update an entity's visual (displayed tile), call either `set_visual(variant, status, frame)` or `set_visual_status(status)` on the entity.

### Traits

Traits take a dictionary of visual statuses, which are passed in on creation, either directly or from an editor.  These define a mapping from a user-defined name, to a status.  They are then used by the trait to update the visual of the owning entity, as required.

## Collective entities

Some entities by their nature come in groups, for instance a pair of boots.  These should be given the **Collective** trait.  They will then be treated as a normal entity with no special properties, except for when the trait's `count` member is reduced, in which case, any action which is performed on them will trigger an additional event called "Entity.Target.<action-name>.IncompleteSet"

