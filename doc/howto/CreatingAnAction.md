# Creating an action

To explain, using a hypothetical *punch* action.

First of all, create subclasses of `Actor` and `Target` and mark them up with the appropriate decorators.  

    from engine.action import Actor, Target, action_actor, action_target, required_stat
    
    @action_actor('Punch')
    class PunchActor(Actor):
      pass
    
    @action_target('Punch')
    class PunchTarget(Target):
      pass

Entities can now be assigned this action.

Now, for each of those two classes, implement `get_valid_count()` and `get_target_location()` if required.  For the actor, this is the number of targets that an entity can punch.  For the target, this is the number of entities that can have the action performed on them.  This latter case is not particularly useful here.

`get_valid_count()` should return a tuple of the number of valid items, and a reason why that number is the case.  If there is no reason, then this means that it's returned the requested number.

    @action_actor('Punch')
    class PunchActor(Actor):
      def get_valid_count(self, actor, target, actionCount, itemCount):
      """  
      Return the number of entities that an actor can punch at once.  
      :param actor: subject actor
      :param target: object target
      :param actionCount: number of targets we want to punch
      :param itemCount: number of targets available
      :return: tuple of the number of targets we can punch, plus the reason why we can't perform this action for the requested (actionCount) number.
      """      
      # Assume we have 2 arms
      numArms = 2
      c = min(actionCount, itemCount)
      reason = None

      if c > numArms:
        c = numArms
        reason = 'You only have two hands'
      elif actionCount > itemCount:
        reason = f'There are only {itemCount} entities available to punch'
      
      return (c, reason)

We don't really need this for targets, unless we'd want to limit the number of actors that can simultaneously punch the same target.

`get_target_location()` should return the x,y position the actor should be on, after the action is successfully executed.
It takes the args and  kwargs that are passed into `ActionParticipant.request_action()` and returns and (x, y) tuple.

    @action_actor('Punch')
    class PunchActor(Actor):
      @classmethod
      def get_target_location(cls, actor, *args, **kwargs):
      """  
      Return the target location after the action is complete.  
      :param actor: subject actor
      :return: (x, y) position tuple 
      """      
      return (actor.x, actor.y)

Here, we don't expect to move during the punch, so we return the current position, but a Move or Teleport
action would be required to parse the kwargs, expecting 'x' and 'y' arguments.

Now, the actor would require the `Body ` trait in order to be certain it has arms with which it can punch.  This would let us get the number of arms the actor has.

    @required_trait('Body')
    @action_actor('Punch')
    class PunchActor(Actor):
      def get_valid_count(self, actor, target, actionCount, itemCount):
      """  
      Return the number of entities that an actor can punch at once.  
      :param actor: subject actor
      :param target: object target
      :param actionCount: number of targets we want to punch
      :param itemCount: number of targets available
      :return: the number of targets we can punch
      """      
      numArms = len(actor.body.arms)
      c = min(actionCount, itemCount)
      reason = None

      if c > numArms:
        c = numArms
        reason = 'You only have two hands'
      elif actionCount > itemCount:
        reason = f'There are only {itemCount} entities available to punch'
      
      return (c, reason)

This action might also require a *strength* stat:

    @required_stat('strength')
    @required_trait('Body')
    @action_actor('Punch')
    class PunchActor(Actor):
      def get_valid_count(self, actor, target, actionCount, itemCount):
      """  
      Return the number of entities that an actor can punch at once.  
      :param actor: subject actor
      :param target: object target
      :param actionCount: number of targets we want to punch
      :param itemCount: number of targets available
      :return: the number of targets we can punch
      """      
      numArms = len(actor.body.arms)
      c = min(actionCount, itemCount)
      reason = None

      if actor.stats.strength < 1:
        c = 1
        reason = 'You are weak like a child'
      elif c > numArms:
        c = numArms
        reason = 'You only have two hands'
      elif actionCount > itemCount:
        reason = f'There are only {itemCount} entities available to punch'
      
      return (c, reason)

Currently `get_valid_count()` is only used for determining what can be stacked in an inventory, for purposes of grouping items which can have an action performed on them.

## Actions for traits

If the action is specifically for a trait, it needs to be registered with the trait.  This means that the action is registered with an entity whenever a trait is, and we do not need to explicitly specify that the entity can perform this action.  This is generally for targets, not actors, but not always.
If we wanted to say that the *punch* action is available for anything with a Body, then we'd add the following to the `Body` class definition:

    @register_trait('Body')
    @exposes_target('Punch', 'owner')
    class Body(trait.Trait):

Here, *owner* is the name of the member of `Body` which we want to add the target to.  This is also used in other areas, for instance `Map`, and can be used to programmatically add actions to entities.

## Facilitators

The next step is to add facilitator(s).  These can follow any naming scheme, but should be in the expected facilitators.py file.  Once done, the functions can be referenced from entity definitions.

These should return a tuple of (success, player message, non-player-entity message)

For instance:

    def try_punch_Human_actor(actor, target):
      target.health -= 1
      return engine.defines.FacilitatorResult(success=True, actionperformed=True, playermsg=f'You punch {target.def_name()}', othermsg=f'{actor.def_name()} punches {target.indef_name()}')

## Prerequisites

The final step is to add any prerequisite functions, as follows:

    @actor_prereq('Punch')
    def prereq_can_punch(actor, target):
      return engine.defines.FacilitatorResult(success=True, actionperformed=True, playermsg=f'You punch {target.def_name()}', othermsg=f'{actor.def_name()} punches {target.indef_name()}')

    @target_prereq('Punch')
    def prereq_can_be_punched(target, actor):
      return engine.defines.ActionRequestResult(passed=True, actionrequired=True, playermsg=None, othermsg=None, prereqactions=[])

Here, the return value is a tuple of whether the check passes, and the reasons (for the player and a non-player entity, respectively) why not.  Adding the decorator means that this prerequisite will be applied to all entities which
have the Punch action, as opposed to just specifying it in the entity definitions, where it will only apply to a particular class and subclasses.  Functions declared like this can be overriden in the entity definitions by firstly
removing them, for instance `-prereq_can_punch` and secondly providing a new one, in the list of prerequisites.

## Aptitude

When entities perform their actions, they need to decide on a priority order between themselves.  One of the keys to this is measuring an entity's aptitude at performing the action it wants to do.  Hence, we also need to create
a function which returns a value for this.  For instance, how good an entity is at the Move action may depend on the Entity's speed characteristic.  This allows a way for entities to gain a subtle advantage over others in that
they get to perform their actions first, when they are more likely to be successful.

To implement an aptitude function, add a function named as follows in the file aptitude.py in your game.  The function should return a value in [0, 1]

def aptitude_for_punch(actor, target):
    return (actor.stats.strength) / STAT_MAX # For given value of STAT_MAX
