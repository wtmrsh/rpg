# Creating a map using Tiled.

Tiled is a 2d map editor, which can be found [here](https://www.mapeditor.org).  These instructions are based on Tiled version 1.7.2

## Setup

Create a new map, and select the following options in the 'New Map - Tiled' dialog box:

- Map
  - Orientation: Orthogonal
  - Tile layer format: CSV
  - Tile render order: Right Up
- Map size: fixed
  - Width: *desired width*
  - Height: *desired height*
- Tile size:
  - Width: *width of tiles, generally 24*
  - Height: *height of tiles, generally 24*

### Properties
  
#### Custom Properties

Now add the following custom properties:

- Name
  - Type: string
  - Value: *name of the map*

The following properties are optional:

- Author:
  - Type: string
  - Value: *map author*
- ShortDescription
  - Type: string
  - Value: *short description of the map*
- LongDescription
  - Type: string
  - Value: *verbose description of the map*
- Script
  - Type: string
  - Value: *name of a python script, without the .py extension, which should be in the same directory as the map.*
  
See below for map scripting.

##### Meta properties

Meta-properties are user-defined properties which can be accessed via
the `Map.metaproperties` dictionary.  They are prefixed with `Meta.`.
For example, these can be set on a map, and then accessed in code.

#### Global properties

Global properties are used to set special properties of a map.  They are prefixed with `Global.`.

- Global.ShowFullMap
  - Type: boolean
  - Value: whether or not to show the full map to the player, or use the player's vision cone.
- Global.ScrollWithPlayer
  - Type: boolean
  - Value: whether or not to keep the player centred as it moves around the map.

#### Facilitators

The map is an `ActionParticipant` and may need to performs actions.  You should specify the facilitators as properties, for instance:

- Facilitor.Open: exec_open_map_target
  
### Layers

Create the following layers, from bottom up:
- Ground (tile layer)
- Overlay (tile layer)
- Walls (tile layer)
- Decoration (tile layer)
- World Entities (object layer)
- Entities (object layer)
- Abstract (object layer, optional)
  - You can create multiple 'Abstract' layers (with the same name)
- Inventory (object layer), optional
  - You can create multiple 'Inventory' layers (with the same name)
- Areas (object layer)
- Lights (object layer)
- Paths (object layer)

### Tilesets

Finally, add tilesets by going to **Map->Add External Tileset...**.  You will need one for background tiles, and one for entities.  Ensure they are visible from **View->Views and Toolbars->Tilesets**.

## Adding background tiles

Non-interactive tiles should be added on the **Ground** layer or the *Walls* layer.  These tiles may have properties which affect gameplay, but you cannot interact directly with them.  See CreatingATiledTileset.md for information on what these properties mean.

Select the tile you want, which should have as a minimum a **Type**, and **Core.Frame** and **Core.Variant** custom properties.  There are three further custom properties for background tiles which are part of the core engine:
- blocks-movement: bool property, which determines whether or not entities can move onto this tile.
- blocks-vision: bool property, which determines whether or not the tile blocks entity vision.
- blocks-audio: bool property, which determines whether or not the tile blocks sound.
- space: int property, which is the "capacity" of the tile, ie how much can be placed/dropped on it.  The value is the capacity of tile's inventory.

If you have automapping rules (see [here](https://doc.mapeditor.org/en/stable/manual/automapping/)), then enable via **Map->Automap While Drawing**, if required.

Currently, it's required that the whole Base layer is filled.  If you need empty tiles, then use a tile with a **Type** set to "Space". 

### Testing

At this point, let's see what happens if we try to load the map in.  Export it as JSON, and load it.  When loading in, you will get an error about a missing module.  This is the script which we specified, but which does not exist yet.  So, create a file named after the script (with .py extension) in the same directory, and set the contents as follows:

    """Script for map <mapname>"""
    
    from engine.mapobjects import map_script_handler
    
    map = None
    player = None
    
    #
    # on_enter()
    #
    # Called when Map.enter() is called
    #
    @map_script_handler('Map.Enter')
    def on_enter(map_, player_):
        global map, player
        map = map_
        player = player_

    #
    # on_exit()
    #
    # Called when Map.exit() is called
    #
    @map_script_handler('Map.Exit')
    def on_exit(map_, exitter_):
        pass

    #
    # try_exit()
    #
    # Check we have fulfilled the map requirements
    #
    @map_script_handler('Trigger.Exit')
    def try_exit(exitId):
        map.exit()

    #
    # set_player()
    #
    # Called when player is changed
    #
    @map_script_handler('Entity.SetPlayer')
    def set_player(entity, isPlayer):
        global player
        player = entity if isPlayer else None

These functions are explained in the Scripting section.
When you try to load the map now, it should create the player entity at (0, 0) and let you move around.

## Setting the start location

In the **Areas** layer, create a rectangular area, snapped to the tile grid, and give it the **Type** "Start" and a custom string property **Core.Direction**, which should be set to one of "NORTH", "NORTHEAST", "EAST", "SOUTHEAST", "SOUTH", "SOUTHWEST", "WEST", "NORTHWEST".

If the area is larger than one tile, then the start location will be chosen randomly from the tiles.  The Direction property specifies the starting facing direction of the player.

**Note**: you can currently only have one start location: the last-read will be the one used, and if the area is more than one tile, the actual location will be randomly chosen on map load.  

## Setting exit locations

In the **Areas** layer, create a rectangular area, snapped to the tile grid, and give it the **Type** "Trigger" and the following custom properties:
- Core.Event: string property, which should be set to **Trigger.Exit**
- Parameters.exitId: int property, which is passed into the `try_exit()` function.  This property should correspond in name to the parameter that `try_exit()` takes.  You can in fact create multiple custom properties here, and they will all be passed in as keyword arguments to any script handler with the `@map_script_handler('Trigger.Exit')` decorator.  You do not need this property, but it is added here as a demonstration.

When you enter this area, the `try_exit()` function in the python script will be called, which in turn will call, `Map.exit()`, which will exit the map (and also call `on_exit()` in your script, in case you need a callback). 

## Setting transitions

**Currently not implemented**

You can create a transition from one map to another, which, as opposed to an area which you simply walk into, can be an action you perform on another entity, for example pressing a switch.

## Adding overlays

Overlay tiles are used for when you have a tile with some transparent parts, which you want to put on top of a base tile.  Currently, these have no affect on gameplay: they do not require any special properties beyond **Type** being set to "FloorOverlay", and the usual **Frame** and **Variant** properties.

## Adding decorations

**Currently not implemented**

## Adding world entities

World entities are a small subset of entities, for things which are generally considered part of the physical makeup of a space, but which require logic.  Good examples would be doors, windows, lifts, etc.

The currently-implemented types are:
- Door
- Window
- WallObject

To set the initial state of the entity, choose the tile with the **State** property that you require: eg for Doors, it would be "Open", "Unlocked" or "Locked".  If you want a door that can be locked, but starts open, then use the "Open" status, and ensure that you add a **Codes** property (and a matching **Key** entity which can unlock it).

## Add entities/items

There are three types of entity:
- Entity
- Item
- Abstract

These are specified by their **Type** in the tileset.  Entity types are the main entity on any given tile (and can be the only one of that type).  Item types are placed into a tile's inventory, and can be picked up, dropped, and otherwise interacted with.
Finally, Abstract entities are placed on an **Abstract** layer, and are used for things such as wiring up events, and used objects, etc.

## Adding entity inventory

Entities can be given starting inventory.  To do this, add **Item** entities on an **Inventory** layer, and set that entity's **Core.Owner** property to the Name of the owning entity.  You will need to give the inventory item a name.  This is required internally for matching inventory items to their owners.

## Adding triggers

Triggers are added in the **Areas** layer, and given the **Type** "Trigger".  Triggers need one of the following custom properties:
- Core.Event: string property, which is used in your map script, as follows: `@observable.on(None, <eventname>)`
- Core.UseTarget.: string property.  The value of any property prefixed with Core.UseTarget is a UseTarget definition, which will be executed.

Further to this there are optional properties:
- Core.Count: int property, the number of times this trigger can be triggered before it removes itself from the map.  Defaults to -1 (no limit)

The following are used solely for UseTargets.  See Useable.md for a description
- Core.SuppressMsg
- Core.PlayerUseMsg
- Core.PlayerFailMsg
- Core.OtherUseMsg
- Core.OtherFailMsg
- Core.AllowedUserNames
- Core.NumUsersNeeded
- Core.UserFilterFunc

Finally, any other properties specified are passed as named parameters to the functions which handle the events, prefixed with 'Parameters'.  For instance *Parameters.value*.

## Adding lights

**Currently broken**

Lights are currently purely visual, and allow for changes in intensity, flickering, etc.  They can also use "vision" to cast themselves against walls, through open doors, etc, so that a radial light which is constrained within a room will increase its distance when doors are opened.
Currently, the code is extremely slow, and should not really be used, except for messing around.

## Adding paths

Paths are used by entities.  They are defined on the **Paths** layer as an open polygon, and have just a **Name** property.  For an entity to use a path, it should have a custom property **Path**: a string set to the name of the path.
Entities will path-find between path nodes (and from the last back to the first) - there is no need to create a path vertex for every individual tile. 

## Tile properties

Tiles have the following properties:
- Type: string property, currently either "Wall", "Floor", "FloorOverlay" or "Space".

And the following custom properties:
- Frame: int property, specifies the starting animation frame, in the case that the tile has multiple frames.
- Variant: string property, this is the name of the visual variant, and is used to distinguish between otherwise-identical tiles, in order to add variety.
- blocks-movement: bool property, specifies whether the tile blocks entity movement.  This will generally be the case for Wall types, but not for Floor or FloorOverlay.
- blocks-vision: bool property, specifies whether the tile blocks entity vision.  This will generally be the case for Wall types, but not for Floor or FloorOverlay.
- blocks-audio: bool property, which determines whether or not the tile blocks sound.
- space: int property, determines the inventory capacity of the tile.

## Entity properties

Entities have the following properties:
- Type: string property, one of:
  - Entity: standard Entity.
  - Item: object is treated as map inventory.
  - WallObject: object is placed on wall based on the custom Direction property (see below).
  - Door: object is set up as a door.
  - Window: object is set up as a window.
		
And the following custom properties:
- Core.Class: string property, the name of the entity class as defined in the entity definitions file.
- Core.Frame: int property, specifies the starting animation frame, in the case that the tile has multiple frames.
- Core.States: comma-separated string, used to set the starting state(s) of the entity.
- Core.Variant: string property, this is the name of the visual variant, and is used to distinguish between otherwise-identical tiles, in order to add variety.

You can override entity properties as defined in the Entity Definition using the following:
- Properties.<property-name>

## Entity traits

You can override a trait's settings (or indeed add a whole new one) to an instance using the following:
- Traits.<trait-name>.Parameters.<parameter-name>
- Traits.<trait-name>.Actors.<action-name>.?
- Traits.<trait-name>.Targets.<action-name>.?
- Traits.<trait-name>.Specialisation

## Entity State Management

You can override an entity's state management, using the following:
- StateManagement.Handlers.<state-name>
- StateManagement.Blockers.<state-name>
- StateManagement.Visuals.<state-name>

In each case, the value should be a list of nested strings, in python format but without any quotes.  For instance:
```
[[-Locked,Open,[Close]]]
```

## Scripting

Each map can have a script, which contains functions which act as callbacks for events that occur on the map.  To do this, you use the `@map_script_handler(event)` decorator with the following events:
- Map.Enter
	- Parameters: `map, player`
- Map.Exit
	- Parameters: `map, exiting_entity`
- Entity.SetPlayer
	- Parameters: `entity, is_entity_player`
- Entity.Action.*action*
	- Parameters: `entity, target, *args, **kwargs`
- Entity.Target.*action*
	- Parameters: `entity, target, *args, **kwargs`

There are others, essentially any event triggered with `obs.trigger`.  You can also create your own, using Trigger areas. 