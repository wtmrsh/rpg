# Creating an Entity subclass

Sometimes an Entity definition is not enough and you need extra logic.  Usually this is the case when the requirement is simple and doesn't warrant a new trait or action.

To create the subclass, you simply need to subclass from `Entity`, placing the file somewhere under the game's 'entities' subdirectory.  At this point, it will be found by the
game module system and made available to use.  You can then create an Entity definition named after the class to give it the required properties.
