# Background tiles

Background tiles should be part of an image atlas.  This is then loaded into Tiled by creating a new tileset.

Then, assign each tile one of the following **Type**s:
- Floor
- FloorOverlay
- Wall
- Space

Then create the following custom properties:
- Core.Frame: int property, specifies the starting animation frame, in the case that the tile has multiple frames.
- Core.Variant: string property, this is the name of the visual variant, and is used to distinguish between otherwise-identical tiles, in order to add variety.
- blocks-movement: bool property, which determines whether or not entities can move onto this tile.
- blocks-vision: bool property, which determines whether or not the tile blocks entity vision.
- blocks-audio: bool property, which determines whether or not the tile blocks sound.
- space: int property, which is the "capacity" of the tile, ie how much can be placed/dropped on it.  The value is the capacity of tile's inventory.

There are a couple of special tiles required here.
- One, with **Type** "Space", and **Core.Variant** "Stars0" which acts as the specifier for the default ambient colour for all tiles.
  This tile should have custom property 'AmbientLight', of type colour, which defines that colour.  It should have **Core.Frame** set to 1.
  This is the tile shown for map tiles not visible to the player.
- One, with **Type** "VisibleToHostile", and nothing else set, which is used to show which tiles hostile entities can see.  

# Entity tiles

Entity tiles should be part of an image atlas.  This is then loaded into Tiled by creating a new tileset.

Then, assign each tile one of the following **Type**s:
- Entity
- Item
- WallObject
- Abstract

Then create the following custom properties:
- Core.Class: string property, the name of the entity class as defined in the entity definitions file.
- Core.Frame: int property, specifies the starting animation frame, in the case that the tile has multiple frames.  This value is one-offset, not zero.
- Core.States: used to set the starting state of the entity.  If there is no specific status, then use "Default".
- Core.Variant: string property, this is the name of the visual variant, and is used to distinguish between otherwise-identical tiles, in order to add variety.
- Core.Direction: optional string property, one of "NORTH", "NORTHEAST", "EAST", "SOUTHEAST", "SOUTH", "SOUTHWEST", "WEST", "NORTHWEST".  Defaults to "NONE".
  This is generally only needed for entities which have vision.