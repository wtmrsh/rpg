# Creating a trait

To explain, using a hypothetical *smashable* trait.

First of all, create a subclass of `Trait`, and mark it up with the appropriate decorators.

    from engine.trait import trait
    from engine.traitfactory import register_trait

    @register_trait('Smashable')
    class Smashable(Trait):

    def __init__(self, owner, visualstatuses):
      super().__init__(owner)
      self.visualstatuses.update(visualstatuses)

Now you are more or less free to implement the rest of the class.  Update __init__.py in the appropriate directory to include it.

Entities can use this trait simply by adding 'Smashable' to the trait list in their definition.  For instance:

    def smash(self):
        self.owner.size = EntitySize.SMALL

You can add targets to it if you wish:

    @register_trait('Smashable')
    @exposes_target('Smash')
    class Smashable(Trait):

You will need to add parsers for Tiled, derived from `engine.TraitParser`, to parse the arguments you intend to pass into the constructor.

Finally, add it to __init__.py to ensure it is loaded.