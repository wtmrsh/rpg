# Traits

Traits are properties which entities have, which define the capabilities and behaviour of an entity.  
They have an owner, which is usually the entity, but other things (eg trigger areas) may have particular traits.

Traits can define states upon the owning Entity, and these can be used to determine.