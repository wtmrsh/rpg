<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.5" tiledversion="1.7.2" name="Oryx SciFi" tilewidth="24" tileheight="24" tilecount="1435" columns="35">
 <image source="resources/images/oryx_16bit_scifi_world_trans.png" width="840" height="984"/>
 <tile id="0" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Tile1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Tile2"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="2" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Tile3"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="3" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Tile4"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="6" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default15"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="7" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default13"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="8" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default5"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="9" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default7"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="10" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default11"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="11" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default10"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="12" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default14"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="13" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default9"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="14" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default3"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="15" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default12"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="16" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default6"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="17" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default0"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="18" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default1"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="19" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default2"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="20" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default8"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="21" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default4"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="35" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Warning1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="36" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Warning2"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="37" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Warning3"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="38" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Warning4"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="42" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="TileDamaged1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="43" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="TileDamaged2"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="44" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="TileDamaged3"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="45" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Bars11"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="46" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Ring11"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="47" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Damaged5"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="48" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Damaged10"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="49" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Bars5"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="70" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="71" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="72" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="73" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="75" type="Wall">
  <properties>
   <property name="Core.Frame" value="1"/>
   <property name="Core.Variant" value="Default5Yellow"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="76" type="Wall">
  <properties>
   <property name="Core.Frame" value="2"/>
   <property name="Core.Variant" value="Default5Yellow"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="77" type="Wall">
  <properties>
   <property name="Core.Frame" value="1"/>
   <property name="Core.Variant" value="Default5Red"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="78" type="Wall">
  <properties>
   <property name="Core.Frame" value="2"/>
   <property name="Core.Variant" value="Default5Red"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="79" type="Wall">
  <properties>
   <property name="Core.Frame" value="1"/>
   <property name="Core.Variant" value="Default5Green"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="80" type="Wall">
  <properties>
   <property name="Core.Frame" value="2"/>
   <property name="Core.Variant" value="Default5Green"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="81" type="Wall">
  <properties>
   <property name="Core.Frame" value="1"/>
   <property name="Core.Variant" value="Default15Yellow"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="82" type="Wall">
  <properties>
   <property name="Core.Frame" value="2"/>
   <property name="Core.Variant" value="Default15Yellow"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="83" type="Wall">
  <properties>
   <property name="Core.Frame" value="1"/>
   <property name="Core.Variant" value="Default15Red"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="84" type="Wall">
  <properties>
   <property name="Core.Frame" value="2"/>
   <property name="Core.Variant" value="Default15Red"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="85" type="Wall">
  <properties>
   <property name="Core.Frame" value="1"/>
   <property name="Core.Variant" value="Default15Green"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
   <property name="blocks-audio" type="bool" value="true"/>
  </properties>
 </tile>
 <tile id="86" type="Wall">
  <properties>
   <property name="Core.Frame" value="2"/>
   <property name="Core.Variant" value="Default15Green"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="true"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="0"/>
  </properties>
 </tile>
 <tile id="105" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="106" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="107" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="108" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="140" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="141" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="142" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="143" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="182" type="Wall">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default"/>
   <property name="blocks-movement" type="bool" value="true"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="true"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="205" type="Floor">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="206" type="Floor">
  <properties>
   <property name="Core.Frame" value="1"/>
   <property name="Core.Variant" value="2"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="241" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Default"/>
  </properties>
 </tile>
 <tile id="1120" type="Space">
  <properties>
   <property name="AmbientLight" type="color" value="#ff303030"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Stars0"/>
  </properties>
 </tile>
 <tile id="1121" type="Space">
  <properties>
   <property name="AmbientLight" type="color" value="#ff303030"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Stars1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1122" type="Space">
  <properties>
   <property name="AmbientLight" type="color" value="#ff303030"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Stars2"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1123" type="Space">
  <properties>
   <property name="AmbientLight" type="color" value="#ff303030"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Stars3"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1225" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay1"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1226" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay2"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1227" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay3"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1228" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay4"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1229" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay5"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1230" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay6"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1231" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay7"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1232" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay8"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1233" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay9"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1234" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay10"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1235" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay11"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1236" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay12"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1237" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay13"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1238" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay14"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1239" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay15"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1240" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay16"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1241" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay17"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1242" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay18"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1243" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay19"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1244" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay20"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1245" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay21"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1246" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay22"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1247" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay23"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1248" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay24"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1249" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay25"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1260" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay26"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1261" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay27"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1262" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay28"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1263" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay29"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1264" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay30"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1265" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay31"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1266" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay32"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1267" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay33"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1268" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay34"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1269" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay35"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1270" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay36"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1271" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay37"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1272" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay38"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1273" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay39"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1274" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay40"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1275" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay41"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1276" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay42"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1277" type="FloorOverlay">
  <properties>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.Variant" value="Overlay43"/>
   <property name="blocks-movement" type="bool" value="false"/>
   <property name="blocks-vision" type="bool" value="false"/>
   <property name="blocks-audio" type="bool" value="false"/>
   <property name="space" type="int" value="9999"/>
  </properties>
 </tile>
 <tile id="1323" type="VisibleToHostile"/>
</tileset>
