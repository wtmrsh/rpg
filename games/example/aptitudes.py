"""Aptitude functions"""


def aptitude_for_move(actor, target):
    return 1.0


def aptitude_patroller_open(actor, target):
    return 0.8


def aptitude_stealthpatroller_lock(actor, target):
    return 0.7
