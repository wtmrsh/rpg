"""Dynamic transitions."""
import engine.gamemodule
from engine.defines import FactionRelation


factionsmodule = engine.gamemodule.get_game_factions_module()
get_faction_relationship = getattr(factionsmodule, 'get_faction_relationship')


def find_hostile_to_attack(actor):
    vision = actor.get_vision()
    if vision:
        ents = vision.visible_entities

        # Filter hostile
        ents = list(filter(lambda e: get_faction_relationship(e, actor) == FactionRelation.HOSTILE, ents))

        # Take first for now
        if len(ents) > 0:
            return True, {
                'Target': ents[0]
            }
        else:
            return False, dict()
