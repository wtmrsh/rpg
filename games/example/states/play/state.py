from collections import Counter

import engine.observable as obs
import engine.state
from engine.container import HorizontalContainer, VerticalContainer
from engine.defines import AlignmentType
from engine.views import *
from engine.exceptions import SpawnChildStateException

import games.example.entities
import games.example.states.play.controller as playcontroller
import games.example.states.play.model as playmodel
from games.example.states.play.world import World
import games.example.states.play.actions
import games.example.views.statusview
import games.example.views.mapview
from games.example.views.hostiletilevisiblemapviewplugin import HostileTileVisibleMapViewPlugin
import games.example.states


class State(engine.state.State):

    def __init__(self):
        """
        Initializer.
        """
        super().__init__('Play')
        self.controller = None
        self.model = None
        self.curturn = 0
        self.statusview = None
        self.mapview = None
        self.msgview = None

    def setup_model(self):
        """
        Create the model/gamestate.  This expects the 'model' data member to be instantiated: this is a freeform class,
        there are no expectations on it as it is intended to be used solely by the client/game.  In this example,
        we have a 'World' class which represents global state, and is capable of creating a map.
        """
        # Create world
        world = World()
        curmap = world.create_map('example10-intelligence2')

        # Create player
        player = curmap.create_entity_on_map(
            curmap.entrypoint[0],  # X
            curmap.entrypoint[1],  # Y
            curmap.entrypoint[2],  # Direction
            'Player',              # Entity class
            'ExamplePlayer',       # Name
            ['Default'],           # Initial start state
            '1',                   # Visual variant
            0,                     # Visual frame
            dict(),                # Overrides
            False,                 # Whether to do wiring
            dict(),                # Usetargets
            dict())                # LogicState wiring

        player.set_player(True)

        # Set up model
        self.model = playmodel.Model(world, player)
        
    def create_views(self, viewmanager):
        """
        Create views.

        :param viewmanager: ViewManager instance.
        """
        viewmanager.clear()
        viewmanager.map = None
        viewmanager.register_events()

        basecontainer = VerticalContainer(0, 0, AlignmentType.LEFT, AlignmentType.TOP)

        self.mapview = games.example.views.mapview.MapView(viewmanager, False)
        basecontainer.add_child(self.mapview)

        bottomcontainer = HorizontalContainer(0, 0, AlignmentType.LEFT, AlignmentType.TOP)
        basecontainer.add_child(bottomcontainer)

        self.statusview = games.example.views.statusview.StatusView(viewmanager)
        bottomcontainer.add_child(self.statusview)

        self.msgview = messageview.MessageView(viewmanager)
        bottomcontainer.add_child(self.msgview)

        viewmanager.set_container_as_root(basecontainer)
        viewmanager.set_focus(self.mapview)

    def intialise_views(self, viewmanager, model):
        viewmanager.map = model.world.map

        self.mapview.set_entity(model.player)
        self.mapview.keepentitycentred = model.world.map.globalproperties.get('ScrollWithPlayer', True)
        self.mapview.add_plugin(HostileTileVisibleMapViewPlugin(model.world.map))

        self.statusview.set_model(model)
        self.msgview.set_entity(model.player)

    def setup_controller(self):
        """
        Set up input controller.  This is a module which defines handle_input_XYZ functions.
        """
        self.controller = playcontroller

    def enter_impl(self, fromstate, *args, **kwargs):
        """
        Called when we enter the state.

        :param fromstate: previous state.
        :param args: ordered arguments passed from previous state.
        :param kwargs: keyword arguments passed from previous state.
        """
        self.curturn = 1

    def enter_post(self):
        """
        Called after entering the state.
        """
        map = self.model.player.map
        map.enter(self.model.player, self.model)

        self.model.player.get_vision().generate_fov_map(wallsvisible=True, calculatevisibleentities=True)

    def exit_impl(self, *args, **kwargs):
        """
        Called when exiting the state.

        :param args: ordered arguments passed from ExitStateException, or other.
        :param kwargs: keyword arguments passed from ExitStateException, or other.
        """
        self.statusview.unregister_events()
        self.mapview.unregister_events()
        self.msgview.unregister_events()

    def resume_impl(self, oldstate, *args, **kwargs):
        """
        Called when resuming a state.

        :param oldstate: previous state.
        :param args: ordered arguments passed from previous state.
        :param kwargs: keyword arguments passed from previous state.
        """
        pass

    def update_pre(self, frametime):
        """
        Called just before main state update.

        :param frametime: time spent on this frame.
        """
        pass

    def update_impl(self, action, frametime):
        """
        Called during state update.

        :param action: ActionResult instance of the action chosen by the player.
        :param frametime: time spent on this frame.
        """
        player = self.model.player
        curmap = player.map

        # Check visuals
        curmap.update_visuals(frametime)

        if action.turntaken:
            curmap.update(self.curturn)
            curmap.entities_turn(self.curturn)

            obs.trigger(None, 'Game.TurnTaken', self.curturn)
            self.curturn += 1
