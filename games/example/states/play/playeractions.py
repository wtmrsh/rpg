from common import *

from engine.exceptions import *
from engine.defines import InputType, ActionResult, ActionRequestResult
from engine.tiletype import TileType


def action_exit(model):
    """
    Exits the state.

    :params model: play state model
    """
    raise ExitStateException(None)


def action_cancel(model):
    """
    Cancel the action you were about to do (in directional or targeted mode).
    
    :params model: play state model
    :return: ActionResult
    """
    return ActionResult(
        turntaken=False,
        printmsg=True,
        playermsg='Action cancelled.',
        othermsg=None, 
        inputtype=InputType.IMMEDIATE, 
        delayedaction=None)


def action_wait(model):
    """
    Do nothing.

    :param model: play state model
    :return: ActionResult
    """
    entity = model.player
    result = entity.request_action('Wait', entity.map)
    return ActionResult(
        turntaken=result.ok,
        printmsg=False,
        playermsg=result.playermsg,
        othermsg=result.othermsg,
        inputtype=InputType.IMMEDIATE,
        delayedaction=None)


def action_move(model, args):
    """
    Move an entity around a map.

    :param model: play state model
    :param args: args[0] is dx, args[1] is dy (movement)
    :return: ActionResult
    """
    success = (False, None, None)

    entity = model.player
    map = entity.map

    # Get target tile
    dx = args[0]
    dy = args[1]
    tx = entity.x + dx
    ty = entity.y + dy

    if 0 <= tx < map.sizeX and 0 <= ty < map.sizeY:
        # If we've moving diagonally, make sure we aren't cutting a corner,
        # or trying to squeeze through a gap
        if dx != 0 and dy != 0:
            t0 = map.tiles[entity.x][ty]
            t1 = map.tiles[tx][entity.y]

            if t0.blocks_pathing(entity) or t1.blocks_pathing(entity):
                return ActionResult(
                    turntaken=False,
                    printmsg=True,
                    playermsg=None,
                    othermsg=None,
                    inputtype=InputType.IMMEDIATE,
                    delayedaction=None)

        # Now determine which action to perform:
        tile = map.tiles[tx][ty]

        if tile.type == TileType.FLOOR:
            # Is there an entity on the floor?
            if tile.entity:
                tent = tile.entity
                if tent.has_trait('Openable'):
                    if tent.has_state('Open'):
                        result = entity.request_action('Close', tent)
                    else:
                        result = entity.request_action('Open', tent)
                elif tent.has_trait('Lockable'):
                    if tent.has_state('Locked'):
                        result = entity.request_action('Unlock', tent)
                    else:
                        result = entity.request_action('Lock', tent)
                elif tent.has_trait('Useable'):
                    result = entity.request_action('Use', tent)
                else:
                    result = ActionRequestResult(passed=False,
                                                 actionrequired=True,
                                                 playermsg=None,
                                                 othermsg=None,
                                                 prereqactions=[])
            else:
                result = entity.request_action('Move', entity.map, entity.x + dx, entity.y + dy)
        elif tile.type == TileType.DOOR:
            if tile.door.has_state('Open'):
                # Try to move into the open doorway.  Other entities may be trying to close the door, so set
                # the target as the door so that dispute-resolution picks this action up.
                result = entity.request_action('Move', entity.map, entity.x + dx, entity.y + dy)
            elif tile.door.has_state('Locked'):
                # Try to unlock the door
                result = entity.request_action('Unlock', tile.door)
            else:
                # Try to open the door
                result = entity.request_action('Open', tile.door)
        elif tile.type == TileType.WALL:
            # Check for inventory
            if tile.inventory and len(tile.inventory) == 1:
                result = entity.request_action('Get', tile.inventory.stacks[0].item, tx, ty, 1)
                return ActionResult(
                    turntaken=result.ok,
                    printmsg=True,
                    playermsg=result.playermsg,
                    othermsg=result.othermsg,
                    inputtype=InputType.IMMEDIATE,
                    delayedaction=None)

            # Check for switches
            # Ensure wall objects are usable
            wallindex = -dx if (dx + dy) < 0 else 2 + dx
            if tile.wallObjects[wallindex]:
                if tile.wallObjects[wallindex].has_trait('Useable'):
                    result = entity.request_action('Use', tile.wallObjects[wallindex])
                else:
                    return ActionResult(
                        turntaken=False,
                        printmsg=True,
                        playermsg=f'You cannot use {tile.wallObjects[wallindex].def_name()}',
                        othermsg=None,
                        inputtype=InputType.IMMEDIATE,
                        delayedaction=None)
            else:
                return ActionResult(
                    turntaken=False,
                    printmsg=True,
                    playermsg=None,
                    othermsg=None,
                    inputtype=InputType.IMMEDIATE,
                    delayedaction=None)
        else:
            return ActionResult(
                turntaken=False,
                printmsg=True,
                playermsg=None,
                othermsg=None,
                inputtype=InputType.IMMEDIATE,
                delayedaction=None)

    return ActionResult(
        turntaken=result.ok,
        printmsg=True,
        playermsg=result.playermsg,
        othermsg=result.othermsg,
        inputtype=InputType.IMMEDIATE,
        delayedaction=None)


def action_get(model):
    """
    Try and pick up something from the ground.

    :param model: play state model
    :return: ActionResult
    """
    entity = model.player
    map = entity.map

    tile = map.tiles[entity.x][entity.y]
    tileinv = tile.inventory

    # Is there anything on the current tile?
    invsize = len(tileinv)

    if invsize == 0:
        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg='There is nothing here for you to pick up.',
            othermsg=None,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    elif invsize == 1:
        success = entity.request_action('Get', tileinv.stacks[0].item, entity.x, entity.y, 1)
        return ActionResult(
            turntaken=success.ok,
            printmsg=True,
            playermsg=success.playermsg,
            othermsg=success.othermsg,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    else:
        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg='There are too many items here to pick up!',
            othermsg=None,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)


def action_drop(model):
    """
    Try and drop something from inventory.

    :param model: play state model
    :return: ActionResult
    """
    entity = model.player
    inv = entity.inventory

    # Is there anything to drop?
    invsize = len(inv)

    if invsize == 0:
        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg='You have nothing to drop.',
            othermsg=None,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    else:
        success = entity.request_action('Drop', inv.stacks[0].item, entity.x, entity.y, 1)
        return ActionResult(
            turntaken=success.ok,
            printmsg=True,
            playermsg=success.playermsg,
            othermsg=success.othermsg,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)


def action_open(model):
    """
    Open a door/window

    :param model: play state model
    :return: ActionResult
    """
    entity = model.player
    map = entity.map

    # Check main directions, building up list.  We check if the doors are closed and ignore if they aren't,
    # because openable objects may be placed in an open doorway.
    openables = list()
    if entity.x > 0:
        tile = map.tiles[entity.x - 1][entity.y]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open'):
            openables.append((tile.door, entity.x - 1, entity.y))
        elif tile.entity and 'Open' in tile.entity.targets:
            openables.append((tile.entity, entity.x - 1, entity.y))
    if entity.x < (map.sizeX - 1):
        tile = map.tiles[entity.x + 1][entity.y]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open'):
            openables.append((tile.door, entity.x + 1, entity.y))
        elif tile.entity and 'Open' in tile.entity.targets:
            openables.append((tile.entity, entity.x + 1, entity.y))
    if entity.y > 0:
        tile = map.tiles[entity.x][entity.y - 1]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open'):
            openables.append((tile.door, entity.x, entity.y - 1))
        elif tile.entity and 'Open' in tile.entity.targets:
            openables.append((tile.entity, entity.x, entity.y - 1))
    if entity.y < (map.sizeY - 1):
        tile = map.tiles[entity.x][entity.y + 1]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open'):
            openables.append((tile.door, entity.x, entity.y + 1))
        elif tile.entity and 'Open' in tile.entity.targets:
            openables.append((tile.entity, entity.x, entity.y + 1))

    # Execute, depending on how many openable things we've found
    if len(openables) == 0:
        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg='There is nothing here that you can open.',
            othermsg=None,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    elif len(openables) == 1:
        result = entity.request_action('Open', openables[0][0])
        return ActionResult(
            turntaken=result.ok,
            printmsg=True,
            playermsg=result.playermsg,
            othermsg=result.othermsg,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    else:
        obs.trigger(None, 'Game.Message', 'Which direction to open in?')

        # Return a closure to execute after we've chosen the direction.
        def action_open_entity(model, delta):
            tx = entity.x + delta[0]
            ty = entity.y + delta[1]
            tile = model.player.map.tiles[tx][ty]

            if tile.type == TileType.DOOR and not tile.entity:
                result = entity.request_action('Open', tile.door)
            elif tile.type == TileType.WINDOW:
                result = entity.request_action('Open', tile.window)
            elif tile.entity:
                result = entity.request_action('Open', tile.entity)
            else:
                result = ActionResult(
                    turntaken=False,
                    printmsg=True,
                    playermsg='There is nothing here that you can open.',
                    othermsg=None,
                    inputtype=InputType.IMMEDIATE,
                    delayedaction=None)
                obs.trigger(None, 'Game.Message', 'There is nothing there to open. %s' % DEBUG_MSG)

            return ActionResult(
                turntaken=result.ok,
                printmsg=True,
                playermsg=result.playermsg,
                othermsg=result.othermsg,
                inputtype=InputType.IMMEDIATE,
                delayedaction=None)

        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg=None,
            othermsg=None,
            inputtype=InputType.DIRECTIONAL,
            delayedaction=action_open_entity)


def action_close(model):
    """
    Close a door/window

    :param model: play state model
    :return: ActionResult
    """
    entity = model.player
    map = entity.map

    # Check main directions, building up list.
    closeables = list()
    if entity.x > 0:
        tile = map.tiles[entity.x - 1][entity.y]
        if tile.type == TileType.DOOR and not tile.entity and tile.door.has_state('Open'):
            closeables.append((tile.door, entity.x - 1, entity.y))
        elif tile.entity and 'Close' in tile.entity.targets:
            closeables.append((tile.entity, entity.x - 1, entity.y))
    if entity.x < (map.sizeX - 1):
        tile = map.tiles[entity.x + 1][entity.y]
        if tile.type == TileType.DOOR and not tile.entity and tile.door.has_state('Open'):
            closeables.append((tile.door, entity.x + 1, entity.y))
        elif tile.entity and 'Close' in tile.entity.targets:
            closeables.append((tile.entity, entity.x + 1, entity.y))
    if entity.y > 0:
        tile = map.tiles[entity.x][entity.y - 1]
        if tile.type == TileType.DOOR and not tile.entity and tile.door.has_state('Open'):
            closeables.append((tile.door, entity.x, entity.y - 1))
        elif tile.entity and 'Close' in tile.entity.targets:
            closeables.append((tile.entity, entity.x, entity.y - 1))
    if entity.y < (map.sizeY - 1):
        tile = map.tiles[entity.x][entity.y + 1]
        if tile.type == TileType.DOOR and not tile.entity and tile.door.has_state('Open'):
            closeables.append((tile.door, entity.x, entity.y + 1))
        elif tile.entity and 'Close' in tile.entity.targets:
            closeables.append((tile.entity, entity.x, entity.y + 1))

    # Execute, depending on how many closeable things we've found
    if len(closeables) == 0:
        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg='There is nothing here that you can close.',
            othermsg=None,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    elif len(closeables) == 1:
        result = entity.request_action('Close', closeables[0][0])
        return ActionResult(
            turntaken=result.ok,
            printmsg=True,
            playermsg=result.playermsg,
            othermsg=result.othermsg,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    else:
        obs.trigger(None, 'Game.Message', 'Which direction to close in?')

        # Return a closure to execute after we've chosen the direction.
        def action_close_entity(model, delta):
            tx = entity.x + delta[0]
            ty = entity.y + delta[1]
            tile = model.player.map.tiles[tx][ty]

            if tile.type == TileType.DOOR and not tile.entity:
                result = entity.request_action('Close', tile.door)
            elif tile.type == TileType.WINDOW:
                result = entity.request_action('Close', tile.window)
            elif tile.entity:
                result = entity.request_action('Close', tile.entity)
            else:
                result = ActionResult(
                    turntaken=False,
                    printmsg=True,
                    playermsg='There is nothing here that you can close.',
                    othermsg=None,
                    inputtype=InputType.IMMEDIATE,
                    delayedaction=None)

                obs.trigger(None, 'Game.Message', 'There is nothing there to close. %s' % DEBUG_MSG)

            return ActionResult(
                turntaken=result.ok,
                printmsg=True,
                playermsg=result.playermsg,
                othermsg=result.othermsg,
                inputtype=InputType.IMMEDIATE,
                delayedaction=None)

        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg=None,
            othermsg=None,
            inputtype=InputType.DIRECTIONAL,
            delayedaction=action_close_entity)


def action_lock(model):
    """
    Lock a Lockable.

    :param model: play state model
    :return: ActionResult
    """
    entity = model.player
    map = entity.map

    # Check main directions, building up list.
    lockables = list()
    if entity.x > 0:
        tile = map.tiles[entity.x - 1][entity.y]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open'):
            lockables.append((tile.door, entity.x - 1, entity.y))
        elif tile.entity and 'Lock' in tile.entity.targets:
            lockables.append((tile.entity, entity.x - 1, entity.y))
    if entity.x < (map.sizeX - 1):
        tile = map.tiles[entity.x + 1][entity.y]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open'):
            lockables.append((tile.door, entity.x + 1, entity.y))
        elif tile.entity and 'Lock' in tile.entity.targets:
            lockables.append((tile.entity, entity.x + 1, entity.y))
    if entity.y > 0:
        tile = map.tiles[entity.x][entity.y - 1]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open'):
            lockables.append((tile.door, entity.x, entity.y - 1))
        elif tile.entity and 'Lock' in tile.entity.targets:
            lockables.append((tile.entity, entity.x, entity.y - 1))
    if entity.y < (map.sizeY - 1):
        tile = map.tiles[entity.x][entity.y + 1]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open'):
            lockables.append((tile.door, entity.x, entity.y + 1))
        elif tile.entity and 'Lock' in tile.entity.targets:
            lockables.append((tile.entity, entity.x, entity.y + 1))

    # Execute, depending on how many lockable things we've found
    if len(lockables) == 0:
        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg='There is nothing here that you can lock.',
            othermsg=None,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    elif len(lockables) == 1:
        success = entity.request_action('Lock', lockables[0][0])
        return ActionResult(
            turntaken=success.ok,
            printmsg=True,
            playermsg=success.playermsg,
            othermsg=success.othermsg,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    else:
        obs.trigger(None, 'Game.Message', 'Which direction to lock in?')

        # Return a closure to execute after we've chosen the direction.
        def action_lock_entity(mdl, delta):
            tx = entity.x + delta[0]
            ty = entity.y + delta[1]
            tl = mdl.player.map.tiles[tx][ty]

            scs = (False, None, None)
            if tl.type == TileType.DOOR and not tl.entity:
                scs = entity.request_action('Lock', tile.door)
            elif tile.entity:
                scs = entity.request_action('Lock', tile.entity)
            else:
                obs.trigger(None, 'Game.Message', 'There is nothing there to lock. %s' % DEBUG_MSG)

            return ActionResult(
                turntaken=scs[0] is True,
                printmsg=True,
                playermsg=scs[1],
                othermsg=scs[2],
                inputtype=InputType.IMMEDIATE,
                delayedaction=None)

        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg=None,
            othermsg=None,
            inputtype=InputType.DIRECTIONAL,
            delayedaction=action_lock_entity)


def action_unlock(model):
    """
    Unlock a Lockable.

    :param model: play state model
    :return: ActionResult
    """
    entity = model.player
    map = entity.map

    # Check main directions, building up list.
    unlockables = list()
    if entity.x > 0:
        tile = map.tiles[entity.x - 1][entity.y]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open'):
            unlockables.append((tile.door, entity.x - 1, entity.y))
        elif tile.entity and 'Unlock' in tile.entity.targets:
            unlockables.append((tile.entity, entity.x - 1, entity.y))
    if entity.x < (map.sizeX - 1):
        tile = map.tiles[entity.x + 1][entity.y]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open'):
            unlockables.append((tile.door, entity.x + 1, entity.y))
        elif tile.entity and 'Unlock' in tile.entity.targets:
            unlockables.append((tile.entity, entity.x + 1, entity.y))
    if entity.y > 0:
        tile = map.tiles[entity.x][entity.y - 1]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open'):
            unlockables.append((tile.door, entity.x, entity.y - 1))
        elif tile.entity and 'Unlock' in tile.entity.targets:
            unlockables.append((tile.entity, entity.x, entity.y - 1))
    if entity.y < (map.sizeY - 1):
        tile = map.tiles[entity.x][entity.y + 1]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open'):
            unlockables.append((tile.door, entity.x, entity.y + 1))
        elif tile.entity and 'Unlock' in tile.entity.targets:
            unlockables.append((tile.entity, entity.x, entity.y + 1))

    # Execute, depending on how many unlockable things we've found
    if len(unlockables) == 0:
        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg='There is nothing here that you can unlock.',
            othermsg=None,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    elif len(unlockables) == 1:
        success = entity.request_action('Unlock', unlockables[0][0])
        return ActionResult(
            turntaken=success[0] is True,
            printmsg=True,
            playermsg=success[1],
            othermsg=success[2],
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    else:
        obs.trigger(None, 'Game.Message', 'Which direction to unlock in?')

        # Return a closure to execute after we've chosen the direction.
        def action_unlock_entity(model, delta):
            tx = entity.x + delta[0]
            ty = entity.y + delta[1]
            tile = model.player.map.tiles[tx][ty]

            success = (False, None, None)
            if tile.type == TileType.DOOR and not tile.entity:
                success = entity.request_action('Unlock', tile.door)
            elif tile.entity:
                success = entity.request_action('Unlock', tile.entity)
            else:
                obs.trigger(None, 'Game.Message', 'There is nothing there to unlock. %s' % DEBUG_MSG)

            return ActionResult(
                turntaken=success[0] is True,
                printmsg=True,
                playermsg=success[1],
                othermsg=success[2],
                inputtype=InputType.IMMEDIATE,
                delayedaction=None)

        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg=None,
            othermsg=None,
            inputtype=InputType.DIRECTIONAL,
            delayedaction=action_unlock_entity)


def action_destroy(model):
    """
    Destroy something

    :param model: play state model
    :return: ActionResult
    """
    entity = model.player
    map = entity.map

    # Check main directions, building up list.
    destroyables = list()
    if entity.x > 0:
        tile = map.tiles[entity.x - 1][entity.y]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open') and not tile.door.has_state('Destroyed'):
            destroyables.append(tile.door)
        elif tile.entity and tile.entity.has_trait('Destroyable') and not tile.entity.has_state('Destroyed'):
            destroyables.append(tile.entity)
    if entity.x < (map.sizeX - 1):
        tile = map.tiles[entity.x + 1][entity.y]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open') and not tile.door.has_state('Destroyed'):
            destroyables.append(tile.door)
        elif tile.entity and tile.entity.has_trait('Destroyable') and not tile.entity.has_state('Destroyed'):
            destroyables.append(tile.entity)
    if entity.y > 0:
        tile = map.tiles[entity.x][entity.y - 1]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open') and not tile.door.has_state('Destroyed'):
            destroyables.append(tile.door)
        elif tile.entity and tile.entity.has_trait('Destroyable') and not tile.entity.has_state('Destroyed'):
            destroyables.append(tile.entity)
    if entity.y < (map.sizeY - 1):
        tile = map.tiles[entity.x][entity.y + 1]
        if tile.type == TileType.DOOR and not tile.door.has_state('Open') and not tile.door.has_state('Destroyed'):
            destroyables.append(tile.door)
        elif tile.entity and tile.entity.has_trait('Destroyable') and not tile.entity.has_state('Destroyed'):
            destroyables.append(tile.entity)

    # Execute, depending on how many openable things we've found
    if len(destroyables) == 0:
        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg='There is nothing here that you can destroy.',
            othermsg=None,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    elif len(destroyables) == 1:
        result = entity.request_action('Destroy', destroyables[0])
        return ActionResult(
            turntaken=result.ok,
            printmsg=True,
            playermsg=result.playermsg,
            othermsg=result.othermsg,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)
    else:
        obs.trigger(None, 'Game.Message', 'Which direction to destroy in?')

        # Return a closure to execute after we've chosen the direction.
        def action_destroy_entity(model, delta):
            tx = entity.x + delta[0]
            ty = entity.y + delta[1]
            tile = model.player.map.tiles[tx][ty]

            if tile.type == TileType.DOOR and not tile.entity:
                result = entity.request_action('Open', tile.door)
            elif tile.type == TileType.WINDOW:
                result = entity.request_action('Open', tile.window)
            elif tile.entity:
                result = entity.request_action('Open', tile.entity)
            else:
                result = ActionResult(
                    turntaken=False,
                    printmsg=True,
                    playermsg='There is nothing here that you can destroy.',
                    othermsg=None,
                    inputtype=InputType.IMMEDIATE,
                    delayedaction=None)
                obs.trigger(None, 'Game.Message', 'There is nothing there to destroy. %s' % DEBUG_MSG)

            return ActionResult(
                turntaken=result.ok,
                printmsg=True,
                playermsg=result.playermsg,
                othermsg=result.othermsg,
                inputtype=InputType.IMMEDIATE,
                delayedaction=None)

        return ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg=None,
            othermsg=None,
            inputtype=InputType.DIRECTIONAL,
            delayedaction=action_destroy_entity)
