from bearlibterminal import terminal

import engine.observable as obs
from engine.defines import InputType

import games.example.states.play.playeractions as pa


def handle_input_view(input, inputtype, view, delayedaction, model):
    """
    Default handler.  This is called as a fallback, if the handler for the view in focus does not explicitly
    handle the input.
    
    :param input: input key
    :param inputtype: InputType - delayed or immediate
    :param view: active View
    :param delayedaction: action to execute after delayed mode is exited
    :param model: game model
    :return: list of [function to call, whether to execute immediately, packed function arguments]
             If [None], then bubble up to the next handler
    """
    if input in [terminal.TK_ESCAPE, terminal.TK_CLOSE]:
        return [pa.action_exit, True]

    # If we get here, ignore input
    return [None]


delayable_inputs = {
    terminal.TK_PERIOD: ['Wait', pa.action_wait, True],
    terminal.TK_KP_5: ['Wait', pa.action_wait, True],
    terminal.TK_UP: ['Move', pa.action_move, True, 0, 1],
    terminal.TK_KP_8: ['Move', pa.action_move, True, 0, 1],
    terminal.TK_DOWN: ['Move', pa.action_move, True, 0, -1],
    terminal.TK_KP_2: ['Move', pa.action_move, True, 0, -1],
    terminal.TK_LEFT: ['Move', pa.action_move, True, -1, 0],
    terminal.TK_KP_4: ['Move', pa.action_move, True, -1, 0],
    terminal.TK_RIGHT: ['Move', pa.action_move, True, 1, 0],
    terminal.TK_KP_6: ['Move', pa.action_move, True, 1, 0]
}

immediate_inputs = {
    terminal.TK_G: ['Get', pa.action_get, True],
    terminal.TK_D: ['Drop', pa.action_drop, True],
    terminal.TK_C: ['Close', pa.action_close, True],
    terminal.TK_O: ['Open', pa.action_open, True],
    terminal.TK_L: ['Lock', pa.action_lock, True],
    terminal.TK_U: ['Unlock', pa.action_unlock, True],
    terminal.TK_X: ['Destroy', pa.action_destroy, True]
}


def handle_input_map(input, inputtype, view, delayedaction, model):
    """
    Handler for MapView.
    
    :param input: input key
    :param inputtype: InputType - delayed or immediate
    :param view: active View
    :param delayedaction: action to execute after delayed mode is exited
    :param model: game model
    :return: list of [function to call, whether to execute immediately, packed function arguments]
             If [None], then bubble up to the next handler
    """
    thismap = model.world.map

    # For the examples, we filter inputs based on a custom property in each map, to ensure that only the
    # functionality we want for the given map is available
    allowed = thismap.metaproperties['AllowedActions'].split('|')

    if inputtype == InputType.IMMEDIATE:
        if input in immediate_inputs and immediate_inputs[input][0] in allowed:
            return immediate_inputs[input][1:]
        elif input in delayable_inputs and delayable_inputs[input][0] in allowed:
            return delayable_inputs[input][1:]
    elif inputtype == InputType.DIRECTIONAL:
        if input == terminal.TK_ESCAPE:
            return [pa.action_cancel, True]
        elif input in delayable_inputs:
            return [delayedaction] + delayable_inputs[input][2:]

    # If we get here, bubble input up
    return [None]
