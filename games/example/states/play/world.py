"""World class."""

#
# Imports
#
import engine.observable as obs
from engine.tiledmaploader import TiledMapLoader


class World:
    """World class, holds sectors/maps"""

    #
    # __init__()
    #
    def __init__(self):
        """
        Constructor.
        """
        self.owner = None
        self.map = None

    #
    # create_map()
    #
    def create_map(self, mapname):
        """
        Create a sector.

        :param mapname: name of map - corresponds to file <mapName>.json
        """
        filename = f'games/example/maps/{mapname}.json'
        loader = TiledMapLoader(filename)
        self.map = loader.load()
        self.map.owner = self

        return self.map
