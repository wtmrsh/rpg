"""
Guard
"""

#
# Imports
#
from functools import partial

import engine.observable as obs
from engine.entity import Entity


#
# Guard
#
class Guard(Entity):

    #
    # __init__()
    #
    def __init__(self, name):
        """
        Constructor.

        :param name: name of Entity
        """
        super().__init__(name)

    def creation_finished(self):
        obs.on(None, 'Alarm.Raised', self.on_alarm_raised)
        obs.on(None, 'Alarm.Cancelled', self.on_alarm_cancelled)

    #
    # on_alarm_raised()
    #
    def on_alarm_raised(self, source, raisingentity):
        print('guard raised')
    
    #
    # on_alarm_cancelled()
    #
    def on_alarm_cancelled(self, source, cancellingentity):
        print('guard cancelled')
