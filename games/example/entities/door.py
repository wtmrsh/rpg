"""
Door
"""
from engine.entity import Entity


class Door(Entity):

    #
    # __init__()
    #
    def __init__(self, name):
        """
        Constructor.

        :param name: name of Entity
        """
        super().__init__(name)

    def check_blocks_movement(self):
        return False

    def check_blocks_pathing(self, blockedentity):
        """
        :param blockedentity: Entity trying to path to the tile this Entity is on.
        :return: whether or not blockedentity can path here.
        """
        if self.has_state('Locked'):
            if len(self.lockable.codes) == 0:
                return True

            keys = set()
            if blockedentity.has_trait('Key'):
                keys |= blockedentity.key.codes
            if blockedentity.has_trait('Inventory'):
                keys |= blockedentity.inventory.get_key_codes()

            if len(keys & self.lockable.codes) == 0:
                return True
        else:
            return False

