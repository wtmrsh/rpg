from engine.traitprereqs import actor_prereq, target_prereq
from engine.defines import ActionRequestResult

# Must match common.py
WEIGHT_CONVERSION = 10.0


@target_prereq('Pick')
def prereq_is_pickable(target, actor, *args, **kwargs):
    """
    Check if the owning Entity can be picked.

    :param target: owning Entity
    :param actor: acting Entity
    :return: PrerequisiteResult instance
    """
    aname = actor.def_name()
    tname = target.def_name()
    if target.has_state('Open'):
        return ActionRequestResult(passed=False,
                                   actionrequired=False,
                                   playermsg=f'{tname} is open.',
                                   othermsg=f'{aname} tries to pick the lock of {tname} but it is open.',
                                   prereqactions=[])
    elif not target.has_state('Locked'):
        return ActionRequestResult(passed=False,
                                   actionrequired=False,
                                   playermsg=f'{tname} is not locked.',
                                   othermsg=f'{aname} tries to pick the lock of {tname} but it is not locked.',
                                   prereqactions=[])
    elif target.pickable.turnsRequired is None:
        return ActionRequestResult(passed=False,
                                   actionrequired=True,
                                   playermsg=f'The lock of {tname} cannot be picked.',
                                   othermsg=f'{aname} tries to pick the lock of {tname} but it cannot be picked.',
                                   prereqactions=[])
    else:
        return ActionRequestResult(passed=True,
                                   actionrequired=True,
                                   playermsg=None,
                                   othermsg=None,
                                   prereqactions=[])


@actor_prereq('Move')
def prereq_can_move(actor, map, x, y):
    """
    Check if the actor can move to the target location.

    :param actor: moving Entity
    :param map: Map the Entity is on
    :param x: target x-position
    :param y: target y-position
    :return: PrerequisiteResult instance
    """
    tile = map.tiles[x][y]
    return ActionRequestResult(passed=not tile.blocks_movement(),
                               actionrequired=True,
                               playermsg=None,
                               othermsg=None,
                               prereqactions=[])


@actor_prereq('Get')
def prereq_can_get(actor, target, *args, **kwargs):
    """
    Check if we can pick up an Entity.

    :param actor: Entity doing the 'getting'.
    :param target: Entity to pick up.
    :return: PrerequisiteResult instance
    """
    if actor.inventory.spare_capacity >= target.weight:
        return ActionRequestResult(passed=True,
                                   actionrequired=True,
                                   playermsg=None,
                                   othermsg=None,
                                   prereqactions=[])
    else:
        aname = actor.def_name()
        tname = target.def_name()
        return ActionRequestResult(passed=False,
                                   actionrequired=True,
                                   playermsg=f'{tname} is too heavy for you to pick up.',
                                   othermsg=f'{tname} is too heavy for {aname} to pick up.',
                                   prereqactions=[])
