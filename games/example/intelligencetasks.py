from engine.intelligencetask import IntelligenceTask, intelligence_task, intelligence_task_types


@intelligence_task('Guard_Chase')
class GuardChaseIntelligenceTask(IntelligenceTask):

    def __init__(self, supertask, actionname, entity, targetname, x, y):
        super().__init__(supertask, 'Guard_Chase', entity, None)
        self.x = x
        self.y = y
        self.movetask = None

    def reset(self):
        super().reset()
        self.target = self.supertask.parameters['Target']
        self.movetask = None

    def turn(self, curturn):
        if self.movetask:
            complete = self.movetask.turn(curturn)
            if not complete:
                return False, None

            self.movetask = None

        # Is the target one tile away?  If so, then just wait, otherwise calculate path
        if self.target.next_to(self.entity):
            self.entity.request_action('Wait', self.entity.map)
        else:
            typefact = intelligence_task_types['Move']
            self.movetask = typefact(supertask=self.supertask,
                                     actionname='Move',
                                     entity=self.entity,
                                     targetname=None,
                                     x=self.target.x,
                                     y=self.target.y)
            self.movetask.reset()

            if not self.movetask.turn(curturn):
                return False, None

        return False, None
