<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.1" name="Oryx Entities" tilewidth="24" tileheight="24" tilecount="324" columns="18">
 <image source="resources/images/oryx_entities.png" width="432" height="432"/>
 <tile id="0" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Open,-Locked,-Destroyed"/>
   <property name="Core.State" value="Open"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="1" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Open,-Locked,Destroyed"/>
   <property name="Core.State" value="Destroyed"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="2" type="Entity">
  <properties>
   <property name="Core.Class" value="Soldier"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Default"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="3" type="Entity">
  <properties>
   <property name="Core.Class" value="Scientist"/>
   <property name="Core.Frame" value="1"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="YoungMale"/>
  </properties>
 </tile>
 <tile id="4" type="Entity">
  <properties>
   <property name="Core.Class" value="Player"/>
   <property name="Core.Frame" value="1"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="5" type="Entity">
  <properties>
   <property name="Core.Class" value="Player"/>
   <property name="Core.Frame" value="1"/>
   <property name="Core.State" value="Dead"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="18" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="-Open,-Locked,-Destroyed"/>
   <property name="Core.State" value="Unlocked"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="19" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="2"/>
   <property name="Core.InitialStates" value="-Open,-Locked,-Destroyed"/>
   <property name="Core.State" value="Unlocked"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="20" type="Entity">
  <properties>
   <property name="Core.Class" value="Patroller"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Default"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="Green"/>
  </properties>
 </tile>
 <tile id="21" type="Item">
  <properties>
   <property name="Core.Class" value="Guard"/>
   <property name="Core.Frame" value="1"/>
   <property name="Core.State" value="Dead"/>
   <property name="Core.Variant" value="Watcher"/>
  </properties>
 </tile>
 <tile id="22" type="Entity">
  <properties>
   <property name="Core.Class" value="Scientist"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="OldMale"/>
  </properties>
 </tile>
 <tile id="36" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="-Open,Locked,-Destroyed"/>
   <property name="Core.State" value="Locked"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="37" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="2"/>
   <property name="Core.InitialStates" value="-Open,Locked,-Destroyed"/>
   <property name="Core.State" value="Locked"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="38" type="Entity">
  <properties>
   <property name="Core.Class" value="Patroller"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Default"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="Blue"/>
  </properties>
 </tile>
 <tile id="39" type="Entity">
  <properties>
   <property name="Core.Class" value="Scientist"/>
   <property name="Core.Frame" value="1"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="YoungFemale"/>
  </properties>
 </tile>
 <tile id="54" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Open,-Locked,-Destroyed"/>
   <property name="Core.State" value="Open"/>
   <property name="Core.Variant" value="2"/>
  </properties>
 </tile>
 <tile id="56" type="Entity">
  <properties>
   <property name="Core.Class" value="ElitePatroller"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Default"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="Red"/>
  </properties>
 </tile>
 <tile id="72" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="-Open,-Locked,-Destroyed"/>
   <property name="Core.State" value="Unlocked"/>
   <property name="Core.Variant" value="2"/>
  </properties>
 </tile>
 <tile id="73" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="2"/>
   <property name="Core.InitialStates" value="-Open,-Locked,-Destroyed"/>
   <property name="Core.State" value="Unlocked"/>
   <property name="Core.Variant" value="2"/>
  </properties>
 </tile>
 <tile id="74" type="Entity">
  <properties>
   <property name="Core.Class" value="StealthPatroller"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Default"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="Yellow"/>
  </properties>
 </tile>
 <tile id="90" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="-Open,Locked,-Destroyed"/>
   <property name="Core.State" value="Locked"/>
   <property name="Core.Variant" value="2"/>
  </properties>
 </tile>
 <tile id="91" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="2"/>
   <property name="Core.InitialStates" value="-Open,Locked,-Destroyed"/>
   <property name="Core.State" value="Locked"/>
   <property name="Core.Variant" value="2"/>
  </properties>
 </tile>
 <tile id="92" type="Entity">
  <properties>
   <property name="Core.Class" value="Toilet"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.State" value="Clean"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="108" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Open,-Locked,-Destroyed"/>
   <property name="Core.State" value="Open"/>
   <property name="Core.Variant" value="2"/>
  </properties>
 </tile>
 <tile id="110" type="Entity">
  <properties>
   <property name="Core.Class" value="Toilet"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.State" value="Dirty"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="126" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="-Open,-Locked,-Destroyed"/>
   <property name="Core.State" value="Unlocked"/>
   <property name="Core.Variant" value="3"/>
  </properties>
 </tile>
 <tile id="127" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="2"/>
   <property name="Core.InitialStates" value="-Open,-Locked,-Destroyed"/>
   <property name="Core.State" value="Unlocked"/>
   <property name="Core.Variant" value="3"/>
  </properties>
 </tile>
 <tile id="128" type="Entity">
  <properties>
   <property name="Core.Class" value="Barrel"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="144" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="-Open,Locked,-Destroyed"/>
   <property name="Core.State" value="Locked"/>
   <property name="Core.Variant" value="3"/>
  </properties>
 </tile>
 <tile id="145" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="2"/>
   <property name="Core.InitialStates" value="-Open,Locked,-Destroyed"/>
   <property name="Core.State" value="Locked"/>
   <property name="Core.Variant" value="3"/>
  </properties>
 </tile>
 <tile id="146" type="Entity">
  <properties>
   <property name="Core.Class" value="Sink"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="-On"/>
   <property name="Core.State" value="Empty"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="162" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Open,-Locked,-Destroyed"/>
   <property name="Core.State" value="Open"/>
   <property name="Core.Variant" value="4"/>
  </properties>
 </tile>
 <tile id="164" type="Entity">
  <properties>
   <property name="Core.Class" value="Sink"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="On"/>
   <property name="Core.State" value="Full"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="165" type="Entity">
  <properties>
   <property name="Core.Class" value="Chest"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="-Open,-Locked"/>
   <property name="Core.State" value="Unlocked"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="166" type="Entity">
  <properties>
   <property name="Core.Class" value="Chest"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Open,-Locked"/>
   <property name="Core.State" value="Open"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="180" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="-Open,-Locked,-Destroyed"/>
   <property name="Core.State" value="Unlocked"/>
   <property name="Core.Variant" value="4"/>
  </properties>
 </tile>
 <tile id="181" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="2"/>
   <property name="Core.InitialStates" value="-Open,-Locked,-Destroyed"/>
   <property name="Core.State" value="Unlocked"/>
   <property name="Core.Variant" value="4"/>
  </properties>
 </tile>
 <tile id="198" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="-Open,Locked,-Destroyed"/>
   <property name="Core.State" value="Locked"/>
   <property name="Core.Variant" value="4"/>
  </properties>
 </tile>
 <tile id="199" type="Door">
  <properties>
   <property name="Core.Class" value="Door"/>
   <property name="Core.Frame" type="int" value="2"/>
   <property name="Core.InitialStates" value="-Open,Locked,-Destroyed"/>
   <property name="Core.State" value="Locked"/>
   <property name="Core.Variant" value="4"/>
  </properties>
 </tile>
 <tile id="216" type="Door">
  <properties>
   <property name="Core.Class" value="BulkheadDoor"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value=""/>
   <property name="Core.State" value="Open"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="217" type="Door">
  <properties>
   <property name="Core.Class" value="BulkheadDoor"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value=""/>
   <property name="Core.State" value="Unlocked"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="234" type="Window">
  <properties>
   <property name="Core.Class" value="Window"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Default"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="Horizontal-1"/>
  </properties>
 </tile>
 <tile id="237" type="WallObject">
  <properties>
   <property name="Core.Class" value="SecurityCamera"/>
   <property name="Core.Direction" value="NORTH"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value=""/>
   <property name="Core.State" value="Enabled"/>
   <property name="Core.Variant" value="SecurityCamera-N"/>
  </properties>
 </tile>
 <tile id="238" type="WallObject">
  <properties>
   <property name="Core.Class" value="SecurityCamera"/>
   <property name="Core.Direction" value="EAST"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value=""/>
   <property name="Core.State" value="Enabled"/>
   <property name="Core.Variant" value="SecurityCamera-E"/>
  </properties>
 </tile>
 <tile id="239" type="WallObject">
  <properties>
   <property name="Core.Class" value="SecurityCamera"/>
   <property name="Core.Direction" value="SOUTH"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value=""/>
   <property name="Core.State" value="Enabled"/>
   <property name="Core.Variant" value="SecurityCamera-S"/>
  </properties>
 </tile>
 <tile id="240" type="WallObject">
  <properties>
   <property name="Core.Class" value="SecurityCamera"/>
   <property name="Core.Direction" value="WEST"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value=""/>
   <property name="Core.State" value="Enabled"/>
   <property name="Core.Variant" value="SecurityCamera-W"/>
  </properties>
 </tile>
 <tile id="252" type="Window">
  <properties>
   <property name="Core.Class" value="Window"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Default"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="Vertical-1"/>
  </properties>
 </tile>
 <tile id="254" type="Abstract">
  <properties>
   <property name="Core.Class" value="DoorController"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="Default"/>
  </properties>
 </tile>
 <tile id="255" type="Abstract">
  <properties>
   <property name="Core.Class" value="IntelligenceOrchestrator"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="Default"/>
  </properties>
 </tile>
 <tile id="270" type="WallObject">
  <properties>
   <property name="Core.Class" value="Switch"/>
   <property name="Core.Direction" value="NORTH"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="On"/>
   <property name="Core.State" value="On"/>
   <property name="Core.Variant" value="Switch-N"/>
  </properties>
 </tile>
 <tile id="271" type="WallObject">
  <properties>
   <property name="Core.Class" value="Switch"/>
   <property name="Core.Direction" value="EAST"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="On"/>
   <property name="Core.State" value="On"/>
   <property name="Core.Variant" value="Switch-E"/>
  </properties>
 </tile>
 <tile id="272" type="WallObject">
  <properties>
   <property name="Core.Class" value="Switch"/>
   <property name="Core.Direction" value="SOUTH"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="On"/>
   <property name="Core.State" value="On"/>
   <property name="Core.Variant" value="Switch-S"/>
  </properties>
 </tile>
 <tile id="273" type="WallObject">
  <properties>
   <property name="Core.Class" value="Switch"/>
   <property name="Core.Direction" value="WEST"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="On"/>
   <property name="Core.State" value="On"/>
   <property name="Core.Variant" value="Switch-W"/>
  </properties>
 </tile>
 <tile id="274" type="Item">
  <properties>
   <property name="Core.Class" value="Goblet"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="275" type="Item">
  <properties>
   <property name="Core.Class" value="Loot_Gold"/>
   <property name="Core.Frame" value="1"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="276" type="Item">
  <properties>
   <property name="Core.Class" value="Armour"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="Basic"/>
  </properties>
 </tile>
 <tile id="288" type="WallObject">
  <properties>
   <property name="Core.Class" value="Switch"/>
   <property name="Core.Direction" value="NORTH"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Off"/>
   <property name="Core.State" value="Off"/>
   <property name="Core.Variant" value="Switch-N"/>
  </properties>
 </tile>
 <tile id="289" type="WallObject">
  <properties>
   <property name="Core.Class" value="Switch"/>
   <property name="Core.Direction" value="EAST"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Off"/>
   <property name="Core.State" value="Off"/>
   <property name="Core.Variant" value="Switch-E"/>
  </properties>
 </tile>
 <tile id="290" type="WallObject">
  <properties>
   <property name="Core.Class" value="Switch"/>
   <property name="Core.Direction" value="SOUTH"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Off"/>
   <property name="Core.State" value="Off"/>
   <property name="Core.Variant" value="Switch-S"/>
  </properties>
 </tile>
 <tile id="291" type="WallObject">
  <properties>
   <property name="Core.Class" value="Switch"/>
   <property name="Core.Direction" value="WEST"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="Off"/>
   <property name="Core.State" value="Off"/>
   <property name="Core.Variant" value="Switch-W"/>
  </properties>
 </tile>
 <tile id="294" type="Item">
  <properties>
   <property name="Core.Class" value="Armour"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="Advanced"/>
  </properties>
 </tile>
 <tile id="296" type="Item">
  <properties>
   <property name="Core.Class" value="MetalKey"/>
   <property name="Core.Frame" value="1"/>
   <property name="Core.InitialStates" value="Default"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
 <tile id="306" type="WallObject">
  <properties>
   <property name="Core.Class" value="Terminal"/>
   <property name="Core.Direction" value="NORTH"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="On"/>
   <property name="Core.State" value="On"/>
   <property name="Core.Variant" value="Terminal-N"/>
  </properties>
 </tile>
 <tile id="307" type="WallObject">
  <properties>
   <property name="Core.Class" value="Terminal"/>
   <property name="Core.Direction" value="EAST"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="On"/>
   <property name="Core.State" value="On"/>
   <property name="Core.Variant" value="Terminal-E"/>
  </properties>
 </tile>
 <tile id="308" type="WallObject">
  <properties>
   <property name="Core.Class" value="Terminal"/>
   <property name="Core.Direction" value="SOUTH"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="On"/>
   <property name="Core.State" value="On"/>
   <property name="Core.Variant" value="Terminal-S"/>
  </properties>
 </tile>
 <tile id="309" type="WallObject">
  <properties>
   <property name="Core.Class" value="Terminal"/>
   <property name="Core.Direction" value="WEST"/>
   <property name="Core.Frame" type="int" value="1"/>
   <property name="Core.InitialStates" value="On"/>
   <property name="Core.State" value="On"/>
   <property name="Core.Variant" value="Terminal-W"/>
  </properties>
 </tile>
 <tile id="319" type="Item">
  <properties>
   <property name="Core.Class" value="Datacube"/>
   <property name="Core.Frame" value="1"/>
   <property name="Core.InitialStates" value="Default"/>
   <property name="Core.State" value="Default"/>
   <property name="Core.Variant" value="1"/>
  </properties>
 </tile>
</tileset>
