"""Factions."""

#
# Imports
#
from enum import IntFlag, auto

from engine.defines import FactionRelation


#
# Faction
#
class Faction(IntFlag):
    PLAYERS = auto()
    HOSTILES = auto()


#
# factionRelations
#
factionRelations = {
    Faction.PLAYERS: {
        Faction.HOSTILES: FactionRelation.HOSTILE
    },
    Faction.HOSTILES: {
        Faction.PLAYERS: FactionRelation.HOSTILE,
    }
}


#
# get_faction_relationship()
#
def get_faction_relationship(ent1, ent2):
    for faction1 in ent1.factions:
        for faction2 in ent2.factions:
            if faction1 == faction2:
                continue

            if factionRelations[faction1][faction2] == FactionRelation.HOSTILE:
                return FactionRelation.HOSTILE

    return FactionRelation.FRIENDLY
