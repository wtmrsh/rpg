from engine.settings import GameSettings
import games.example.states.play.state as playstate

GameSettings.windowWidth = 82
GameSettings.windowHeight = 61

GameSettings.programName = 'Example'
GameSettings.programVersion = 0.1


def get_initial_state():
    return playstate.State


def get_user_function_module(moduletype):
    mt = moduletype.lower()
    if mt == 'facilitators':
        return 'facilitators'
    elif mt == 'prerequisites':
        return 'prerequisites'
    elif mt == 'aptitudes':
        return 'aptitudes'
    elif mt == 'factions':
        return 'faction'
    elif mt == 'dynamictrans':
        return 'dynamictrans'
    elif mt == 'intelligencetasks':
        return 'intelligencetasks'
    else:
        raise Exception(f"Unknown module type '{moduletype}'.")
