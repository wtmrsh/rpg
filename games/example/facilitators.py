"""Facilitators"""

# Naming convention:
# exec_<action>_[actor|EntityType]_[target|EntityType]

# Facilitators:
# Actor
# Target
# World state

# Actor and Target should have functions associated with them that are called automatically, ie before
# the actual try function is, to see if we can 'early out', eg if actor doing Attack has no ammunition.
# To do this, a class can implement a method and give it a decorator to register it.  Eg RangedWeapon could
# have an Ammo class, and then have a method which checks that Ammo instance.  This way we can set Ammo in
# subclasses in constructor and allow values passed in by config.

from engine.defines import FacilitatorResult


def exec_setintelligencetask_actor_target(actor, target, taskname, parameters):
    target.intelligence.set_curtask(taskname, parameters)
    return FacilitatorResult(success=True, actionperformed=True, playermsg=None, othermsg=None)


def exec_open_map_target(map, target):
    target.set_state('Open')
    return FacilitatorResult(success=True, actionperformed=True, playermsg=None, othermsg=None)


def exec_unlock_map_target(map, target):
    target.set_state('Unlocked')
    return FacilitatorResult(success=True, actionperformed=True, playermsg=None, othermsg=None)


def exec_move_actor_map(actor, map):
    return FacilitatorResult(success=True, actionperformed=True, playermsg=None, othermsg=None)


def exec_wait_actor_map(actor, map):
    return FacilitatorResult(success=True, actionperformed=True, playermsg=None, othermsg=None)


def exec_get_actor_target(actor, target, tilex, tiley, count, **kwargs):
    tile = actor.map.tiles[tilex][tiley]
    added = tile.inventory.transfer_entity(target, actor.inventory, count)
    youmsg = f'You pick up {target.def_name_plural(count)}.'
    othermsg = f'{actor.def_name()} picks up {target.indef_name_plural(count)}'
    return FacilitatorResult(success=True, actionperformed=True, playermsg=youmsg, othermsg=othermsg)


def exec_drop_actor_target(actor, target, tilex, tiley, count, **kwargs):
    tile = actor.map.tiles[tilex][tiley]
    dropped = actor.inventory.transfer_entity(target, tile.inventory, count)
    youmsg = f'You drop {target.def_name_plural(count)}.'
    othermsg = f'{actor.def_name()} drops {target.indef_name_plural(count)}'
    return FacilitatorResult(success=True, actionperformed=True, playermsg=youmsg, othermsg=othermsg)


def exec_use_actor_target(actor, target):
    # This is slightly different from other actions, as it has no prerequisites,
    # other than possibly needing some particular body part, but it actually
    # spawns other actions, which will have prerequisites.
    return target.useable.use(actor)


def exec_modifylogicstate_actor_target(actor, target, op, state, value):
    op = op.lower()
    if op == 'set':
        res = target.logicState.set_state(actor, state, value)
    elif op == 'add':
        res = target.logicState.add_state(actor, state, value)
    elif op == 'and':
        res = target.logicState.and_state(actor, state, value)
    elif op == 'or':
        res = target.logicState.or_state(actor, state, value)
    elif op == 'xor':
        res = target.logicState.xor_state(actor, state, value)
    elif op == 'not':
        res = target.logicState.not_state(actor, state, value)
    else:
        raise Exception('Unknown operator: ' + op)

    return FacilitatorResult(success=res[0], actionperformed=True, playermsg=res[1], othermsg=res[2])


def exec_open_actor_target(actor, target):
    target.set_state('Open')
    if target.has_trait('Inventory'):
        if len(target.inventory) == 0:
            youmsg = f'You open {target.def_name()}.  It is empty.'
            othermsg = f'{actor.def_name()} opens {target.indef_name()}.'
        else:
            youmsg = f'You open {target.def_name()}.'
            othermsg = f'{actor.def_name()} opens {target.indef_name()}.'
    else:
        youmsg = f'You open {target.def_name()}.'
        othermsg = f'{actor.def_name()} opens {target.indef_name()}.'

    return FacilitatorResult(success=True, actionperformed=True, playermsg=youmsg, othermsg=othermsg)


def exec_close_actor_target(actor, target):
    target.unset_state('Open')
    youmsg = f'You close {target.def_name()}.'
    othermsg = f'{actor.def_name()} closes {target.indef_name()}.'

    return FacilitatorResult(success=True, actionperformed=True, playermsg=youmsg, othermsg=othermsg)


def exec_lock_actor_target(actor, target):
    # TODO: check key codes
    target.set_state('Locked')

    # TODO: handle messages for locked/unlocked
    youmsg = f'You lock {target.def_name()}.'
    othermsg = f'{actor.def_name()} locks {target.indef_name()}'
    return FacilitatorResult(success=True, actionperformed=True, playermsg=youmsg, othermsg=othermsg)


def exec_unlock_actor_target(actor, target):
    # TODO: check key codes
    target.unset_state('Locked')

    # TODO: handle messages for locked/unlocked
    youmsg = f'You unlock {target.def_name()}.'
    othermsg = f'{actor.def_name()} unlocks {target.indef_name()}'
    return FacilitatorResult(success=True, actionperformed=True, playermsg=youmsg, othermsg=othermsg)


def exec_pick_actor_target(actor, target):
    if target.pickable.turnsRequired is None:
        youmsg = f'The lock of {target.def_name()} cannot be picked.'
        othermsg = f'{actor.def_name()} tries and fails to pick the lock of {target.indef_name()}.'
        return FacilitatorResult(success=False, actionperformed=False, playermsg=youmsg, othermsg=othermsg)

    target.pickable.pick(actor)

    turnsrequired = target.pickable.turnsRequired
    turnsstr = 'turn' if turnsrequired == 1 else 'turns'

    if turnsrequired == 0:
        youmsg = f'The successfully pick the lock of {target.def_name()}.'
        othermsg = f'{actor.def_name()} successfully picks the lock of {target.indef_name()}.'
    else:
        youmsg = f'You try to pick the lock of {target.def_name()} ({turnsrequired} more {turnsstr} required).'
        othermsg = f'{actor.def_name()} tries to pick the lock of {target.indef_name()}.'

    return FacilitatorResult(success=True, actionperformed=True, playermsg=youmsg, othermsg=othermsg)


def exec_enabletrait_actor_target(actor, target, traitname, enabled):
    trait = target.get_trait(traitname)
    if trait:
        trait.enabled = enabled
        return FacilitatorResult(success=True, actionperformed=True, playermsg=None, othermsg=None)
    else:
        raise Exception(f"Entity: {target.name} does not have trait '{trait}'")


def exec_destroy_actor_target(actor, target):
    target.destroyable.destroy(actor, False)

    youmsg = f'You destroy {target.def_name()}.'
    othermsg = f'{actor.def_name()} destroys {target.indef_name()}.'

    return FacilitatorResult(success=True, actionperformed=True, playermsg=youmsg, othermsg=othermsg)
