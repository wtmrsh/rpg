import engine.gamemodule
import engine.tiledefinitions
from engine.defines import FactionRelation
from engine.views.mapview import MapViewPlugin


class HostileTileVisibleMapViewPlugin(MapViewPlugin):

    def __init__(self, map):
        super().__init__()
        self.enabled = True
        self.showtypes = 2  # Bitfield, player=4, hostiles=2, nonhostiles=1
        self.tilesseenbyvisionentities = None
        self.vistohostileoffset = engine.tiledefinitions.definitions['VisibleToHostile']['_']['_'][0].offset

        factionsmodule = engine.gamemodule.get_game_factions_module()
        self.get_faction_relationship = getattr(factionsmodule, 'get_faction_relationship')

    def show_entity_types(self, player=False, hostiles=True, nonhostiles=False):
        self.showtypes ^= (-int(player) ^ self.showtypes) & 4
        self.showtypes ^= (-int(hostiles) ^ self.showtypes) & 2
        self.showtypes ^= (-int(nonhostiles) ^ self.showtypes) & 1

    def enable(self, enabled):
        self.enabled = enabled

    def prepare(self, mapview):
        if not self.enabled:
            return

        self.tilesseenbyvisionentities = set()
        for entity in mapview.visionentities:
            vision = entity.get_vision()
            if vision:
                # Filter based on the type of entity/relationship with viewing entity
                isviewent = entity == mapview.entity
                if isviewent:
                    if self.showtypes & 4 == 0:
                        continue
                else:
                    hostile = self.get_faction_relationship(entity, mapview.entity) == FactionRelation.HOSTILE
                    if hostile and (self.showtypes & 2 == 0):
                        continue
                    if not hostile and (self.showtypes & 1 == 0):
                        continue

                vision.generate_fov_map(wallsvisible=isviewent, calculatevisibleentities=True)
                self.tilesseenbyvisionentities |= vision.visible_tiles

    def get_tile(self, tile):
        if not self.enabled:
            return None
        else:
            return self.vistohostileoffset if tile in self.tilesseenbyvisionentities else None
