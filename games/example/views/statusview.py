from bearlibterminal import terminal

from collections import Counter

import engine.view
import engine.observable as obs
from engine.settings import GraphicsSettings
from engine.colour import Colour


class StatusView(engine.view.View):
    """View for showing current game info."""

    def __init__(self, viewmanager):
        super().__init__('Status', 2, viewmanager)
        self.model = None
        self.currentTurn = 0
        self.inventory = None
        self.register_events()

    def register_events(self):
        """Register events."""
        obs.on(None, 'Inventory.ItemAdded', self.on_entity_new_item)
        obs.on(None, 'Game.TurnTaken', self.on_turn_taken)

    def unregister_events(self):
        """Unregister events."""
        obs.off(None, 'Inventory.ItemAdded', self.on_entity_new_item)
        obs.off(None, 'Game.TurnTaken', self.on_turn_taken)

    def set_model(self, model):
        self.model = model
        self.inventory = model.player.inventory

        self.set_dirty(self)

    def on_turn_taken(self, source, turn):
        self.currentTurn = turn
        obs.trigger(self, 'UI.RefreshView', self)

    def on_entity_new_item(self, source, inventory, item, count):
        """
        Callback for when an Entity picks something up

        :param inventory: inventory added to
        :param item: item added
        :param count: number of the item added
        """
        if self.model:
            if inventory == self.model.player.inventory:
                obs.trigger(self, 'UI.RefreshView', self)

    def render(self):
        """Render status view."""
        terminal.layer(self.base_layer + 1)
        terminal.color(Colour.from_name('white').to_bgra())

        xpos = self.client_x + 1
        ypos = self.client_y + 1

        terminal.print(xpos, ypos, f'Turn: {self.currentTurn}')

        if self.inventory:
            ypos += 2
            for item in self.inventory:
                if item[1] == 1:
                    terminal.print(xpos, ypos, item[0].name)
                else:
                    terminal.print(xpos, ypos, f'{item[0].name} x{item[1]}')
                ypos += 1
