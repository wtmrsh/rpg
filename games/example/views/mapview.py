from bearlibterminal import terminal
import engine.views.mapview
from engine.cursor import Cursor


class MapView(engine.views.mapview.MapView):

    def __init__(self, viewmanager, keepentitycentred):
        super().__init__(viewmanager, keepentitycentred)
