"""
Test MoveIntelligenceTask.

"""
import engine.observable as obs

import engine.mapobjects as mapobjects
import games.example.maps.shared as shared


#
# The Map.Enter event is triggered as the very last step of loading the map.
# At this point it can be assumed that everything is fully set up.
#
@mapobjects.event_handler('Map.Enter')
def on_map_enter(source, map, player, model):
    shared.this_map = map
    shared.this_player = player
    shared.this_model = model

    obs.trigger(None, 'Game.Message', map.properties['ShortDescription'], player)


@mapobjects.event_handler('Entity.Intelligence.TaskComplete')
def task_completed(entity, task, message):
    obs.trigger(None, 'Game.Message', message, shared.this_player)
