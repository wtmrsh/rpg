import engine.entityfactories
import engine.mapobjects as mapobjects
from engine.exceptions import ExitStateException

#
# We capture the map and player for usage in other functions in the script.
# The model gives us access to world state, ie at a higher level than the map.
#
this_map = None
this_player = None
this_model = None


#
# The Map.Exit event is triggered when Map.exit() is called.  This can be called from various
# places.  The handler is required to raise one of the following exceptions:
# - TransitionToStateException
# - SpawnChildStateException
# - ExitStateException
# - ExitException
#
@mapobjects.event_handler('Map.Exit')
def on_map_exit(map, player):
    raise ExitStateException(None)


#
# Catch our exit trigger in the map.  This calls Map.exit()
# which in turn will fire the 'Map.Exit' event.  We may want to check if we are ready to exit the
# map at this point - maybe we haven't fulfilled the map's conditions - and this can be done here.
# We also unset the globals for safety's sake.
#
@mapobjects.event_handler('Trigger.Exit')
def on_exit_triggered(source):
    global this_map, this_player, this_model

    tmap = this_map
    this_map = None
    this_player = None
    this_model = None

    tmap.exit()


#
# Helper to create an entity and add it to an inventory
#
def create_entity_in_inventory(entityclass, variant, status, targetent):
    # TODO: rework to work as expected
    ent = engine.entityfactories.create_entity(entityclass,
                                               entityname=None,
                                               visualvariant=variant,
                                               visualframe=0,
                                               overrides=dict())

    targetent.inventory.add_entity(ent, 1)
    this_map.add_entity(ent, [status], False, list(), list())
    return ent
