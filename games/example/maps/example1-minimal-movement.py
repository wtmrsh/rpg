"""
Example 1.

This map is intended to demonstrate minimal functionality.  You control an entity of type 'Player', which is
derived from a basic human archetype.
"""
import engine.observable as obs
import engine.mapobjects as mapobjects


#
# The Map.Enter event is triggered as the very last step of loading the map.
# At this point it can be assumed that everything is fully set up.
#
@mapobjects.event_handler('Map.Enter')
def on_map_enter(source, map, player, model):
    obs.trigger(None, 'Game.Message', map.properties['ShortDescription'], player)
