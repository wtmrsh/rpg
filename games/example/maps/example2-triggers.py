"""
Example 2.

This map demonstrates triggers.
"""
import engine.observable as obs
import engine.mapobjects as mapobjects
from engine.exceptions import ExitStateException

#
# We capture the map and player for usage in other functions in the script.
# The model gives us access to world state, ie at a higher level than the map.
#
this_map = None
this_player = None
this_model = None


#
# The Map.Enter event is triggered as the very last step of loading the map.
# At this point it can be assumed that everything is fully set up.
#
@mapobjects.event_handler('Map.Enter')
def on_map_enter(source, map, player, model):
    global this_map, this_player, this_model
    this_map = map
    this_player = player
    this_model = model
    obs.trigger(None, 'Game.Message', map.properties['ShortDescription'], player)


#
# The Map.Exit event is triggered when Map.exit() is called.  This can be called from various
# places.  The handler is required to raise one of the following exceptions:
# - TransitionToStateException
# - SpawnChildStateException
# - ExitStateException
# - ExitException
#
@mapobjects.event_handler('Map.Exit')
def on_map_exit(map, player):
    raise ExitStateException(None)


#
# Catch our exit trigger in the map.  This calls Map.exit()
# which in turn will fire the 'Map.Exit' event.  We may want to check if we are ready to exit the
# map at this point - maybe we haven't fulfilled the map's conditions - and this can be done here.
#
@mapobjects.event_handler('Trigger.Exit')
def on_exit_triggered(source):
    this_map.exit()
