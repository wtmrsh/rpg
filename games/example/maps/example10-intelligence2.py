"""
Example 10.

This map demonstrates Intelligence functionality.
"""
import engine.observable as obs
import engine.mapobjects as mapobjects
import games.example.maps.shared as shared


#
# The Map.Enter event is triggered as the very last step of loading the map.
# At this point it can be assumed that everything is fully set up.
#
@mapobjects.event_handler('Map.Enter')
def on_map_enter(source, map, player, model):
    shared.this_map = map
    shared.this_player = player
    shared.this_model = model

    # Get chest and add some random objects to it.
    chest = map.get_named_entity('TestChest')
    shared.create_entity_in_inventory('Datacube', '1', 'Default', chest)

    obs.trigger(None, 'Game.Message', map.properties['ShortDescription'], player)


#
# When we open the chest, transfer all contents to the player.
# This gets registered before the player is set, so we cannot count on the 'source' argument being the player,
# so we have to explicitly check.  We could also check the 'actor' argument as well.
#
@mapobjects.event_handler('Entity.Actor.Open')
def check_chest_opened(source, action, actor, target):
    if source == shared.this_player and target.name == 'TestChest':
        target.inventory.transfer_all_entities(actor.inventory)
        obs.trigger(None, 'Game.Message', 'You grab the contents of the chest', shared.this_player)

