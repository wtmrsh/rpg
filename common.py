#
# Global settings
#
ENGINE_INI_FILE         = 'rpg.ini'
GAMES_DIR               = 'games'
CELL_SIZE               = 12
FPS_LIMIT               = 20

ENABLE_VSYNC            = True
TAB_WIDTH               = 4

#
# Logging
#
BLT_LOGFILE             = 'blt.log'
BLT_LOGLEVEL            = 'warning' # 'none' | 'fatal' | 'error' | 'warning' | 'info' | 'debug' | 'trace'

#
# Entity settings
#
MAX_ENTITY_ACTOR_HISTORY = 5
MAX_ENTITY_TARGET_HISTORY = 5
MAX_ENTITY_NAME_LENGTH  = 16
MAX_ENTITY_WEIGHT       = 999
MAX_ENTITIES_PER_STACK  = 999
MAX_STACKABLE_ENTITY_TYPES = 52

#
# Tile offsets
#
TILE_CODEPAGE           = 0xE100

#
# Rendering
#
LIGHT_UPDATE_FREQUENCY  = 0.1

#
# Weight
#
WEIGHT_IN_KILOS         = 10.0

#
# Debug message
#
DEBUG_MSG               = '[YOU SHOULD NOT BE SEEING THIS]'
