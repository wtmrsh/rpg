"""
Base entity properties.
"""

#
# Imports
#
import engine.entityproperties
from engine.entity import EntityStateFilter
from engine.gamemodule import get_game_factions_module


#
# TiledNamesEntityProperty
#
class TiledNamesEntityProperty(engine.entityproperties.NamesEntityProperty):
    def __init__(self):
        super().__init__()

    def convert_to_type(self, value):
        return [x.strip() for x in value.split(',')]

    def validate_definition_type(self, value):
        return type(value) is str


#
# TiledCategoryEntityProperty
#
class TiledCategoryEntityProperty(engine.entityproperties.CategoryEntityProperty):
    def __init__(self):
        super().__init__()

    def validate_definition_type(self, value):
        return type(value) is str


#
# TiledColourEntityProperty
#
class TiledColourEntityProperty(engine.entityproperties.ColourEntityProperty):
    def __init__(self):
        super().__init__()


#
# TiledInventoryTypeEntityProperty
#
class TiledInventoryTypeEntityProperty(engine.entityproperties.InventoryTypeEntityProperty):
    def __init__(self):
        super().__init__()


#
# TiledFactionsEntityProperty
#
class TiledFactionsEntityProperty(engine.entityproperties.FactionsEntityProperty):
    def __init__(self):
        super().__init__()

    def validate_definition_type(self, value):
        return type(value) is str

    def convert_to_type(self, value):
        factionEnum = getattr(get_game_factions_module(), 'Faction')
        factions = [x.strip() for x in value.split(',')]
        return {factionEnum[f] for f in factions}


#
# Factory
#
tiledEntityPropertyFactory = {
    'names': TiledNamesEntityProperty,
    'category': TiledCategoryEntityProperty,
    'colour': TiledColourEntityProperty,
    'inventory-type': TiledInventoryTypeEntityProperty,
    'factions': TiledFactionsEntityProperty
}
