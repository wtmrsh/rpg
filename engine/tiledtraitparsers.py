"""
Trait system: TraitParsers for Tiled.
"""

import json

import engine.gamemodule
from engine.traitparser import TraitParser


class TiledCollectiveTraitParser(TraitParser):
    @staticmethod
    def parse_argument(argname, argvalue):
        if argname == 'count':
            return int(argvalue)
        elif argname in ['name', 'namePlural']:
            return argvalue
        else:
            raise ValueError(f'{argname} should be one of count|name|namePlural')


class TiledDestroyableTraitParser(TraitParser):
    @staticmethod
    def parse_argument(argname, argvalue):
        if argname in ['startdestroyed']:
            if type(argvalue) != bool:
                raise TypeError(f"'{argname}' parameter should be a boolean.")
        elif argname in ['blocksmovement', 'blockspathing', 'blocksvision', 'blocksaudio']:
            if type(argvalue) != dict:
                raise TypeError(f"'{argname}' parameter should be a dictionary.")
            for k, v in argvalue:
                if type(k) != str:
                    raise TypeError(f"Keys in '{argname}' should be strings.")
                if type(v) != bool:
                    raise TypeError(f"Values in '{argname}' should be booleans.")
        else:
            raise ValueError(f'{argname} should be one of blocksmovement|blockspathing|blocksvision|blocksaudio|startdestroyed')


class TiledIntelligenceTraitParser(TraitParser):

    @staticmethod
    def validate_subtask(subtask):
        pass

    @staticmethod
    def validate_task(taskdef):
        dtransmodule = engine.gamemodule.get_game_dynamic_trans_module()

        # A definition is made up of a list of tasks, a count, and transitions
        tasks = taskdef.get('Tasks', list())
        if len(tasks) == 0:
            raise ValueError("Task's 'Tasks' entry is either empty or has not been specified")

        for task in tasks:
            TiledIntelligenceTraitParser.validate_subtask(task)

        # Count
        loopcount = int(taskdef.get('LoopCount', -1))
        if loopcount == 0:
            raise ValueError("LoopCount should be non-zero")

        # Special error case to look out for: cyclic task-list with one move task
        if loopcount != 1 and len(tasks) == 1 and tasks[0]['Type'] == 'Move':
            raise ValueError("Cyclic task lists with one move task are not allowed.  LoopCount should be 1.")

        # Transitions
        if 'Transitions' in taskdef:
            statictrans = taskdef['Transitions'].get('Static', dict())
            definedcallbacks = ['OnFinished']
            for trancallback, trantarget in statictrans.items():
                if trancallback not in definedcallbacks:
                    raise ValueError('Static transition should be one of %s' % '|'.join(definedcallbacks))

            dynamictrans = taskdef['Transitions'].get('Dynamic', list())
            for transdef in dynamictrans:
                function = transdef['Function']
                f = getattr(dtransmodule, function, None)
                if not f:
                    raise ValueError(f"Dynamic transition function '{function}' not registered.")

    @staticmethod
    def validate_tasks(tasks):
        for taskname, taskdef in tasks.items():
            TiledIntelligenceTraitParser.validate_task(taskdef)

    @staticmethod
    def parse_argument(argname, argvalue):
        if argname == 'tasks':
            tasksdef = json.loads(argvalue)

            initialtask = tasksdef.get('InitialTask', None)
            if not initialtask:
                raise ValueError("Top-level 'InitialTask' entry not specified")

            tasks = tasksdef.get('Tasks')
            if not tasks:
                raise ValueError("Top-level 'Tasks' entry not specified")
            elif len(tasks) == 0:
                raise ValueError("Top-level 'Tasks' entry is empty")
            else:
                TiledIntelligenceTraitParser.validate_tasks(tasks)

            inittaskname = initialtask.get('Task')
            if inittaskname:
                if inittaskname not in tasks:
                    raise ValueError(f"No initial task named '{inittaskname}' specified")
            else:
                raise ValueError(f"No initial task specified")

            return tasksdef
        elif argname == 'taskhandlers':
            taskhandlersdef = json.loads(argvalue)

            if type(taskhandlersdef) != dict:
                raise TypeError("'taskhandlers' parameter should be a dictionary")

            for key, value in taskhandlersdef.items():
                if type(key) is not str:
                    raise TypeError(f"'taskhandlers' key {key} should be a string")
                if type(value) is not str:
                    raise TypeError(f"'taskhandlers' value {value} should be a string")

            return taskhandlersdef
        else:
            raise ValueError(f'{argname} should be one of tasks|taskhandlers')


class TiledInventoryTraitParser(TraitParser):
    @staticmethod
    def parse_argument(argname, argvalue):
        if argname == 'capacity':
            return int(argvalue)
        elif argname == 'searchable':
            return True if argvalue else False
        else:
            raise ValueError(f'{argname} should be one of capacity|searchable')


class TiledKeyTraitParser(TraitParser):
    @staticmethod
    def parse_argument(argname, argvalue):
        if argname == 'codes':
            return [int(x.strip()) for x in argvalue.split(',')]
        else:
            raise ValueError(f'{argname} should be codes')


class TiledLockableTraitParser(TraitParser):
    @staticmethod
    def parse_argument(argname, argvalue):
        if argname == 'codes':
            return [int(x.strip()) for x in argvalue.split(',')]
        else:
            raise ValueError(f'{argname} should be codes')


class TiledHearingTraitParser(TraitParser):
    @staticmethod
    def parse_argument(argname, argvalue):
        return argvalue


class TiledLogicStateTraitParser(TraitParser):
    @staticmethod
    def parse_argument(argname, argvalue):
        raise ValueError(f'takes no parameters')


class TiledOpenableTraitParser(TraitParser):
    @staticmethod
    def parse_argument(argname, argvalue):
        if argname in ['startopen', 'autoclose']:
            if type(argvalue) != bool:
                raise TypeError(f"'{argname}' parameter should be a boolean.")
        elif argname in ['blocksmovement', 'blockspathing', 'blocksvision', 'blocksaudio']:
            if type(argvalue) != dict:
                raise TypeError(f"'{argname}' parameter should be a dictionary.")
            for k, v in argvalue:
                if type(k) != str:
                    raise TypeError(f"Keys in '{argname}' should be strings.")
                if type(v) != bool:
                    raise TypeError(f"Values in '{argname}' should be booleans.")
        else:
            raise ValueError(f'{argname} should be one of blocksmovement|blockspathing|blocksvision|blocksaudio|startopen|autoclose')


class TiledUseableTraitParser(TraitParser):
    @staticmethod
    def parse_argument(argname, argvalue):
        if argname in ['suppressmsg', 'playerusemsg', 'playerfailmsg', 'otherusemsg', 'otherfailmsg', 'numusersneeded']:
            return argvalue
        elif argname in ['allowedusernames', 'visualcycle']:
            return [x.strip() for x in argvalue.split(',')]
        elif argname == 'userfilterfunc':
            return eval('lambda entity: ' + argvalue)
        else:
            raise ValueError(f'{argname} should be one of suppressmsg|playerusemsg|playerfailmsg|otherusemsg|otherfailmsg|numusersneeded|allowedusernames|userfilterfunc|visualcycle')


class TiledVisionTraitParser(TraitParser):
    @staticmethod
    def parse_argument(argname, argvalue):
        if argname == 'fov':
            return float(argvalue)
        elif argname == 'viewdistance':
            return int(argvalue)
        elif argname == 'mask':
            args = argvalue.split(',')
            return args[0], int(args[1]), int(args[2])
        else:
            raise ValueError(f'{argname} should be one of fov|viewdistance|mask')


class TiledWearableTraitParser(TraitParser):
    @staticmethod
    def parse_argument(argname, argvalue):
        raise ValueError(f'takes no parameters')


#
# Factory
#
tiledTraitParserFactory = {
    'Collective': TiledCollectiveTraitParser,
    'Destroyable': TiledDestroyableTraitParser,
    'Intelligence': TiledIntelligenceTraitParser,
    'Inventory': TiledInventoryTraitParser,
    'Key': TiledKeyTraitParser,
    'Hearing': TiledHearingTraitParser,
    'Lockable': TiledLockableTraitParser,
    'LogicState': TiledLogicStateTraitParser,
    'Openable': TiledOpenableTraitParser,
    'Useable': TiledUseableTraitParser,
    'Vision': TiledVisionTraitParser,
    'Wearable': TiledWearableTraitParser
}