from collections import namedtuple

from engine.mapdecorator import owned_by_map

EntityTransitionDefinition = namedtuple('TransitionDefinition', 'entity,actions,exit,turn')


@owned_by_map
class Transition:

    def __init__(self,  owner, entity, accesses):
        self.owner = owner
        self.entity = entity
        self.accesses = accesses
        self.entities = dict()

    def get_exit(self, position):
        if position == self.accesses[0]:
            return self.accesses[1]
        elif position == self.accesses[1]:
            return self.accesses[0]
        else:
            return None

    def register_entity_action(self, entity, action, turn):
        if entity in self.entities:
            self.entities[entity].actions.add(action)
        else:
            exit = self.get_exit((entity.x, entity.y))
            etd = EntityTransitionDefinition(entity=entity, actions={action}, exit=exit, turn=turn)
            self.entities[entity] = etd

    def unregister_entity(self, entity):
        if entity in self.entities:
            self.entities.pop(entity)

    def get_other_users(self, entity):
        return [x for x in self.entities if x != entity]
