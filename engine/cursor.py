"""Mouse cursor."""

from collections import namedtuple

#
# Cursor
#
class Cursor:
    """Store for cursor information"""

    #
    # Definition
    #
    class Definition:

        Entry = namedtuple('Entry', 'dx,dy,offset')

        #
        # __init__()
        #
        def __init__(self, imageSet):
            self.imageSet = imageSet
            self.cursors = {}

        #
        # add_cursor()
        #
        def add_cursor(self, name, dx, dy):
            self.cursors[name] = Cursor.Definition.Entry(dx, dy, self.imageSet.tiles[name].offset + self.imageSet.offset)

    definitions = {}
