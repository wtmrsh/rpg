def owned_by_map(cls):
    def get_map(self):
        obj = self
        while type(obj).__name__ != 'Map':
            if obj is None:
                return None
            obj = obj.owner
        return obj

    prop = property(get_map)
    setattr(cls, 'map', prop)
    return cls
