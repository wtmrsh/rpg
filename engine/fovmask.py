"""
Mask for Entity field of view.

Formatted 17/09
"""

#
# Imports
#
import math

from common import *

from engine.defines import CompassDirection
from engine.exceptions import LogicError


#
# FovMaskCache
#
class FovMaskCache:
    """Cache for FovMasks."""
    
    cache = dict()
    
    #
    # get_mask_from_fov_and_radius()
    #
    @staticmethod
    def get_mask_from_fov_and_radius(fov, viewRadius):
        """
        Get cached FovMask with given field of view and radius.
        
        :param fov: field of view
        :param viewRadius: view distance/radius
        :return: cached FovMask
        """
        try:
            m = FovMaskCache.cache[(fov, viewRadius)]
        except KeyError:
            m = RadialFovMask(fov, viewRadius)
            FovMaskCache.cache[(fov, viewRadius)] = m
        return m

    @staticmethod
    def get_mask_from_file(file, sizeX, sizeY):
        """
        Get cached FovMask using given file.

        :param file: filename
        :param sizeX: x size of mask
        :param sizeY: y size of mask
        :return: cached FovMask
        """
        try:
            m = FovMaskCache.cache[file]
        except KeyError:
            m = MaskFovMask(file, sizeX, sizeY)
            FovMaskCache.cache[file] = m
        return m


#
# FovMask
#
class FovMask:

    #
    # in_mask()
    #
    def in_mask(self, x, y, compassDirection):
        """
        Are we in the mask?

        :param x: x location
        :param y: y location
        :param compassDirection: direction
        :return: whether or not (x, y) is within the mask
        """
        return False

    #
    # get_bounding_box()
    #
    def get_bounding_box(self, compassDirection):
        """
        Return the bounding box.

        :param compassDirection: direction
        :return: box bounds in world coords
        """
        return 0, 0, 0, 0


#
# MaskFovMask
#
class MaskFovMask(FovMask):

    #
    # Mask
    #
    class Mask:

        #
        # __init__()
        #
        def __init__(self, sizeX, sizeY):
            """
            Constructor.

            :param sizeX: x size of mask in pixels
            :param sizeY: y size of mask in pixels
            """
            self.sizeX = sizeX
            self.sizeY = sizeY
            self.originX = 0
            self.originY = 0
            self.buffer = None

        #
        # create()
        #
        def create(self, bytes, fileWidth, fileHeight, offsetX, offsetY):
            """
            Load data into mask buffer.

            :param bytes: data buffer
            :param fileWidth: width of file in pixels
            :param fileHeight: height of file in pixels
            :param offsetX: x position to index into byte array
            :param offsetY: y position to index into byte array
            """
            self.buffer = [[False for y in range(self.sizeY)] for x in range(self.sizeX)]

            for y in range(self.sizeY):
                # Flip to align the image y-axis with the world y-axis
                byteOffset = ((offsetY + ((self.sizeY - 1) - y)) * fileWidth + offsetX) * 3
                for x in range(self.sizeX):
                    r = bytes[byteOffset + 0]
                    g = bytes[byteOffset + 1]
                    b = bytes[byteOffset + 2]

                    if r == 255 and g == 255 and b == 255:
                        # White: in mask
                        self.buffer[x][y] = True
                    elif r == 255 and g == 0 and b == 0:
                        # Red: origin, in mask
                        self.buffer[x][y] = True
                        self.originX = x
                        self.originY = y
                    elif r == 255 and g == 0 and b == 255:
                        # Magenta: origin, not in mask
                        self.buffer[x][y] = False
                        self.originX = x
                        self.originY = y
                    elif r == 0 and g == 0 and b == 0:
                        # Any other colour: not in mask
                        self.buffer[x][y] = False

                    byteOffset += 3

            # Trim empty space and adjust size accordingly, to optimise vision calculations.
            for yStart in range(self.sizeY):
                for x in range(self.sizeX):
                    if self.buffer[x][yStart]:
                        break
                else:
                    continue
                break

            for yEnd in range(self.sizeY - 1, -1, -1):
                for x in range(self.sizeX):
                    if self.buffer[x][yEnd]:
                        break
                else:
                    continue
                break

            for xStart in range(self.sizeX):
                for y in range(self.sizeY):
                    if self.buffer[xStart][y]:
                        break
                else:
                    continue
                break

            for xEnd in range(self.sizeX - 1, -1, -1):
                for y in range(self.sizeY):
                    if self.buffer[xEnd][y]:
                        break
                else:
                    continue
                break

            # Trim x
            self.buffer = self.buffer[xStart:xEnd + 1]
            self.sizeX = (xEnd + 1) - xStart
            self.originX -= xStart

            # Trim y
            for x in range(self.sizeX):
                self.buffer[x] = self.buffer[x][yStart:yEnd + 1]
            self.sizeY = (yEnd + 1) - yStart
            self.originY -= yStart

        #
        # in_mask
        #
        def in_mask(self, x, y):
            """
            Test whether the point is in the mask.

            :param x: x position
            :param y: y position
            :return: whether (x,y) is within the mask
            """
            ox = self.originX + x
            oy = self.originY + y

            if ox < 0 or ox >= self.sizeX or oy < 0 or oy >= self.sizeY:
                return False

            return self.buffer[ox][oy]

    #
    # __init__
    #
    def __init__(self, filename, sizeX, sizeY):
        """
        Constructor.

        :param filename: file to load
        :param sizeX: x size of mask in pixels
        :param sizeY: y size of mask in pixels
        """
        self.masks = [None] * 9

        fileWidth = sizeX * 3
        fileHeight = sizeY * 3

        with open(filename, 'rb') as f:
            bytes = f.read(fileWidth * fileHeight * 3)

        for dir in CompassDirection:
            if dir == CompassDirection.NONE:
                index = 0
                offsetX, offsetY = sizeX, sizeY
            else:
                index = int(math.log(int(dir), 2)) + 1

                # Genius or madness?  It's a thin line.
                offsetX = (round(0 - math.sin(math.radians(-45 * (index - 1)))) + 1) * sizeX
                offsetY = (1 - round(0 + math.cos(math.radians(-45 * (index - 1))))) * sizeY

            mask = MaskFovMask.Mask(sizeX, sizeY)
            mask.create(bytes, fileWidth, fileHeight, offsetX, offsetY)
            self.masks[index] = mask

    #
    # in_mask()
    #
    def in_mask(self, x, y, compassDirection):
        """
        Returns whether location is within the mask, for the given direction.

        :param x: x position
        :param y: y position
        :param compassDirection: CompassDirection
        :return: whether the point is in view/visible
        """
        index = 0 if compassDirection == CompassDirection.NONE else int(math.log(int(compassDirection), 2)) + 1
        mask = self.masks[index]
        return mask.in_mask(x, y)

    #
    # get_bounding_box()
    #
    def get_bounding_box(self, compassDirection):
        """
        Return the bounding box.

        :param compassDirection: direction
        :return: box bounds in world coords
        """
        index = 0 if compassDirection == CompassDirection.NONE else int(math.log(int(compassDirection), 2)) + 1
        mask = self.masks[index]

        return -mask.originX, -mask.originY, mask.sizeX - mask.originX, mask.sizeY - mask.originY


#
# RadialFovMask
#
class RadialFovMask(FovMask):

    #
    # __init__()
    #
    def __init__(self, angle, viewRadius):
        """
        Constructor.

        :param angle: field of view, in degrees
        :param viewRadius: view distance, in world units
        """
        self.viewRadius = viewRadius
        self.angle = angle

    #
    # in_mask()
    #
    def in_mask(self, x, y, compassDirection):
        """
        Returns whether location is within the mask, for the given direction.

        :param x: x position
        :param y: y position
        :compassDirection: CompassDirection
        :return: whether the point is in view/visible
        """
        if x < -self.viewRadius or y < -self.viewRadius or x > self.viewRadius or y > self.viewRadius:
            return False

        if (x == 0 and y == 0) or self.angle == 360:
            return True

        if compassDirection == CompassDirection.NONE:
            raise Exception(f'No compass direction given for entity, which requires one as it has a fovmask.')

        targetAngle = math.degrees(math.atan2(y, x))
        targetAngle = (180 - targetAngle) - 90

        facingAngle = math.log(int(compassDirection), 2) * 45
        angleDiff = (facingAngle - targetAngle + 180 + 360) % 360 - 180

        halfFov = self.angle / 2
        return -halfFov <= angleDiff <= halfFov

    #
    # get_bounding_box()
    #
    def get_bounding_box(self, compassDirection):
        """
        Return the bounding box.

        :param compassDirection: direction
        :return: box bounds in world coords
        """
        return -self.viewRadius, -self.viewRadius, self.viewRadius, self.viewRadius
