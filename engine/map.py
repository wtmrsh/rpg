import math
import importlib.util
import sys
import itertools
from collections import defaultdict

import libtcodpy as libtcod

from common import *

import engine.trait
import engine.traitdecorators
from engine.traits import *
from engine.actionparticipant import ActionParticipant
from engine.defines import FacilitatorResult
from engine.actionresolver import ActionResolver

import engine.action
import engine.utils
import engine.entitydefinitions
import engine.observable as obs
import engine.gamemodule
import engine.mapobjects
import engine.tiledefinitions
import engine.entityfactories
from engine.exceptions import LogicError, TransitionToStateException, SpawnChildStateException, ExitStateException, ExitException
from engine.tile import Tile, TileType
from engine.defines import CompassDirection, EntityCategory
from engine.colour import Colour
from engine.traits.useable import UseTarget


@engine.action.exposes_actor('Open')
@engine.action.exposes_actor('Close')
@engine.action.exposes_target('Move')
class Map(ActionParticipant):
    """Class to hold a single level of the game, and render and update it as appropriate."""

    def __init__(self):
        """
        Constructor.
        """
        super().__init__('Map')
        self.eventidentifier = 0
        self.name = ''                       # name
        self.owner = None                    # Owning object, eg some kind of overarching world structure
        self.sizeX = 0                       # max x-dimension of map
        self.sizeY = 0                       # max y-dimension of map
        self.tiles = None                    # jagged array of tiles
        self.scriptmodulename = None
        self.scriptmodule = None
        self.triggerareas = list()
        self.entrypoint = [0, 0, CompassDirection.NONE]  # starting point for this level
        self.entities = set()
        self.namedentities = dict()
        self.properties = dict()
        self.metaproperties = dict()
        self.globalproperties = dict()
        self.visionmap = None
        self.maxviewdistance = 0
        self.paths = dict()
        self.player = None
        self.doortransitions = dict()

        # Animation lookups
        self.animatedvisuals = set()
        self.changedanimatedvisuals = set()

        # Factions
        factionsmodule = engine.gamemodule.get_game_factions_module()
        self.get_faction_relationship = getattr(factionsmodule, 'get_faction_relationship')

        # For wiring up entities which need other entities
        self.usetargetwiring = dict()
        self.logicstatewiring = dict()

    def __del__(self):
        if self.visionmap:
            libtcod.map_delete(self.visionmap)

    @property
    def map(self):
        return self

    def def_name(self):
        return '<map>'

    def indef_name(self):
        return '<map>'

    def register_events(self):
        """Register events."""
        obs.on(None, 'Entity.SetPlayer', self.on_set_player)

        # Add trigger area events
        for area in self.triggerareas:
            area.register_events()

    def unregister_events(self):
        """Unregister events."""
        obs.off(None, 'Entity.SetPlayer', self.on_set_player)

        # Remove trigger area events
        for area in self.triggerareas:
            area.unregister_events()

    def set_size(self, sizex, sizey):
        """
        Set the map's size and allocate empty tiles.
        
        :param sizex: max x-dimension of map
        :param sizey: max y-dimension of map
        """
        self.sizeX = sizex
        self.sizeY = sizey

        # Tiles
        self.tiles = [[Tile(x, y) for y in range(sizey)] for x in range(sizex)]

        # Vision
        if self.visionmap:
            libtcod.map_delete(self.visionmap)

        self.visionmap = libtcod.map_new(self.sizeX, self.sizeY)

    def set_tile(self, x, y, tile):
        tile.x = x
        tile.y = y
        self.tiles[x][y] = tile

    def indef_name(self, colour=None, getlength=False):
        """
        Get the indefinite name, ie 'a'/'an' object.  Needs to be overridden for silent 'h's and 'y's.

        :param colour: colour to use, or Entity colour if none.
        :param getlength: if True, returns length of name (without formatting)
        :return: formatted string, or plain length
        """
        colourStr = '[color=%i]' % (colour or Colour(255, 255, 255).to_bgra())
        formatStr = '{}{}{}'

        if getlength:
            return len(self.name)
        else:
            return formatStr.format(colourStr, self.name, '[/color]')

    def def_name(self, colour=None, getlength=False):
        """
        Get the map name.

        :param colour: colour to use, or Entity colour if none.
        :param getlength: if True, returns length of name (without formatting)
        :return: formatted string, or plain length
        """
        colourStr = '[color=%i]' % (colour or Colour(255, 255, 255).to_bgra())
        formatStr = '{}{}{}'

        if getlength:
            return len(self.name)
        else:
            return formatStr.format(colourStr, self.name, '[/color]')

    @property
    def is_player(self):
        return False

    def set_script_module(self, modulename):
        self.scriptmodulename = modulename

    def enter(self, player, model):
        self.player = player

        # Load script module
        if self.scriptmodulename:
            if self.scriptmodulename not in sys.modules:
                scriptfilepath = self.scriptmodulename.replace('.', '/') + '.py'
                spec = importlib.util.spec_from_file_location(self.scriptmodulename, scriptfilepath)
                self.scriptmodule = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(self.scriptmodule)

                globals()[self.scriptmodulename] = self.scriptmodule
            else:
                if self.scriptmodule is None:
                    self.scriptmodule = globals()[self.scriptmodulename]

                importlib.reload(self.scriptmodule)

        obs.trigger(self, 'Map.Enter', self, player, model)

    def exit(self):
        try:
            obs.trigger(self, 'Map.Exit', self.player)
        except (TransitionToStateException, SpawnChildStateException, ExitStateException, ExitException) as e:
            raise e
        finally:
            engine.mapobjects.remove_script_objects()

    def call_script_function(self, fn, **kwargs):
        return getattr(self.scriptmodule, fn)(**kwargs)

    def update_vision_map(self, visionchangers=None):
        """Generates a map for libtcod to use, to determine what is visible and what is not."""
        if visionchangers:
            for entity in visionchangers:
                tile = entity.owning_tile
                libtcod.map_set_properties(self.visionmap, tile.x, tile.y, not tile.blocks_vision, True)
        else:
            for y in range(self.sizeY):
                for x in range(self.sizeX):
                    tile = self.tiles[x][y]
                    libtcod.map_set_properties(self.visionmap, x, y, not tile.blocks_vision, True)

    def flush_entity_wiring(self):
        """
        Create wiring for usetargets
        :return:
        """
        for entity, usetargets in self.usetargetwiring.items():
            for usetarget in usetargets:
                entity.useable.add_use_target(self.namedentities[usetarget.name] if usetarget.name else entity,
                                              usetarget.count,
                                              usetarget.passnext,
                                              usetarget.updateonfailure,
                                              usetarget.actions,
                                              usetarget.states)

        self.usetargetwiring.clear()

        for entity, callbacks in self.logicstatewiring.items():
            for callback in callbacks:
                usetargets = [UseTarget(self.namedentities[ut.name],
                                        ut.count,
                                        ut.passnext,
                                        ut.updateonfailure,
                                        ut.actions)
                              for ut in callback.usetargets]
                entity.logicState.register_callback(state=callback.state, verifier=callback.verifier, usetargets=usetargets)

        self.logicstatewiring.clear()

    def create_wallobject_on_map(self, tile, entityclass, entityname, startstates, visualvariant, visualframe,
                                 overrides, direction, dowiring, usetargets, callbacks, xoffset=0, yoffset=0):
        """
        Create a wall object.

        :param tile: tile on which to place object
        :param entityclass: object class
        :param startstates: visual status of the variant
        :param visualvariant: visual variant of entity type
        :param visualframe: animation frame
        :param direction: CompassDirection, must be one of the four main directions
        :return: object instance
        """
        if tile.wallTile is None:
            raise LogicError('Wall objects must be placed on wall tiles.')

        if direction not in [CompassDirection.NORTH, CompassDirection.EAST, CompassDirection.SOUTH, CompassDirection.WEST]:
            raise LogicError('Wall objects must be placed on north, east, south or west side of wall.')

        index = int(math.log(int(direction), 4))

        entity = engine.entityfactories.create_entity(entityclass,
                                                      entityname,
                                                      visualvariant,
                                                      visualframe,
                                                      overrides,
                                                      xoffset,
                                                      yoffset)

        entity.direction = direction
        tile.wallObjects[index] = entity

        self.add_entity_to_map(tile.x, tile.y, direction, entity, startstates, dowiring, usetargets, callbacks)
        return entity

    def create_world_entity_on_map(self, tile, entitytype, entityclass, entityname, startstates, visualvariant, visualframe, overrides, dowiring, usetargets, callbacks, xoffset=0, yoffset=0):
        """
        Create a world entity.

        :param tile: tile on which to place object
        :param entityclass: object class
        :param startstates: visual status of the variant
        :param visualvariant: visual variant of entity type
        :param visualframe: animation frame
        :return: object instance
        """
        entity = engine.entityfactories.create_entity(entityclass,
                                                      entityname,
                                                      visualvariant,
                                                      visualframe,
                                                      overrides,
                                                      xoffset,
                                                      yoffset)

        if entitytype == 'Door':
            tile.set_type(TileType.DOOR)
            tile.door = entity
            tile.door.tile = tile
        elif entitytype == 'Window':
            tile.set_type(TileType.WINDOW)
            tile.window = entity
            tile.window.tile = tile

        self.add_entity_to_map(tile.x, tile.y, CompassDirection.NONE, entity, startstates, dowiring, usetargets, callbacks)
        return entity

    def create_collective_entity(self, entityclass, count, entityname, startstates, visualvariant, visualframe, overrides, xoffset = 0, yoffset = 0):
        """
        Create an collective Entity instance.
        
        :param entityclass: class of Entity
        :param count: collective count
        :param startstates: visual status of the variant
        :param visualvariant: visual variant of entity type
        :param visualframe: animation frame
        :return: Entity instance
        """
        entity = engine.entityfactories.create_entity(entityclass,
                                                      entityname,
                                                      visualvariant,
                                                      visualframe,
                                                      overrides,
                                                      xoffset,
                                                      yoffset)
        if entity.has_trait('Collective'):
            entity.collective.count = count
            return entity
        else:
            raise LogicError("Tried to create collective Entity '{}' but it does not have that trait.".format(entityclass))

    def create_entity_on_map(self, x, y, direction, entityclass, entityname, startstates, visualvariant, visualframe, overrides, dowiring, usetargets, callbacks, xoffset=0, yoffset=0):
        """
        Create an Entity instance and place on map.
        
        :param x: x position to place Entity - or None for an abstract entity
        :param y: y position to place Entity - or None for an abstract entity
        :param direction: CompassDirection to face in
        :param entityclass: type of Entity
        :param entityname: scripted name of Entity
        :param startstates: visual status of the variant
        :param visualvariant: visual variant of entity type
        :param visualframe: animation frame
        :return: Entity instance
        """        
        entity = engine.entityfactories.create_entity(entityclass,
                                                      entityname,
                                                      visualvariant,
                                                      visualframe,
                                                      overrides,
                                                      xoffset,
                                                      yoffset)
        self.add_entity_to_map(x, y, direction, entity, startstates, dowiring, usetargets, callbacks)
        return entity

    def create_collective_entity_on_map(self, x, y, direction, entityclass, count, entityname, startstates, visualvariant, visualframe, overrides, dowiring, usetargets, callbacks, xoffset=0, yoffset=0):
        """
        Create an collective Entity instance and place on map.
        
        :param x: x position to place Entity
        :param y: y position to place Entity
        :param direction: CompassDirection to face in
        :param entityclass: type of Entity
        :param count: collective count                
        :param startstates: visual status of the variant
        :param visualvariant: visual variant of entity type
        :param visualframe: animation frame
        :return: Entity instance
        """        
        entity = self.create_collective_entity(entityclass, count, entityname, startstates, visualvariant, visualframe, overrides, xoffset, yoffset)
        self.add_entity_to_map(x, y, direction, entity, startstates, dowiring, usetargets, callbacks)
        return entity

    def create_entity_as_inventory(self, x, y, entityclass, entityname, startstates, visualvariant, visualframe, overrides, dowiring, usetargets, callbacks, xoffset, yoffset, count=1):
        """
        Create an Entity instance and place on a Tile as inventory.
        
        :param x: x position to place Entity
        :param y: y position to place Entity
        :param entityclass: type of Entity
        :param startstates: visual status of the variant
        :param visualvariant: visual variant of entity type
        :param visualframe: animation frame
        :param count: count
        :return: Entity instances
        """
        entities = list()
        for i in range(count):
            entity = engine.entityfactories.create_entity(entityclass,
                                                          entityname,
                                                          visualvariant,
                                                          visualframe,
                                                          overrides,
                                                          xoffset,
                                                          yoffset)
            self.add_entity_as_inventory(x, y, entity, startstates, dowiring, usetargets, callbacks)
            entities.append(entity)
            
        return entities

    def create_collective_entity_as_inventory(self, x, y, entityclass, entityname, startstates, visualvariant,
                                              visualframe, overrides, collectivecount, dowiring, usetargets, callbacks, xoffset, yoffset, count=1):
        """
        Create an Entity instance and place on a Tile as inventory.
        
        :param x: x position to place Entity
        :param y: y position to place Entity
        :param entityclass: type of Entity
        :param entityname: name of Entity
        :param startstates: visual status of the variant
        :param visualvariant: visual variant of entity type
        :param visualframe: animation frame
        :param overrides: overrides for this particular entity
        :param collectivecount: collective count
        :param xoffset:
        :param count: entity count
        :return: Entity instance
        """                
        entity = None
        for i in range(count):
            entity = self.create_collective_entity(entityclass, None, collectivecount, entityname, startstates, visualvariant, visualframe, overrides, xoffset, yoffset)
            self.add_entity_as_inventory(x, y, entity, startstates, dowiring, usetargets, callbacks)
        
        return entity

    def add_entity(self, entity, startstates, dowiring, usetargets, callbacks):
        # Set start states
        for state in startstates:
            if state[0] == '-':
                entity.states.unset_state(state, True)
            else:
                entity.states.set_state(state, True)

        # Set direction if we have vision
        vision = entity.get_vision()
        if vision:
            self.maxviewdistance = max(self.maxviewdistance, vision.viewdistance)
            vision.update()

        # Animation lookup
        if entity.visual.visualframecount > 1:
            self.animatedvisuals.add(entity.visual)

        # Lookup list
        self.entities.add(entity)

        # Add usetargets and callbacks
        if len(usetargets) > 0:
            self.usetargetwiring[entity] = usetargets

        if len(callbacks) > 0:
            self.logicstatewiring[entity] = callbacks

        # Add scripted entities
        if entity.script_name:
            self.namedentities[entity.script_name] = entity

        if entity.has_trait('Inventory'):
            for stack in entity.inventory:
                if stack.item.script_name:
                    self.namedentities[stack.item.script_name] = stack.item

        if dowiring:
            self.flush_entity_wiring()

        entity.on_added_to_map(self)
        obs.trigger(entity, 'Entity.AddedToMap', entity, self)

    def add_entity_to_map(self, x, y, direction, entity, startstates, dowiring, usetargets, callbacks):
        """
        Add an existing Entity to the map in the specified location.
        
        :param x: x position to place Entity - or None for an abstract entity
        :param y: y position to place Entity - or None for an abstract entity
        :param direction: CompassDirection to face in
        :param entity: Entity instance
        """
        if entity.is_abstract:
            entity.owner = self
        else:
            tile = self.tiles[x][y]
            if not tile.entity:
                entity.owner = tile
                entity.direction = direction
                if entity.category not in [EntityCategory.WORLD, EntityCategory.WALL]:
                    tile.entity = entity
            else:
                raise LogicError('Entity placed on a tile where another entity already resides.')

        self.add_entity(entity, startstates, dowiring, usetargets, callbacks)

    def add_entity_as_inventory(self, x, y, entity, startstates, dowiring, usetargets, callbacks):
        """
        Add an existing Entity to a Tile as inventory in the specified location.
        
        :param x: x position to place Entity
        :param y: y position to place Entity
        :param entity: Entity instance
        """                
        tile = self.tiles[x][y]
        if tile.inventory is None:
            raise LogicError('Entity placed as inventory on a tile with no inventory.')
        else:
            tile.inventory.add_entity(entity, 1)
            entity.owner = tile

        self.add_entity(entity, startstates, dowiring, usetargets, callbacks)

    def remove_entity(self, entity):
        # Animation lookup
        if entity.visualframecount > 1:
            self.animatedvisuals.remove(entity.visual)

        # Lookup
        self.entities.remove(entity)

        # Events
        entity.unregister_events()

        # Remove scripted entities
        if entity.script_name:
            del self.namedentities[entity.script_name]

        # Remove owned items
        if entity.has_trait('Inventory'):
            for stack in entity.inventory:
                item = stack.item

                # Remove from scripted cache
                if item.script_name:
                    del self.namedentities[item.script_name]

                # Unregister events
                item.unregister_events()

    def remove_entity_from_map(self, entity):
        """
        Remove an entity.

        :param entity: Entity to remove.
        """
        entity.on_removed_from_map(self)

        if not entity.is_abstract:
            tile = self.tiles[entity.x][entity.y]
            tile.entity = None

        self.remove_entity(entity)
        obs.trigger(entity, 'Entity.RemovedFromMap', entity, self)

    def get_entities_by_type(self, entitytype):
        entities = list()
        for y in range(self.sizeY):
            for x in range(self.sizeX):
                tile = self.tiles[x][y]
                if tile.inventory:
                    entities.extend(tile.inventory.get_entities_by_type(entitytype))

        return entities

    def get_entities_by_class(self, cls):
        entities = list()
        for y in range(self.sizeY):
            for x in range(self.sizeX):
                tile = self.tiles[x][y]
                if tile.entity and tile.entity.actorType == cls:
                    entities.append(tile.entity)

        return entities

    def get_named_entity(self, name):
        return self.namedentities.get(name)

    def print_debug_round_actions(self):
        print('--------')
        for entity in self.entities:
            if entity.roundaction:
                ead = entity.roundaction

                if isinstance(ead.target, Tile):
                    targetname = 'Tile'
                elif ead.target:
                    targetname = ead.target.name
                else:
                    targetname = '<None>'

                print(ead.actor.name, f'{ead.action} {ead.actor.name} {targetname} {ead.args} {ead.kwargs}')
            else:
                print(entity.name, 'None')

    def perform_round_actions(self, playeraction, actiontargets):
        """
        Perform all requested actions, in order of priority

        :param playeraction: EntityActionDefinition for the player's action
        :param actiontargets: dictionary of action targets mapped to list of their EntityActionDefinitions
        """
        # We first get the player's target tile, as all entities wanting to move there will have to pass.
        # The target tile is the tile the player intends to be on when the action is complete, not necessarily
        # the tile that the action is being performed on (in the case of a target entity)
        playertargettile = self.tiles[playeraction.actortargetpos[0]][playeraction.actortargetpos[1]]
        passactions = defaultdict(list)

        # Resolve moves affecting the player first of all.
        playercurtile = playeraction.actor.owning_tile
        removefromtargettile = list()
        for ead in actiontargets[playertargettile]:
            if ead != playeraction:
                if not ead.is_move_action and ead.actor == playertargettile.entity:
                    # We have a non-moving actor in the player's way.  Swap it with the player.
                    newmove = ActionResolver.convert_action_to_move(ead, playercurtile.x, playercurtile.y)

                    # All actors targetting the player's current tile will now have to be set to Pass.
                    for ead2 in actiontargets[playercurtile]:
                        passactions[ead2.actor.owning_tile].append(ActionResolver.convert_action_to_pass(ead2))

                    actiontargets[playercurtile] = [newmove]
                else:
                    removefromtargettile.append(ead)
                    passactions[ead.actor.owning_tile].append(ActionResolver.convert_action_to_pass(ead))

        for remove in removefromtargettile:
            actiontargets[playertargettile].remove(remove)

        # If there is something on the player's target tile which is moving to the player's current tile (ie they
        # are swapping positions) then this entity should get priority over others whose target is the player's
        # current position.
        swappedead = None
        for ead in actiontargets[playercurtile]:
            if ead != playeraction:
                if ead.actor.x == playeraction.actortargetpos[0] and ead.actor.y == playeraction.actortargetpos[1]:
                    swappedead = ead
                else:
                    passactions[ead.actor.owning_tile].append(ActionResolver.convert_action_to_pass(ead))

        if swappedead:
            actiontargets[playercurtile] = [swappedead]

        # At this point, everything should be resolved so that the player is the only one contending for its target.
        for contentiontarget, eads in actiontargets.items():
            if type(contentiontarget) is Tile or len(eads) < 2:
                continue

            # Get the aptitudes for the contending actors
            maxpriority = max(eads, key=lambda x: x.priority.value).priority.value
            victor = -1
            maxapt = -1
            for i, ead in enumerate(eads):
                if ead.priority.value == maxpriority:
                    apt = ead.actor.aptitudes[ead.action](ead.actor, ead.target)
                    if apt > maxapt:
                        victor = i
                        maxapt = apt

            # Set the losing actors' action to Pass
            for i, ead in enumerate(eads):
                if victor == i:
                    continue

                newpass = ActionResolver.convert_action_to_pass(ead)
                atarget = ead.actor.map.tiles[newpass.actortargetpos[0]][newpass.actortargetpos[1]]
                passactions[atarget].append(newpass)

            actiontargets[contentiontarget] = [eads[victor]]

        # Add the new Pass actions in
        for passactiontarget, passactions in passactions.items():
            actiontargets[passactiontarget].extend(passactions)

        # Resolve actions
        resolver = ActionResolver(self)
        resolvedactions = resolver.get_action_target_locations(list(itertools.chain(*actiontargets.values())))

        # Now go through actions and check prerequisites again, and then execute it.  If
        # actor position is not the same as target position, then do in 2 phases
        filteredmovers = list()
        filterednonmovers = list()
        for ead in resolvedactions:
            entity = ead.actor
            newx, newy = ead.actortargetpos
            srctile = entity.owning_tile
            tgttile = self.tiles[newx][newy]

            # Successful movers have different srctile and tgttile, and the same tgttile and newtile
            # Bumpees have different srctile and newtile.  We can't just check for the Move action because there
            # may be custom actions like teleportation.
            if srctile != tgttile:
                result = entity.check_action_prerequisites(ead.action, entity.map, newx, newy)
                if result.ok:
                    filteredmovers.append(ead)
            else:
                filterednonmovers.append(ead)

        # Apply moves in 2 phases to avoid invalidating entity positions
        for ead in filteredmovers:
            entity = ead.actor
            newx, newy = ead.actortargetpos
            oldtile = entity.owning_tile
            newtile = self.tiles[newx][newy]

            if oldtile != newtile:
                oldtile.entity = None

        for ead in filteredmovers:
            entity = ead.actor
            newx, newy = ead.actortargetpos
            oldtile = entity.owning_tile
            newtile = self.tiles[newx][newy]

            if oldtile != newtile:
                newtile.entity = entity
                entity.owner = newtile
                entity.set_direction(newtile.x - oldtile.x, newtile.y - oldtile.y)

                # Run post-execution code
                facres = FacilitatorResult(success=True, actionperformed=True, playermsg=None, othermsg=None)
                entity.post_execute_action(facres, ead.action, entity.map, newtile.x, newtile.y)

        for ead in filterednonmovers:
            ead.actor.execute_action(ead.action, ead.target, *ead.args, **ead.kwargs)

    def entities_turn(self, curturn):
        """
        Run a turn for all the entities on this map.  Because entities may have inventory, we go depth-first.
        
        :param curturn: current turn number
        """
        for entity in self.entities:
            if not entity.is_player:
                entity.roundaction = None

        actiontargets = defaultdict(list)
        playeraction = None
        for entity in self.entities:
            entity.turn(curturn)

            if entity.is_player:
                playeraction = entity.roundaction
            else:
                entity.choose_action()

            if entity.roundaction:
                action = entity.roundaction
                targetpos = action.actortargetpos
                target = action.target
                actiontarget = target.tiles[targetpos[0]][targetpos[1]] if target.actorType == 'Map' else target
                actiontargets[actiontarget].append(action)

        # Process entity actions
        self.perform_round_actions(playeraction, actiontargets)

        # Vision lookup
        visionchangingentities = list()

        # Post-turn update, after actions have taken place
        for entity in self.entities:
            res = entity.post_turn()
            if res.visionchanged:
                visionchangingentities.append(entity)

            # Clear player's vision so rendering will pick up changes
            if entity.is_player and entity.get_vision():
                entity.vision.reset()

        # Update map tile visibility
        self.update_vision_map(visionchangingentities)

        # Update entity vision
        for entity in self.entities:
            vision = entity.get_vision()
            if vision:
                vision.update()

    def update_visuals(self, frametime):
        self.changedanimatedvisuals.clear()
        for visual in self.animatedvisuals:
            if visual.update_visual(frametime):
                self.changedanimatedvisuals.add(visual)

        if len(self.changedanimatedvisuals) > 0:
            obs.trigger(None, 'UI.RefreshViews')

    def update(self, curturn):
        pass

    def on_set_player(self, source, entity, isplayer):
        self.player = entity if isplayer else None
