"""
Container for Views.

Formatted 17/09
"""

#
# Imports
#
from engine.defines import AlignmentType
from engine.settings import GameSettings


class Container:
    """Base Container class.  Holds children which can be either Views or other Containers."""

    #
    # __init__()
    #
    def __init__(self, x, y, halign=AlignmentType.LEFT, valign=AlignmentType.TOP):
        """
        Constructor.
        
        :param x: x position relative to parent, or screen origin if no parent
        :param y: y position relative to parent, or screen origin if no parent
        :param halign: horizontal alignment within parent, or screen size if no parent
        :param valign: vertical alignment within parent, or screen size if no parent
        """
        self.parent = None
        self.x = x
        self.y = y
        self.halign = halign
        self.valign = valign
        self.children = list()

    #
    # offset_x
    #
    @property
    def offset_x(self):
        """Get absolute x position on screen."""
        if self.parent:
            return self.parent.offset_x + self.parent.child_x_offset(self) + self.x
        else:
            if self.halign == AlignmentType.LEFT:
                return self.x
            elif self.halign == AlignmentType.CENTRE:
                return (GameSettings.windowWidth - self.width) // 2 + self.x

    #
    # offset_y
    #
    @property
    def offset_y(self):
        """Get absolute y position on screen."""
        if self.parent:
            return self.parent.offset_y + self.parent.child_y_offset(self) + self.y
        else:
            if self.valign == AlignmentType.TOP:
                return self.y
            elif self.halign == AlignmentType.CENTRE:
                return (GameSettings.windowHeight - self.height) // 2 + self.y

    #
    # width
    #
    @property
    def width(self):
        """
        Get combined width of all Views within Container.
        
        :return: combined width
        """
        return max(child.width for child in self.children)

    #
    # height
    #
    @property
    def height(self):
        """
        Get combined height of all Views within Container.
        
        :return: combined height
        """    
        return max(child.height for child in self.children)

    #
    # child_x_offset()
    #
    def child_x_offset(self, child):
        """
        Get the x offset of the given child with this Container, relative to container's offset_x.
        To be implemented by subclasses.
        
        :param child: child object (View or Container)
        :return: relative x offset
        """
        return 0

    #
    # child_y_offset
    #
    def child_y_offset(self, child):
        """
        Get the y offset of the given child with this Container, relative to container's offset_y.
        To be implemented by subclasses.
        
        :param child: child object (View or Container)
        :return: relative y offset
        """
        return 0

    #
    # on_added_to_container()
    #
    def on_added_to_container(self, container):
        """
        Callback for when added to another Container as a child.
        
        :param container: new parent Container.
        """
        pass

    #
    # on_removed_from_container()
    #
    def on_removed_from_container(self, container):
        """
        Callback for when removed from another Container.
        
        :param container: previous parent Container.
        """
        pass

    #
    # add_child()
    #
    def add_child(self, child):
        """
        Add a child.
        
        :param child: either Container subclass or View subclass
        """
        child.parent = self
        self.children.append(child)
        child.on_added_to_container(self)

    #
    # remove_child()
    #
    def remove_child(self, child):
        """
        Remove a child.
        
        :param child: either Container subclass or View subclass
        """
        child.parent = None
        self.children.remove(child)
        child.on_removed_from_container(self)

    #
    # change_map
    #
    def change_map(self, map):
        for child in self.children:
            child.change_map(map)

    #
    # update()
    #
    def update(self, frametime):
        for child in self.children:
            child.update(frametime)

    #
    # display()
    #
    def display(self):
        """Render Container contents."""
        for child in self.children:
            child.display()


#
# HorizontalContainer
#
class HorizontalContainer(Container):
    """Holds children in left to right alignment."""
    
    #
    # __init__()
    #
    def __init__(self, x, y, halign = AlignmentType.LEFT, valign = AlignmentType.TOP):
        """
        Constructor.
        
        :param x: x position relative to parent, or screen origin if no parent
        :param y: y position relative to parent, or screen origin if no parent
        :param halign: horizontal alignment within parent, or screen size if no parent
        :param valign: vertical alignment within parent, or screen size if no parent
        """
        super().__init__(x, y, halign, valign)

    #
    # child_x_offset()
    #
    def child_x_offset(self, child):
        """
        Get the x offset of the given child with this Container, relative to container's offset_x.
        
        :param child: child object (View or Container)
        :return: relative x offset
        """
        xOffset = 0
        for c in self.children:
            if c == child:
                return xOffset
            else:
                xOffset += c.width


#
# VerticalContainer
#
class VerticalContainer(Container):

    #
    # __init__()
    #
    def __init__(self, x, y, halign = AlignmentType.LEFT, valign = AlignmentType.TOP):
        """
        Constructor.
        
        :param x: x position relative to parent, or screen origin if no parent
        :param y: y position relative to parent, or screen origin if no parent
        :param halign: horizontal alignment within parent, or screen size if no parent
        :param valign: vertical alignment within parent, or screen size if no parent
        """
        super().__init__(x, y, halign, valign)

    #
    # child_y_offset()
    #
    def child_y_offset(self, child):
        """
        Get the y offset of the given child with this Container, relative to container's offset_y.
        
        :param child: child object (View or Container)
        :return: relative y offset
        """
        yOffset = 0
        for c in self.children:
            if c == child:
                return yOffset
            else:
                yOffset += c.height
