#
# MapLoader
#

#
# Base class for loading map
#
class MapLoader:
    """MapLoader"""

    #
    # __init__()
    #
    def __init__(self, maptype):
        """
        Constructor
        """
        self.maptype = maptype

    #
    # load()
    #
    def load(self):
        """
        Load map

        :return: created Map
        """
        pass