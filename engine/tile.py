from common import *
from engine.colour import Colour
from engine.visual import Visual
from engine.traits import inventory
from engine.tiletype import TileType
from engine.mapdecorator import owned_by_map


@owned_by_map
class Tile:
    """Tiles - part of the map."""
    
    #
    # __init__()
    #
    def __init__(self, owner, x=None, y=None, inventorysize=0):
        """
        Constructor.

        :param inventorysize: capacity of tile
        """
        super().__init__()

        self.x = x
        self.y = y
        self.name = 'Tile'
        self.type = TileType.UNKNOWN
        self.owner = owner

        # Visual
        self.visual = Visual(self)

        # Physical settings
        self._blocksVision = False
        self._blocksMovement = False
        self._blocksAudio = False
        self.playerSeen = False
        self.ambientLight = Colour(255, 255, 255)

        self.overlayTile = None
        self.overlayOpacity = 1.0

        self.wallTile = None

        # Contents
        self.entity = None
        self.inventory = inventory.Inventory(self, inventorysize, False) if inventorysize > 0 else None

    @property
    def visual_tile(self):
        return self.visual.get_visual()

    def blocks_movement(self):
        """
        Gets whether or not this tile blocks immediate movement.

        :return: whether or not an can move here.
        """
        if self._blocksMovement:
            return True
        elif self.entity and self.entity.blocks_movement():
            return True
        elif self.type == TileType.DOOR and self.door.blocks_movement():
            return True
        elif self.type == TileType.WINDOW and self.window.blocks_movement():
            return True

        return False

    def blocks_pathing(self, blockedentity=None):
        """
        Gets whether or not this tile blocks pathing.

        :param blockedentity: Entity trying to move to this tile.
        :return: whether or not blockedentity can move here.
        """
        if self._blocksMovement:
            return True
        elif self.entity:
            return self.entity.blocks_pathing(blockedentity)
        elif self.type == TileType.DOOR:
            return self.door.blocks_pathing(blockedentity)
        elif self.type == TileType.WINDOW:
            return self.window.blocks_pathing(blockedentity)
        else:
            return False

    @property
    def blocks_vision(self):
        """
        Gets whether or not this tile blocks vision.
        """
        if self._blocksVision:
            return True
        elif self.entity:
            return self.entity.blocks_vision
        elif self.type == TileType.DOOR:
            return self.door.blocks_vision
        elif self.type == TileType.WINDOW:
            return self.window.blocks_vision
        else:
            return False

    @property
    def blocks_audio(self):
        """Gets whether or not this tile blocks audio."""
        if self._blocksAudio:
            return True
        elif self.entity:
            return self.entity.blocks_audio
        elif self.type == TileType.DOOR:
            return self.door.blocks_audio
        elif self.type == TileType.WINDOW:
            return self.window.blocks_audio
        else:
            return False

    def set_type(self, tiletype, tilevariant=None, tilestatus=None):
        self.type = tiletype

        if self.type in [TileType.WALL, TileType.WINDOW]:
            visualtype = 'Wall'
        elif self.type in [TileType.FLOOR, TileType.DOOR]:
            visualtype = 'Floor'
        elif self.type in [TileType.SPACE]:
            visualtype = 'Space'
        else:
            raise Exception(f"Unknown tile type '{tiletype}'")

        self.visual.set_visual(vtype=visualtype, vvariant=tilevariant, vstatus=tilestatus)

