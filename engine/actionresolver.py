import igraph
import itertools
from collections import defaultdict

from engine.exceptions import LogicError
from engine.defines import ActionPriority, ActionPriorityData, EntityActionDefinition


class ActionResolver:

    def __init__(self, map):
        self.map = map
        self.loc2action = dict()
        self.worldactions = list()
        self.nonmoveactions = list()

    def get_entity_bump_location(self, bumpee, bumper):
        """
        When a lower-priority entity is 'bumped'' out of the way, it needs to find a place to go.

        :param bumpee: Entity being bumped
        :param bumper: Entity bumping the other one
        :return: x, y location, or None if no location was found
        """
        # First check the tile that bumper is on, as there's a good chance it will be free and that will
        # be the best tile to use.
        if not self.loc2action.get((bumper.x, bumper.y)):
            return bumper.x, bumper.y

        # If no good, then get all tiles around.
        candidates = [(1, 0), (0, 1), (-1, 0), (0, -1), (1, 1), (1, -1), (-1, -1), (-1, 1)]
        for i, c in enumerate(candidates):
            x = bumpee.x + c[0]
            y = bumpee.y + c[1]

            if 0 <= x < self.map.sizeX and 0 <= y < self.map.sizeY:
                # For diagonals check the sides
                if i >= 4 and (self.map.tiles[x + candidates[i][0]][y].blocks_movement() or
                               self.map.tiles[x][y + candidates[i][1]].blocks_movement()):
                    continue

                if not self.map.tiles[x][y].blocks_movement() and (x, y) not in self.loc2action:
                    return x, y

        return None, None

    def process_graph_cycles(self, graph, stack, visited):
        cycles = list()

        # Find the target vertex for the top of the stack
        for e in graph.es:
            if e.source == stack[-1]:
                v = e.target
                if visited[v] == 0:
                    stack.append(v)
                    visited[v] = 1
                    cycles.extend(self.process_graph_cycles(graph, stack, visited))
                elif visited[v] == 1:
                    # Found cycle
                    cycle = stack[stack.index(v):]
                    cycles.append(cycle)
        visited[stack[-1]] = 2
        stack.pop()
        return cycles

    @staticmethod
    def convert_action_to_pass(ead):
        """
        Helper to take an EntityActionDefinition and convert it to a Pass action.

        :param ead: EntityActionDefinition instance
        :return:  new EntityActionDefinition with 'Pass' action
        """
        return EntityActionDefinition(action='Pass',
                                      actor=ead.actor,
                                      target=ead.actor.map,
                                      actortargetpos=(ead.actor.x, ead.actor.y),
                                      priority=ActionPriorityData(ActionPriority.NoAction, ead.actor.prioritymodifier),
                                      args=list(),
                                      kwargs=dict())

    @staticmethod
    def convert_action_to_move(ead, tx, ty):
        """
        Helper to take an EntityActionDefinition and convert it to a Pass action.

        :param ead: EntityActionDefinition instance
        :param tx: target x position
        :param ty: target y position
        :return:  new EntityActionDefinition with 'Move' action
        """
        return EntityActionDefinition(action='Move',
                                      actor=ead.actor,
                                      target=ead.actor.map,
                                      actortargetpos=(tx, ty),
                                      priority=ActionPriorityData(ActionPriority.Movement, ead.actor.prioritymodifier),
                                      args=[tx, ty],
                                      kwargs=dict())

    def walk_movement_graph(self, graph):
        # Get cycles
        cycles = list()
        visited = [0] * len(graph.vs)
        for i in range(len(graph.vs)):
            if visited[i] == 0:
                stack = [i]
                visited[i] = 1
                cycles.extend(self.process_graph_cycles(graph, stack, visited))

        # Add cycles to lookup.  These are guaranteed to be OK.
        for cycle in cycles:
            for v in cycle:
                d = graph.vs[v]['definition']
                self.loc2action[d.actortargetpos] = d

        # Delete the cycle vertices from graph
        graph.delete_vertices(list(itertools.chain(*cycles)))

        # Do chains.  These may get blocked, so we change action to Pass for these.
        # Build edge lookups.
        edgelookup1 = dict()
        edgelookup2 = defaultdict(list)
        for e in graph.es:
            edgelookup1[e.source] = e.target
            edgelookup2[e.target].append(e.source)

        # Get initial vertices.  These are vertices which do not appear as a target in any edge.
        vertices = [vx.index for vx in graph.vs if vx.index in edgelookup1 and len(edgelookup2[vx.index]) == 0]
        vertices = sorted(vertices, key=lambda vx: graph.vs[vx]['definition'].priority.value, reverse=True)

        # Build up all the chains.
        chains = list()
        for vertex in vertices:
            chain = [vertex]
            target = edgelookup1.get(vertex)
            while target is not None:
                chain.append(target)
                d = graph.vs[target]['definition']
                if not d:
                    break

                target = edgelookup1.get(target)

            chains.append(chain)

        chains = sorted(chains, key=lambda x: -len(x))
        processed = set()
        for chain in chains:
            c = reversed(chain)
            for link in c:
                d = graph.vs[link]['definition']
                if link in processed or not d:
                    continue

                if d.actortargetpos in self.loc2action:
                    d = self.convert_action_to_pass(d)

                self.loc2action[d.actortargetpos] = d
                processed.add(link)

        # Add non-move actions.  These are subject to bumping.
        nonmoveactions = sorted(self.nonmoveactions, key=lambda x: x.priority.value, reverse=True)
        for action in nonmoveactions:
            d = self.loc2action.get(action.actortargetpos)
            if d:
                # We can get the situation where an open door or other world entity is just passing, and another entity
                # trying to move onto its tile, and here the door would get bumped, which we clearly don't want.  Only
                # bump if it can actually move.
                if action.actor.is_mover:
                    bx, by = self.get_entity_bump_location(action.actor, d.actor)
                    if bx is not None and by is not None:
                        self.loc2action[(bx, by)] = self.convert_action_to_move(action, bx, by)
                    else:
                        raise LogicError('Entity has nowhere to exist.')
                else:
                    self.worldactions.append(self.convert_action_to_pass(action))
            else:
                self.loc2action[action.actortargetpos] = action

    def get_action_target_locations(self, actions):
        """
        Perform all requested actions, in order of priority

        :param actions: list of EntityActionDefinitions, sorted by priority
        :return: entities with remaining action points
        """
        # Build vertex list
        actionref = dict()
        vset = set()
        edgelist = list()
        for actiondef in actions:
            src = (actiondef.actor.x, actiondef.actor.y)
            dst = actiondef.actortargetpos

            # We only want move actions in the graph, or those involving doors.  Save the others for later.
            # We add the player here so that the graph calculations can use it for bumping.
            if actiondef.priority.ptype == ActionPriority.Player:
                self.loc2action[actiondef.actortargetpos] = actiondef
            elif actiondef.priority.ptype == ActionPriority.Movement:
                actionref[src] = actiondef
                vset.add(src)
                vset.add(dst)
                edgelist.append([src, dst])
            elif actiondef.priority.ptype in [ActionPriority.NoAction, ActionPriority.Action]:
                self.nonmoveactions.append(actiondef)

        vlist = list(vset)
        vattrs = [actionref.get(src) for src in vlist]

        # Mutate edgelist from locations to indices in the vertex list
        for ie in range(len(edgelist)):
            for iv in range(len(vlist)):
                if edgelist[ie][0] == vlist[iv]:
                    edgelist[ie][0] = iv
                if edgelist[ie][1] == vlist[iv]:
                    edgelist[ie][1] = iv

        # Create graph
        g = igraph.Graph(directed=True)
        g.add_vertices(len(vlist))
        g.vs['definition'] = vattrs
        g.add_edges(edgelist)

        self.walk_movement_graph(g)
        return self.loc2action.values()
