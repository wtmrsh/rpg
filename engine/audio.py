from openal import *

import engine.pathfinder

sounds = dict()


def load_sounds(directory):
    for file in os.listdir(directory):
        if file.endswith(".wav"):
            filename = os.path.join(directory, file)
            sounds[file] = oalOpen(filename)


def unload_sounds():
    oalQuit()


def start_sound(soundfile, gain):
    sound = sounds[soundfile]
    sound.set_gain(gain)
    sound.play()


def update_sounds(frametime):
    pass