import os
import sys
import importlib.util
import engine.entity
from engine.settings import GameSettings
from common import GAMES_DIR


gamefacilitatorsmodules = 'facilitators'
gameprerequisitesrsmodules = 'prerequisites'
gameaptitudesmodules = 'aptitudes'
gamefactionsmodules = 'factions'
gamedynamictransmodules = 'dynamictrans'
gameintelligencetasksmodules = 'intelligencetasks'


gameEntityDict = None
gamePrereqModule = None
gameFactionsModule = None
gameFacilitatorsModule = None
gameAptitudeModule = None
gameDynamicTransModule = None
gameIntelligenceTasksModule = None


def set_function_modules(facilitators, prerequisites, aptitudes, factions, dynamictrans, intelligencetasks):
    global gamefacilitatorsmodules, \
        gameprerequisitesrsmodules, \
        gameaptitudesmodules, \
        gamefactionsmodules, \
        gamedynamictransmodules, \
        gameintelligencetasksmodules

    gamefacilitatorsmodules = facilitators
    gameprerequisitesrsmodules = prerequisites
    gameaptitudesmodules = aptitudes
    gamefactionsmodules = factions
    gamedynamictransmodules = dynamictrans
    gameintelligencetasksmodules = intelligencetasks


def is_entity_subclass(module, name):
    import inspect
    obj = getattr(module, name)
    return inspect.isclass(obj) and issubclass(obj, engine.entity.Entity)


def get_game_entity_dict():
    global gameEntityDict
    if gameEntityDict is None:
        gameEntityDict = dict()
        gameModuleDir = f'{GAMES_DIR}/{GameSettings.game}/entities'

        for root, dirs, files in os.walk(gameModuleDir):
            for file in files:
                if file.endswith('.py'):
                    filepath = os.path.join(root, file)
                    moduleName = root.replace('/', '.').replace('\\', '/') + '.' + file[:-3]

                    spec = importlib.util.spec_from_file_location(moduleName, filepath)
                    module = importlib.util.module_from_spec(spec)
                    sys.modules[moduleName] = module
                    spec.loader.exec_module(module)
                    names = [x for x in module.__dict__ if is_entity_subclass(module, x)]
                    gameEntityDict.update({k: getattr(module, k) for k in names})

    return gameEntityDict


def load_game_module(name):
    mname = f'{GAMES_DIR}.{GameSettings.game}.{name}'
    spec = importlib.util.spec_from_file_location(mname, mname.replace('.', '/') + '.py')
    m = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(m)
    return m


def get_game_prereq_module():
    global gamePrereqModule
    if not gamePrereqModule:
        gamePrereqModule = load_game_module(gameprerequisitesrsmodules)
    return gamePrereqModule


def get_game_factions_module():
    global gameFactionsModule
    if not gameFactionsModule:
        gameFactionsModule = load_game_module(gamefactionsmodules)
    return gameFactionsModule


def get_game_facilitators_module():
    global gameFacilitatorsModule
    if not gameFacilitatorsModule:
        gameFacilitatorsModule = load_game_module(gamefacilitatorsmodules)

    return gameFacilitatorsModule


def get_game_aptitudes_module():
    global gameAptitudeModule
    if not gameAptitudeModule:
        gameAptitudeModule = load_game_module(gameaptitudesmodules)

    return gameAptitudeModule


def get_game_dynamic_trans_module():
    global gameDynamicTransModule
    if not gameDynamicTransModule:
        gameDynamicTransModule = load_game_module(gamedynamictransmodules)

    return gameDynamicTransModule


def get_game_intelligence_tasks_module():
    global gameIntelligenceTasksModule
    if not gameIntelligenceTasksModule:
        gameIntelligenceTasksModule = load_game_module(gameintelligencetasksmodules)

    return gameIntelligenceTasksModule
