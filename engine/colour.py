"""
Colour.

Formatted 17/09
"""

#
# Imports
#
from bearlibterminal import terminal

#
# Colour
#
class Colour():
    """Class for manipulating RGBA colours, wrapping/augmenting bearlibterminal's functionality."""
    
    #
    # __init__()
    # 
    __slots__ = ['r', 'g', 'b', 'a']
    def __init__(self, r, g, b, a = 255):
        """
        Constructor.
        
        :param r: red component
        :param g: green component
        :param b: blue component
        :param a: alpha component
        """
        self.r = r
        self.g = g
        self.b = b
        self.a = a
        

    #
    # unitised()
    #
    def unitised(self):
        """
        Return colour components in [0,1] range.
        
        :return: individual colour components in unit form
        """
        return self.r / 255.0, self.g / 255.0, self.b / 255.0, self.a / 255.0
        
    #
    # to_greyscale()
    #
    def to_greyscale(self, amount = 1.0):
        """
        Return colour converted to greyscale.
        
        :param amount: value in [0,1] to determine how grey
        :return: Colour instance in greyscale
        """
        greyscale = int(self.r * 0.3 + self.g * 0.59 + self.b * 0.11)

        r = int(self.r + (greyscale - self.r) * amount)
        g = int(self.g + (greyscale - self.g) * amount)
        b = int(self.b + (greyscale - self.b) * amount)

        return Colour(r, g, b)

    #
    # make_greyscale()
    #
    def make_greyscale(self, amount = 1.0):
        """
        Convert instance to greyscale.
        
        :param amount: value in [0,1] to determine how grey
        """
        grey = self.to_greyscale(amount)
        self.r = grey.r
        self.g = grey.g
        self.b = grey.b
        
    #
    # to_bgra()
    #
    def to_bgra(self):
        """
        Convert to 32 integer in AARRGGBB format.
        
        :return: integer in AARRGGBB format
        """
        result  = self.a << 24
        result += self.r << 16
        result += self.g << 8
        result += self.b

        return result

    #
    # from_bgra()
    #
    @classmethod
    def from_bgra(cls, value):
        """
        Create an instance from an integer in AARRGGBB format
        
        :param value: integer in AARRGGBB format
        :return: Colour instance
        """
        b = value % 256
        g = (value >> 8) % 256
        r = (value >> 16) % 256
        a = (value >> 24) % 256

        return cls(r, g, b, a)

    #
    # from_bgr16()
    #
    @classmethod
    def from_bgr16(cls, value):
        """
        Create an instance from an hex integer in 0xRRGGBB format
        
        :param value: hex integer in 0xRRGGBB format
        :return: Colour instance
        """
        val10 = int(value, 16)
        val10 += (255 << 24)

        return Colour.from_bgra(val10)

    #
    # from_unitised()
    #
    @classmethod
    def from_unitised(cls, r, g, b, a = 1.0):
        """
        Create an instance from components values in [0,1]
        
        :param r: red component
        :param g: green component
        :param b: blue component
        :param a: alpha component
        :return: Colour instance
        """
        return cls(int(r * 255), int(g * 255), int(b * 255), int(a * 255))
     
    #
    # from_name()
    #
    @classmethod
    def from_name(cls, name):
        """
        Create an instance from a named colour
        
        :param name:colour name, from bearlibterminal
        :return: Colour instance
        """
        return Colour.from_bgra(terminal.color_from_name(name))
    
    #
    # from_string()
    #
    @classmethod
    def from_string(cls, c):
        """
        Create an instance from a string.  Allowed formats are:
        - '#AARRGGBB' (integer base 16)
        - '0xRRGGBB' (integer base 16)
        - 'name' (colour name)
        - 'R G B' (individual components in [0,255]
        
        :param c: colour string
        :return: Colour instance, or black (0, 0, 0) if parsing failed
        """
        if c[0] == '#':
            return Colour.from_bgra(int(c[1:], 16))
        elif c[0] == '0' and c[1].lower() == 'x':
                return Colour.from_bgr16(c)
        elif c[0].isalpha():
            return Colour.from_name(c)
        elif c[0].isdigit():
            values = list(filter(bool, c.split(' ')))
            if len(values) == 3:
                return cls(int(values[0]), int(values[1]), int(values[2]))
            elif len(values) == 4:
                return cls(int(values[0]), int(values[1]), int(values[2]), int(values[3]))
        else:
            return cls(0, 0, 0)
    

    #
    # __str()__
    #
    def __str__(self):
        """String representation."""
        return '(%i, %i, %i, %i)' % (self.r, self.g, self.b, self.a)
        
    #
    # __repr__
    #
    def __repr__(self):
        """Internal representation."""
        return 'Colour(%i, %i, %i, %i)' % (self.r, self.g, self.b, self.a)
     
    #
    # lerp()
    #
    def lerp(self, c, v):
        """
        Interpolate between two colours.
        
        :param c: other colouur
        :param v: amount in [0,1]
        :return: interpolated Colour instance
        """
        return self + (c - self) * v
        
    #
    # __eq__
    #
    def __eq__(self, c):
        """
        Test equivalence.
        
        :param c: other colour
        :return: whether the colours are the same
        """
        return self.r == c.r and self.g == c.g and self.b == c.b and self.a == c.a

    #
    # __ne__
    #
    def __ne__(self, c):
        """
        Test inequivalence.
        
        :param c: other colour
        :return: whether the colours are different
        """
        return not (self == c)
        
    #
    # __mul__
    # 
    def __mul__(self, c):
        """
        Multiply colour by either another Colour or a scalar in [0,1]
        
        :param c: other colour
        :return: new Colour instance
        """
        if isinstance(c, Colour):
            return Colour(int(self.r * c.r / 255.0), int(self.g * c.g / 255.0), int(self.b * c.b / 255.0), int(self.a * c.a / 255.0))
        else:
            return Colour(int(self.r * c), int(self.g * c), int(self.b * c), self.a)

    def __rmul__(self, c):
        """
        """
        return self * c
        
    #
    # __add__
    # 
    def __add__(self, c):
        """
        Add colour to this one, clamping at 255.
        
        :param c: other colour
        :return: new Colour instance
        """
        return Colour(min(self.r + c.r, 255), min(self.g + c.g, 255), min(self.b + c.b, 255), self.a)

        #
    # Subtraction
    # 
    def __sub__(self, c):
        """
        Subtract colour from this one, clamping at 0.
        
        :param c: other colour
        :return: new Colour instance
        """
        return Colour(max(0, self.r - c.r), max(0, self.g - c.g), max(0, self.b - c.b), self.a)
