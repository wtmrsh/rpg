"""View manager"""

import os

from bearlibterminal import terminal

from common import *

import engine.observable as obs
from engine.view import View
from engine.container import Container
from engine.colour import Colour
from engine.cursor import Cursor
from engine.defines import ActionResult, InputType


class ViewManager:

    viewDefinitions = None

    def __init__(self, name):
        self.name = name
        self.root = None
        self.focus = None
        self.hover = None
        self.mouseCellX = 0
        self.mouseCellY = 0
        self.mousePixelX = 0
        self.mousePixelY = 0
        self.mouseCellX = 0
        self.lastMouseCellX = 0
        self.lastMouseCellY = 0
        self.lastMousePixelX = 0
        self.lastMousePixelY = 0
        self.stateIndex = -1
        self.map = None

    def refresh_all_views(self, source):
        self.refresh_container(self.root)

    def clear_all_views(self, source):
        self.clear_container(self.root)

    def register_events(self):
        obs.on(None, 'UI.RefreshViews', self.refresh_all_views)
        obs.on(None, 'UI.ClearViews', self.clear_all_views)

    def unregister_events(self):
        obs.off(None, 'UI.RefreshViews', self.refresh_all_views)
        obs.off(None, 'UI.ClearViews', self.clear_all_views)

    def change_map(self, map):
        self.unregister_events()
        self.map = map
        self.register_events()

        if self.root:
            self.root.change_map(map)

    def clear(self):
        self.root = None

    def set_container_as_root(self, container):
        container.parent = None
        self.root = container

    def set_focus(self, view):
        if self.focus:
            self.focus.set_focus(False)

        self.focus = view
        view.set_focus(True)

    def refresh_container(self, container):
        for child in container.children:
            if isinstance(child, View):
                obs.trigger(self, 'UI.RefreshView', child)
            else:
                self.refresh_container(child)

    def clear_container(self, container):
        for child in container.children:
            if isinstance(child, View):
                obs.trigger(None, 'UI.ClearView', child)
            else:
                self.clear_container(child)

    def get_view_depth(self, x, y, view, depth):
        if isinstance(view, Container):
            curMax = (None, -1)
            for child in view.children:
                viewData = self.get_view_depth(x, y, child, depth + 1)
                if viewData[0] and viewData[1] > curMax[1]:
                    curMax = viewData

            return curMax
        else:
            if x >= view.offset_x and x < view.extent_x and y >= view.offset_y and y < view.extent_y:
                return (view, depth)
            else:
                return (None, None)

    def update_mouse(self):
        self.lastMousePixelX = self.mousePixelX
        self.lastMousePixelY = self.mousePixelY
        self.lastMouseCellX = self.mouseCellX
        self.lastMouseCellY = self.mouseCellY

        self.mousePixelX = terminal.state(terminal.TK_MOUSE_PIXEL_X)
        self.mousePixelY = terminal.state(terminal.TK_MOUSE_PIXEL_Y)
        self.mouseCellX = terminal.state(terminal.TK_MOUSE_X)
        self.mouseCellY = terminal.state(terminal.TK_MOUSE_Y)

    def handle_input(self, action, controller, model):
        noaction = [None]
                
        if not terminal.has_input():
            return noaction

        input = terminal.read()

        # Mouse input
        if input == terminal.TK_MOUSE_MOVE:
            self.update_mouse()

            # Find containing view
            hover = self.get_view_depth(self.mouseCellX, self.mouseCellY, self.root, 0)[0]
            if hover != self.hover:
                if self.hover:
                    self.hover.mouse_exited(self.hover)

                if hover:
                    hover.mouse_entered(hover, self.mouseCellX, self.mouseCellY, self.mousePixelX, self.mousePixelY)

                self.hover = hover
            elif self.hover:
                self.hover.mouse_moved(self.mouseCellX, self.mouseCellY, self.mousePixelX, self.mousePixelY)

            return noaction

        if self.focus:
            funcName = ('handle_input_%s' % self.focus.name).lower()

            if not hasattr(controller, funcName):
                raise Exception("No controller found for view '{}'".format(self.focus.name))

            func = getattr(controller, funcName)
            newAction = func(input, action.inputtype, self.focus, action.delayedaction, model)
            if not newAction[0]:
                newAction = controller.handle_input_view(
                    input,
                    action.inputtype,
                    None,
                    action.delayedaction,
                    model)
        else:
            newAction = controller.handle_input_view(
                input,
                action.inputtype,
                None,
                action.delayedaction,
                model)

        return newAction

    def update(self, frametime):
        self.root.update(frametime)

    def clear_cursor(self):
        terminal.layer(255)
        terminal.clear_area(self.lastMouseCellX, self.lastMouseCellY, 1, 1)

    def render_cursor(self):
        cursorName = 'Pointer' if not self.hover else self.hover.get_cursor()
        cursorTile = Cursor.definitions['Cursor0'].cursors[cursorName]

        terminal.layer(255)
        terminal.color(Colour.from_name('white').to_bgra())
        terminal.put_ext(self.mouseCellX, self.mouseCellY, cursorTile.dx + self.mousePixelX % CELL_SIZE, cursorTile.dy + self.mousePixelY % CELL_SIZE, cursorTile.offset)

    def display(self, isactive):
        self.root.display()

        if isactive:
            self.clear_cursor()
            self.render_cursor()

        terminal.refresh()

    @staticmethod
    def get_view_setting(viewName, *tree):
        viewDef = ViewManager.viewDefinitions[viewName]
        defaultDef = ViewManager.viewDefinitions['_']

        notInDefault = False
        for branch in tree:
            try:
                if not notInDefault:
                    defaultDef = defaultDef[branch]
            except KeyError:
                notInDefault = True

            if branch in viewDef:
                viewDef = viewDef[branch]
            else:
                if notInDefault:
                    raise Exception('Could not find configuration setting for {}: {}'.format(viewName, tree))
                else:
                    viewDef = defaultDef

        return viewDef

