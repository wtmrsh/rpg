from engine.mapdecorator import owned_by_map
import engine.tiledefinitions


@owned_by_map
class Visual:

    #
    # __init__()
    #
    def __init__(self, owner, visualtype='_'):
        self.owner = owner
        self.visualtype = visualtype
        self.visualvariant = '_'
        self.visualstatus = None
        self.visualframe = 0
        self.visualframecount = 0
        self.visualframetimer = 0.0

    def get_visual(self):
        return engine.tiledefinitions.definitions[self.visualtype][self.visualvariant][self.visualstatus][self.visualframe].offset

    #
    # set_visual()
    #
    def set_visual(self, vtype=None, vvariant=None, vstatus=None, vframe=0):
        if vtype is not None:
            self.visualtype = vtype
        if vvariant is not None:
            self.visualvariant = vvariant
        if vstatus is not None:
            self.visualstatus = vstatus

        self.visualframe = vframe

        if self.map and self.visualtype is not None and self.visualvariant is not None and self.visualstatus is not None:
            self.update_map_animations()

    #
    # update_map_animations()
    #
    def update_map_animations(self):
        anim = engine.tiledefinitions.definitions[self.visualtype][self.visualvariant][self.visualstatus]
        visualframecount = len(anim)

        # Update map's animation lookup
        if visualframecount > 1 and self.visualframecount < 2:
            self.map.animatedvisuals.add(self)
        elif visualframecount < 2 and self.visualframecount > 1:
            self.map.animatedvisuals.remove(self)

        self.visualframecount = visualframecount

    #
    # update_visual()
    #
    def update_visual(self, frametime):
        self.visualframetimer += frametime
        if self.visualframetimer > 0.6:
            self.visualframetimer -= 0.6
            self.visualframe = (self.visualframe + 1) % self.visualframecount
            return True
        else:
            return False
