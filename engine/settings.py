"""
Settings.

Formatted 17/09
"""

#
# GameSettings
#
class GameSettings():
    """Holds game data from config"""

    # Main settings, either passed in from command line, or hardcoded in game
    game = None
    windowWidth = 0
    windowHeight = 0
    programName = ''
    programVersion = 0.0

    # Game-specific settings
    settings = {}

#
# GraphicsSettings
#
class GraphicsSettings():
    """Holds graphics data from config."""
    
    colourLookup = {}
    viewDecorators = {}
    uiTiles = {}
    fontSize = 0
