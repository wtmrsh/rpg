#
# Trait factory
#
trait_types = dict()


#
# register_trait_type()
#
def register_trait_type(name, type):
    """
    Register a type for instantiation.

    :param name: name of trait
    :param type: type
    """
    global trait_types
    trait_types[name] = type


#
# get_trait_type()
#
def get_trait_type(name):
    """
    Get a trait type.

    :param name: name of trait
    :return: type to instantiate
    """
    return trait_types[name]


#
# register_trait()
#
def register_trait(name):
    """
    Decorator to register a trait on declaration.

    :param name: name of trait
    :return: wrapped class
    """
    def inner(cls):
        register_trait_type(name, cls)
        return cls
    return inner
