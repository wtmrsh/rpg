from collections import namedtuple

from common import *

import engine.observable as obs
from engine.defines import CompassDirection, EntityCategory, EntityPostTurnResult
from engine.traitfactory import get_trait_type
from engine.visual import Visual
from engine.actionparticipant import ActionParticipant
from engine.colour import Colour
from engine.mapdecorator import owned_by_map
from engine.stateset import StateSet

EntityStateFilter = namedtuple('EntityStateFilter', 'required,rejected,value')


@owned_by_map
class Entity(ActionParticipant):
    """
    Entities are designed to have a shallow inheritance hierarchy, ideally only one subclass deep.
    """

    idgenerator = 1  # 0 is map

    #
    # __init__()
    #
    def __init__(self, name):
        """
        Entity constructor.

        :param name: name
        """
        ActionParticipant.__init__(self, name)

        self.eventidentifier = Entity.idgenerator
        Entity.idgenerator += 1

        self._traitLookup = dict()
        self.visualstatesetters = dict()
        self.states = StateSet()

        # Render offsets within a tile, from the origin
        self.xoffset = 0
        self.yoffset = 0

        self.eventhandlers = list()
        self.owner = None
        self._scriptname = None
        self.updatedturn = 0  # This gets updated to the current turn each time the entity takes a turn.

        self.direction = CompassDirection.NONE

        self.intraturndata = dict()  # Used to store information which can change during a turn

        # Visual
        self.visual = Visual(self, name)

        # Config settings
        self.category = EntityCategory.NONE
        self._name = ''
        self._nameplural = ''
        self.size = None
        self.inventorytype = 'MISC'
        self.rgba = Colour.from_name('white')
        self.factions = set()

        # Blockers
        self.blocksvision = False
        self.blocksaudio = False
        self.blocksmovement = False
        self.blockspathing = False

        # Events
        self.register_event(self, 'Sound.Head', self.on_sound_heard)

    def __str__(self):
        return self.name

    #
    # is_abstract
    #
    @property
    def is_abstract(self):
        """
        Is this entity an abstract one?

        :return: Whether or not the entity is abstract
        """
        return self.category == EntityCategory.ABSTRACT

    #
    # name
    #
    @property
    def name(self):
        if self.has_trait('Collective'):
            if self.collective.count == 1:
                return self.collective.name
            elif 1 < self.collective.count < self.collective.maxCount:
                return self.collective.namePlural
        
        return self.script_name or self._name
    
    #
    # name_plural
    #
    @property
    def name_plural(self):
        if self.has_trait('Collective'):
            if self.collective.count == 1:
                return self.collective.name
            elif 1 < self.collective.count < self.collective.maxCount:
                return self.collective.namePlural

        # As entities with a script name must be unique, this cannot be one if we are requesting the plural of it
        return self._nameplural

    #
    # script_name
    #
    @property
    def script_name(self):
        return self._scriptname

    @script_name.setter
    def script_name(self, value):
        if value == 'SELF':
            raise Exception("Entities cannot be named 'SELF': this is a reserved name to refer to the same Entity in scripts")
        self._scriptname = value

    #
    # x
    #
    @property
    def x(self):
        """
        Entities generally belong to a tile.
        
        :return: x-position of owning object (generally a tile)
        """
        return None if self.is_abstract else self.owning_tile.x

    #
    # y
    #
    @property
    def y(self):
        """
        Entities generally belong to a tile.
        
        :return: y-position of owning object (generally a tile)
        """
        return None if self.is_abstract else self.owning_tile.y
    
    #
    # owning_tile
    #
    @property
    def owning_tile(self):
        obj = self.owner
        while obj:
            if type(obj).__name__ == 'Tile':
                return obj
            obj = obj.owner

        return obj

    #
    # owning_entity
    #
    @property
    def owning_entity(self):
        if self.is_abstract:
            return None
        else:
            obj = self
            while obj.owner:
                if not isinstance(obj.owner, Entity):
                    return obj
                obj = obj.owner

            return obj

    @property
    def visual_tile(self):
        return self.visual.get_visual()

    #
    # indef_name
    #
    def indef_name(self, colour=None, getlength=False):
        """
        Get the indefinite name, ie 'a'/'an' object.  Needs to be overridden for silent 'h's and 'y's.
        
        :param colour: colour to use, or Entity colour if none.
        :param getlength: if True, returns length of name (without formatting)
        :return: formatted string, or plain length
        """
        colourStr = '[color=%i]' % (colour or self.rgba.to_bgra())

        if self.script_name:
            formatStr = '{}{}{}'
        elif self.has_trait('Collective') and 1 < self.collective.count < self.collective.maxCount:
            formatStr = 'some {}{}{}'
        elif self.name.lower()[0] in ['a', 'e', 'i', 'o', 'u']:
            formatStr = 'an {}{}{}'
        else:
            formatStr = 'a {}{}{}'

        if getlength:
            return (len(formatStr) - 6) + len(self.name)
        else:
            return formatStr.format(colourStr, self.name, '[/color]')

    #
    # indef_name_plural
    #
    def indef_name_plural(self, count, colour=None, getlength=False):
        """
        Get the indefinite name, ie 'a'/'an' object.  Needs to be overridden for silent 'h's and 'y's.

        :param count: number of Entities in question
        :param colour: colour to use, or Entity colour if none.
        :param getlength: if True, returns length of name (without formatting)
        :return: formatted string, or plain length
        """
        if count == 1:
            return self.indef_name(colour, getlength=getlength)
        else:
            colourStr = '[color=%i]' % (colour or self.rgba.to_bgra())
            formatStr = '%d {}{}{}' % count

            if getlength:
                return (len(formatStr) - 6) + len(self.name_plural)
            else:
                return formatStr.format(colourStr, self.name_plural, '[/color]')

    #
    # def_name
    #
    def def_name(self, colour=None, getlength=False):
        """
        Get the definite name, ie 'the' object.
        
        :param colour: colour to use, or Entity colour if none.
        :param getlength: if True, returns length of name (without formatting)
        :return: formatted string, or plain length
        """
        colourStr = '[color=%i]' % (colour or self.rgba.to_bgra())

        if self.script_name:
            formatStr = '{}{}{}'
        else:
            formatStr = 'the {}{}{}'

        if getlength:
            return (len(formatStr) - 6) + len(self.name)
        else:
            return formatStr.format(colourStr, self.name, '[/color]')

    #
    # def_name_plural
    #
    def def_name_plural(self, count, colour=None, getlength=False):
        """
        Get the definite name, ie 'the' objects.
        
        :param count: number of Entities in question
        :param colour: colour to use, or Entity colour if none.
        :param getlength: if True, returns length of name (without formatting)
        :return: formatted string, or plain length
        """
        if count == 1:
            return self.def_name(colour, getlength=getlength)
        else:
            colourStr = '[color=%i]' % (colour or self.rgba.to_bgra())
            formatStr = 'the %d {}{}{}' % count

            if getlength:
                return (len(formatStr) - 6) + len(self.name_plural)
            else:
                return formatStr.format(colourStr, self.name_plural, '[/color]')

    #
    # colour
    #
    @property
    def colour(self):
        """
        Get the colour representing this Entity.

        :return: Colour
        """
        return Colour(255, 255, 255)

    #
    # stackable
    #
    @property
    def stackable(self):
        return self.category == EntityCategory.ITEM and not self.script_name

    #
    # manhattan_distance_to
    #
    def manhattan_distance_to(self, entity):
        """
        Returns the distance to target
        :param entity:
        :return:
        """
        if self.is_abstract:
            raise Exception('Entity is abstract')

        return max(abs(self.x - entity.x), abs(self.y - entity.y))

    #
    # performs_actions
    #
    @property
    def performs_actions(self):
        return self.category in [EntityCategory.STANDARD, EntityCategory.ABSTRACT, EntityCategory.WALL]

    #
    # is_mover()
    #
    @property
    def is_mover(self):
        return self.category == EntityCategory.STANDARD

    #
    # check_blocks_movement()
    #
    def check_blocks_movement(self):
        """
        Designed to be overridden by subclasses such as Door, Window, to see if they're closed or not.

        :return: whether or not this entity blocks another from moving here.
        """
        return not self.is_mover

    #
    # blocks_movement()
    #
    def blocks_movement(self):
        if self.category in [EntityCategory.ITEM, EntityCategory.ABSTRACT]:
            return False

        return self.blocksmovement or self.check_blocks_movement()

    #
    # check_blocks_pathing()
    #
    def check_blocks_pathing(self, pathingentity):
        """
        Designed to be overridden by subclasses such as Door, Window, to see if the blocked entity
        is able to path past them

        :param pathingentity: Entity trying to path to the tile this Entity is on.
        :return: whether or not pathingentity can path here.
        """
        return False

    #
    # blocks_pathing()
    #
    def blocks_pathing(self, pathingentity):
        # Players block pathing because they can't be moved out of the way, even if they are technically a 'mover'.
        # But when calculating a path, an entity can generally assume that if a player is in the way, it will move out
        # of the way.  So players only block is the pathing entity is right next to them and wants to move there the
        # next turn.
        if self.is_player and self.next_to(pathingentity, False):
            return True
        elif self.is_mover or self.category in [EntityCategory.ABSTRACT, EntityCategory.WALL, EntityCategory.ITEM]:
            return False
        else:
            return self.blockspathing

    #
    # check_blocks_vision()
    #
    def check_blocks_vision(self):
        """
        Designed to be overridden by subclasses.

        :return: whether or not this entity blocks line of sight.
        """
        return False

    #
    # blocks_vision()
    #
    @property
    def blocks_vision(self):
        return self.blocksvision or self.check_blocks_vision()

    #
    # check_blocks_audio()
    #
    def check_blocks_audio(self):
        """
        Designed to be overridden by subclasses such as Door, Window, to see if they're closed or not.

        :return: whether or not this entity blocks audio.
        """
        return False

    #
    # blocks_audio()
    #
    @property
    def blocks_audio(self):
        return self.blocksaudio or self.check_blocks_audio()

    #
    # fits_inventory()
    #
    @property
    def fits_inventory(self):
        return self.category == EntityCategory.ITEM

    #
    # set_state()
    #
    def set_state(self, state):
        self.states.set_state(state)

    #
    # unset_state()
    #
    def unset_state(self, state):
        self.states.unset_state(state)

    #
    # toggle_state()
    #
    def toggle_state(self, state):
        self.states.toggle_state(state)

    #
    # has_state()
    #
    def has_state(self, state):
        return self.states.is_state_set(state)

    #
    # set_direction()
    #
    def set_direction(self, dx, dy):
        """
        Set direction based on delta movement.
        
        :param dx: x movement 
        :param dy: y movement
        """
        if dx == 0 and dy == 1:
            self.direction = CompassDirection.NORTH
        elif dx == 1 and dy == 1:
            self.direction = CompassDirection.NORTHEAST
        elif dx == 1 and dy == 0:
            self.direction = CompassDirection.EAST
        elif dx == 1 and dy == -1:
            self.direction = CompassDirection.SOUTHEAST
        elif dx == 0 and dy == -1:
            self.direction = CompassDirection.SOUTH
        elif dx == -1 and dy == -1:
            self.direction = CompassDirection.SOUTHWEST
        elif dx == -1 and dy == 0:
            self.direction = CompassDirection.WEST
        elif dx == -1 and dy == 1:
            self.direction = CompassDirection.NORTHWEST

    #
    # weight
    #
    @property
    def weight(self):
        if self.is_abstract:
            return 0

        baseweight = getattr(self.stats, 'weight', 0)
        invweight = self.inventory.used_capacity if self.has_trait('Inventory') else 0

        factor = self.collective.maxCount if self.has_trait('Collective') else 1
        return (baseweight + invweight) / factor

    #
    # has_trait()
    #    
    def has_trait(self, traitType):
        """
        Check whether the Entity has the named trait
        
        :param traitType: name of trait
        :return: True or False
        """
        return traitType in self._traitLookup

    #
    # get_trait()
    #
    def get_trait(self, traitType):
        """
        Return a trait by name.

        :param traitType: name of trait
        :return: trait instance
        """
        return self._traitLookup.get(traitType)

    #
    # remove_trait()
    #
    def remove_trait(self, traittype):
        del self._traitLookup[traittype]

        traitmembername = traittype[0].lower() + traittype[1:]
        traitinst = getattr(self, traitmembername)

        traitinst.unregister_events()
        delattr(self, traitmembername)

    #
    # on_added_to_map()
    #
    def on_added_to_map(self, map):
        if not self.owner:
            raise Exception(f"Entity '{self.name}' does not have an owner.")

        # We can update map animations now
        self.visual.set_visual()

        self.register_events()

        for traitname, traitinst in self._traitLookup.items():
            traitinst.on_added_to_map(map)

    #
    # on_removed_from_map()
    #
    def on_removed_from_map(self, map):
        for traitname, traitinst in self._traitLookup.items():
            traitinst.on_removed_from_map(map)

        self.unregister_events()

    #
    # register_event()
    #
    def register_event(self, topic, event, handler):
        self.eventhandlers.append((topic, event, handler))

    #
    # register_events()
    #
    def register_events(self):
        for (topic, event, handler) in self.eventhandlers:
            obs.on(topic, event, handler)

    #
    # unregister_events()
    #
    def unregister_events(self):
        for (topic, event, handler) in self.eventhandlers:
            obs.off(topic, event, handler)

        # Unregister trait events
        for traitname, traitinst in self._traitLookup.items():
            traitinst.unregister_events()

    #
    # instantiate_traits()
    #
    def instantiate_traits(self):
        """Instantiate all traits."""
        if hasattr(self, '_traitInformation'):
            traitInfo = self._traitInformation
            for memberName, memberInfo in traitInfo.items():
                self.instantiate_trait(memberName, memberInfo)
            del self._traitInformation

    #
    # instantiate_trait()
    #
    def instantiate_trait(self, name, traitinfo):
        """Instantiate a trait instance.

        :param name: instance name
        :param traitinfo: list of type and kwargs
        """
        traitinst = None
        if traitinfo:
            traittype = get_trait_type(traitinfo[1])
            traitinst = traittype(self, **traitinfo[2])
            setattr(self, name, traitinst)
            self._traitLookup[traitinfo[0]] = traitinst

        return traitinst

    #
    # creation_finished()
    #
    def creation_finished(self):
        """
        Called when the entity has been fully set up.  Designed to be overridden
        """
        pass

    #
    # _do_turn()
    #
    def _do_turn(self, curturn):
        """
        Default turn implementation.
        
        :param curturn: turn number
        """
        pass
    
    #
    # turn()
    #
    def turn(self, curturn):
        """
        Gets called once a turn, to update this entity.
        
        :param curturn: turn number
        """     
        # Make sure the entity does not get updated more than once a turn.
        if curturn <= self.updatedturn:
            return

        # Store whether or not this vision current blocks others' sight, so we can record it if this changes, for
        # map sight calculations
        self.intraturndata['blocks_vision'] = self.blocks_vision

        # Invalidate the vision cache
        if self.has_trait('Vision'):
            self.get_vision().reset()

        # Update traits, except intelligence (which is used to choose actions)
        if not self.is_player:

            vision = self.get_vision()
            if vision:
                vision.turn(curturn)

            if self.has_trait('Openable'):
                self.openable.turn(curturn)

        # Turn implementation
        self._do_turn(curturn)

        # Update items in inventory
        if self.has_trait('Inventory'):
            self.inventory.turn(curturn)

        self.updatedturn = curturn

    #
    # choose_action()
    #
    def choose_action(self):
        """
        Choose an action.
        """
        if self.has_trait('Intelligence'):
            self.intelligence.turn(self.updatedturn)

        # Default to passing if nothing chosen.  If it's abstract, or in another entity's (or tile's) inventory,
        # do nothing.
        if self.is_abstract or isinstance(self.owner, Entity):
            self.roundaction = None
        elif not self.roundaction:
            self.request_action('Pass', self.map)

    #
    # post_turn()
    #
    def post_turn(self):
        """
        Called after entity movement has been resolved
        """
        visionchanged = self.blocks_vision != self.intraturndata['blocks_vision']

        # Check if any tasks have completed
        if self.has_trait('Intelligence'):
            for event in ['Complete', 'Aborted']:
                task, status = self.intelligence.taskstatus
                if status == event and event in task.events:
                    eventargs = task.events[event]
                    obs.trigger(self, f'Entity.Intelligence.Task{event}', task.name, *eventargs)

        return EntityPostTurnResult(visionchanged=visionchanged)

    #
    # add_message()
    #
    def add_message(self, playermsg, othermsg=None):
        """
        Add a message to the Entity.
        
        :param playermsg: message to use if Entity is player-controlled
        :param othermsg: message to use if Entity not is player-controlled
        """
        if self.is_player and playermsg:
            obs.trigger(None, 'Game.Message', playermsg, self)
        elif othermsg:
            obs.trigger(None, 'Game.Message', othermsg, self)

    #
    # emit_sound()
    #
    def emit_sound(self, verb, noun, soundfx=None, volume=1.0):
        obs.trigger(self, 'Entity.Sound', verb, noun, soundfx, volume)

    #
    # on_sound_heard()
    #
    def on_sound_heard(self, soundmaker, verb, noun):
        if self.is_player:
            vision = self.get_vision()
            if not (vision or vision.is_tile_in_vision(soundmaker.owning_tile)):
                if noun:
                    obs.trigger(None, 'Game.Message', f'You hear {noun}.')

    #
    # get_vision()
    #
    def get_vision(self):
        """
        Return vision object.

        :return: Vision object.
        """
        if self.has_trait('Vision'):
            return self.vision
        else:
            return None

    def next_to(self, entity, cardinalonly=True):
        if abs(self.x - entity.x) > 1:
            return False
        if abs(self.y - entity.y) > 1:
            return False

        if (self.x == entity.x) ^ (self.y == entity.y):
            return True
        if not cardinalonly:
            return (self.x != entity.x) and (self.y != entity.y)
        else:
            return False

    def max_dim_distance_to(self, entity):
        ax = abs(self.x - entity.x)
        ay = abs(self.y - entity.y)
        return max(ax, ay)
