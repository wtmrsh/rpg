from collections import namedtuple
from enum import IntEnum, IntFlag, auto
from dataclasses import dataclass, field


class EntityCategory(IntEnum):
    NONE = auto()
    STANDARD = auto()
    ABSTRACT = auto()
    WORLD = auto()
    WALL = auto()
    ITEM = auto()


class ActionPriority(IntEnum):
    NoAction = 0
    Action = 1
    Movement = 2
    Player = 3


@dataclass
class ActionPriorityData:
    ptype: ActionPriority
    modifier: int = 0
    elevated: bool = False

    def __post_init__(self):
        if abs(self.modifier) > 99:
            raise ValueError('Priority modifier must be within [-99, 99]')

    @property
    def value(self):
        return self.ptype.value * 200 + 100 + self.modifier


class BodyLimbType(IntEnum):
    NONE = auto()
    HEAD = auto()
    ARM = auto()
    LEG = auto()
    TAIL = auto()
    WING = auto()


class ClothingPartType(IntEnum):
    NONE = auto()
    HEAD = auto()
    TORS = auto()
    LEGS = auto()
    HANDS = auto()
    FEET = auto()


class AlignmentType(IntEnum):
    """Container alignment."""
    NONE = auto()
    CENTRE = auto()
    LEFT = auto()
    TOP = auto()
    RIGHT = auto()
    BOTTOM = auto()


class InputType(IntEnum):
    """Input processing mode."""
    NONE = auto()
    IMMEDIATE = auto()
    DIRECTIONAL = auto()
    TARGETED = auto()


class CompassDirection(IntFlag):
    """Compass direction."""
    NONE = 0
    NORTH = auto()
    NORTHEAST = auto()
    EAST = auto()
    SOUTHEAST = auto()
    SOUTH = auto()
    SOUTHWEST = auto()
    WEST = auto()
    NORTHWEST = auto()


class FactionRelation(IntEnum):
    FRIENDLY = auto()
    HOSTILE = auto()


@dataclass(frozen=True)
class EntityActionDefinition:
    action: str
    actor: None
    target: None
    actortargetpos: tuple
    priority: int
    args: list
    kwargs: dict

    @property
    def is_move_action(self):
        return (self.actor.x, self.actor.y) != self.actortargetpos


@dataclass(frozen=True)
class ActionRequestResult:
    passed: bool
    actionrequired: bool
    playermsg: str = None
    othermsg: str = None
    prereqactions: list = field(default_factory=list)

    @property
    def ok(self):
        return self.passed and self.actionrequired


@dataclass(frozen=True)
class FacilitatorResult:
    success: bool
    actionperformed: bool
    playermsg: str
    othermsg: str

    @property
    def already_done(self):
        return self.success and not self.actionperformed


@dataclass(frozen=True)
class EntityPostTurnResult:
    visionchanged: bool


ActionResult = namedtuple('ActionResult', 'turntaken,printmsg,playermsg,othermsg,inputtype,delayedaction')

