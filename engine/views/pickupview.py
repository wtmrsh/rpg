"""Pickup view."""

#
# Imports
#
from engine.view import View
import engine.observable as obs
from engine.container import HorizontalContainer, VerticalContainer
from engine.views.inventoryview import InventoryView
from engine.views.windowview import WindowView
from engine.tile import Tile

#
# PickupView
#
class PickupView(View):
    """View for choosing what to pick up from the ground."""

    #
    # __init__()
    #
    def __init__(self, viewManager, entity, targetInventory):
        super().__init__('Pickup', 1, viewManager, entity.map)

        self.container = VerticalContainer(0, 0)
        self.invContainer = HorizontalContainer(0, 0)

        self.selectedItem = 0
        self.show = False

        inventoryTypes = [
            'WEAPONS',
            'AMMO',
            'CLOTHING',
            'COMESTIBLES',
            'MEDICINE',
            'MISC'
        ]

        self.targetView = InventoryView(viewManager,
            actions=['Get'],
            actionDecorators=[lambda s, e: s],
            filter='Get',
            entity=entity,
            inventory=targetInventory,
            displayTypeHeadings=True,
            displayNonInteractable=True,
            displayWeights=True,
            displayTitle=False,
            displayStats=False if isinstance(targetInventory.owner, Tile) else True,
            inventoryTypes=inventoryTypes,
            setMinColumns=1,
            setMaxColumns=1,
            setMinRows=20,
            setMaxRows=30,
            footerSize=1,
            setFocus=True)

        self.actorView = InventoryView(viewManager,
            actions = ['Drop'],
            actionDecorators=[lambda s, e: s],
            filter = 'Drop',
            entity = entity,
            inventory = entity.inventory,
            displayTypeHeadings = True,
            displayNonInteractable = True,
            displayWeights=True,
            displayTitle=False,
            displayStats=True,
            inventoryTypes=inventoryTypes,
            setMinColumns = 1,
            setMaxColumns = 1,
            setMinRows = self.targetView.numRows,
            setMaxRows = self.targetView.numRows,
            footerSize=1,
            setFocus=False)

        self.selectedView = self.targetView

        self.textView = WindowView(viewManager, self.targetView.width + self.actorView.width, 2)
        self.textView.add_text(0, 0, '[[Enter]] Accept')
        self.textView.add_text(0, 1, '[[Esc]]   Cancel')
        self.textView.add_text(self.targetView.width, 0, 'Space: %d' % entity.inventory.spare_capacity)
        self.textView.add_text(self.targetView.width, 1, 'Count: %d' % (self.selectedView.actionCount or 1))

        self.invContainer.add_child(self.targetView)
        self.invContainer.add_child(self.actorView)

        self.container.add_child(self.invContainer)
        self.container.add_child(self.textView)

        self.register_events()

    def register_events(self):
        self.targetView.register_events()
        self.actorView.register_events()
        self.textView.register_events()
        obs.on(None, 'Inventory.ItemAdded', self.on_view_changed)
        obs.on(None, 'Inventory.ItemRemoved', self.on_view_changed)
        obs.on(None, 'UI.InventoryView.ActionCountChanged', self.on_action_count_changed)
        obs.on(None, 'UI.InventoryView.ItemHovered', self.on_item_hovered)
        obs.on(None, 'UI.View.MouseEntered', self.on_mouse_changed_inventory)

    def unregister_events(self):
        self.targetView.unregister_events()
        self.actorView.unregister_events()
        self.textView.unregister_events()
        obs.off(None, 'Inventory.ItemAdded', self.on_view_changed)
        obs.off(None, 'Inventory.ItemRemoved', self.on_view_changed)
        obs.off(None, 'UI.InventoryView.ActionCountChanged', self.on_action_count_changed)
        obs.off(None, 'UI.InventoryView.ItemHovered', self.on_item_hovered)
        obs.off(None, 'UI.View.MouseEntered', self.on_mouse_changed_inventory)

    def select_tile_view(self):
        if self.selectedView != self.targetView:

            if len(self.targetView.inventory) > 0:
                self.targetView.set_focus(True)
                self.targetView.rebuild(False)

                self.actorView.set_focus(False)
                self.actorView.rebuild(False)

                self.selectedView = self.targetView

    def select_actor_view(self):
        if self.selectedView != self.actorView:

            if len(self.actorView.inventory) > 0:
                self.actorView.set_focus(True)
                self.actorView.rebuild(False)

                self.targetView.set_focus(False)
                self.targetView.rebuild(False)

                self.selectedView = self.actorView

    def get_action_count(self):
        return self.selectedView.get_action_count()

    def set_action_count(self, actionCount):
        self.selectedView.set_action_count(actionCount)

    def scroll_down(self):
        self.selectedView.scroll_down()

    def scroll_up(self):
        self.selectedView.scroll_up()

    def get_source_view(self):
        return self.selectedView
        
    def get_target_view(self):
        return self.targetView if self.selectedView == self.actorView else self.actorView
        
    def transfer(self, item, count):
        sourceInv = self.get_source_view()
        targetInv = self.get_target_view()

        if item:
            sourceInv.inventory.transfer_entity(item, targetInv.inventory, count)
            sourceInv.set_action_count(None)
            if len(sourceInv.inventory) == 0:
                if sourceInv == self.targetView:
                    self.select_actor_view()
                else:
                    self.select_tile_view()

    def get_item_by_key(self, key):
        sourceinv = self.selectedView
        item = sourceinv.get_item_by_key(key)
        itemcount = sourceinv.get_item_count_by_key(key)
        return item, itemcount

    def set_display_non_interactable(self, display):
        self.selectedView.set_display_non_interactable(display)

    def get_display_non_interactable(self):
        return self.selectedView.get_display_non_interactable()

    def on_added_to_container(self, container):
        container.add_child(self.container)

    def on_view_changed(self, source, inventory, entity, count):
        if inventory == self.actorView.inventory:
            self.textView.set_text(2, 'Space: %d' % self.actorView.inventory.spare_capacity)

    def on_action_count_changed(self, source, inventory, actionCount):
        if inventory == self.selectedView:
            self.textView.set_text(3, 'Count: %d' % (actionCount or 1))

    def on_item_hovered(self, source, view, item):
        if view == self.actorView and self.selectedView != self.actorView:
            self.select_actor_view()
        elif view == self.targetView and self.selectedView != self.targetView:
            self.select_tile_view()

    def on_mouse_changed_inventory(self, source, view, cellX, cellY, pixelX, pixelY):
        if view == self.actorView:
            self.select_actor_view()
        else:
            self.select_tile_view()

    def render(self):
        pass