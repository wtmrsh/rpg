"""Info view."""

#
# Imports
#
from bearlibterminal import terminal

import engine.view
import engine.observable as obs
from engine.settings import GraphicsSettings
from engine.colour import Colour

#
# InfoView
#
class InfoView(engine.view.View):
    """View for showing entity info."""
    
    #
    # __init__()
    #
    def __init__(self, viewManager):
        super().__init__('Info', 2, viewManager)
        self.entity = None
        self.register_events()

    #
    # register_events()
    #
    def register_events(self):
        """Register events."""
        obs.on(None, 'Entity.ItemWorn', self.on_entity_changed)
        obs.on(None, 'Entity.ItemUnworn', self.on_entity_changed)
        obs.on(None, 'Entity.ItemEquipped', self.on_entity_changed)
        obs.on(None, 'Entity.ItemUnequipped', self.on_entity_changed)

    #
    # unregister_events()
    #
    def unregister_events(self):
        """Unregister events."""
        obs.off(None, 'Entity.ItemWorn', self.on_entity_changed)
        obs.off(None, 'Entity.ItemUnworn', self.on_entity_changed)
        obs.off(None, 'Entity.ItemEquipped', self.on_entity_changed)
        obs.off(None, 'Entity.ItemUnequipped', self.on_entity_changed)

    #
    # on_entity_changed()
    #
    def on_entity_changed(self, source, entity, changingEntity):
        """
        Callback for when an Entity puts something on or takes it off, or (un)equips something.

        :param entity: Entity in question
        :param changingEntity: item owned by entity that has changed
        """
        if self.entity == entity:
            obs.trigger(self, 'UI.RefreshView', self)

    #
    # render()
    #
    def render(self):
        """Render info view."""
        defaultColour = Colour.from_name('white').to_bgra()

        terminal.layer(self.base_layer + 1)
        terminal.color(defaultColour)

