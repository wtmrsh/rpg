"""Window view."""

#
# Imports
#
from bearlibterminal import terminal

from common import *

import engine.view
import engine.observable as obs
from engine.colour import Colour


#
# WindowView
#
class WindowView(engine.view.View):
    """Generic view for text."""

    #
    # __init__()
    #
    def __init__(self, viewManager, width, height):
        super().__init__('Window', 2, viewManager, None)
        self.viewWidth = width
        self.viewHeight = height
        self.text = []
        self.register_events()

    def add_text(self, x, y, text):
        self.text.append((x, y, text))
        obs.trigger(self, 'UI.RefreshView', self)

    def set_text(self, index, text):
        textentry = self.text[index]

        self.text[index] = (textentry[0], textentry[1], text)
        obs.trigger(self, 'UI.RefreshView', self)

    def get_width(self):
        return self.viewWidth

    def get_height(self):
        return self.viewHeight

    #
    # render()
    #
    def render(self):

        terminal.layer(self.base_layer + 1)
        
        defaultColour = Colour.from_name('white').to_bgra()
        terminal.color(defaultColour)

        for text in self.text:
            terminal.print(self.client_x + text[0], self.client_y + text[1], text[2])