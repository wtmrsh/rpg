"""Inventory view."""

#
# Imports
#
import string
import itertools
import math
from enum import IntEnum, auto
from collections import namedtuple
from functools import partial

from bearlibterminal import terminal

from common import *

import engine.view
import engine.observable as obs
from engine.utils import *
from engine.button import Button
from engine.colour import Colour
from engine.settings import GraphicsSettings, GameSettings
from engine.defines import ActionResult, InputType
from engine.exceptions import LogicError

#
# InventoryView
#
class InventoryView(engine.view.View):
    """Inventory view for an entity."""

    class SelectionMode(IntEnum):
        ITEM   = auto()
        FILTER = auto()

    StackEntry = namedtuple('StackEntry', 'stack,availablecount,interacts,key')

    #
    # RowData
    #
    class RowData:

        class RowDataType(IntEnum):
            ITEM     = auto()
            HEADER   = auto()
            BLANK    = auto()

        RowEntry = namedtuple('RowEntry', 'tile,key,item,count,interactablecount,offset,reason')

        #
        # __init__()
        #
        def __init__(self, rowNumber, type, entry):
            self.rowNumber = rowNumber
            self.type = type
            self.entry = entry

    #
    # __init__()
    #
    def __init__(self,
                 viewManager,
                 actions,
                 actionDecorators,
                 filter,
                 entity,
                 inventory,
                 displayTypeHeadings,
                 displayNonInteractable,
                 displayWeights,
                 displayTitle,
                 displayStats,
                 inventoryTypes,
                 setMinColumns,
                 setMaxColumns,
                 setMinRows,
                 setMaxRows,
                 footerSize,
                 setFocus,
                 name='Inventory'):
        super().__init__(name, 2, viewManager, entity.map)
        self.actions = actions
        self.actionDecorators = actionDecorators
        self.actionFilter = None
        self.actionFilterDecorator = None
        self.entity = entity
        self.inventory = inventory
        self.displayTypeHeadings = displayTypeHeadings
        self.displayNonInteractable = displayNonInteractable
        self.displayWeights = displayWeights
        self.displayTitle = displayTitle
        self.displayItemTiles = self.viewManager.get_view_setting(self.name, 'show-tiles')
        self.displayStats = displayStats
        self.inventoryTypes = inventoryTypes
        self.sortStackFunc = self.sort_stacks_by_type_name
        self.rowData = []
        self.rowKeys = {}
        self.keyLookup = {}
        self.keyString = '{})'
        self.scrollPosition = 0
        self.columnSplit = 0
        self.headerSize = 0
        self.footerSize = footerSize
        self.actionCount = None
        self.selectionMode = InventoryView.SelectionMode.ITEM if filter else InventoryView.SelectionMode.FILTER
        self.hoverEntry = None
        self.filterButtons = []

        self.set_focus(setFocus)
        self.set_filter(filter, rebuild=False)

        self.calculate_sizing(setMinColumns, setMaxColumns, setMinRows, setMaxRows)

        # Build data structures to be rendered
        self.rebuild(True, True)

        # Colours
        transAmount = int(self.viewManager.get_view_setting(self.name, 'transparency') * 255.0)
        transColour = Colour(255, 255, 255, 255 - transAmount)
        self.footerColour = (Colour.from_bgra(self.viewManager.get_view_setting(self.name, 'colours', 'footer')) * transColour).to_bgra()
        self.statsColour = (Colour.from_bgra(self.viewManager.get_view_setting(self.name, 'colours', 'stats-text')) * transColour).to_bgra()

        self.register_events()

    #
    # register_events()
    #
    def register_events(self):
        # Rerender when inventory we're displaying changes
        obs.on(None, 'Inventory.ItemAdded', self.on_inventory_item_added)
        obs.on(None, 'Inventory.ItemRemoved', self.on_inventory_item_removed)

    #
    # unregister_events()
    #
    def unregister_events(self):
        obs.off(None, 'Inventory.ItemAdded', self.on_inventory_item_added)
        obs.off(None, 'Inventory.ItemRemoved', self.on_inventory_item_removed)

    #
    # get_width()
    #
    def get_width(self):
        return self.numColumns * self.columnWidth + self.vbordersize * 2

    #
    # get_height()
    #
    def get_height(self):
        return self.columnHeight + self.hBorderSize * 2

    #
    # get_max_column_length()
    #
    def get_max_column_length(self):
        return max(len(col) for col in self.rowData)

    #
    # format_action_string()
    #
    def format_action_string(self, action):
        colourLookup = self.viewManager.get_view_setting(self.name, 'colours')

        defaultColour = Colour.from_name('white').to_bgra()
        actionColour = colourLookup['action']
        separatorColour = colourLookup['action-separator']
        hotkeyColour = colourLookup['action-hotkey']
        selectedColour = colourLookup['action-selected']

        ac = selectedColour if self.actionFilter == action else actionColour
        if action[0].isupper():
            return '[color=%d]%c[color=%d]%s[color=%d]' % (hotkeyColour, action[0], ac, action[1:], separatorColour)
        else:
            for i in range(len(action)):
                if action[i].isupper():
                    break
            else:
                raise LogicError('Action does not have a capital letter.')
            return '[color=%d]%s[color=%d]%c[color=%d]%s[color=%d]' % (ac, action[:i], hotkeyColour, action[i], ac, action[1:], separatorColour)

    #
    # on_added_to_container()
    #
    def on_added_to_container(self, container):
        # Calculate where to add filter buttons
        strLength = len(' | '.join(self.actions))

        def set_filter(model, action):
            self.set_filter(action)
            self.mouse_moved(self.mousecellx, self.mousecelly, self.mousepixelx, self.mousepixely)

            return ActionResult(
                turntaken=False,
                printmsg=True,
                playermsg=None,
                othermsg=None,
                inputtype=InputType.IMMEDIATE,
                delayedaction=None)

        self.filterButtons = []
        xPos = (self.client_width - strLength) // 2
        for action in self.actions:
            colouredAction = self.format_action_string(action)
            helpText = 'Select items to %s.' % action.lower()
            keyText = self._get_action_key(action)
            button = Button(self, xPos, 1, 1, colouredAction, helpText, keyText, False, partial(set_filter, action=action))
            button.width = len(action) # Fix width as the coloured text has markup
            button.show = self._display_action_list()

            self.add_button(button, 0)
            self.filterButtons.append(button)

            xPos += len(action) + 3

    #
    # calculate_sizing()
    #
    def calculate_sizing(self, setMinColumns, setMaxColumns, setMinRows, setMaxRows):
        # Calculate column width: tile, key, entity text, space for plural and spaces on next line
        self.columnWidth = (2 if self.displayItemTiles else 0) \
                           + (len(self.keyString) - 1) \
                           + MAX_ENTITY_NAME_LENGTH \
                           + int(math.log(MAX_ENTITIES_PER_STACK, 10)) + 1

        self.weightDisplayOffset = 0
        if self.displayWeights:
            self.weightDisplayOffset = int(math.log(MAX_ENTITY_WEIGHT, 10)) + 1

        self.columnWidth += self.weightDisplayOffset
        self.columnWidth += 3 if self.displayItemTiles else 2

        # Add to column width for decorators
        self.columnWidth += 10

        # Calculate the header height: one space, and actions plus another space if we have more than one action
        self.headerSize = 3 if self._display_action_list() or self.displayTitle else 0

        # Calculate maximum number of columns that can be fit into the view
        self.minColumns = setMinColumns or 1
        self.maxColumns = int((GameSettings.windowWidth - self.vbordersize * 2 - 1) / self.columnWidth)

        # Calculate maximum number of rows that can be displayed in a single column
        self.minRows = setMinRows or 2 if self.displayItemTiles else 1
        self.maxRows = GameSettings.windowHeight - self.hBorderSize * 2 - self.headerSize - self.footerSize

        # Calculate number of actual columns
        if setMinColumns:
            self.minColumns = setMinColumns
        if setMaxColumns:
            self.maxColumns = setMaxColumns
        if setMinRows:
            self.minRows = setMinRows
        if setMaxRows:
            self.maxRows = setMaxRows

    #
    # build_row_data()
    #
    def build_row_data(self, stackTypes):

        # First, create one row of all the eligible entries.
        rowEntries = []
        rowNumber = 0

        # Helpers to build list
        def add_entry(entry):
            nonlocal rowNumber
            rd = InventoryView.RowData(rowNumber, InventoryView.RowData.RowDataType.ITEM, entry)
            rowEntries.append(rd)
            rowNumber += 1
            return rd

        def add_header(typeName):
            nonlocal rowNumber
            rd = InventoryView.RowData(rowNumber, InventoryView.RowData.RowDataType.HEADER, typeName)
            rowEntries.append(rd)
            rowNumber += 1
            return rd

        def add_blank():
            nonlocal rowNumber
            rd = InventoryView.RowData(rowNumber, InventoryView.RowData.RowDataType.BLANK, None)
            rowEntries.append(rd)
            rowNumber += 1
            return rd

        for entityType, stackDataList in stackTypes.items():

            # Header
            if self.displayTypeHeadings:
                add_blank()
                add_header(entityType)

                if not self.displayItemTiles:
                    add_blank()

            # Stacks
            codepage = 0
            for stackData in stackDataList:

                if not self.displayNonInteractable and stackData.interacts[0] == 0:
                    continue

                entry = InventoryView.RowData.RowEntry(
                    tile=codepage + stackData.stack.item.visual_tile if self.displayItemTiles else None,
                    key=stackData.key,
                    item=stackData.stack.item,
                    count=stackData.availablecount,
                    interactablecount=stackData.interacts[0],
                    offset=CELL_SIZE // 2 if self.displayItemTiles else 0,
                    reason=stackData.interacts[1]
                )

                add_entry(entry)

                if self.displayItemTiles:
                    add_blank()

        return rowEntries

    #
    # rebuild()
    #
    def rebuild(self, buildEntryKeys, setSizes=False):

        rowData = self.build_row_data(self.build_stacks(self.actionFilter, None, buildEntryKeys))

        # Now calculate optimal columns/rows
        dataLength = len(rowData)
        minColumnsRequired = int(dataLength / self.maxRows) + 1
        maxColumnsRequired = int(dataLength / self.minRows) + 1

        minRequired = max(minColumnsRequired, self.minColumns)
        maxRequired = min(maxColumnsRequired, self.maxColumns)

        if minRequired > maxRequired:
            scrolling = True
            if setSizes:
                self.numColumns = maxRequired
        else:
            scrolling = False
            if setSizes:
                self.numColumns = minRequired

        if setSizes:
            self.numRows = min(max(self.minRows, dataLength // self.numColumns), self.maxRows)

            # Need an even number of rows for displaying tiles
            if self.displayItemTiles and self.numRows % 2:
                if self.numRows < GameSettings.windowHeight - self.vbordersize * 2 - self.headerSize - self.footerSize - 1:
                    self.numRows += 1
                else:
                    self.numRows -= 1

            self.columnHeight = self.numRows + self.headerSize + self.footerSize

        self.columnSplit = dataLength // (self.numColumns if scrolling else self.numRows)

        # Split rowData: make sure not to leave a header at the bottom of a row
        if self.columnSplit == 0:
            self.rowData = [rowData]
        else:
            self.rowData = [rowData[i:i + self.columnSplit] for i in range(0, len(rowData), self.columnSplit)]

        # If we're displaying tiles, make sure the largest column is a multiple of 2 in length
        if self.displayItemTiles:
            longestColumn, maxLength = -1, 0
            for i, column in enumerate(self.rowData):
                if len(column) > maxLength:
                    maxLength = len(column)
                    longestColumn = i

            # Move the row from the next one along.
            if len(self.rowData[longestColumn]) % 2:
                if longestColumn < len(self.rowData) - 1:
                    self.rowData[longestColumn].append(self.rowData[longestColumn + 1].pop(0))
                else:
                    entry = InventoryView.RowData(maxLength - 1, InventoryView.RowData.RowDataType.BLANK, None)
                    self.rowData[longestColumn].append(entry)

        obs.trigger(self, 'UI.RefreshView', self)

    #
    # build_stacks()
    #
    def build_stacks(self, action, types, buildEntryKeys):

        def interact_count(actor, target, action, actionCount, itemCount):
            if action is None:
                return 0, 'There is no action selected'
            elif not actor.can_be_actor_on(action, target):
                return 0, 'You cannot %s this item.' % action.lower()

            # Check if we can perform the action on this particular type,
            # ignoring how many we are trying to perform it on.
            prereqcheck = actor.check_action_prerequisites(action, target)
            if not prereqcheck.passed:
                return 0, prereqcheck.playermsg

            # Now get the maximum number we can perform it on.
            actorCount = actor.actors[action].get_valid_count(actor, target, actionCount, itemCount)
            targetCount = target.targets[action].get_valid_count(actor, target, actionCount, itemCount)

            if actorCount[0] <= targetCount[0]:
                validCount = actorCount[0]
                reason = actorCount[1]
            else:
                validCount = targetCount[0]
                reason = targetCount[1]

            return validCount, reason

        # Use an intermediate dictionary as StackEntry is a tuple (immutable), so can't set key post-sort
        stackData = [AttributeDict(
            stack=stack,
            availablecount=stack.count,
            interacts=interact_count(self.entity, stack.item, action, self.actionCount or 1, stack.count))
            for stack in self.inventory if types is None or stack.item.inventory in types]

        # Sort, and then generate keys
        stackData = self.sortStackFunc(stackData)

        def get_next_key():
            for key in string.ascii_letters:
                yield key

        keyGen = get_next_key()

        stackList = [InventoryView.StackEntry(
            stack=data.stack,
            availablecount=data.availablecount,
            interacts=data.interacts,
            key=next(keyGen) if buildEntryKeys else self.keyLookup[data.stack.item])
            for data in stackData]

        # Set key lookup
        self.rowKeys = {stack.key: (stack.stack.item, stack.interacts[0]) for stack in stackList}
        self.keyLookup = {stack.stack.item: stack.key for stack in stackList}

        # Split into types
        groupedTypes = {}
        for k, g in itertools.groupby(sorted(stackList,
                                             key=lambda s: self.inventoryTypes.index(s.stack.item.inventorytype)),
                                      key=lambda s: s.stack.item.inventorytype):
            groupedTypes[k] = list(g)

        return groupedTypes

    #
    # sort_stacks_by_type_name()
    #
    def sort_stacks_by_type_name(self, stacks):
        return sorted(stacks, key=lambda stack: (stack.stack.item.inventorytype, stack.stack.item.name))

    #
    # sort_stacks_by_name()
    #
    def sort_stacks_by_name(self, stacks):
        return sorted(stacks, key=lambda stack: stack.stack.item.name)

    #
    # scroll_down()
    #
    def scroll_down(self):
        diff = self.get_max_column_length() - self.numRows

        if diff > 0:
            self.scrollPosition += 2 if self.displayItemTiles else 1
            if self.scrollPosition > diff:
                self.scrollPosition = diff

        obs.trigger(self, 'UI.RefreshView', self)

    #
    # scroll_up()
    #
    def scroll_up(self):
        self.scrollPosition -= 2 if self.displayItemTiles else 1
        if self.scrollPosition < 0:
            self.scrollPosition = 0

        obs.trigger(self, 'UI.RefreshView', self)

    #
    # set_filter()
    #
    def set_filter(self, actionFilter, rebuild=True):
        for i, action in enumerate(self.actions):
            if actionFilter == action:
                for button in self.filterButtons:
                    button.show = False

                self.actionFilter = actionFilter
                self.actionFilterDecorator = self.actionDecorators[i]
                self.selectionMode = InventoryView.SelectionMode.ITEM

                if rebuild:
                    self.rebuild(False)
                return

    #
    # set_filter_by_key()
    #
    def set_filter_by_key(self, key, rebuild=True):
        for i, action in enumerate(self.actions):
            if key == self._get_action_key(action):
                for button in self.filterButtons:
                    button.show = False

                self.actionFilter = action
                self.actionFilterDecorator = self.actionDecorators[i]
                self.selectionMode = InventoryView.SelectionMode.ITEM

                if rebuild:
                    self.rebuild(False)
                return

    #
    # set_selection_mode()
    #
    def set_selection_mode(self, mode):
        if self.selectionMode != mode:
            self.selectionMode = mode
            if self.selectionMode == InventoryView.SelectionMode.FILTER:
                for button in self.filterButtons:
                    button.show = True

                self.actionFilter = None
            else:
                for button in self.filterButtons:
                    button.show = False

            self.rebuild(False)


    #
    # get_action_count()
    #
    def get_action_count(self):
        return self.actionCount

    #
    # set_action_count()
    #
    def set_action_count(self, actionCount):
        self.actionCount = actionCount
        self.rebuild(False)
        obs.trigger(self, 'UI.InventoryView.ActionCountChanged', self, self.actionCount)

    #
    # get_display_non_interactable()
    #
    def get_display_non_interactable(self):
        return self.displayNonInteractable

    #
    # set_display_non_interactable()
    #
    def set_display_non_interactable(self, display):
        self.displayNonInteractable = display
        self.rebuild(False)

    #
    # get_item_by_key()
    #
    def get_item_by_key(self, key):
        if key in self.rowKeys and self._display_entry(self.rowKeys[key]):
            return self.rowKeys[key][0]
        else:
            return None

    #
    # get_item_count_by_key()
    #
    def get_item_count_by_key(self, key):
        if key in self.rowKeys and self._display_entry(self.rowKeys[key]):
            return self.rowKeys[key][1]
        else:
            return None

    #
    # on_inventory_item_added()
    #
    def on_inventory_item_added(self, source, inventory, entity, count):
        if self.inventory == inventory:
            self.rebuild(True)

    #
    # on_inventory_item_removed()
    #
    def on_inventory_item_removed(self, source, inventory, entity, count):
        if self.inventory == inventory:
            self.rebuild(False)

    #
    # _get_action_key()
    #
    def _get_action_key(self, action):
        for i in range(len(action)):
            if action[i].isupper():
                return action[i].lower()

        raise LogicError('Action does not have a key.')

    #
    # _display_action_list()
    #
    def _display_action_list(self):
        return len(self.actions) > 1 and not self.actionFilter

    #
    # _display_entry()
    #
    def _display_entry(self, entry):
        if not self.displayNonInteractable and entry[1] == 0:
            return False
        
        return (self.actionCount is None or self.actionCount <= entry[1]) and entry[1] > 0

    #
    # render_background()
    #
    def render_background(self):
        terminal.layer(self.base_layer)

        if self.client_width == self.width and self.client_height == self.height:
            # No border
            terminal.color(self.backgroundcolour)
            for y in range(self.offset_y, self.extent_y):
                for x in range(self.offset_x, self.extent_x):
                    terminal.put(x, y, self.centrepiece)
        else:
            if self.hasfocus:
                terminal.color(self.focussedcolour)
            else:
                terminal.color(self.bordercolour)

            if self.client_width != self.width and self.client_height != self.height:
                terminal.put(self.client_x - self.hBorderSize, self.client_y - self.vbordersize, self.topleftpiece)
                terminal.put(self.client_extent_x, self.client_y - self.vbordersize, self.toprightpiece)
                terminal.put(self.client_x - self.hBorderSize, self.client_extent_y, self.bottomleftpiece)
                terminal.put(self.client_extent_x, self.client_extent_y, self.bottomrightpiece)

            if self.hBorderSize > 0:
                for x in range(self.client_x, self.client_extent_x):
                    terminal.put(x, self.offset_y, self.toppiece)
                    terminal.put(x, self.client_extent_y, self.bottompiece)

            if self.vbordersize > 0:
                for y in range(self.client_y, self.client_extent_y):
                    terminal.put(self.offset_x, y, self.leftpiece)
                    terminal.put(self.client_extent_x, y, self.rightpiece)

            terminal.color(self.backgroundcolour)
            for y in range(self.client_y, self.client_y + self.headerSize + self.numRows):
                for x in range(self.client_x, self.client_extent_x):
                    terminal.put(x, y, self.centrepiece)

            # Footer
            terminal.color(self.footerColour)
            footerOffset = self.client_y + self.headerSize + self.numRows
            for y in range(footerOffset, self.client_extent_y):
                for x in range(self.client_x, self.client_extent_x):
                    terminal.put(x, y, self.centrepiece)

    #
    # render_entities()
    #
    def render_entities(self):

        colourLookup = self.viewManager.get_view_setting(self.name, 'colours')

        categoryColour = colourLookup['category']
        hotkeyColour = colourLookup['hotkey']
        hoveredColour = colourLookup['hovered']
        defaultColour = Colour.from_name('white').to_bgra()

        terminal.color(defaultColour)
        for columnNum, columnData in enumerate(self.rowData):
            visibleRows = columnData[self.scrollPosition:self.scrollPosition + self.numRows]
            for columnRowNum, rowData in enumerate(visibleRows):

                if rowData.type == InventoryView.RowData.RowDataType.BLANK:
                    continue

                x = self.client_x + self.columnWidth * columnNum
                y = self.client_y + self.headerSize + columnRowNum

                # Check if we've rendered far enough
                #if y >= (self.client_y + self.client_height - self.footerSize):
                if y >= self.client_y + self.headerSize + self.numRows:
                    break

                # Print row entry
                if rowData.type == InventoryView.RowData.RowDataType.HEADER:
                    headerString = '[color=%d]%s' % (categoryColour, rowData.entry)
                    terminal.print(x, y, headerString)
                elif rowData.type == InventoryView.RowData.RowDataType.ITEM:
                    indent = 0
                    entry = rowData.entry

                    # Tile
                    if entry.tile:
                        terminal.put(x + indent, y, entry.tile)
                        indent += 3

                    # Key
                    checkData = (entry.item, entry.interactablecount)
                    if entry.key and self.hasfocus and self._display_entry(checkData):
                        keyStr = '[offset=0,%d][color={}]%s' % (entry.offset, self.keyString)
                        terminal.print(x + indent, y, keyStr.format(hotkeyColour, entry.key))

                    indent += len(self.keyString)

                    # Item
                    isHoveredAndSelectable = self.hoverEntry == entry and entry.interactablecount > 0
                    if entry.interactablecount > 0:
                        itemStr = '[offset=0,%d]%s' % (entry.offset, entry.item.indef_name_plural(entry.count, hoveredColour if isHoveredAndSelectable else None))
                    else:
                        unselectableColour = entry.item.colour.to_greyscale(0.5).to_bgra()
                        itemStr = '[offset=0,%d]%s' % (entry.offset, entry.item.indef_name_plural(entry.count, unselectableColour))

                    # Decorate with action-specifc text
                    if self.actionFilterDecorator:
                        itemStr = self.actionFilterDecorator(itemStr, entry.item)
                    terminal.print(x + indent, y, itemStr)

                    # Weight
                    if self.displayWeights:
                        weightStr = '[offset=0,%d][color=%d]%s' % (entry.offset, defaultColour, str(int(entry.item.weight)).rjust(self.weightDisplayOffset, ' '))
                        terminal.print(x + self.columnWidth - self.weightDisplayOffset, y, weightStr)
                        
                    # Is this the hovered item?
                    #if not isHoveredAndSelectable:
                    #    print(entry.reason)

    #
    # render_action_list()
    #
    def render_action_list(self):
        colourLookup = self.viewManager.get_view_setting(self.name, 'colours')

        defaultColour = Colour.from_name('white').to_bgra()

        actionStr = ' | '.join(self.actions)
        strLength = len(actionStr)

        terminal.color(defaultColour)

        xPos = self.client_x + (self.client_width - strLength) // 2
        yPos = self.client_y + 1
        for i in range(strLength):
            if actionStr[i] == '|':
                terminal.put(xPos + i, yPos, '|')

    #
    # render_title()
    #
    def render_title(self):
        colourLookup = self.viewManager.get_view_setting(self.name, 'colours')

        defaultColour = Colour.from_name('white').to_bgra()
        actionColour = colourLookup['action']

        terminal.color(defaultColour)

        actionCount = self.actionCount or 1
        titleStr = '[color={0}]Select {1} ([color={2}]{4}[color={0}]) to [color={2}]{3}[color={0}]'.format(
            defaultColour,
            'item' if actionCount == 1 else 'items',
            actionColour,
            self.actionFilter.lower(),
            actionCount)

        terminal.print(self.client_x + 1, self.client_y + 1, titleStr)

    #
    # render_footer()
    #
    def render_footer(self):
        footerOffset = self.client_y + self.headerSize + self.numRows

        # Stats
        x = self.client_x
        y = footerOffset

        if self.displayStats:
            terminal.color(self.statsColour)
            terminal.print(x, y, 'Space left: %d' % self.inventory.spare_capacity)
            y += 1

    #
    # render()
    #
    def render(self):
        """Renders an inventory in the given view.
        
        Arguments:
        entity - the entity which will be interacting with this inventory.
        
        """

        # Render entity list
        terminal.layer(self.base_layer + 1)

        if self._display_action_list():
            self.render_action_list()
        elif self.displayTitle:
            self.render_title()

        self.render_entities()

        self.render_footer()

    #
    # on_mouse_entered()
    #
    def on_mouse_entered(self, fromView):
        self.on_mouse_moved(self)

    #
    # on_mouse_exited()
    #
    def on_mouse_exited(self):
        self.hoverEntry = None

    #
    # on_mouse_moved()
    #
    def on_mouse_moved(self, view):

        if view.name == 'Inventory':
            refreshView = False

            # Check items
            hoverEntry = None

            column = (view.mousecellx - view.client_x) // view.columnWidth
            row = view.mousecelly - (view.client_y + view.headerSize) + view.scrollPosition
            if 0 <= column < len(view.rowData):
                columnData = view.rowData[column]
                if 0 <= row < len(columnData):

                    rd = columnData[row]

                    # Adjust for offset if we're displaying tiles
                    if view.displayItemTiles:
                        if rd.type == InventoryView.RowData.RowDataType.BLANK:
                            if row > 0 and columnData[row - 1].type == InventoryView.RowData.RowDataType.ITEM:
                                rd = columnData[row - 1]
                                if (view.mousepixely % CELL_SIZE) > rd.entry.offset:
                                    rd = None
                        elif rd.type == InventoryView.RowData.RowDataType.ITEM:
                            if (view.mousepixely % CELL_SIZE) < rd.entry.offset:
                                rd = None

                    if rd and rd.type == InventoryView.RowData.RowDataType.ITEM:
                        x0 = view.client_x + view.columnWidth * column
                        x1 = x0 + len(view.keyString) + (3 if rd.entry.tile else 0) + rd.entry.item.indef_name_plural(count=rd.entry.count, getlength=True)
                        if view.mousecellx >= x0 and view.mousecellx < x1:
                            hoverEntry = rd.entry

                if hoverEntry and not view.hoverEntry:
                    obs.trigger(self, 'UI.InventoryView.ItemHovered', self, hoverEntry.item)
                    view.tooltiptimer = 0.0
                    refreshView = True
                elif not hoverEntry and view.hoverEntry:
                    obs.trigger(self, 'UI.InventoryView.ItemUnhovered', self, view.hoverEntry.item)
                    view.tooltiptimer = None
                    refreshView = True

                view.hoverEntry = hoverEntry
                if view.hoverEntry and view.hoverEntry.interactablecount > 0:
                    view.cursor = 'Hand'

            return refreshView
        else:
            return False

    #
    # show_tooltip()
    #
    def show_tooltip(self, show):
        wasShown = self.tooltiptext is not None
        if show:
            if self.hoverbutton:
                self.tooltipText = self.hoverbutton.helpText
            elif self.hoverEntry:
                if self.hoverEntry.reason:
                    self.tooltipText = self.hoverEntry.reason
                else:
                    self.tooltipText = '%s this item.' % self.actionFilter
        else:
            self.tooltipText = None

        if show or wasShown:
            obs.trigger(self, 'UI.RefreshView', self)
