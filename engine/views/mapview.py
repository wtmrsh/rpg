import itertools

from bearlibterminal import terminal

from common import *

import engine.debug
import engine.gamemodule
import engine.tiledefinitions
import engine.observable as obs
from engine.cursor import Cursor
from engine.view import View
from engine.colour import Colour
from engine.tiletype import TileType
from engine.pathfinder import PathFinder


class MapViewPlugin:

    def __init__(self):
        pass

    def prepare(self, mapview):
        pass

    def get_tile(self, tile):
        return None


class MapView(View):
    """View to render map onto."""
    
    def __init__(self, viewmanager, keepentitycentred):
        super().__init__('Map', 1, viewmanager)
        self.plugins = list()
        self.entity = None
        self.visionentities = list()
        self.keepentitycentred = keepentitycentred
        self.rendersize = 2
        self.redrawall = True

        gamefactionsmodule = engine.gamemodule.get_game_factions_module()
        self.get_faction_relationship = getattr(gamefactionsmodule, 'get_faction_relationship')

        self.register_events()

    def add_plugin(self, plugin):
        self.plugins.append(plugin)

    def register_events(self):
        obs.on(None, 'Game.TurnTaken', self.on_turn_taken)
        obs.on(None, 'View.SetPovEntity', self.on_set_entity)
        obs.on(None, 'Entity.AddedToMap', self.on_add_entity_to_map)
        obs.on(None, 'Entity.RemovedFromMap', self.on_remove_entity_from_map)
        obs.on(None, 'Entity.Vision.Changed', self.on_entity_vision_changed)

    def unregister_events(self):
        obs.off(None, 'Game.TurnTaken', self.on_turn_taken)
        obs.off(None, 'View.SetPovEntity', self.on_set_entity)
        obs.off(None, 'Entity.AddedToMap', self.on_add_entity_to_map)
        obs.off(None, 'Entity.RemovedFromMap', self.on_remove_entity_from_map)
        obs.off(None, 'Entity.Vision.Changed', self.on_entity_vision_changed)

    def clear(self, view):
        if self.redrawall:
            super().clear(view)
        else:
            return

    def render_background(self):
        terminal.layer(self.base_layer)

        if self.client_width != self.width or self.client_height != self.height:
            if self.hasfocus:
                terminal.color(self.focussedcolour)
            else:
                terminal.color(self.bordercolour)

            if self.client_width != self.width and self.client_height != self.height:
                terminal.put(self.client_x - self.hBorderSize, self.client_y - self.vbordersize, self.topleftpiece)
                terminal.put(self.client_extent_x, self.client_y - self.vbordersize, self.toprightpiece)
                terminal.put(self.client_x - self.hBorderSize, self.client_extent_y, self.bottomleftpiece)
                terminal.put(self.client_extent_x, self.client_extent_y, self.bottomrightpiece)

            if self.hBorderSize > 0:
                for x in range(self.client_x, self.client_extent_x):
                    terminal.put(x, self.offset_y, self.toppiece)
                    terminal.put(x, self.client_extent_y, self.bottompiece)

            if self.vbordersize > 0:
                for y in range(self.client_y, self.client_extent_y):
                    terminal.put(self.offset_x, y, self.leftpiece)
                    terminal.put(self.client_extent_x, y, self.rightpiece)

    #
    # get_render_extents()
    #
    def get_render_extents(self):
        viewcellsw = int(self.client_width / self.rendersize)  # value will be odd
        viewcellsh = int(self.client_height / self.rendersize)  # value will be odd

        x0 = self.entity.x - int(viewcellsw / 2)
        y0 = self.entity.y - int(viewcellsh / 2)

        x1 = x0 + viewcellsw
        y1 = y0 + viewcellsh

        if self.keepentitycentred:
            if x1 > self.entity.map.sizeX:
                x1 = self.entity.map.sizeX

            if y1 > self.entity.map.sizeY:
                y1 = self.entity.map.sizeY
        else:
            if x0 < 0:
                x1 -= x0
                x0 -= x0

            if x1 > self.entity.map.sizeX:
                x0 -= (x1 - self.entity.map.sizeX)
                x1 -= (x1 - self.entity.map.sizeX)

            if y0 < 0:
                y1 -= y0
                y0 -= y0

            if y1 > self.entity.map.sizeY:
                y0 -= (y1 - self.entity.map.sizeY)
                y1 -= (y1 - self.entity.map.sizeY)

        return x0, y0, x1, y1

    #
    # render_non_visible()
    #
    def render_non_visible(self, tile, x, y, xt, yt, colour, codepage, blackoffset):
        if tile.playerSeen:
            terminal.color(colour)
            terminal.put(xt, yt, codepage + blackoffset)
        else:
            terminal.color(colour)
            terminal.put(xt, yt, codepage + blackoffset)

    #
    # render()
    #
    def render(self):
        """Render map."""
        if not (self.entity and self.entity.has_trait('Vision')):
            return

        map = self.entity.map
        vision = self.entity.get_vision()

        # Cache offsets and tiles
        codepage = 0

        blackoffset = engine.tiledefinitions.definitions['Space']['Stars0']['_'][0].offset
        visToHostileOffset = engine.tiledefinitions.definitions['VisibleToHostile']['_']['_'][0].offset

        # Cache colours
        whitecolour = Colour.from_name('white')
        whitebgra = whitecolour.to_bgra()

        viscount = 0
        ox = cx = self.client_x
        oy = cy = self.client_y

        # Set up plugins
        for plugin in self.plugins:
            plugin.prepare(self)

        # Calculate bounds for rendering
        x0, y0, x1, y1 = self.get_render_extents()

        if x0 < 0:
            ox -= x0 * self.rendersize
            x0 = 0

        if y0 < 0:
            oy -= y0 * self.rendersize
            y0 = 0

        terminal.layer(self.base_layer)

        xtbase = ox
        ytbase = cy + self.client_height - (oy + self.rendersize)

        # Generate list of tiles to render
        if len(map.changedanimatedvisuals) == 0 or self.redrawall:
            locations = itertools.product(range(x0, x1), range(y0, y1))
            self.redrawall = False
        else:
            locations = [(visual.owner.x, visual.owner.y) for visual in map.changedanimatedvisuals if x0 <= visual.owner.x < x1 and y0 <= visual.owner.y < y1]

        usefullvis = engine.debug.ShowFullMap or map.globalproperties.get('ShowFullMap', False)
        for xy in locations:
            x = xy[0]
            y = xy[1]

            if x < x0 or x >= x1 or y < y0 or y >= y1:
                continue

            xt = xtbase + (x - x0) * self.rendersize
            yt = ytbase - (y - y0) * self.rendersize

            tile = map.tiles[x][y]
            tiledistsq = (x - self.entity.x) * (x - self.entity.x) + (y - self.entity.y) * (y - self.entity.y)
            visible = vision.viewdistance * vision.viewdistance >= tiledistsq and vision.is_tile_in_vision(tile)

            if visible or usefullvis:
                viscount += 1

                tile.playerSeen = True

                if engine.debug.ShowFullyLit:
                    colourModifier = whitebgra
                else:
                    colourModifier = tile.ambientLight

                colourmodbgra = colourModifier.to_bgra()

                # World objects
                if tile.type == TileType.DOOR:
                    terminal.color(colourmodbgra)
                    terminal.put(xt, yt, codepage + tile.visual_tile)
                    terminal.put(xt, yt, codepage + tile.door.visual_tile)
                elif tile.type == TileType.WINDOW:
                    terminal.color(colourmodbgra)
                    terminal.put(xt, yt, codepage + tile.visual_tile)
                    windowcolour = tile.ambientLight
                    terminal.color(windowcolour.to_bgra())
                    terminal.put(xt, yt, codepage + tile.window.visual_tile)
                else:
                    terminal.color(colourmodbgra)
                    terminal.put(xt, yt, codepage + tile.visual_tile)

                    # Overlay
                    if tile.overlayTile is not None:
                        if tile.overlayOpacity != 1.0:
                            overlaycolour = Colour(
                                colourModifier.r,
                                colourModifier.g,
                                colourModifier.b,
                                int(colourModifier.a * tile.overlayOpacity))
                            terminal.color(overlaycolour.to_bgra())

                        terminal.put(xt, yt, codepage + tile.overlayTile)

                    # Wall
                    if tile.wallTile is not None:
                        terminal.color(colourModifier.to_bgra())
                        terminal.put(xt, yt, codepage + tile.wallTile)

                    # Wall objects
                    if tile.type == TileType.WALL:
                        if tile.wallObjects[0] and self.entity.y > y:
                            terminal.put(xt, yt, codepage + tile.wallObjects[0].visual_tile)
                        if tile.wallObjects[1] and self.entity.x > x:
                            terminal.put(xt, yt, codepage + tile.wallObjects[1].visual_tile)
                        if tile.wallObjects[2] and self.entity.y < y:
                            terminal.put(xt, yt, codepage + tile.wallObjects[2].visual_tile)
                        if tile.wallObjects[3] and self.entity.x < x:
                            terminal.put(xt, yt, codepage + tile.wallObjects[3].visual_tile)

                # Render entities in the world first
                terminal.color(colourmodbgra)
                if tile.inventory and len(tile.inventory):
                    # Render top entity
                    te = tile.inventory.stacks[0].item
                    terminal.put_ext(xt, yt, te.xoffset, -te.yoffset, codepage + te.visual_tile)
                if tile.entity:
                    te = tile.entity
                    terminal.put_ext(xt, yt, te.xoffset, -te.yoffset, codepage + te.visual_tile)

                # Plugins
                for plugin in self.plugins:
                    t = plugin.get_tile(tile)
                    if t is not None:
                        terminal.put(xt, yt, codepage + t)
            else:
                # Not visible
                self.render_non_visible(tile, x, y, xt, yt, whitebgra, codepage, blackoffset)

    #
    # set_entity()
    #
    def set_entity(self, entity):
        self.entity = entity
        self.set_dirty(self)
        self.redrawall = True

    #
    # on_mouse_moved()
    #
    def on_mouse_moved(self, view):
        return False

    #
    # on_turn_taken()
    #
    def on_turn_taken(self, source, turnnumber):
        self.set_dirty(self)
        self.redrawall = True

    #
    # on_set_entity()
    #
    def on_set_entity(self, source, entity):
        self.set_entity(entity)

    def on_add_entity_to_map(self, source, entity, map):
        if entity.has_trait('Vision'):
            self.visionentities.append(entity)

    def on_remove_entity_from_map(self, source, entity, map):
        if entity.has_trait('Vision'):
            self.visionentities.remove(entity)

    def on_entity_vision_changed(self, source, entity, vision):
        if entity in self.visionentities:
            self.set_dirty(self)
            self.redrawall = True
