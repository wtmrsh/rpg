"""Message view."""

#
# Imports
#
from bearlibterminal import terminal

from common import *

import engine.view
import engine.observable as obs
from engine.colour import Colour


#
# MessageView
#
class MessageView(engine.view.View):
    """View for showing game messages."""
    
    def __init__(self, viewmanager):
        super().__init__('Message', 3, viewmanager)
        self.messages = list()
        self.entity = None
        self.register_events()

    def register_events(self):
        obs.on(None, 'Game.Message', self.add_message)
        obs.on(None, 'Entity.SetPlayer', self.on_set_player)

    def unregister_events(self):
        obs.off(None, 'Game.Message', self.add_message)
        obs.off(None, 'Entity.SetPlayer', self.on_set_player)

    def on_added_to_container(self, container):
        terminal.layer(self.base_layer + 2)
        self.crop()

    def add_message(self, source, msg, fromentity=None):
        """Add a message to be shown.
        
        Arguments:
        msg - message
        """
        if not self.entity:
            return

        entityok = fromentity is None or fromentity == self.entity
        if entityok or (self.entity.get_vision() and fromentity in self.entity.get_vision().visible_entities):
            if not msg[0].isupper():
                msg = msg[0].upper() + msg[1:]

            self.messages.append(msg)
            obs.trigger(self, 'UI.RefreshView', self)

    def set_entity(self, entity):
        self.entity = entity

    def on_set_player(self, source, entity, isplayer):
        """
        Callback for when the player changes.  This is so we can work out whether to display
        messages from non-player entities.

        :param entity: Entity in question
        :param isplayer: whether or not they are the player
        """
        self.set_entity(entity if isplayer else None)

    def render(self):
        """Render message view."""
        msg_count = self.client_height
        draw_msgs = self.messages[-msg_count:]

        terminal.layer(self.base_layer + 2)
        self.crop(self.client_x, self.client_y, self.client_width, self.client_height)

        renderpos = self.client_height
        for i, msg in enumerate(reversed(draw_msgs)):
            msgsize = terminal.measure(msg, self.client_width, self.client_height)
            renderpos -= msgsize[1]

            shade = max(128, 255 - i * 24)
            terminal.color(Colour(shade, shade, shade).to_bgra())
            terminal.print(self.client_x, self.client_y + renderpos, msg, self.client_width, self.client_height)
