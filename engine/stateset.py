from collections import defaultdict


class StateSetCallback:

    def __init__(self, rule, func):
        self.rule = rule
        self.function = func

    def matches(self, curstates):
        for state in self.rule:
            if state[0] == '-':
                if state[1:] in curstates:
                    return False
            else:
                if state not in curstates:
                    return False

        return True

    def evaluate(self, curstates):
        if self.matches(curstates):
            self.function()
            return True
        else:
            return False


class StateSetHandler:

    def __init__(self, matchall, requirematch):
        self.callbacks = list()
        self.fallback = None
        self.matchall = matchall
        self.requirematch = requirematch

    def add_callback(self, rule, func):
        callback = StateSetCallback(rule, func)
        self.callbacks.append(callback)

    def set_fallback(self, func):
        self.fallback = func

    def check_callbacks(self, curstates):
        matched = False
        for callback in self.callbacks:
            if callback.evaluate(curstates):
                matched = True
                if not self.matchall:
                    break

        if not matched:
            if self.requirematch:
                raise Exception('StateSet callback required a match but none succeeded')
            elif self.fallback:
                self.fallback()


class StateSetHandlerSet:

    def __init__(self):
        self.handlers = defaultdict(list)

    def add_handlers(self, name, handlers):
        self.handlers[name].append(handlers)

    def check_handlers(self, state, curstates):
        handlers = self.handlers[state]

        for handler in handlers:
            handler.check_callbacks(curstates)


class StateSet:

    def __init__(self):
        self.states = set()
        self.set_named_handlers = StateSetHandlerSet()
        self.unset_named_handlers = StateSetHandlerSet()
        self.set_all_handlers = StateSetHandlerSet()
        self.unset_all_handlers = StateSetHandlerSet()

    def add_handlers(self, state=None, sethandlers=None, unsethandlers=None):
        if sethandlers:
            if state is not None:
                self.set_named_handlers.add_handlers(state, sethandlers)
            else:
                self.set_all_handlers.add_handlers(state, sethandlers)
        if unsethandlers:
            if state is not None:
                self.unset_named_handlers.add_handlers(state, unsethandlers)
            else:
                self.unset_all_handlers.add_handlers(state, unsethandlers)

    def check_callbacks(self, state, handlers):
        handlers.check_handlers(state, self.states)

    def is_state_set(self, state):
        return state in self.states

    def set_state(self, state, force=False):
        already_set = self.is_state_set(state)
        self.states.add(state)

        if not already_set or force:
            self.check_callbacks(None, self.set_all_handlers)
            self.check_callbacks(state, self.set_named_handlers)

    def set_states(self, *states):
        not_already_set = list()

        for state in states:
            if not self.is_state_set(state):
                not_already_set.append(state)

            self.states.add(state)

        if len(not_already_set) != 0:
            self.check_callbacks(None, self.set_all_handlers)

        for state in not_already_set:
            self.check_callbacks(state, self.set_named_handlers)

    def unset_state(self, state, force=False):
        already_unset = not self.is_state_set(state)
        self.states.discard(state)

        if not already_unset or force:
            self.check_callbacks(None, self.unset_all_handlers)
            self.check_callbacks(state, self.unset_named_handlers)

    def unset_states(self, *states):
        not_already_unset = list()

        for state in states:
            if not self.is_state_unset(state):
                not_already_unset.append(state)

            self.states.discard(state)

        if len(not_already_unset) != 0:
            self.check_callbacks(None, self.unset_all_handlers)

        for state in not_already_unset:
            self.check_callbacks(state, self.unset_named_handlers)

    def toggle_state(self, state):
        if self.is_state_set(state):
            self.unset_state(state)
        else:
            self.set_state(state)
