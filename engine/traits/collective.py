"""
Trait system: Collective entity.

Formatted 23/09
"""

#
# Imports
#
import engine.trait as trait
from engine.traitfactory import register_trait


#
# Collective
#
@register_trait('Collective')
class Collective(trait.Trait):
    """
    Trait for entities which are composed of more than one item.

    Examples would be a pair of boots, gloves, skis, etc.
    """
    
    #
    # __init__()
    #
    def __init__(self, owner, count, name, nameplural):
        """
        Constructor.

        :param owner: entity that has this trait.
        :param count: number of items comprimising a standard Entity
        :param name: name of one of these items
        :param nameplural: name of multiple of these items (when less than count)
        """
        super().__init__(owner)
        self.maxCount = count
        self.count = count
        self.name = name
        self.namePlural = nameplural

    #
    # full_set()
    #
    def is_full_set(self):
        return self.count == self.maxCount
