"""
Trait system: Inventory.

Formatted 01/22
"""
from collections import namedtuple

import engine.trait as trait
import engine.action as action
import engine.observable as obs
from engine.traitfactory import register_trait
from engine.exceptions import LogicError


class InventoryStack:
    """Holds one type of entity."""

    View = namedtuple('View', 'item,count')
    
    def __init__(self, item, count):
        """
        Constructor.
        
        :param item: Entity.
        :param count: number of Entities to add.
        """
        self.item = item
        self.count = count

    def __str__(self):
        """
        String representation.
        """
        if self.count == 1:
            return self.item.indef_name()
        else:
            return self.item.indef_name_plural(self.count)
          
    def add(self, count):
        """
        Add to a stack.
        
        :param count: number of items to add.
        """
        self.count += count
        
    def remove(self, count):
        """
        Remove from a stack.
        
        :param count: number of items to remove.
        """        
        self.count -= count
          
    @property
    def weight(self):
        """
        Get stack weight.
        """
        return self.count * self.item.weight
    

@register_trait('Inventory')
class Inventory(trait.Trait):
    """
    Inventory.  Holds a list of items (entities), up to a certain capacity.
    """
    
    def __init__(self, owner, capacity=1, searchable=False):
        """
        Initialise the inventory with a given capacity.

        :param owner: owning Entity.
        :capacity: maximum capacity (can be increased later).
        :searchable: whether or not this inventory can be searched.
        """
        super().__init__(owner)
        
        if capacity < 0:
            raise LogicError('Inventory capacity cannot be negative!')

        self.maxCapacity = capacity
        self.searchable = searchable

        self.cacheUsedCapacity = True
        self.usedCapacity = 0

        # TODO: stack should really be a dictionary of type to count
        self.stacks = list()

        # Add search target
        #if searchable:
        #    action.target_inst(owner, 'Search')

    def deepcopy(self):
        """
        Make a copy of the inventory.

        :return
        """
        inv = Inventory(self.owner, None, self.maxCapacity, self.searchable)

        for stack in self:
            inv.add_entity(stack.item, stack.count)

        return inv

    def __iter__(self):
        """
        Return an iterator for the inventory.
        This will be invalidated if items are added or removed while it is in use.

        :return: iterator.
        """
        return InventoryIterator(self)
        
    def __len__(self):
        """
        Total number of items in inventory.

        :return: total number of items in inventory.
        """
        return sum(s.count for s in self.stacks)
        
    def __str__(self):
        """
        String representation.

        :return: string representation.
        """
        slen = len(self.stacks)
        if slen == 0:
            return 'nothing'
        elif slen == 1:
            return str(self.stacks[0])
        elif slen == 2:
            return '%s and %s' % (str(self.stacks[0]), str(self.stacks[1]))
        elif slen == 3:
            msg = str(self.stacks[0])
            for i in range(slen - 2):
                msg += ', %s' % str(self.stacks[i + 1])
            msg += ' and %s' % str(self.stacks[slen - 1])
            return msg
        else:
            return 'many items'

    @property
    def spare_capacity(self):
        """
        Gets the remaining amount of capacity (in game weight units) that this inventory can hold.

        :return: space capacity.
        """
        return self.maxCapacity - self.used_capacity

    @property
    def used_capacity(self):
        """
        Gets the used amount of this inventory.

        :return: used space.
        """
        if self.cacheUsedCapacity:
            self.usedCapacity = sum(stack.weight for stack in self.stacks)
            self.cacheUsedCapacity = False

        return self.usedCapacity

    @property
    def unique_entity_types(self):
        """
        Get unique entity types.

        :return: the number of unique entity types in this inventory.
        """
        return len(self.stacks)

    def resize_capacity(self, change, forcereduction=False, suppressevent=False):
        """
        Change capacity by a given amount.  If we are reducing the capacity to a point where we are overburdened,
        then do not reduce it, unless specified that we should reduce it as much as possible.
        
        :param change: amount to change by.
        :param forcereduction: if True, and we are reducing capacity, then reduce as much as possible.
        :param suppressevent: if True, then don't fire off event.
        :return: new capacity.
        """
        newcapacity = self.maxCapacity + change
        if self.used_capacity > newcapacity:
            if forcereduction:
                newcapacity = self.used_capacity
            else:
                return self.maxCapacity

        self.maxCapacity = newcapacity

        if not suppressevent:
            obs.trigger(self, 'Inventory.CapacityChanged', self)

        return self.maxCapacity

    def _add_entity(self, entity, count, suppressevent, newstack=False):
        """
        Internal helper to add an entity to the inventory.

        :param entity: Entity to add.
        :param count: number to add.
        :param suppressevent: if True, then don't fire off event.
        :param newstack: whether to add to a new stack.
        :return: the added entity.
        """
        entity.owner = self.owner

        # Do we need to create a new stack for this?
        stacks = [stack for stack in self.stacks if stack.item.actorType == entity.actorType and entity.stackable]
        if entity.has_trait('Collective'):
            stacks = list(filter(lambda s: s.item.collective.is_full_set(), stacks))

        if len(stacks) == 0 or newstack:
            self.stacks.append(InventoryStack(entity, count))
            added = entity
        else:
            stacks[0].add(count)
            added = stacks[0].item

        self.cacheUsedCapacity = True

        if not suppressevent:
            obs.trigger(self, 'Inventory.ItemAdded', self, entity, count)

        return added

    #
    # add_entity()
    #
    def add_entity(self, entity, count, suppressevent=False):
        """
        Add an Entity to the inventory.

        :param entity: Entity to add.
        :param count: number of the Entity to add.
        :param suppressevent: if True, then don't fire off event.
        :return: the added entity.
        """
        if (entity.weight * count) > self.spare_capacity:
            raise LogicError(f'Could not add entity {entity} to inventory as it is too heavy')

        if not entity.has_trait('Collective'):
            return self._add_entity(entity, count, suppressevent)
        else:
            if entity.collective.is_full_set():
                return self._add_entity(entity, count, suppressevent)
            else:
                # Get all stacks of same actorType which are not complete,
                # ignore whether it's stackable or not at this point
                incompletestacks = [stack for stack in self.stacks
                                    if entity.actorType == stack.item.actorType 
                                    and not stack.item.collective.is_full_set()]

                if len(incompletestacks) == 0:
                    # No incomplete stacks to merge with
                    return self._add_entity(entity, count, suppressevent, True)
                else:
                    workstack = incompletestacks[0]
                    workstack.item.collective.count += entity.collective.count
                    entity.collective.count = 0

                    if workstack.item.collective.count > workstack.item.collective.maxCount:
                        entity.collective.count = workstack.item.collective.count - workstack.item.collective.maxCount
                        workstack.item.collective.count = workstack.item.collective.maxCount
                        return self._add_entity(entity, count, suppressevent)
                    elif workstack.item.collective.is_full_set():
                        if entity.stackable:
                            # See if it can be merged
                            completestacks = [stack for stack in self.stacks
                                              if entity.actorType == stack.item.actorType 
                                              and stack.item.collective.is_full_set() 
                                              and stack != workstack]
                            
                            if len(completestacks) > 0:
                                completestacks[0].add(count)
                                self.stacks.remove(workstack)
                                return completestacks[0].item
                            else:
                                return workstack.item
                        elif entity.collective.count > 0:
                            return self._add_entity(entity, count, suppressevent)
                        else:
                            return workstack.item
                    else:
                        return workstack.item

    def remove_entity(self, entity, count, suppressevent=False):
        """
        Remove an Entity from the inventory.

        :param entity: Entity to remove.
        :param count: number of the Entity to remove.
        :param suppressevent: if True, then don't fire off event.
        """
        stacks = [stack for stack in self.stacks if stack.item.name == entity.name]
        stacks[0].remove(count)
        if stacks[0].count == 0:
            self.stacks.remove(stacks[0])

        self.cacheUsedCapacity = True

        if not suppressevent:
            obs.trigger(self, 'Inventory.ItemRemoved', self, entity, count)

    def transfer_entity(self, entity, targetinv, count, suppressevent=False):
        """
        Transfer an Entity to another inventory.

        :param entity: Entity to transfer.
        :param targetinv: target Inventory.
        :param count: number of the Entity to transfer.
        :param suppressevent: if True, then don't fire off event.
        """
        # Don't let the inventories fire off their changed events until both have changed
        self.remove_entity(entity, count, True)
        added = targetinv.add_entity(entity, count, True)

        if not suppressevent:
            obs.trigger(self, 'Inventory.ItemRemoved', self, entity, count)
            obs.trigger(self, 'Inventory.ItemAdded', targetinv, added, count)

        return added

    def transfer_all_entities(self, targetinv, suppressevent=False):
        """
        Transfer all items to another inventory.

        :param targetinv: target Inventory.
        :param suppressevent: if True, then don't fire off event.
        """
        while len(self.stacks) > 0:
            stack = self.stacks[-1]
            try:
                self.transfer_entity(stack.item, targetinv, stack.count, suppressevent)
            except LogicError:
                return

    def get_entity_count_by_name(self, entityname):
        """
        Get the number of entities of the given type name in this inventory.

        :param entityname: name of Entity.
        :return: number stored in inventory.
        """
        for stack in self.stacks:
            if stack.item.actorType == entityname:
                return stack.count

        return 0

    def get_entity_count_by_class(self, entitytype):
        """
        Get the number of entities of the given class in this inventory.

        :param entitytype: type of Entity.
        :return: number stored in inventory.
        """
        for stack in self.stacks:
            if type(stack.item) == entitytype:
                return stack.count

        return 0

    def get_entity_count_by_type(self, inventorytype):
        """
        Get the number of entities of the given type in this inventory.

        :param inventorytype: type of Entity.
        :return: number stored in inventory.
        """
        for stack in self.stacks:
            if stack.item.inventorytype == inventorytype:
                return stack.count

        return 0

    def get_entity_count_by_action_targets(self, actor, action):
        """
        Count the number of entities in the Inventory that can have an action performed on them.

        :param actor: Entity to perform action
        :param action: action to perform
        :return: count
        """
        return sum(1 for stack in self.stacks if stack.item.can_be_target_of(action, actor))

    def get_entities_by_type(self, type):
        """
        Get stacks of the given entity type.

        :param type: inventory type
        :return: list of stacks
        """
        return [stack for stack in self if stack.item.inventorytype == type]

    def get_key_codes(self):
        """
        Return all codes stored by keys in this inventory.
        
        :return: set of integer codes.
        """
        allkeys = [stack.item.key.codes for stack in self.stacks if stack.item.has_trait('Key')]
        return {code for subset in allkeys for code in subset}

    #
    # turn()
    #
    def turn(self, curturn):
        """
        Update items in inventory.

        :param curturn: current turn.
        """
        for stack in self.stacks:
            stack.item.turn(curturn)


class InventoryIterator:
    """
    Simple iterator class to iterate over an inventory, returning items in order.
    """
    
    def __init__(self, inv):
        """
        Initialise iterator with inventory.

        :param inv: Inventory to iterate over.
        """
        self.current = 0
        self.inventory = inv

    def __next__(self):
        """
        Required for an iterator.  Return next element in sequence.

        :return: next element in iterator sequence, or StopIteration.
        """
        if self.current >= len(self.inventory.stacks):
            raise StopIteration
        else:
            stack = self.inventory.stacks[self.current]
            self.current += 1
            return InventoryStack.View(item=stack.item, count=stack.count)
