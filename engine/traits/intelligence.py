import random
from collections import defaultdict
from copy import deepcopy

import engine.action as action
import engine.trait as trait
from engine.traitfactory import register_trait
import engine.intelligencetask
import engine.gamemodule


@register_trait('Intelligence')
@action.exposes_target('SetIntelligenceTask')
class Intelligence(trait.Trait):

    class TaskInstance:

        def __init__(self, name, taskhandlers):
            """
            Initializer.
            """
            self.name = name
            self.taskhandlers = taskhandlers
            self.tasks = list()
            self.events = dict()
            self.parameters = dict()

            self.curtask = -1
            self.loopcount = -1
            self.curloopcount = -1
            self.statictrans = defaultdict(list)
            self.dynamictrans = list()
            self.onrequestfailure = None
            self.onexecutionfailure = None

        def add_subtask(self, subtask, supertask, entity):
            """
            Add a subtask.

            :param subtask: subtask definition.
            :param supertask: owning supertask.
            :param entity: owning entity.
            """
            tasktype = subtask['Type']
            targetname = subtask.get('Target')

            # If x and y are not in the task parameters, take the previous task's values
            taskparams = subtask.get('Params', dict())
            taskparams.setdefault('x', self.tasks[-1].x if len(self.tasks) > 0 else None)
            taskparams.setdefault('y', self.tasks[-1].y if len(self.tasks) > 0 else None)

            # Get task dictionary
            try:
                if tasktype in self.taskhandlers:
                    typefact = engine.intelligencetask.intelligence_task_types[self.taskhandlers[tasktype]]
                else:
                    typefact = engine.intelligencetask.intelligence_task_types[tasktype]
            except KeyError:
                if tasktype not in action.actor_types:
                    raise Exception(f"No AI task or action named '{tasktype}' registered.")
                else:
                    typefact = engine.intelligencetask.intelligence_task_types['_']

            t = typefact(supertask=self, actionname=tasktype, entity=entity, targetname=targetname, **taskparams)

            # Set handlers
            t.onrequestfailure = self.parse_handler(subtask.get('OnRequestFailure'), supertask) or deepcopy(self.onrequestfailure)
            t.onexecutionfailure = self.parse_handler(subtask.get('OnExecutionFailure'), supertask) or deepcopy(self.onexecutionfailure)

            self.tasks.append(t)

        @staticmethod
        def parse_handler(definition, supertaskdef):
            if definition:
                retrycount = definition.get('RetryCount', 0)
                continuetonext = definition.get('Continue', False)
                changetask = definition.get('ChangeTask', None)

                if continuetonext and changetask:
                    raise Exception("Task definition cannot specify both 'Continue' and a ChangeTask.")

                if not changetask:
                    continuetonext = True

                if changetask and changetask not in supertaskdef:
                    raise Exception(f"ChangeTask '{changetask}' not found in definition.")

                return engine.intelligencetask.IntelligenceTaskHandler(retrycount, continuetonext, changetask)
            else:
                return None

        def set_request_failure(self, definition, supertaskdef):
            self.onrequestfailure = self.parse_handler(definition, supertaskdef)

        def set_execution_failure(self, definition, supertaskdef):
            self.onexecutionfailure = self.parse_handler(definition, supertaskdef)

        def set_event(self, name, args):
            """
            Add arguments for a given event to fire.

            :param name: event handler name (eg success or failure, etc)
            :param args: arguments for the event
            """
            self.events[name] = args

        def set_loopcount(self, loopcount):
            """
            Set the number of times to loop the subtasks.

            :param loopcount: count.  Less than zero for infinite.
            """
            self.loopcount = loopcount

        def add_static_transition(self, callback, task, parameters):
            """
            Add a predefined static transition.

            :param callback: name of static transition.
            :param task: task to move to.
            :param parameters: task parameters
            """
            self.statictrans[callback].append((task, parameters))

        def add_dynamic_transition(self, funcname, target):
            """
            Add a function to check for dynamic transitions.

            :param funcname: function.
            :param target: task to move to.
            """
            dtransmodule = engine.gamemodule.get_game_dynamic_trans_module()
            f = getattr(dtransmodule, funcname)
            self.dynamictrans.append((f, target))

        def reset(self, parameters=None):
            """
            Reset this task.
            """
            self.parameters = parameters or dict()
            self.curloopcount = self.loopcount
            self.curtask = 0
            self.tasks[self.curtask].reset()

        def check_dynamic_transitions(self, entity):
            """
            See if any transition functions pass.  Take the first passing one.

            :param entity: entity to call on
            :return: name of task to move to, or None
            """
            for tran in self.dynamictrans:
                tranfunc, trantarget = tran
                res, nexttaskparams = tranfunc(entity)
                if res:
                    return trantarget, nexttaskparams

            return None, None

        def turn(self, curturn):
            """
            Run this task's turn.

            :param curturn: current turn.
            """
            t = self.tasks[self.curtask]

            complete, changetask = t.turn(curturn)
            while complete:
                if changetask:
                    # The subtask has failed and we're requesting we abort this supertask
                    return True, 'Aborted', changetask

                # The task did not do anything because it's complete, so choose a new task and try again
                self.curtask += 1

                # If we reach the end of the task list, reset and start again
                if self.curtask >= len(self.tasks):
                    self.curloopcount -= 1
                    self.curtask = 0
                    if self.curloopcount == 0:
                        return True, 'Complete', None

                t = self.tasks[self.curtask]
                t.reset()
                complete, nexttask = t.turn(curturn)

            return False, None, None

    def __init__(self, owner, tasks=None, taskhandlers=None):
        """
        Constructor.

        :param owner: entity that has this trait.
        """
        super().__init__(owner)
        self.tasks = None
        self.curtask = None
        self.taskstatus = (None, None)
        self.initialtaskname = None
        self.initialparams = None
        self.taskdefs = tasks
        self.taskhandlers = taskhandlers or dict()

        self.register_event(owner, 'Sound.Head', self.on_sound_heard)

    #
    # on_added_to_map()
    #
    def on_added_to_map(self, map):
        self.register_events()

        # Create task instances
        self.tasks = dict()
        self.curtask = None
        self.taskstatus = (None, None)

        if self.taskdefs:
            taskdefs = self.taskdefs['Tasks']
            for taskname, taskdef in taskdefs.items():
                task = self.create_task(taskname, self.owner, taskdef, self.taskdefs['Tasks'], self.taskhandlers)
                self.tasks[taskname] = task

            it = self.taskdefs['InitialTask']
            self.initialtaskname = it['Task']
            self.initialparams = it['Params']
        else:
            self.initialtaskname = None
            self.initialparams = None

    def on_sound_heard(self, soundmaker, verb, noun):
        pass

    def create_task(self, taskname, entity, taskdef, maindef, taskhandlers):
        """
        Create a task instance from the definition.

        :param taskname: name of supertask
        :param entity: owning entity.
        :param taskdef: task definition.
        :param maindef: overarching definition.
        :param taskhandlers: mapping of taskname to the registered IntelligenceTask to implement it
        :return: TaskInstance instance.
        """
        ti = Intelligence.TaskInstance(taskname, taskhandlers)

        ti.set_request_failure(taskdef.get('OnRequestFailure'), maindef)
        ti.set_execution_failure(taskdef.get('OnExecutionFailure'), maindef)

        for subtaskdef in taskdef['Tasks']:
            ti.add_subtask(subtaskdef, maindef, entity)

        # Set events
        taskevents = taskdef.get('Events')
        if taskevents:
            for eventname in ['Complete', 'Aborted']:
                eventdef = taskevents.get(eventname)
                if eventdef:
                    ti.set_event(eventname, eventdef)

        ti.set_loopcount(taskdef.get('LoopCount', -1))

        if 'Transitions' in taskdef:
            static_trans = taskdef['Transitions'].get('Static', dict())
            for callback, target in static_trans.items():
                for t in target:
                    ti.add_static_transition(callback, t['Task'], t['Params'])

            dynamictrans = taskdef['Transitions'].get('Dynamic', list())
            for transdef in dynamictrans:
                function = transdef['Function']
                targettask = transdef['Task']
                ti.add_dynamic_transition(function, targettask)

        return ti

    def set_curtask(self, taskname, taskparams):
        """
        Set the current task.

        :param taskname: name of task.
        :param taskparams: parameters to assign task...
        """
        if taskname:
            self.curtask = self.tasks[taskname]
            self.curtask.reset(taskparams)
        else:
            self.curtask = None

    def turn(self, curturn):
        """
        Update tasks.

        :param curturn: current turn.
        """
        entity = self.owner
        self.taskstatus = (self.curtask, 'InProgress')

        if not self.curtask:
            if self.initialtaskname:
                self.set_curtask(self.initialtaskname, self.initialparams)
                self.initialtaskname = None
            else:
                entity.request_action('Pass', entity.map)
                return

        # Dynamic transitions
        dynamictarget, nexttaskparams = self.curtask.check_dynamic_transitions(entity)
        if dynamictarget:
            self.set_curtask(dynamictarget, nexttaskparams)

        complete, reason, nexttask = self.curtask.turn(curturn)
        if complete:
            self.taskstatus = (self.curtask, reason)
            if reason == 'Complete':
                # Choose random transition
                trans = self.curtask.statictrans.get('OnFinished')
                if trans:
                    t = random.choice(trans)
                    self.set_curtask(t[0], t[1])
                else:
                    self.set_curtask(None, dict())
            elif reason == 'Aborted':
                self.set_curtask(nexttask, dict())
