"""
Trait system: Useable.

Formatted 01/22
"""
import engine.trait as trait
from engine.action import exposes_target
from engine.traitfactory import register_trait
from engine.defines import FacilitatorResult


class UseTarget:
    """
    Helper class for encapsulating a set of actions to run against a target entity.
    """

    def __init__(self, target, count, passnext, updateonfailure, actions=None, states=None):
        """
        Constructor.

        :param target: target entity
        :param count: number of times to run this.  -1 means no limit.
        :param passnext: in the event that the action is redundant (target entity is already in the desired state,
                         whether or not to return success, or to try and perform the next action instead.
        :param updateonfailure: whether to update the visual status if the action fails.
        :param actions:  a list of actions and their arguments, in a dict with 'name' and 'args' entries.
        :param states:  a list of states to be set, unset or toggled
        """
        self.target = target
        self.count = count
        self.passnext = passnext
        self.updateonfailure = updateonfailure
        self.actions = actions
        self.states = states
        self.index = 0


@register_trait('Useable')
@exposes_target('Use', 'owner')
class Useable(trait.Trait):
    """
    Trait for entities which can be used.
    """

    def __init__(self, owner, spoofuser=True, suppressmsg=False, playerusemsg=None, playerfailmsg=None,
                 otherusemsg=None, otherfailmsg=None, allowedusernames=None, numusersneeded=1, userfilterfunc=None, 
                 visualcycle=None):
        """
        Constructor.

        :param owner: entity that has this trait.
        :param spoofuser: whether or not the owner of this trait should be the actor, when executing usetargets.
               If false, the user is.
        :param suppressmsg: whether or not to return a message for display
        :param playerusemsg: custom message for when the player successfully uses.
        :param playerfailmsg: custom message for when the player fails to use.
        :param otherusemsg: custom message for when the player successfully uses.
        :param otherfailmsg: custom message for when the player fails to use.
        :param allowedusernames: optional list of names of entities who are allowed to use this
               (otherwise no restrictions).
        :param numusersneeded: number of unique users to trigger.
        :param userfilterfunc: lambda function to determine whether an entity can use this.
        :param visualcycle: list of visual states to cycle between
        """
        super().__init__(owner)

        self.spoofuser = spoofuser
        self.suppressmsg = suppressmsg
        self.playerusemsg = playerusemsg or 'You use %s.' % self.owner.def_name()
        self.playerfailmsg = playerfailmsg or 'You cannot use %s.' % self.owner.def_name()
        self.allowedusernames = set(allowedusernames) if allowedusernames else None
        self.numusersneeded = numusersneeded
        self.userfilterfunc = userfilterfunc
        self.usersused = set()

        self.otherusemsg = otherusemsg or '%s' + (' uses %s.' % self.owner.indef_name())
        self.otherfailmsg = otherfailmsg or '%s' + (' fails to use %s.' % self.owner.indef_name())
        self.visualcycle = visualcycle
        self.curvisual = 0

        self.activated = True
        self.targets = list()

    def add_use_target(self, target, count, passnext, updateonfailure, actions, states):
        """
        Add a use target.

        :param target: entity instance
        :param count: number of times to run this.  -1 means no limit.
        :param passnext: in the event that the action is redundant (target entity is already in the desired state,
                         whether or not to return success, or to try and perform the next action instead.
        :param updateonfailure: whether to update the visual status if the action fails.
        :param actions: a list of actions and their arguments, in a dict with 'name' and 'args' entries.
        """
        self.targets.append(UseTarget(target, count, passnext, updateonfailure, actions, states))

    def activate(self):
        """
        Set this to be activated.
        """
        self.activated = True

    def deactivate(self):
        """
        Deactivate this.  It will ignore any requests to be used.
        """
        self.activated = False

    def use(self, user):
        """
        Use this Entity.

        :param user: using Entity (actor)
        """
        if not self.activated:
            return FacilitatorResult(success=False,
                                     actionperformed=False,
                                     playermsg=self.playerfailmsg,
                                     othermsg=self.otherfailmsg % user.def_name())

        owner = self.owner
        actor = owner if self.spoofuser else user

        # See if the user is allowed to use this entity.  We have both a whitelist of named entities
        # and a filter function to help achieve this.
        allowed = self.allowedusernames is None or user.name in self.allowedusernames
        if not allowed or (self.userfilterfunc and not self.userfilterfunc(user)):
            return FacilitatorResult(success=False,
                                     actionperformed=False,
                                     playermsg=self.playerfailmsg,
                                     othermsg=self.otherfailmsg % user.def_name())

        # We can specify that an object must be used by at least a certain number of different users before it fires.
        self.usersused.add(user)
        if self.numusersneeded - len(self.usersused) > 0:
            playermsg = 'Nothing happens.'
            othermsg = f'{user.indef_name()} tries to use {owner.def_name()} but nothing happens.'
            return FacilitatorResult(success=False,
                                     actionperformed=False,
                                     playermsg=playermsg,
                                     othermsg=othermsg)

        successcount = 0
        updateonfailure = False

        for target in self.targets:
            if target.count == 0:
                continue

            index = target.index
            updateonfailure = target.updateonfailure or updateonfailure
            dotargets = len(target.actions) > 0

            while True:
                if dotargets:
                    action = target.actions[target.index]
                    res = actor.execute_action(action['name'], target.target, **action['args'])
                    if not res.success:
                        break
                else:
                    state = target.states[target.index]
                    for s in state['set']:
                        owner.set_state(s)
                    for s in state['unset']:
                        owner.unset_state(s)
                    for s in state['toggle']:
                        owner.toggle_state(s)

                successcount += 1

                # Next action
                if dotargets:
                    target.index = (target.index + 1) % len(target.actions)
                else:
                    target.index = (target.index + 1) % len(target.states)

                if target.index == 0:
                    target.count -= 1

                # Here, we need to differentiate between success, and it already being in the desired state.
                # On success, break.  On already being in the desired state, then move to the next and try that.
                # On failure, abort.
                if dotargets:
                    if res.already_done:
                        if not target.passnext:
                            break
                    else:
                        break
                else:
                    break

                # Check if we've hit an infinite loop
                if target.index == index:
                    playermsg = f'{owner.def_name()} appears to be broken.'
                    othermsg = f'user.def_name() tries {owner.def_name()} but it appears to be broken'
                    return FacilitatorResult(success=False,
                                             actionperformed=False,
                                             playermsg=playermsg,
                                             othermsg=othermsg)

        if successcount > 0 or updateonfailure:
            # Update visuals
            if self.visualcycle:
                self.curvisual += 1
                if self.curvisual == len(self.visualcycle):
                    self.curvisual = 0

                owner.visual.set_visual(vstatus=self.visualcycle[self.curvisual])

            if self.suppressmsg:
                return FacilitatorResult(success=True,
                                         actionperformed=True,
                                         playermsg=None,
                                         othermsg=None)
            else:
                return FacilitatorResult(success=True,
                                         actionperformed=True,
                                         playermsg=self.playerusemsg,
                                         othermsg=self.otherusemsg % user.def_name())
        else:
            if self.suppressmsg:
                return FacilitatorResult(success=False,
                                         actionperformed=False,
                                         playermsg=None,
                                         othermsg=None)
            else:
                return FacilitatorResult(success=False,
                                         actionperformed=False,
                                         playermsg=self.playerfailmsg,
                                         othermsg=self.otherfailmsg % user.def_name())
