"""
Trait system: Wearable.
"""

#
# Imports
#
import engine.trait as trait
from engine.action import exposes_target
from engine.traitfactory import register_trait


#
# Wearable
#
@register_trait('Wearable')
@exposes_target('Wear', 'owner')
class Wearable(trait.Trait):
    """Subclass for entities which can be worn."""

    #
    # __init__()
    #
    def __init__(self, owner):
        """
        Constructor.

        :param owner: entity that has this trait.
        """
        super().__init__(owner)
        self.isWorn = False

    #
    # wear()
    #
    def wear(self, wearer):
        """
        Wear this Entity.

        :param wearer: wearer Entity (actor)
        """
        self.isWorn = True

    #
    # unwear()
    #
    def unwear(self, unwearer):
        """
        Unwear this Entity.

        :param unwearer: unwearing Entity (actor)
        """
        self.isWorn = False

    #
    # turn()
    #
    def turn(self, currentTurn):
        pass