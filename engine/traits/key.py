"""
Trait system: Key.

Formatted 01/22
"""
import engine.trait as trait
from engine.traitfactory import register_trait


@register_trait('Key')
class Key(trait.Trait):
    """
    Subclass for entities which can act as a key.

    Key/lock interface is an integer.  Lockable entities have a set of integers, which are codes which
    unlock them.  Keys have a set of integers which correspond to the codes that the lockable entities require.
    """

    def __init__(self, owner, codes=None):
        """
        Constructor.

        :param owner: entity that has this trait.
        :param codes: list of codes
        """
        super().__init__(owner)
        self.codes = set(codes) if codes else set()
       