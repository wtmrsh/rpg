import engine.observable as obs
import engine.audio
import engine.pathfinder
from engine.action import exposes_target
import engine.trait as trait
from engine.traitfactory import register_trait


@register_trait('Hearing')
@exposes_target('EnableTrait', 'owner')
class Hearing(trait.Trait):
    """
    Trait for entities which can hear.
    """

    def __init__(self, owner):
        """
        Constructor.

        :param owner: entity that has this trait.
        """
        super().__init__(owner)

        self.register_event(None, 'Entity.Sound', self.on_sound_heard)

    @staticmethod
    def get_sound_volume(soundmaker, listener):
        pather = engine.pathfinder.PathFinder(soundmaker.map)
        route = pather.calculate_audio_path(soundmaker.x, soundmaker.y, listener.x, listener.y)
        return len(route)

    def on_sound_heard(self, soundmaker, verb, noun, soundfx, volume):
        maxdist = int(5 * volume)
        listener = self.owner

        # Early out checks
        if soundmaker == listener:
            return

        if soundmaker.max_dim_distance_to(listener) > maxdist:
            return

        routelen = self.get_sound_volume(soundmaker, listener)
        if routelen <= maxdist:
            obs.trigger(listener, 'Sound.Heard', soundmaker, verb, noun)

            if listener.is_player and soundfx:
                gain = 1.0 - routelen / maxdist
                engine.audio.start_sound(soundfx, gain)

    def update(self):
        """
        Update this trait.
        """
        pass

    def turn(self, curturn):
        """
        Update for this turn.

        :param curturn: curent turn.
        """
        self.update()
