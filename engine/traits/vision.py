"""
Trait system: Vision.

Formatted 01/22
"""
import libtcodpy as libtcod

import engine.fovmask
import engine.gamemodule
import engine.observable as obs
from engine.action import exposes_target
import engine.trait as trait
from engine.defines import FactionRelation
from engine.traitfactory import register_trait


@register_trait('Vision')
@exposes_target('EnableTrait', 'owner')
class Vision(trait.Trait):
    """
    Trait for entities which can see.
    """

    def __init__(self, owner, fov=None, viewdistance=None, mask=None):
        """
        Constructor.
        
        :param owner: entity that has this trait.
        :param fov: default field of view, in degrees, sans any effects.
        :param viewdistance: default view distance, in tiles, sans any effects.
        :param mask: list of file, x-size, y-size.
        """
        super().__init__(owner)

        self.show = False
        self._visibletiles = None
        self._visibleentities = None

        if mask:
            self.usemask = True

            mfile, msizex, msizey = mask
            self.maskdata = mask

            self.fov = self.originalfov = 0
            self.viewdistance = max(msizex, msizey)

            self.fovmask = engine.fovmask.FovMaskCache.get_mask_from_file(mfile, msizex, msizey)
        else:
            self.usemask = False
            self.fov = self.originalfov = fov
            self.viewdistance = viewdistance

            self.fovmask = engine.fovmask.FovMaskCache.get_mask_from_fov_and_radius(self.fov, self.viewdistance)

        factionsmodule = engine.gamemodule.get_game_factions_module()
        self.get_faction_relationship = getattr(factionsmodule, 'get_faction_relationship')

        self.register_event(self.owner.owning_entity, 'Entity.Actor.Move', self.on_move)
        self.register_event(self.owner.owning_entity, 'Entity.SetPlayer', self.on_set_player)

    #
    # on_added_to_map()
    #
    def on_added_to_map(self, map):
        self.register_events()

    def on_move(self, source, action, actor, target, dx, dy):
        """
        Callback for when owning Entity moves.

        :param action: action peformed.
        :param actor: entity doing the action.
        :param target: action target (unused in this case for moving).
        :param dx: move distance in x direction.
        :param dy: move distance in y direction.
        """
        vision = actor.get_vision()
        if vision and vision.enabled:
            tileinv = actor.owning_tile.inventory
            if tileinv and len(tileinv):
                actor.add_message(f'You see {tileinv} on the ground.')

    def on_set_player(self, source, entity, isplayer):
        """
        Callback for when owning Entity becomes player-controlled.

        :param entity: entity in question
        :param isplayer: whether the Entity is now player-controlled
        """
        vision = entity.get_vision()
        if vision:
            if vision.usemask:
                mfile, msizex, msizey = vision.maskdata
                vision.fovmask = engine.fovmask.FovMaskCache.get_mask_from_file(mfile, msizex, msizey)
            else:
                if isplayer:
                    vision.fov = 360
                else:
                    vision.fov = vision.originalfov

                vision.fovmask = engine.fovmask.FovMaskCache.get_mask_from_fov_and_radius(vision.fov, vision.viewdistance)

    def reset(self):
        self._visibletiles = None
        self._visibleentities = None

    def is_tile_in_vision(self, tile):
        """
        Check whether a location is visible.

        :param tile: tile to check.
        :return: whether or not the tile is visible.
        """
        if not self.enabled:
            return False

        return tile in self.visible_tiles

    @property
    def bounding_box(self):
        """
        Get bounding box of mask.

        :return: FovMask bounding box
        """
        return self.fovmask.get_bounding_box(self.owner.owning_entity.direction)

    @property
    def visible_entities(self):
        if not self._visibleentities:
            self.generate_fov_map(wallsvisible=True, calculatevisibleentities=True)
        return self._visibleentities

    @property
    def visible_tiles(self):
        if not self._visibletiles:
            self.generate_fov_map(wallsvisible=True, calculatevisibleentities=True)
        return self._visibletiles

    def generate_fov_map(self, wallsvisible=True, calculatevisibleentities=False):
        """
        Generates a map for libtcod to use, to determine what is visible and what is not.

        :param wallsvisible: whether walls should be treated as visible.
        :param calculatevisibleentities: whether we should calculate visible entities at the same time.
        """
        self._visibletiles = set()
        self._visibleentities = set() if calculatevisibleentities else None

        if not self.enabled:
            return

        owner = self.owner.owning_entity
        ox = owner.x
        oy = owner.y

        libtcod.map_compute_fov(
            owner.map.visionmap,
            ox,
            oy,
            self.viewdistance,
            wallsvisible,
            libtcod.FOV_PERMISSIVE(6))

        x0, y0, x1, y1 = self.bounding_box
        x0 = max(0, min(x0 + ox, owner.map.sizeX))
        y0 = max(0, min(y0 + oy, owner.map.sizeY))
        x1 = max(0, min(x1 + ox, owner.map.sizeX))
        y1 = max(0, min(y1 + oy, owner.map.sizeY))

        for x in range(x0, x1):
            for y in range(y0, y1):
                infov = libtcod.map_is_in_fov(owner.map.visionmap, x, y)
                inmask = not self.usemask or self.fovmask.in_mask(x - owner.x, y - owner.y, owner.direction)

                if infov and inmask:
                    tile = owner.map.tiles[x][y]
                    self._visibletiles.add(tile)
                    if calculatevisibleentities and tile.entity:
                        self._visibleentities.add(tile.entity)

        obs.trigger(None, 'Entity.Vision.Changed', self.owner, self)

    def update(self):
        pass

    def turn(self, curturn):
        """
        Update for this turn.

        :param curturn: curent turn.
        """
        if self.enabled:
            self.update()
