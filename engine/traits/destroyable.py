from functools import partial

import engine.action as action
import engine.trait as trait
from engine.traitfactory import register_trait


@register_trait('Destroyable')
@action.exposes_target('Destroy')
class Destroyable(trait.Trait):
    """
    Trait for entities which can be destroyed.
    """

    def __init__(self, owner, startdestroyed=False):
        """
        Constructor.

        :param owner: entity that has this trait.
        :param startdestroyed: whether or not to start destroyed.
        """
        super().__init__(owner)
        self.startdestroyed = startdestroyed

    #
    # on_added_to_map()
    #
    def on_added_to_map(self, map):
        self.register_events()

        if self.startdestroyed:
            self.owner.states.set_state('Destroyed')
        else:
            self.owner.states.unset_state('Destroyed')

