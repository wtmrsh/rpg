"""
Trait system: Lockable.

Formatted 01/22
"""
from functools import partial

import engine.trait as trait
from engine.action import exposes_target
from engine.traitfactory import register_trait


@register_trait('Lockable')
@exposes_target('Lock', 'owner')
@exposes_target('Unlock', 'owner')
class Lockable(trait.Trait):
    """
    Trait for entities which can be locked

    Key/lock interface is an integer.  Lockable entities have a set of integers, which are codes which
    unlock them.  Keys have a set of integers which correspond to the codes that the lockable entities require.
    """
    
    def __init__(self, owner, startlocked=False, codes=None):
        """
        Constructor.

        :param owner: entity that has this trait.
        :param startlocked: whether or not to start locked.
        :param codes: list of codes, or None.
        """
        super().__init__(owner)

        self.codes = set(codes) if codes else set()
        self.startlocked = startlocked

        #
        # on_added_to_map()
        #
        def on_added_to_map(self, map):
            self.register_events()

            if startlocked:
                self.owner.states.set_state('Locked')
            else:
                self.owner.states.unset_state('Locked')
