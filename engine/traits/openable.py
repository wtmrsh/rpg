"""
Trait system: Openable.

Formatted 01/22
"""
from functools import partial

import engine.audio
import engine.observable as obs
import engine.trait as trait
from engine.action import exposes_target
from engine.traitfactory import register_trait


@register_trait('Openable')
@exposes_target('Open', 'owner')
@exposes_target('Close', 'owner')
class Openable(trait.Trait):
    """
    Trait for entities which can be opened.
    """

    def __init__(self, owner, startopen=False, autoclose=False):
        """
        Constructor.

        :param owner: entity that has this trait.
        :param startopen: whether or not to start open.
        :param autoclose: an integer defining the number of turns after which this automatically closed, or -1.
        """
        super().__init__(owner)
        self.startopen = startopen

        #
        # on_added_to_map()
        #
        def on_added_to_map(self, map):
            self.register_events()

            if startopen:
                self.owner.set_state('Open')
            else:
                self.owner.unset_state('Open')
