"""
Trait system: LogicState.

Formatted 01/22
"""
from collections import defaultdict
import engine.trait as trait
from engine.action import exposes_target
from engine.traitfactory import register_trait


class LogicStateCallback:
    """
    Wrapper class for callbacks.
    """
    
    def __init__(self, state, verifier=None, usetargets=None, value=None, function=None):
        """
        Constructor.

        :param state: name of the state.
        :param verifier: function to check if this callback should be triggered.
        :param usetargets: targets to activate.
        :param value: value to check, as an alternative to verifier.
        :param function: function to execute if conditions match.
        """
        self.state = state
        self.verifier = verifier
        self.value = value
        self.usetargets = usetargets
        self.function = function


@register_trait('LogicState')
@exposes_target('ModifyLogicState', 'owner')
class LogicState(trait.Trait):
    """
    Trait for entities which can have an internal logic state which can be manipulated.
    """

    def __init__(self, owner, spoofuser=True):
        """
        Constructor.

        :param owner: entity that has this trait.
        :param spoofuser: whether or not the owner of this trait should be the actor, when executing usetargets.
                          If false, the user is.
        """
        super().__init__(owner)
        self.spoofuser = spoofuser
        self.logicstates = defaultdict(int)
        self.callbacks = defaultdict(list)

    def register_callback(self, state, verifier=None, usetargets=None, value=None, function=None):
        """
        Register callback function for when the internal state matches.
        
        :param state: name of the state.
        :param verifier: function to check if this callback should be triggered.
        :param usetargets: targets to activate.
        :param value: value to check, as an alternative to verifier.
        :param function: function to execute if conditions match.
        """
        self.callbacks[state].append(LogicStateCallback(state, verifier, usetargets, value, function))

    def run_callbacks(self, user, state):
        """
        Execute callbacks.
        
        :param user: 
        :param state: 
        :return: 
        """
        owner = self.owner
        actor = owner if self.spoofuser else user

        value = self.logicstates[state]
        for callback in self.callbacks[state]:
            if (callback.verifier and callback.verifier(value)) or (callback.value == value):
                if callback.usetargets:
                    for usetarget in callback.usetargets:
                        for action in usetarget.actions:
                            # We don't care about the result, here
                            actor.execute_action(action['name'], usetarget.target, **action['args'])
                else:
                    callback.function()

    def set_state(self, user, state, value):
        """
        Set the state to a particular value.

        :param user: using Entity (actor).
        :param state: named state.
        :param value: integer value.
        """
        oldvalue = self.logicstates[state]
        self.logicstates[state] = value
        if value != oldvalue:
            self.run_callbacks(user, state)
        return True, None, None

    #
    # add_state()
    #
    def add_state(self, user, state, value):
        """
        Add a particular value to the state.

        :param user: using Entity (actor).
        :param state: named state.
        :param value: integer value.
        """
        oldvalue = self.logicstates[state]
        self.logicstates[state] += value
        if self.logicstates[state] != oldvalue:
            self.run_callbacks(user, state)
        return True, None, None

    #
    # and_state()
    #
    def and_state(self, user, state, value):
        """
        Bitwise AND the state with a particular value.

        :param user: using Entity (actor).
        :param state: named state.
        :param value: integer value.
        """
        oldvalue = self.logicstates[state]
        self.logicstates[state] &= value
        if self.logicstates[state] != oldvalue:
            self.run_callbacks(user, state)
        return True, None, None

    #
    # or_state()
    #
    def or_state(self, user, state, value):
        """
        Bitwise OR the state with a particular value.

        :param user: using Entity (actor).
        :param state: named state.
        :param value: integer value.
        """
        oldvalue = self.logicstates[state]
        self.logicstates[state] |= value
        if self.logicstates[state] != oldvalue:
            self.run_callbacks(user, state)
        return True, None, None

    #
    # xor_state()
    #
    def xor_state(self, user, state, value):
        """
        Bitwise XOR the state with a particular value.

        :param user: using Entity (actor).
        :param state: named state.
        :param value: integer value.
        """
        oldvalue = self.logicstates[state]
        self.logicstates[state] ^= value
        if self.logicstates[state] != oldvalue:
            self.run_callbacks(user, state)
        return True, None, None

    #
    # not_state()
    #
    def not_state(self, user, state):
        """
        Bitwise NOT the state.

        :param user: using Entity (actor).
        :param state: named state.
        """
        self.logicstates[state] = ~self.logicstates[state]
        self.run_callbacks(user, state)
        return True, None, None
