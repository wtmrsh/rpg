__all__ = [
    'useable',
    'collective',
    'destroyable',
    'hearing',
    'intelligence',
    'inventory',
    'key',
    'lockable',
    'logicstate',
    'openable',
    'vision',
    'wearable']
