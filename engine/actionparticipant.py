from collections import defaultdict
from common import *

import engine.observable as obs
from engine.action import actor_types, action_affects_movement
from engine.defines import EntityActionDefinition, ActionPriority, ActionPriorityData, ActionRequestResult, FacilitatorResult


class ActionParticipant:
    """Base for classes which take part in actions."""

    #
    # __init__()
    #
    def __init__(self, actortype):
        """
        Constructor.
        
        :param actorType: type of actor
        """
        self.actorType = actortype
        self.actorPrerequisites = defaultdict(list)
        self.targetPrerequisites = defaultdict(list)
        self.facilitators = dict()
        self.aptitudes = dict()
        self.roundaction = None
        self.lastaction = None
        self.isplayer = False
        self.prioritymodifier = 0

    def set_player(self, isplayer):
        """
        Set the Entity to be player-controlled.

        :param isplayer: whether Entity is player-controlled
        """
        self.isplayer = isplayer
        obs.trigger(self, 'Entity.SetPlayer', self, isplayer)

    @property
    def is_player(self):
        """
        Check if the Entity is player-controlled.

        :return: whether Entity is player-controlled.
        """

        return self.isplayer

    def is_action_actor(self, action):
        """
        Returns whether or not the Entity can perform the specified action.

        :param action: name of the action
        :return: boolean success        
        """
        return action in self.actors and self.actors[action]

    def is_action_target(self, action):
        """
        Returns whether or not the Entity can have the specified action performed on it.

        :param action: name of the action
        :return: boolean success
        """
        return action in self.targets and self.targets[action]

    def can_be_actor_on(self, action, target):
        """
        Returns whether or not the Entity can perform the specified action on the specified target Entity.

        :param action: name of the action
        :param target: target to perform on
        :return: boolean success
        """
        return self.is_action_actor(action) and target.is_action_target(action)

    def can_be_target_of(self, action, actor):
        """
        Returns whether or not the Entity can have the specified action performed on it by the specified target Entity.

        :param action: name of the action
        :param actor: performing actor
        :return: boolean success
        """
        return actor.is_action_actor(action) and self.is_action_target(action)

    def check_action_prerequisites(self, action, target, *args, **kwargs):
        """
        Check the prerequisites for the action to take place.
        
        :param action: name of the action
        :param target: target object
        :return: tuple of (success, player message, other message)
        """
        for prereq in self.actorPrerequisites[action]:
            res = prereq(self, target, *args, **kwargs)
            if not res.passed:
                return res

        if target:
            for prereq in target.targetPrerequisites[action]:
                res = prereq(target, self, *args, **kwargs)
                if not res.passed:
                    return res

        return ActionRequestResult(passed=True, actionrequired=True, playermsg=None, othermsg=None, prereqactions=[])

    def request_action(self, action, target, *args, **kwargs):
        """
        Perform the specified action on the specified target Entity.  This does not check to see if the 
        Entities implement the actions.

        :param action: name of the action
        :param target: target object
        :param args: positional arguments to facilitator
        :param kwargs: keyword arguments to facilitator
        :return: bool success, depending on whether the performing of the action was successful, or simply 
                 False if the Entities do not implement the action as requested
        """
        result = self.check_action_prerequisites(action, target, *args, **kwargs)

        if result.passed:
            actioncls = actor_types[action]
            tx, ty = actioncls.get_target_location(self, *args, **kwargs)

            # Work out priority
            elevated = False

            if self.is_player:
                priority = ActionPriority.Player
            elif action == 'Pass':
                priority = ActionPriority.NoAction
            elif tx != self.x or ty != self.y or action_affects_movement(action, self, target):
                priority = ActionPriority.Movement
            else:
                priority = ActionPriority.Action

            ead = EntityActionDefinition(action=action,
                                         actor=self,
                                         target=target,
                                         actortargetpos=(tx, ty),
                                         priority=ActionPriorityData(priority, self.prioritymodifier, elevated),
                                         args=args,
                                         kwargs=kwargs)
            self.roundaction = ead
        return result

    def post_execute_action(self, result, action, target, *args, **kwargs):
        # Store action and result against the target
        self.lastaction = (action, target, args, kwargs, result)

        # Perform callbacks
        if result.success:
            obs.trigger(self, 'Entity.Actor.%s' % action, action, self, target, *args)
            if target:
                obs.trigger(target, 'Entity.Target.%s' % action, action, target, self, *args)

            # If this was a collective entity which is not a full set, handle this
            if target.actorType != 'Map' and target.has_trait('Collective') and not target.collective.is_full_set():
                obs.trigger(None, 'Entity.Target.%s.IncompleteSet' % action, target, *args)

    def execute_action(self, action, target, *args, **kwargs):
        # Check prereqs again
        prereqcheck = self.check_action_prerequisites(action, target, *args, **kwargs)
        if prereqcheck.passed:
            facilitator = self.facilitators[action]
            result = facilitator(self, target, *args, **kwargs)
            self.add_message(result.playermsg, result.othermsg)
            self.post_execute_action(result, action, target, *args, **kwargs)
            return result
        else:
            return FacilitatorResult(success=False,
                                     actionperformed=False,
                                     playermsg=prereqcheck.playermsg,
                                     othermsg=prereqcheck.othermsg)

    def add_message(self, playermsg, othermsg=None):
        pass
