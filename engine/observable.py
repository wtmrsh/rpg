from collections import defaultdict

from engine.exceptions import TransitionToStateException, SpawnChildStateException, ExitStateException, ExitException

LOGGING = False
DEBUG = False


class HandlerNotFound(Exception):
    """Raised if a handler wasn't found"""

    def __init__(self, event, handler):
        self.event = event
        self.handler = handler

    def __str__(self):
        return f"Handler {self.handler} wasn't found for event {self.event}"


class EventNotFound(Exception):
    """Raised if an event wasn't found"""

    def __init__(self, event):
        self.event = event

    def __str__(self):
        return f"Event {self.event} wasn't found"


events = dict()
stats = list()


def on(topic, event, *handlers):  # pylint: disable=invalid-name
    """Register a handler to a specified event"""

    def _on_wrapper(*handlers):
        """wrapper for on decorator"""
        if event not in events:
            events[event] = defaultdict(list)

        sink = events[event]
        sink[topic].extend(handlers)

        if LOGGING:
            for handler in handlers:
                print(f"Added handler for event '{event}': {handler}")

        return handlers[0]

    if handlers:
        return _on_wrapper(*handlers)
    return _on_wrapper


def off(topic, event, *handlers):
    """Unregister an event or handler from an event"""

    if event not in events:
        raise EventNotFound(event)

    sink = events[event]

    if topic not in sink:
        raise EventNotFound(event)

    for handler in handlers:
        if handler not in sink[topic]:
            raise HandlerNotFound(event, handler)
        while handler in sink[topic]:
            sink[topic].remove(handler)

        if LOGGING:
            print(f"Removed handler {handler} from '{event}'")

    return True


def once(topic, event, *handlers):
    """Register a handler to a specified event, but remove it when it is triggered"""

    def _once_wrapper(*handlers):
        """Wrapper for 'once' decorator"""

        def _wrapper(*args, **kw):
            """Call wrapper"""
            for handler in handlers:
                handler(*args, **kw)

            off(topic, event, _wrapper)

        return _wrapper

    if handlers:
        return on(topic, event, _once_wrapper(*handlers))
    return lambda x: on(topic, event, _once_wrapper(x))


def _fire_handlers(topic, event, functions, *args):
    statex = None
    if functions:
        for func in functions:
            # If we encounter a state exception, store it and continue with the other events
            try:
                if DEBUG:
                    pargs = ', '.join([str(v) for v in args])
                    print(f'{event}: {topic} -> {func} ({pargs})')
                func(topic, *args)
            except (TransitionToStateException, SpawnChildStateException, ExitStateException, ExitException) as e:
                if statex:
                    raise statex
                else:
                    statex = e

    return statex


def trigger(topic, event, *args):
    """Trigger all functions which are subscribed to an event"""
    sink = events.get(event)
    if not sink:
        return

    # Fire universal handlers
    statex = _fire_handlers(topic, event, sink.get(None), *args)

    # Fire specific handlers
    if topic is not None:
        statex2 = statex
        statex = _fire_handlers(topic, event, sink.get(topic), *args)
        if statex2:
            statex = statex2

    if statex:
        raise statex
