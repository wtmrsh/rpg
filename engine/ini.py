import os
import sys
import copy
import configparser
import json
import logging
import optparse
from xml.etree import ElementTree
from collections import namedtuple

from bearlibterminal import terminal

from common import *
import engine.traitprereqs
import engine.tiledefinitions
import engine.entityfactories
from engine.entityproperties import entityPropertyFactory
import engine.gamemodule
from engine.exceptions import LogicError
from engine.cursor import Cursor
from engine.colour import Colour
from engine.settings import GameSettings, GraphicsSettings
from engine.tileset import ImageTileSet


TileDefinition = namedtuple('TileDefinition', 'offset,type,variant,state,frame,colour,properties')


def process_command_line(argv):
    """
    Process command line arguments.
    
    :param argv: list of arguments, or none for sys.argv[1:].
    :return: tuple of (settings object, args list)
    """
    if argv is None:
        argv = sys.argv[1:]

    # Initialize the parser object:
    parser = optparse.OptionParser(formatter=optparse.TitledHelpFormatter(width=78), add_help_option=None)

    # define options here:
    parser.add_option(
        '-g',
        '--game',
        dest='game',
        default=None,
        help='Specify which game to load.')

    parser.add_option(
        '-h',
        '--help',
        action='help',
        help='Show this help message and exit.')

    parser.add_option(
        '-e',
        '--events',
        dest='events',
        default=False,
        action='store_true',
        help='Print events to console.')

    settings, args = parser.parse_args(argv)

    # Check number of arguments, verify values, etc.
    if args:
        parser.error('program takes no command-line arguments, ignoring values passed in.')

    # Further process settings & args if necessary
    return settings, args


def parse_ini_file(file):
    """
    Parse named file into a dictionary

    :param file: file to load
    """
    config = configparser.ConfigParser()
    config.optionxform = str  # Don't convert to lower case

    try:
        with open(file) as f:
            config.read_file(f)
    except IOError:
        raise Exception('Could not read ini file: ' + file)

    inisettings = dict()
    for section in ['Logging', 'Display', 'Game', 'UserInterface', 'ViewDecorators', 'Cursors']:
        try:
            settings = dict(config.items(section))
        except configparser.NoSectionError:
            continue

        inisettings[section] = settings

        if section in ['UserInterface', 'ViewDecorators', 'Cursors']:
            for sectionDef in settings.keys():
                sectionSettings = dict(config.items(sectionDef))
                inisettings[sectionDef] = sectionSettings

    return inisettings


def create_settings_from_ini(config):
    """
    Parse dictionary of setting.
    
    :param config: dictionary to parse for settings
    """
    # Game settings
    gamesettings = config['Game']

    for setting, value in gamesettings.items():
        GameSettings.settings[setting] = value

    logsettings = config['Logging']

    logfile = logsettings['File'].strip()
    filelevel = logsettings['FileLevel'].strip()
    stdlevel = logsettings['StdoutLevel'].strip()
    
    if filelevel == 'NONE':
        flevel = None
    if filelevel == 'DEBUG':
        flevel = logging.DEBUG
    elif filelevel == 'INFO':
        flevel = logging.INFO
    elif filelevel == 'WARNING':
        flevel = logging.WARNING
    elif filelevel == 'ERROR':
        flevel = logging.ERROR
    elif filelevel == 'CRITICAL':
        flevel = logging.CRITICAL
    else:
        flevel = None
        
    if stdlevel == 'NONE':
        slevel = None
    if stdlevel == 'DEBUG':
        slevel = logging.DEBUG
    elif stdlevel == 'INFO':
        slevel = logging.INFO
    elif stdlevel == 'WARNING':
        slevel = logging.WARNING
    elif stdlevel == 'ERROR':
        slevel = logging.ERROR
    elif stdlevel == 'CRITICAL':
        slevel = logging.CRITICAL
    else:
        slevel = None
        
    logger = create_logger(logfile, flevel, slevel)

    # Read display settings
    display = config['Display']

    fontSize = int(display['FontSize'])
    fontFile = './{}/{}/resources/fonts/{}'.format(GAMES_DIR, GameSettings.game, display['Font'])
    terminal.set('font: {}, size={}'.format(fontFile, fontSize))
    terminal.set('map font: {}, size={}, align=top-left'.format(fontFile, fontSize * 2))

    GraphicsSettings.fontSize = fontSize

    # Load in UI
    ui = config['UserInterface']
    for uiName, uiSettings in ui.items():
        uidata = uiSettings.split(',')
        uifile = uidata[0].strip()
        uisize = int(uidata[1]) * CELL_SIZE
        uisizex = int(uidata[2])
        uisizey = int(uidata[3])
        uioffset = int(uidata[4], 16)

        uiTiles = ImageTileSet(uioffset)

        namedUI = config[uiName]
        for piece, location in namedUI.items():
            values = location.split(',')
            x = int(values[0])
            y = int(values[1])

            # Get offset into image file
            t = uiTiles.add_tile(piece, offset=y * uisizex + x)

        imageFile = './{}/{}/resources/images/{}'.format(GAMES_DIR, GameSettings.game, uifile)
        setString = 'U+{0}: {1}, size={2}x{2}, align=top-left'.format(hex(uioffset), imageFile, uisize)
        terminal.set(setString)

        GraphicsSettings.uiTiles[uiName] = uiTiles

        logger.info("Loaded UI '{}': {} at codepage {}".format(uiName, uifile, hex(uioffset)))    
    
    # Load in view decorators
    viewDecorators = config['ViewDecorators']
    for viewDecoratorName, viewDecoratorSettings in viewDecorators.items():
        viewdecoratordata = viewDecoratorSettings.split(',')
        viewdecoratorfile = viewdecoratordata[0].strip()
        viewdecoratorsize = int(viewdecoratordata[1]) * CELL_SIZE
        viewdecoratorsizex = int(viewdecoratordata[2])
        viewdecoratorsizey = int(viewdecoratordata[3])
        viewdecoratoroffset = int(viewdecoratordata[4], 16)

        viewDecTiles = ImageTileSet(viewdecoratoroffset)

        namedViewDec = config[viewDecoratorName]
        for piece, location in namedViewDec.items():
            values = location.split(',')
            x = int(values[0])
            y = int(values[1])

            # Get offset into image file
            t = viewDecTiles.add_tile(piece, offset=y * viewdecoratorsizex + x)

        imageFile = './{}/{}/resources/images/{}'.format(GAMES_DIR, GameSettings.game, viewdecoratorfile)
        setString = 'U+{0}: {1}, size={2}x{2}, align=top-left'.format(hex(viewdecoratoroffset), imageFile, viewdecoratorsize)
        terminal.set(setString)

        GraphicsSettings.viewDecorators[viewDecoratorName] = viewDecTiles

        logger.info("Loaded view decorator '{}': {} at codepage {}".format(viewDecoratorName, viewdecoratorfile, hex(viewdecoratoroffset)))

    # Cursors
    cursors = config['Cursors']
    for cursorName, cursorDef in cursors.items():
        cursorData = cursorDef.split(',')
        cursorFile = cursorData[0].strip()
        cursorScale = int(cursorData[1])
        cursorSizeX = int(cursorData[2])
        cursorSizeY = int(cursorData[3])
        cursorOffset = int(cursorData[4], 16)

        imageFile = './{}/{}/resources/images/{}'.format(GAMES_DIR, GameSettings.game, cursorFile)
        setString = 'U+{0}: {1}, size={2}x{2}, align=top-left'.format(hex(cursorOffset), imageFile, CELL_SIZE * cursorScale)
        terminal.set(setString)
        logger.info("Using cursor set '{0}': {1} at codepage {2}".format(cursorName, cursorFile, hex(cursorOffset)))

        cursorTiles = ImageTileSet(cursorOffset)

        cursorDefinition = Cursor.Definition(cursorTiles)

        cursorSection = config[cursorName]
        for name, data in cursorSection.items():
            cData = data.split(',')
            x = int(cData[0])
            y = int(cData[1])
            dx = int(cData[2])
            dy = int(cData[3])

            t = cursorTiles.add_tile(name, offset = y * cursorSizeY + x)
            cursorDefinition.add_cursor(name, dx, dy)
            logger.debug("Cursor tiles: set '{0}' to {1}".format(name, t.offset))

        Cursor.definitions[cursorName] = cursorDefinition


def create_logger(filename, flevel, slevel):
    """
    Create logger.
    
    :param filename: file to log to
    :param flevel: log level to use in file
    :param slevel: log level to use in stdout
    :return: logger
    """
    logger = logging.getLogger()
    
    LOG_FORMAT = '%(asctime)s - %(levelname)s - %(message)s'

    logger.setLevel(flevel)

    if flevel:
        handler = logging.FileHandler(filename)
        handler.setLevel(flevel)

        formatter = logging.Formatter(LOG_FORMAT)
        handler.setFormatter(formatter)

        logger.addHandler(handler)
        
    if slevel:
        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(slevel)

        formatter = logging.Formatter(LOG_FORMAT)
        handler.setFormatter(formatter)

        logger.addHandler(handler)

    return logger


def parse_view_definitions(filename):
    """
    Parse View definitions.
    
    :param filename: file to read
    :return: JSON data
    """
    with open(filename) as file:
        contents = file.read()
        jsonData = json.loads(contents)

    # Parse data
    for defName, defData in jsonData.items():

        # Colours
        if 'colours' in defData:
            colours = defData['colours']
            for colourKey, colourValue in colours.items():
                colours[colourKey] = Colour.from_string(colourValue).to_bgra()

        # Sizes
        if 'sizes' in defData:
            sizes = defData['sizes']
            for sizeKey, sizeValue in sizes.items():
                sizes[sizeKey] = int(sizeValue)

        # Transparency
        if 'transparency' in defData:
            defData['transparency'] = float(defData['transparency'])

    return jsonData


def get_action_prereqs(prereqs, gameprereqmodule):
    funcs = list()
    for prereq in prereqs:
        p = getattr(gameprereqmodule, prereq, None) or getattr(sys.modules['engine.traitprereqs'], prereq, None)
        if p:
            funcs.append(p)
        else:
            raise Exception(f'Could not find prerequisite {prereq}')

    return funcs


def get_action_facilitator(func, gamefacilitatormodule):
    return getattr(gamefacilitatormodule, func, None)


def get_action_aptitude(func, gameaptitudemodule):
    if func:
        return getattr(gameaptitudemodule, func, None)
    else:
        return lambda a, t: 0.5


def parse_trait_prereqs(defname, traitdef, section, outdef):
    setting = traitdef[section]
    for action, funcs in setting.items():
        prereqs = funcs.get('Prerequisites', list())
        facilitator = funcs.get('Facilitator', None)
        aptitude = funcs.get('Aptitude', None)

        if section not in outdef:
            outdef[section] = dict()

        if action not in outdef[section]:
            outdef[section][action] = {
                'Prerequisites': list(),
            }

            if section == 'Actors':
                outdef[section][action]['Facilitator'] = None
                outdef[section][action]['Aptitude'] = None

        actiondef = outdef[section][action]

        actiondef['Prerequisites'].extend(prereqs)
        actiondef['Prerequisites'] = list(set(actiondef['Prerequisites']))

        if section == 'Actors' and actiondef['Facilitator'] and facilitator:
            raise Exception(f"Entity '{defname}' has a facilitator specified at both Actor and Trait level"
                            f" for action '{action}'.")

        if section == 'Actors' and actiondef['Aptitude'] and aptitude:
            raise Exception(f"Entity '{defname}' has an aptitude function specified at both Actor and Trait level"
                            f" for action '{action}'.")

        actiondef['Facilitator'] = facilitator
        actiondef['Aptitude'] = aptitude

    # Remove prereqs from trait section once they've been copied to the root section
    traitdef.pop(section)


def parse_list_of_list_of_strings(defname, section, value):
    outer = value
    if not isinstance(outer, list):
        t = type(outer)
        raise Exception(f"Entity '{defname}' section {section} should be a list, but is {t.__name__}.")

    for inner in outer:
        if not isinstance(inner, list):
            t = type(inner)
            raise Exception(f"Entity '{defname}' section {section} should be a list of lists, but outer list contains {t.__name__}.")

        for item in inner:
            if not isinstance(item, str):
                t = type(item)
                raise Exception(f"Entity '{defname}' section {section} should be a list of list of strings, but an inner list contains {t.__name__}.")


def parse_statemanagement_handlerdef(defname, section, value):
    if not isinstance(value, dict):
        t = type(value)
        raise Exception(f"Entry in entity '{defname}' StateManagement section {section} should be a dictionary, but is {t.__name__}.")

    if 'RequiredStates' in value:
        rs = value['RequiredStates']
        if not isinstance(rs, list):
            t = type(rs)
            raise Exception(f"Entry in entity '{defname}' StateManagement section {section}.RequiredStates should be a list, but is {t.__name__}.")

    if 'Handler' in value:
        h = value['Handler']
        if not isinstance(h, dict):
            t = type(h)
            raise Exception(f"Entry in entity '{defname}' StateManagement section {section}.Handler should be a dict, but is {t.__name__}.")


def parse_entity_definition(name, data, logger):
    logger.info(f'Parsing entity definition for {name}')
    gameEntityDict = engine.gamemodule.get_game_entity_dict()

    # Actors
    if 'Actors' in data:
        setting = data['Actors']
        for action, funcs in setting.items():
            if 'Prerequisites' not in setting[action]:
                setting[action]['Prerequisites'] = list()
            if 'Facilitator' not in setting[action]:
                setting[action]['Facilitator'] = None
            if 'Aptitude' not in setting[action]:
                setting[action]['Aptitude'] = None

    # Targets
    if 'Targets' in data:
        setting = data['Targets']
        for action, funcs in setting.items():
            if 'Prerequisites' not in setting[action]:
                setting[action]['Prerequisites'] = list()

    # Traits
    if 'Traits' in data:
        for traitName, traitsettings in data['Traits'].items():
            if 'Actors' in traitsettings:
                parse_trait_prereqs(name, traitsettings, 'Actors', data)

            if 'Targets' in traitsettings:
                parse_trait_prereqs(name, traitsettings, 'Targets', data)

            if 'Parameters' in traitsettings:
                for setname, setvalue in traitsettings['Parameters'].items():
                    setting = traitsettings['Parameters']
                    if isinstance(setvalue, list):
                        setting[setname] = [gameEntityDict.get(str(entry), eval(str(entry))) for entry in setvalue]
                    elif isinstance(setvalue, dict):
                        setting[setname] = {key: gameEntityDict.get(str(entry), eval(str(entry))) for key, entry in setvalue.items()}
                    else:
                        setting[setname] = gameEntityDict.get(str(setname), eval(str(setvalue)))

    # State management
    if 'StateManagement' in data:
        defstatemgt = data['StateManagement']
        if 'Handlers' in defstatemgt:
            defhandlers = defstatemgt['Handlers']  # Dictionary of state to handlerdef
            for defhandler in defhandlers.values():
                for handler in defhandler:
                    parse_statemanagement_handlerdef(name, 'StateManagement.Handlers', handler)
        if 'Blockers' in defstatemgt:
            defblockers = defstatemgt['Blockers']  # Dictionary of blocker type to handlerdef
            for defblockername, defblocker in defblockers.items():
                for blocker in defblocker:
                    parse_statemanagement_handlerdef(name, f'StateManagement.Blockers.{defblockername}', blocker)
        if 'Visuals' in defstatemgt:
            defvisuals = defstatemgt['Visuals']  # Ordered list of handlerdefs
            for defvisual in defvisuals:
                parse_statemanagement_handlerdef(name, 'StateManagement.Visuals', defvisual)
        if 'Audio' in defstatemgt:  # Dictionary of state to handlerdef
            defaudios = defstatemgt['Audio']
            for defaudioname, defaudio in defaudios.items():
                for audio in defaudio:
                    parse_statemanagement_handlerdef(name, f'StateManagement.Audio.{defaudioname}', audio)

    # Stats
    if 'Stats' in data:
        defstats = data['Stats']
        for statname, statvalue in defstats.items():
            if isinstance(statvalue, list):
                defstats[statname] = [eval(str(entry)) for entry in statvalue]
            elif isinstance(statvalue, dict):
                defstats[statname] = {key: eval(str(entry)) for key, entry in statvalue.items()}
            else:
                defstats[statname] = eval(str(statvalue))
    return data


def get_definition_parents(defname, defdata):
    if 'Inherits' in defdata:
        return [defdata['Inherits']]
    else:
        gameEntityDict = engine.gamemodule.get_game_entity_dict()
        nameType = gameEntityDict.get(defname)
        return engine.utils.get_class_parents(nameType)


def get_definition_class(defname, defdata):
    if defname not in defdata:
        raise LogicError(f"Entity '{defname}' does not have a definition.")

    gameEntityDict = engine.gamemodule.get_game_entity_dict()
    classtype = gameEntityDict.get(defname)

    if not classtype:
        d = defdata[defname]
        if 'Inherits' in d:
            return get_definition_class(d['Inherits'], defdata)
        else:
            # Default to the base entity class
            classtype = gameEntityDict.get(defname) or gameEntityDict.get('Entity')
    return classtype


def calculate_prereqs(defname, isroot, isactor, action, base, agg, aggroot):
    """
    Calculate the set of prerequisites required.

    :param defname: entity name
    :param isroot: whether or not this is the root entity.
    :param isactor: whether or not these are actor prereqs
    :param action: name of action.
    :param base: base entity dictionary.
    :param agg: aggregrated entity dictionary.
    :param aggroot: aggregrated entity dictionary base.
    :return: prerequisite names.
    """
    if isroot:
        if isactor:
            baseadd = {f.__name__ for f in engine.traitprereqs.actorPrereqs[action]}
        else:
            baseadd = {f.__name__ for f in engine.traitprereqs.targetPrereqs[action]}
    else:
        baseadd = set()

    baseadd |= {f for f in base[action]['Prerequisites'] if f[0] != '-'}

    baseremove = {f[1:] for f in base[action]['Prerequisites'] if f[0] == '-'}
    aggadd = {f for f in agg[action]['Prerequisites'] if f[0] != '-'}

    return list((aggadd | baseadd) - baseremove)


def merge_definition_section(basedef, aggdef, section):
    if section in basedef:
        if section not in aggdef:
            aggdef[section] = dict()

        aggdef[section].update(basedef[section])


def merge_action_section(defname, basedef, aggdef, section):
    if section in basedef:
        isactors = section == 'Actors'
        if section not in aggdef:
            aggdef[section] = dict()

        for action in basedef[section]:
            isroot = action not in aggdef[section]
            if isroot:
                aggdef[section][action] = {
                    'Prerequisites': list()
                }

                if isactors:
                    aggdef[section][action]['Facilitator'] = None
                    aggdef[section][action]['Aptitude'] = None

            aggdef[section][action]['Prerequisites'] = calculate_prereqs(defname,
                                                                         isroot,
                                                                         isactors,
                                                                         action,
                                                                         basedef[section],
                                                                         aggdef[section],
                                                                         aggdef)

            if isactors:
                fac = basedef[section][action].get('Facilitator')
                if fac:
                    aggdef[section][action]['Facilitator'] = fac

                if not aggdef[section][action]['Facilitator']:
                    raise Exception(f"Entity '{defname}' has no facilitator specified for action '{action}'.")

                apt = basedef[section][action].get('Aptitude')
                if apt:
                    aggdef[section][action]['Aptitude'] = apt


def merge_entity_definition(defname, basedef, aggdef):
    merge_definition_section(basedef, aggdef, 'Arguments')
    merge_definition_section(basedef, aggdef, 'Properties')
    merge_action_section(defname, basedef, aggdef, 'Actors')
    merge_action_section(defname, basedef, aggdef, 'Targets')

    if 'Stats' in basedef:
        aggdef['Stats'] = dict(basedef['Stats'])

    if 'Traits' in basedef:
        if 'Traits' not in aggdef:
            aggdef['Traits'] = dict()

        for traitname, basetraitdata in basedef['Traits'].items():
            aggtraitdata = aggdef['Traits'].get(traitname, dict())

            if 'Specialisation' in basetraitdata:
                aggtraitdata['Specialisation'] = basetraitdata['Specialisation']

            if 'Parameters' in basetraitdata:
                aggtraitdata['Parameters'] = dict(basetraitdata['Parameters'])

            aggdef['Traits'][traitname] = aggtraitdata

    if 'StateManagement' in basedef:
        if 'StateManagement' not in aggdef:
            aggdef['StateManagement'] = dict()

        smdef = aggdef['StateManagement']

        # Replace any handlers in aggregated def that are defined in basedef
        handlersdata = basedef['StateManagement'].get('Handlers', dict())
        if len(handlersdata) > 0 and 'Handlers' not in aggdef['StateManagement']:
            smdef['Handlers'] = dict()

        for key, handlers in handlersdata.items():
            smdef['Handlers'][key] = handlers

        # Replace any blockers in aggregated def that are defined in basedef
        blockersdata = basedef['StateManagement'].get('Blockers', dict())
        if len(blockersdata) > 0 and 'Blockers' not in aggdef['StateManagement']:
            smdef['Blockers'] = dict()

        for key, blockers in blockersdata.items():
            smdef['Blockers'][key] = blockers

        # Replace visuals in aggregated def that are defined in basedef
        visualsdata = basedef['StateManagement'].get('Visuals')
        if visualsdata:
            smdef['Visuals'] = visualsdata

        # Replace audio in aggregated def that are defined in basedef
        audiosdata = basedef['StateManagement'].get('Audio', dict())
        if len(audiosdata) > 0 and 'Audio' not in aggdef['StateManagement']:
            smdef['Audio'] = dict()

        for key, handlers in audiosdata.items():
            smdef['Audio'][key] = handlers


def aggregrate_entity_definition(defname, basedefs, aggregateddefs):
    # Find parents
    thisdef = basedefs[defname]
    parents = list(filter(lambda x: x in basedefs, get_definition_parents(defname, thisdef)))

    if len(parents) > 1:
        raise Exception(f"Entity definition for '{defname}' specifies more than one parent.")

    for parent in parents:
        if parent not in aggregateddefs:
            aggregrate_entity_definition(parents, basedefs, aggregateddefs)

    # New aggregrated definition
    if len(parents) == 1:
        aggdef = copy.deepcopy(aggregateddefs[parents[0]])
        aggdef['__parents__'].append(parents[0])
    else:
        aggdef = {
            '__parents__': list()
        }

    aggregateddefs[defname] = aggdef

    # Merge in our definition
    merge_entity_definition(defname, thisdef, aggdef)


def parse_entity_definitions(filename):
    """
    Parse Entity definitions.
    
    :param filename: file to read
    :return: JSON data
    """
    gameprereqmodule = engine.gamemodule.get_game_prereq_module()
    gamefacilitatormodule = engine.gamemodule.get_game_facilitators_module()
    gameaptitudemodule = engine.gamemodule.get_game_aptitudes_module()

    logger = logging.getLogger()
    logger.info(f'Parsing entity definitions from {filename}')
    with open(filename) as file:
        contents = file.read()
        jsondata = json.loads(contents)

    # Parse base definitions
    for defname, defdata in jsondata.items():
        parse_entity_definition(defname, defdata, logger)

    # Aggregate subclasses
    entitytypedict = dict()
    aggregateddefinitions = dict()
    for defname in jsondata:
        aggregrate_entity_definition(defname, jsondata, aggregateddefinitions)
        entitytypedict[defname] = get_definition_class(defname, jsondata)

    engine.entityfactories.factories = entitytypedict

    # Convert types and check that everything is present
    for defname, definition in aggregateddefinitions.items():
        # Properties
        missingprops = set()
        unknownprops = set()
        propertydefs = definition['Properties']

        for propname, proptype in entityPropertyFactory.items():
            propinst = proptype()
            if propname in propertydefs:
                propinst.parse_from_definition(defname, propertydefs[propname])
            elif propinst.required:
                missingprops.add(propname)

            propertydefs[propname] = propinst

        for propname in propertydefs:
            if propname not in entityPropertyFactory:
                unknownprops.add(propname)

        if 'Actors' in definition:
            for action in definition['Actors']:
                prereqs = definition['Actors'][action]['Prerequisites']
                facilitator = definition['Actors'][action]['Facilitator']
                aptitude = definition['Actors'][action]['Aptitude']
                definition['Actors'][action]['Prerequisites'] = get_action_prereqs(prereqs, gameprereqmodule)

                f = get_action_facilitator(facilitator, gamefacilitatormodule)
                if not f:
                    raise Exception(f"Could not find facilitator function '{facilitator}' for action '{action}'"
                                    f" in entity definition for '{defname}'.")
                else:
                    definition['Actors'][action]['Facilitator'] = f

                f = get_action_aptitude(aptitude, gameaptitudemodule)
                if not f:
                    raise Exception(f"Could not find aptitude function '{aptitude}' for action '{action}'"
                                    f" in entity definition for '{defname}'.")
                else:
                    definition['Actors'][action]['Aptitude'] = f

        if 'Targets' in definition:
            for action in definition['Targets']:
                prereqs = definition['Targets'][action]['Prerequisites']
                definition['Targets'][action]['Prerequisites'] = get_action_prereqs(prereqs, gameprereqmodule)

        # Check errors
        if len(missingprops) > 0:
            errmsg = f"Entity definition for '{defname}' is missing the following required properties: "
            errmsg += ', '.join(list(missingprops))
            logger.error(errmsg)

        if len(unknownprops) > 0:
            errmsg = f"Entity definition for '{defname}' specified the following unknown properties: "
            errmsg += ', '.join(list(unknownprops))
            logger.error(errmsg)

        if len(missingprops) + len(unknownprops) > 0:
            raise Exception(f"Entity definition for '{defname}' has bad properties.")

    logger.info(f'Finished parsing entity definitions from {filename}')
    return aggregateddefinitions


def load_tile_definitions(filename, offset, tiledefs, lookup=None):
    tree = ElementTree.parse(filename)
    root = tree.getroot()

    tilesetname = root.attrib['name']
    tilewidth = int(root.attrib['tilewidth'])
    tileheight = int(root.attrib['tileheight'])
    tilecount = int(root.attrib['tilecount'])

    # Check sizes
    requiredtilesize = CELL_SIZE * 2

    if tilewidth != requiredtilesize:
        raise Exception(f'Invalid tilewidth in {filename}')

    if tileheight != requiredtilesize:
        raise Exception(f'Invalid tileheight in {filename}')

    imagenode = [child for child in root if child.tag == 'image'][0]
    tilesetimage = imagenode.attrib['source']

    # tilesetimage is relative to location of filename
    filepath = os.path.realpath(filename)
    tilesetbasedir = os.path.split(filepath)[0]
    tilesetimage = os.path.join(tilesetbasedir, tilesetimage)
    tilesetoffset = TILE_CODEPAGE + offset

    # Set tileset
    setstring = 'U+{0}: {1}, size={2}x{2}, align=top-left'.format(hex(tilesetoffset), tilesetimage, requiredtilesize)
    terminal.set(setstring)

    # Read in tile properties
    for child in root.iter('tile'):
        tileid = int(child.attrib['id'])

        # Associated class
        if 'type' not in child.attrib:
            raise Exception(f'Tile in {filename} with id {tileid} has no type specified.')

        tiletype = child.attrib['type']

        # Generic properties
        tileproperties = dict()
        for propnode in child.iter('property'):
            propname = propnode.attrib['name']

            if propname in ['Core.Variant', 'Core.State', 'Core.Frame']:
                continue

            propvalue = propnode.attrib['value']

            if 'type' in propnode.attrib:
                if propnode.attrib['type'] == 'int':
                    propvalue = int(propvalue)
                elif propnode.attrib['type'] == 'bool':
                    propvalue = propvalue == 'true'
                elif propnode.attrib['type'] == 'float':
                    propvalue = float(propvalue)

            tileproperties[propname] = propvalue

        # Parse according to type
        if tiletype in ['VisibleToHostile']:
            # Special tiles
            variant = '_'
            state = '_'
            frame = 1
        elif tiletype in ['Wall', 'Floor', 'FloorOverlay', 'Space']:
            # Variant, frame
            variantnode = child.find("./properties/property[@name='Core.Variant']")
            if variantnode is None:
                raise Exception(f'Tile in {filename} with id {tileid} has no variant specified.')
            variant = variantnode.attrib['value']

            state = '_'

            framenode = child.find("./properties/property[@name='Core.Frame']")
            if framenode is None:
                raise Exception(f'Tile in {filename} with id {tileid}, variant {variant} and state {state} has no frame specified.')
            frame = int(framenode.attrib['value'])
        elif tiletype in ['Item']:
            # Variant, frame
            variantnode = child.find("./properties/property[@name='Core.Variant']")
            if variantnode is None:
                raise Exception(f'Tile in {filename} with id {tileid} has no variant specified.')
            variant = variantnode.attrib['value']

            state = '_'
            statenode = child.find("./properties/property[@name='Core.State']")
            if statenode is not None:
                state = statenode.attrib['value']

            framenode = child.find("./properties/property[@name='Core.Frame']")
            if framenode is None:
                raise Exception(f'Tile in {filename} with id {tileid}, variant {variant} and status {state} has no frame specified.')
            frame = int(framenode.attrib['value'])
        else:
            # Variant, status, frame
            variantnode = child.find("./properties/property[@name='Core.Variant']")
            if variantnode is None:
                raise Exception(f'Tile in {filename} with id {tileid} has no variant specified.')
            variant = variantnode.attrib['value']

            statenode = child.find("./properties/property[@name='Core.State']")
            if statenode is None:
                raise Exception(f'Tile in {filename} with id {tileid} and variant {variant} has no state specified.')
            state = statenode.attrib['value']

            framenode = child.find("./properties/property[@name='Core.Frame']")
            if framenode is None:
                raise Exception(f'Tile in {filename} with id {tileid}, variant {variant} and state {state} has no frame specified.')
            frame = int(framenode.attrib['value'])

        # Get/create tile definition
        tileclassname = tiletype
        if 'Core.Class' in tileproperties:
            tileclassname = tileproperties['Core.Class']

        if tileclassname not in tiledefs:
            tiledefs[tileclassname] = dict()

        tiledef = tiledefs[tileclassname]

        # Get/create variant definition
        if variant not in tiledef:
            tiledef[variant] = dict()

        variantdef = tiledef[variant]

        # Get/create status definition
        if state not in variantdef:
            variantdef[state] = list()

        statusdef = variantdef[state]

        # Set tile id
        while len(statusdef) < frame:
            statusdef.append(None)

        tileoffset = tilesetoffset + tileid
        tiledef = TileDefinition(
            offset=tileoffset,
            type=tiletype,
            variant=variant,
            state=state,
            frame=frame - 1,
            colour=None,
            properties=tileproperties)
        statusdef[frame - 1] = tiledef

        # Add to lookup for the map loading routines
        if lookup:
            lookup[tileoffset] = tiledef

    # Return the new offset
    return tilecount


def create_tile_lookup():
    lookup = dict()
    for className, classData in engine.tiledefinitions.definitions.items():
        for variantName, variantData in classData.items():
            for statesName, statesData in variantData.items():
                for frame in statesData:
                    lookup[frame.offset] = frame
    return lookup
