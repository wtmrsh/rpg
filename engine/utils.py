"""Utils"""

import collections.abc
import copy


class AttributeDict(dict):
    """
    Allow attribute-style access to dictionary keys
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__dict__ = self
        
    def __missing__(self, key):
        return None
        
    def __getattr__(self, key):
        return None


def get_class_bases(classType):
    """

    :param classType:
    :return:
    """
    ret = [] if classType.__name__ == 'object' else [classType.__name__]
    for base in classType.__bases__:
        ret += get_class_bases(base)
    return ret


def get_class_parents(classtype):
    if classtype.__name__ == 'object':
        return None
    else:
        return [ct.__name__ for ct in classtype.__bases__]


def merge_dictionaries(d, u):
    dc = copy.deepcopy(d)
    for k, v in u.items():
        if isinstance(v, collections.abc.Mapping):
            dc[k] = merge_dictionaries(dc.get(k, dict()), v)
        else:
            dc[k] = v
    return dc


def format_column_string(*columns):
    # Columns are string/width pair
    result = ''
    for column in columns:
        str = column[0]
        width = column[1]

        # Clip to column width
        fullstrlen = len(str)
        
        # Calculate length of column string (excluding formatting which does not get rendered)
        strlen = 0
        intag = False
        for i in range(fullstrlen):
            if str[i] == '[':
                intag = True
            elif str[i] == ']':
                intag = False
            elif not intag:
                strlen += 1
        
        if strlen > width:
            keep = width
            intag = False
            newstr = ''
            for i in range(fullstrlen):
                if str[i] == '[':
                    intag = True
                    newstr += str[i]
                elif str[i] == ']':
                    intag = False
                    newstr += str[i]
                elif intag:
                    newstr += str[i]
                elif keep > 0:
                    newstr += str[i]
                    keep -= 1
            str = newstr

        tagslen = fullstrlen - strlen
        str = str.ljust(width + tagslen)
        result += str

    return result
