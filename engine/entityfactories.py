from collections import defaultdict
from functools import partial

from engine.utils import merge_dictionaries
from engine.defines import FacilitatorResult
from engine.stateset import StateSetHandler
import engine.entitydefinitions
import engine.tiledefinitions
import engine.entityfactories


factories = dict()


def create_entity(entityclass, entityname, visualvariant, visualframe, overrides, xoffset=0, yoffset=0):
    """
    Create an Entity instance.

    :param entityclass: class of Entity
    :param entityname: scripted name of Entity
    :param visualvariant: visual variant of entity type
    :param visualframe: animation frame
    :param overrides: entity-specific overrides
    :param xoffset: tile x sub-offset for rendering
    :param yoffset: tile x sub-offset for rendering
    :return: Entity instance
    """
    # Get definition, and merge in overrides
    entitydef = merge_dictionaries(engine.entitydefinitions.definitions[entityclass], overrides)

    # Create entity
    codeclass = entitydef.get('CodeClass', entityclass)
    ent = engine.entityfactories.factories[codeclass](entityclass, **entitydef.get('Arguments', dict()))
    ent.xoffset = xoffset
    ent.yoffset = yoffset

    # Set properties
    entityprops = entitydef['Properties']
    for propclass in entityprops.values():
        propclass.set_property(ent)

    # Set visual
    if entityclass in engine.tiledefinitions.definitions:
        ent.visual.set_visual(vvariant=visualvariant, vframe=visualframe)

    ent.script_name = entityname

    setattr(ent, 'stats', engine.utils.AttributeDict())

    # Add config
    if 'actors' not in ent.__dict__:
        setattr(ent, 'actors', defaultdict(None))
    if 'targets' not in ent.__dict__:
        setattr(ent, 'targets', defaultdict(None))

    # Actors
    if 'Actors' in entitydef:
        for action in entitydef['Actors']:
            engine.action.actor_inst(ent, action)
            ent.actorPrerequisites[action] = entitydef['Actors'][action]['Prerequisites']
            ent.facilitators[action] = entitydef['Actors'][action]['Facilitator']
            ent.aptitudes[action] = entitydef['Actors'][action]['Aptitude']

        # We need to add a Wait actor in by default, as this is the default action
        if 'Pass' not in entitydef['Actors']:
            engine.action.actor_inst(ent, 'Pass')
            ent.actorPrerequisites['Pass'] = list()
            ent.facilitators['Pass'] = lambda a, m: FacilitatorResult(success=True,
                                                                      actionperformed=False,
                                                                      playermsg=None,
                                                                      othermsg=None)

    # Targets
    if 'Targets' in entitydef:
        for action in entitydef['Targets']:
            engine.action.target_inst(ent, action)
            ent.targetPrerequisites[action] = entitydef['Targets'][action]['Prerequisites']

    # Traits
    entitytraits = set()
    if 'Traits' in entitydef:
        traitsdict = entitydef['Traits']
        for traitType, traitSettings in traitsdict.items():
            entitytraits.add(traitType)

            traitclass = traitSettings.get('Specialisation', traitType)
            arguments = traitSettings.get('Parameters', dict())

            engine.traitdecorators.base_trait_inst(ent, traitType, traitclass, **arguments)

    # State Management
    if 'StateManagement' in entitydef:
        smsettings = entitydef['StateManagement']

        if 'Handlers' in smsettings:
            pass

        # Create a callback for each blocker rule
        blockers = smsettings.get('Blockers', dict())

        # Movement blockers
        ssh = StateSetHandler(False, False)
        for blocker in blockers.get('Movement', list()):
            def set_entity_blocks_movement():
                ent.blocksmovement = True

            ssh.add_callback(blocker['RequiredStates'], set_entity_blocks_movement)

        def unset_entity_blocks_movement():
            ent.blocksmovement = False

        ssh.set_fallback(unset_entity_blocks_movement)
        ent.states.add_handlers(None, ssh, ssh)

        # Pathing blockers
        ssh = StateSetHandler(False, False)
        for blocker in blockers.get('Pathing', list()):
            def set_entity_blocks_pathing():
                ent.blockspathing = True

            ssh.add_callback(blocker['RequiredStates'], set_entity_blocks_pathing)

        def unset_entity_blocks_pathing():
            ent.blockspathing = False

        ssh.set_fallback(unset_entity_blocks_pathing)
        ent.states.add_handlers(None, ssh, ssh)

        # Vision blockers
        ssh = StateSetHandler(False, False)
        for blocker in blockers.get('Vision', list()):
            def set_entity_blocks_vision():
                ent.blocksvision = True

            ssh.add_callback(blocker['RequiredStates'], set_entity_blocks_vision)

        def unset_entity_blocks_vision():
            ent.blocksvision = False

        ssh.set_fallback(unset_entity_blocks_vision)
        ent.states.add_handlers(None, ssh, ssh)

        # Audio blockers
        ssh = StateSetHandler(False, False)
        for blocker in blockers.get('Audio', list()):
            def set_entity_blocks_audio():
                ent.blocksaudio = True

            ssh.add_callback(blocker['RequiredStates'], set_entity_blocks_audio)

        def unset_entity_blocks_audio():
            ent.blocksaudio = False

        ssh.set_fallback(unset_entity_blocks_audio)
        ent.states.add_handlers(None, ssh, ssh)

        # Create a callback for each visual rule
        visuals = smsettings.get('Visuals', list())
        ssh = StateSetHandler(False, True)
        for visual in visuals:
            rules = visual['RequiredStates']
            visualstate = visual['Handler']['Visual']

            def set_entity_visual(vs=visualstate):
                ent.visual.set_visual(vstatus=vs)

            ssh.add_callback(rules, set_entity_visual)

        ent.states.add_handlers(None, ssh, ssh)

        # Audio callbacks
        sounds = smsettings.get('Audio')
        if sounds:
            ssh = StateSetHandler(True, False)
            for soundstate, sounddefs in sounds.items():
                for sounddef in sounddefs:
                    handler = sounddef['Handler']
                    audiocf = partial(ent.emit_sound, handler['ActorAction'], handler['YouHear'],
                                      handler['SoundFx'])
                    ssh.add_callback([soundstate], audiocf)

            ent.states.add_handlers(None, ssh, ssh)

    # Stats
    if 'Stats' in entitydef:
        statsdict = entitydef['Stats']
        for statname, statvalue in statsdict.items():
            ent.stats[statname] = statvalue

    # Check all required stats and traits are added
    if hasattr(ent, 'actors'):
        for actorType, actorInstance in ent.actors.items():
            if hasattr(actorInstance, 'requiredStats'):
                for stat in actorInstance.requiredStats:
                    if stat not in ent.stats:
                        raise Exception(
                            f"Stat '{stat}' required for actor action '{actorType}' but not available in entity '{entityclass.__name__}'")
            if hasattr(actorInstance, 'requiredTraits'):
                for trait in actorInstance.requiredTraits:
                    if trait not in entitytraits:
                        raise Exception(
                            f"Trait '{trait}' required for actor action '{actorType}' but not available in entity '{entityclass.__name__}'")
            if hasattr(actorInstance, 'forbiddenTraits'):
                for trait in actorInstance.forbiddenTraits:
                    if trait in entitytraits:
                        raise Exception(
                            f"Trait '{trait}' forbidden by actor action '{actorType}' but available in entity '{entityclass.__name__}'")
            if hasattr(actorInstance, 'forbiddenMembers'):
                for member, value in actorInstance.forbiddenMembers.items():
                    instvalue = getattr(ent, member)
                    if instvalue == value:
                        raise Exception(
                            f"Member '{member}' forbidden by actor action '{actorType}' to have value: {value}")

    if hasattr(ent, 'targets'):
        for targetType, targetInstance in ent.targets.items():
            if hasattr(targetInstance, 'requiredStats'):
                for stat in targetInstance.requiredStats:
                    if stat not in ent.stats:
                        raise Exception(
                            f"Stat '{stat}' required for target action '{targetType}' but not available in entity '{entityclass.__name__}'")
            if hasattr(targetInstance, 'requiredTraits'):
                for trait in targetInstance.requiredTraits:
                    if trait not in entitytraits:
                        raise Exception(
                            f"Trait '{trait}' required for target action '{targetType}' but not available in entity '{entityclass.__name__}'")
            if hasattr(targetInstance, 'forbiddenTraits'):
                for trait in targetInstance.forbiddenTraits:
                    if trait in entitytraits:
                        raise Exception(
                            f"Trait '{trait}' forbidden by target action '{targetType}' but available in entity '{entityclass.__name__}'")
            if hasattr(targetInstance, 'forbiddenMembers'):
                for member, value in targetInstance.forbiddenMembers.items():
                    instvalue = getattr(ent, member)
                    if instvalue == value:
                        raise Exception(
                            f"Member '{member}' forbidden by actor target '{targetType}' to have value: {entityclass.__name__}")

    ent.instantiate_traits()
    ent.creation_finished()
    return ent
