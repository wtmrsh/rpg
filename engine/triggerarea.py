import engine.area
import engine.observable as obs
from engine.traits.useable import Useable
from engine.mapdecorator import owned_by_map


@owned_by_map
class TriggerArea(engine.area.Area):
    """TriggerArea."""

    #
    # __init__
    #
    def __init__(self, owner, func, triggercount, suppress, messages, usetargets, allowedusernames, numusersneeded, userfilterfunc, rect):
        super().__init__()

        if len(usetargets) > 0:
            self.useable = Useable(owner,
                                   dict(),  # No visual statuses for a trigger
                                   suppressmsg=suppress,
                                   playerusemsg=messages[0],
                                   playerfailmsg=messages[1],
                                   otherusemsg=messages[2],
                                   otherfailmsg=messages[3],
                                   allowedusernames=allowedusernames,
                                   numusersneeded=numusersneeded,
                                   userfilterfunc=userfilterfunc)

            for usetarget in usetargets:
                target, count, passnext, updateonfailure, actions = usetarget
                self.useable.add_use_target(target, count, passnext, updateonfailure, actions, list())
        else:
            self.useable = None

        self.owner = owner
        self.func = func
        self.triggercount = triggercount
        self.add_rect(rect)

    #
    # register_events()
    #
    def register_events(self):
        """Register events."""
        obs.on(None, 'Entity.Actor.Move', self.on_entity_moved)

    #
    # unregister_events()
    #
    def unregister_events(self):
        """Unregister events."""
        obs.off(None, 'Entity.Actor.Move', self.on_entity_moved)

    #
    # on_entity_moved()
    #
    def on_entity_moved(self, source, action, entity, target, dx, dy):
        """
        Check whether an entity we're interested in has entered the area.

        :param entity:
        :param target:
        :param dx:
        :param dy:
        :return:
        """
        if self.triggercount == 0:
            return

        if self.entered_area(entity.x, entity.y, entity.x - dx, entity.y - dy):
            # Check if use targets
            if self.useable:
                self.useable.use(entity)

            # Fire the callback
            self.func()

            if self.triggercount > 0:
                self.triggercount -= 1

            if self.triggercount == 0:
                self.unregister_events()
