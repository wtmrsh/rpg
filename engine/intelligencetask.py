from enum import IntEnum, auto
from copy import deepcopy

import engine.pathfinder
from engine.tiletype import TileType
from engine.exceptions import LogicError

intelligence_task_types = dict()


def intelligence_task(name):
    """
    Register a class as an IntelligenceTask.

    :param name: name of task
    """
    def inner(cls):
        global intelligence_task_types
        intelligence_task_types[name] = cls
        return cls

    return inner


class IntelligenceTaskHandler:

    def __init__(self, retrycount, continuetonext, changesupertask):
        self.retrycount = retrycount
        self.initialretrycount = retrycount
        self.continuetonext = continuetonext
        self.changesupertask = changesupertask

    def reset(self):
        self.retrycount = self.initialretrycount


class IntelligenceTask:

    def __init__(self, supertask, actionname, entity, targetname):
        self.supertask = supertask
        self.initialaction = actionname
        self.actionnames = [self.initialaction]

        self.entity = entity
        self.targetname = targetname
        self.target = None

        self.onrequestfailure = None
        self.onexecutionfailure = None

        self.lastrequestedaction = None

    def handle_request_failure(self):
        if self.onrequestfailure:
            if self.onrequestfailure.retrycount != 0:
                self.onrequestfailure.retrycount -= 1
                return [False, self.entity.request_action('Pass', self.entity.map)]
            else:
                if self.onrequestfailure.continuetonext:
                    return [True, None]
                elif self.onrequestfailure.changesupertask:
                    return [True, self.onrequestfailure.changesupertask]
                else:
                    return [False, self.entity.request_action('Pass', self.entity.map)]
        else:
            return [False, self.entity.request_action('Pass', self.entity.map)]

    def handle_execution_failure(self):
        if self.onexecutionfailure:
            if self.onexecutionfailure.retrycount != 0:
                self.onexecutionfailure.retrycount -= 1
                return [False, self.entity.request_action('Pass', self.entity.map)]
            else:
                if self.onexecutionfailure.continuetonext:
                    return [True, None]
                elif self.onexecutionfailure.changesupertask:
                    return [True, self.onexecutionfailure.changesupertask]
                else:
                    return [False, self.entity.request_action('Pass', self.entity.map)]
        else:
            return [None, None]

    def reset(self):
        self.target = self.entity.map.get_named_entity(self.targetname) if self.targetname else self.entity.map

        self.actionnames = [self.initialaction]
        if self.onrequestfailure:
            self.onrequestfailure.reset()
        if self.onexecutionfailure:
            self.onexecutionfailure.reset()
        self.lastrequestedaction = None

    def turn(self, curturn):
        self.entity.request_action('Pass', self.entity.map)
        return False


@intelligence_task('Move')
class MoveIntelligenceTask(IntelligenceTask):

    class State(IntEnum):
        UNKNOWN = auto()
        MOVE = auto()
        UNLOCK_OPEN = auto()
        CLOSE_LOCK = auto()

    def __init__(self, supertask, actionname, entity, targetname, x, y, closedoors=True, lockdoors=True):
        super().__init__(supertask, 'Move', entity, None)
        self.x = x
        self.y = y
        self.closedoors = closedoors
        self.lockdoors = lockdoors
        self.route = None
        self.targetpos = None
        self.transition = None
        self.state = MoveIntelligenceTask.State.UNKNOWN

    def reset(self):
        super().reset()
        self.calculate_route(self.x, self.y)
        self.set_state(MoveIntelligenceTask.State.MOVE)
        self.transition = None

    def calculate_route(self, tx, ty):
        pather = engine.pathfinder.PathFinder(self.entity.map)
        self.route = pather.generate_path(self.entity, tx, ty)
        self.targetpos = (self.entity.owning_tile.x, self.entity.owning_tile.y)

    def set_state(self, state):
        self.state = state

    def turn(self, curturn):

        # TODO: store the proper route when we switch to move-to-door so we don't have to recalc it.
        entitytile = self.entity.owning_tile
        entitypos = (entitytile.x, entitytile.y)

        # At the start of each turn, we expect to be on the tile we are going to perform the action on.
        # If we aren't, then we calculate a path to it, and start moving.
        if self.targetpos and self.targetpos != entitypos:
            self.calculate_route(self.targetpos[0], self.targetpos[1])
            self.set_state(MoveIntelligenceTask.State.MOVE)

        actionchosen = False
        while not actionchosen:
            if self.state == MoveIntelligenceTask.State.MOVE:
                try:
                    # Are we moving through a door?
                    if self.transition:
                        # By default, all entities will have the 'Move' action
                        etd = self.transition.entities[self.entity]
                        if self.targetpos and self.targetpos == etd.exit:
                            # We're at the exit of the door.  If there's other entities which have registered as 'Move'
                            # with this door, then let them close it, unless we have registered as the locker.
                            otherusers = self.transition.get_other_users(self.entity)
                            if len(otherusers) == 0:
                                self.set_state(MoveIntelligenceTask.State.CLOSE_LOCK)
                                continue

                            # Someone else is using the door, so delegate closing and locking
                            for action in ['Open', 'Unlock']:
                                if action in etd.actions:
                                    self.transition.register_entity_action(otherusers[0], action, curturn)

                            self.transition.unregister_entity(self.entity)
                            self.transition = None

                    # Get the next tile to move to.  If there isn't one, we've reached the end of this current
                    # move and have to either generate a new route or finish the task.
                    self.targetpos = next(self.route)
                    if not self.targetpos:
                        # We couldn't get the route, so pass
                        result = self.entity.request_action('Pass', self.entity.map)
                        if result.passed:
                            actionchosen = True
                    else:
                        tx, ty = self.targetpos
                        tile = self.entity.map.tiles[tx][ty]

                        if tile.type == TileType.DOOR and not self.transition:
                            self.transition = self.entity.map.doortransitions[tile.door]
                            self.targetpos = entitypos
                            self.set_state(MoveIntelligenceTask.State.UNLOCK_OPEN)
                        else:
                            # Is there anything blocking the tile, which could be moved out of the way?
                            if tile.entity:
                                if tile.entity.blocks_movement():
                                    self.calculate_route(self.x, self.y)
                                else:
                                    result = self.entity.request_action('Move', self.entity.map, tx, ty)
                                    actionchosen = result.passed
                            else:
                                result = self.entity.request_action('Move', self.entity.map, tx, ty)
                                actionchosen = result.passed
                except StopIteration:
                    if entitypos[0] == self.x and entitypos[1] == self.y:
                        return True, None
                    else:
                        self.calculate_route(self.x, self.y)
            elif self.state == MoveIntelligenceTask.State.UNLOCK_OPEN:
                # If the door is locked, then try to unlock it, otherwise try and open it
                if not self.transition.entity.has_state('Open'):
                    if not self.transition.entity.has_state('Locked'):
                        dooraction = 'Open'
                    else:
                        dooraction = 'Unlock'

                    result = self.entity.request_action(dooraction, self.transition.entity)
                    if result.passed:
                        actionchosen = result.passed
                        if (dooraction == 'Open' and self.closedoors) or (dooraction == 'Unlock' and self.lockdoors):
                            self.transition.register_entity_action(self.entity, dooraction, curturn)
                    else:
                        self.transition.unregister_entity(self.entity)
                        self.transition = None
                        self.calculate_route(self.x, self.y)
                        self.set_state(MoveIntelligenceTask.State.MOVE)
                else:
                    # Door is open.  We need to check if it's blocked.  If it isn't, then set path to the other side of
                    # the door and move.  If it is, then we do essentially the same thing but have to clear the
                    # transition as we aren't going through the door.
                    targetpos = self.transition.get_exit(self.targetpos)
                    self.calculate_route(targetpos[0], targetpos[1])

                    if self.transition.entity.owning_tile.blocks_pathing(self.entity):
                        self.transition = None
                    else:
                        self.transition.register_entity_action(self.entity, 'Move', curturn)

                    self.set_state(MoveIntelligenceTask.State.MOVE)
            elif self.state == MoveIntelligenceTask.State.CLOSE_LOCK:
                # If we tried to open the door then close it
                dooraction = None
                etd = self.transition.entities[self.entity]
                if self.transition.entity.has_state('Open') and 'Open' in etd.actions:
                    dooraction = 'Close'
                elif not self.transition.entity.has_state('Locked') and 'Unlock' in etd.actions:
                    dooraction = 'Lock'

                if dooraction:
                    result = self.entity.request_action(dooraction, self.transition.entity)
                    actionchosen = result.passed
                    if not actionchosen and len(self.transition.entities) > 1:
                        # We've failed to close or lock the door.  If there's others registered as going through
                        # the door, then wait, otherwise give up.
                        result = self.entity.request_action('Wait', self.entity.map)
                        actionchosen = result.passed

                # Nothing to do
                if not actionchosen:
                    self.transition.unregister_entity(self.entity)
                    self.transition = None
                    self.targetpos = (self.x, self.y)
                    self.calculate_route(self.targetpos[0], self.targetpos[1])
                    self.set_state(MoveIntelligenceTask.State.MOVE)

            else:
                raise LogicError(f'Unhandled MoveIntelligenceTask.State: {self.state}')

        if actionchosen and result.passed:
            self.lastrequestedaction = (self.entity.roundaction.action,
                                        self.entity.roundaction.target,
                                        self.entity.roundaction.args,
                                        self.entity.roundaction.kwargs)

        return False, None


@intelligence_task('_')
class ActionIntelligenceTask(IntelligenceTask):

    def __init__(self, supertask, actionname, entity, targetname, x, y, **kwargs):
        super().__init__(supertask, actionname, entity, targetname)
        self.x = x
        self.y = y
        self.movetask = None
        self.args = kwargs

    def reset(self):
        super().reset()
        self.movetask = None

    def turn(self, curturn):
        if self.movetask:
            complete = self.movetask.turn(curturn)
            if not complete:
                return False, None

            self.movetask = None

        # See if our last action was a) what we requested and b) successful
        if self.entity.lastaction and self.lastrequestedaction:
            if self.entity.lastaction[:4] == self.lastrequestedaction[:4] and self.entity.lastaction[4].success:
                self.actionnames.pop()
                if len(self.actionnames) == 0:
                    return True, None
            else:
                complete, res = self.handle_execution_failure()
                if complete is True:
                    return True, res
                elif complete is False:
                    return False, None

        if self.x is not None and self.y is not None and (self.entity.x != self.x or self.entity.y != self.y):
            typefact = intelligence_task_types['Move']
            self.movetask = typefact(supertask=self.supertask,
                                     actionname='Move',
                                     entity=self.entity,
                                     targetname=None,
                                     x=self.x,
                                     y=self.y)
            self.movetask.reset()

            if not self.movetask.turn(curturn):
                return False, None
        else:
            res = self.entity.request_action(self.actionnames[-1], self.target, **self.args)

            # If action does not pass and there are no prereqs, and action is required, then call on-request-failure
            # For on-execution-failure and on-execution-success need to be done when the action is actually executed
            while not res.passed:
                if res.actionrequired:
                    if len(res.prereqactions) > 0:
                        pa = res.prereqactions[0]
                        self.actionnames.append(pa['Action'])
                        res = self.entity.request_action(pa['Action'], self.target, **pa['Args'])
                    else:
                        complete, res = self.handle_request_failure()
                        if complete:
                            return True, res
                else:
                    return True, None

            if res.passed:
                self.lastrequestedaction = (self.entity.roundaction.action,
                                            self.entity.roundaction.target,
                                            self.entity.roundaction.args,
                                            self.entity.roundaction.kwargs)

        return False, None

