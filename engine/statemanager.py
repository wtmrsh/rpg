from engine.exceptions import *


class StateManager:

    def __init__(self, initialstate=None):
        self.states = list()

        if initialstate:
            self.move_to(initialstate, False)

    def move_to(self, state, join, *args, **kwargs):
        self.states.append(state)
        if join:
            self.join(*args, **kwargs)

    def join(self, *args, **kwargs):
        curstate = self.states[-2] if len(self.states) > 1 else None
        newstate = self.states[-1]

        if curstate:
            curstate.suspend(newstate)
        newstate.enter(curstate, len(self.states) - 1, *args, **kwargs)

    def exit_current(self, playeraction, *args, **kwargs):
        newstate = self.states[-2] if len(self.states) > 1 else None
        curstate = self.states[-1]

        self.states.remove(curstate)
        curstate.exit(*args, **kwargs)

        if newstate:
            newstate.resume(curstate, playeraction, *args, **kwargs)
        else:
            raise ExitException()

    def transition_to(self, tostate, popparent, *args, **kwargs):
        # Exit old
        curstate = self.states[-1]

        self.states.remove(curstate)
        curstate.exit(*args, **kwargs)

        if popparent:
            curstate = self.states[-1]

            self.states.remove(curstate)
            curstate.exit(*args, **kwargs)

        # Enter new
        self.states.append(tostate)
        tostate.enter(curstate, len(self.states) - 1, *args, **kwargs)

    def handle_input(self):
        try:
            self.states[-1].handle_input()
        except SpawnChildStateException as e:
            self.move_to(e.tostate, True, *e.args, **e.kwargs)
        except TransitionToStateException as e:
            self.transition_to(e.tostate, e.popparent, *e.args, **e.kwargs)
        except ExitStateException as e:
            self.exit_current(e.playeraction, *e.args, **e.kwargs)

    def update(self, frametime):
        try:
            self.states[-1].update(frametime)
        except SpawnChildStateException as e:
            self.move_to(e.tostate, True, *e.args, **e.kwargs)
        except TransitionToStateException as e:
            self.transition_to(e.tostate, e.popparent, *e.args, **e.kwargs)
        except ExitStateException as e:
            self.exit_current(e.playeraction, *e.args, **e.kwargs)

    def render(self):
        numStates = len(self.states)
        for i, state in enumerate(self.states):
            state.render(i == numStates - 1)

