from collections import defaultdict

from engine.tiletype import TileType

actor_types = dict()
target_types = dict()


def register_actor_type(name, type):
    """
    Register a type for instantiation.

    :param name: name of action
    :param type: type
    """
    global actor_types
    actor_types[name] = type


def register_target_type(name, type):
    """
    Register a type for instantiation.

    :param name: name of action
    :param type: type
    """
    global target_types
    target_types[name] = type


def action_actor(*names):
    """
    Class decorator to add names of actions that the object can perform.
    
    :param names: action names
    :return: wrapped class
    """
    def inner(cls):
        cls.names = [*names]

        for name in names:
            register_actor_type(name, cls)

        return cls
    return inner


def action_target(*names):
    """
    Class decorator to add names of actions that the object can have performed on it.
    
    :param names: action names
    :return: wrapped class.
    """
    def inner(cls):
        cls.names = [*names]

        for name in names:
            register_target_type(name, cls)

        return cls
    return inner


def actor_inst(entity, action):
    """
    Add an instance of an action to an object.
    
    :param entity: entity to add to
    :param action: name of action to instantiate
    """
    if 'actors' not in entity.__dict__:
        setattr(entity, 'actors', defaultdict(None))

    # Add all aliases
    actionInst = actor_types[action]()
    for name in actionInst.names:
        if name in entity.actors:
            continue
        entity.actors[name] = actionInst


def exposes_actor(action, membername=None):
    """
    Class decorator to wrap actor_inst()
    Used when defining entities or traits in code and not config.

    :param action: name of action
    :param membername: name of entity member to add to, or None for self
    :return: wrapped class
    """
    def inner(cls):
        oldinit = cls.__init__

        def newinit(self, *args, **kwargs):
            oldinit(self, *args, **kwargs)
            member = self.__dict__[membername] if membername else self
            actor_inst(member, action)

        cls.__init__ = newinit
        return cls
    return inner


def target_inst(entity, action):
    """
    Add an instance of an action to an object.
    
    :param entity: entity to add to
    :param action: name of action to instantiate
    """
    if 'targets' not in entity.__dict__:
        setattr(entity, 'targets', defaultdict(None))

    actionInst = target_types[action]()
    for name in actionInst.names:
        if name in entity.targets:
            continue
        entity.targets[name] = actionInst


def exposes_target(action, membername=None):
    """
    Class decorator to wrap target_inst()
    Used when defining entities or traits in code and not config.

    :param action: name of action
    :param membername: name of entity member to add to, or None for self
    :return: wrapped class
    """

    def inner(cls):
        oldinit = cls.__init__

        def newinit(self, *args, **kwargs):
            oldinit(self, *args, **kwargs)
            member = self.__dict__[membername] if membername else self
            target_inst(member, action)

        cls.__init__ = newinit
        return cls
    return inner


def required_stat(stat):
    """
    Class decorator to specify that an object requires a specific stat
    
    :param stat: name of stat required
    :return: wrapped class
    """
    def inner(cls):
        oldinit = cls.__init__

        def newinit(self, *args, **kwargs):
            if not hasattr(self, 'requiredStats'):
                setattr(self, 'requiredStats', set())
            self.requiredStats.add(stat)
            oldinit(self, *args, **kwargs)

        cls.__init__ = newinit
        return cls
    return inner


def required_trait(trait):
    """
    Class decorator to specify that an action requires a specific trait

    :param trait: name of trait required
    :return: wrapped class
    """
    def inner(cls):
        oldinit = cls.__init__

        def newinit(self, *args, **kwargs):
            if not hasattr(self, 'requiredTraits'):
                setattr(self, 'requiredTraits', set())
            self.requiredTraits.add(trait)
            oldinit(self, *args, **kwargs)

        cls.__init__ = newinit
        return cls
    return inner


def forbidden_trait(trait):
    """
    Class decorator to specify that an action means a specific trait cannot be used

    :param trait: name of trait required
    :return: wrapped class
    """
    def inner(cls):
        oldinit = cls.__init__

        def newinit(self, *args, **kwargs):
            if not hasattr(self, 'forbiddenTraits'):
                setattr(self, 'forbiddenTraits', set())
            self.forbiddenTraits.add(trait)
            oldinit(self, *args, **kwargs)

        cls.__init__ = newinit
        return cls
    return inner


def forbidden_member(member, value):
    """
    Class decorator to specify that an action means an instance member cannot have a particular value

    :param member: name of member
    :param value: value
    :return: wrapped class
    """
    def inner(cls):
        oldinit = cls.__init__

        def newinit(self, *args, **kwargs):
            if not hasattr(self, 'forbiddenMembers'):
                setattr(self, 'forbiddenMembers', dict())
            self.forbiddenMembers[member] = value
            oldinit(self, *args, **kwargs)

        cls.__init__ = newinit
        return cls
    return inner


def action_affects_movement(action, actor, target):
    """
    This function determines whether or not an action has an impact on movement resolution, and hence whether it
    should be considered a 'movement' action, and processed with actual moves.  It does not check if the action is
    a move itself.

    :param action: action name.
    :param actor: actor instance.
    :param target: target instance.
    :return: whether or not the action has an effect on movement processing
    """
    if target.actorType == 'Map':
        return False

    tile = target.owning_tile
    if tile:
        if tile.type == TileType.DOOR and target == tile.door:
            return True
        elif tile.type == TileType.WINDOW and target == tile.window:
            return True

    return False


class Actor:
    def get_valid_count(self, actor, target, actioncount, itemcount):
        """
        Return the number of entities that an actor can perform an action on.
        
        :param actor: subject actor
        :param target: object target
        :param actioncount: number of target entities desired to act on
        :param itemcount: number of target entities available
        :return: the number of target entities the actor can perform the action on
        """
        if actioncount > 1:
            return 1, 'You can only act on one of these per turn.'
        else:
            return 1, None

    @classmethod
    def get_target_location(cls, actor, *args, **kwargs):
        """
        Get the expected location of the actor after the action.

        :param actor: actor in question.
        :param args: passed into ActionParticipant.request_action()
        :param kwargs: passed into ActionParticipant.request_action()
        :return: (x, y) position tuple
        """
        return actor.x, actor.y


class Target:
    def get_valid_count(self, actor, target, actioncount, itemcount):
        """
        Return how many target entities can have an action performed on them.
        
        :param actor: subject actor
        :param target: object target
        :param actioncount: number of target entities desired to act on
        :param itemcount: number of target entities available
        :return: the number of target entities the actor can perform the action on
        """
        if actioncount > 1:
            return 1, 'Only one of these can be acted on per turn.'
        else:
            return 1, None


@action_actor('Move')
class MoveActor(Actor):

    @classmethod
    def get_target_location(cls, actor, *args, **kwargs):
        """
        Get the expected location of the actor after the action.

        :param actor: actor in question.
        :param args: passed into ActionParticipant.request_action()
        :param kwargs: passed into ActionParticipant.request_action()
        :return: (x, y) position tuple
        """
        return args[0], args[1]


@action_target('Move')
class MoveTarget(Target):
    pass


@action_actor('Pass')
class PassActor(Actor):
    pass


@action_actor('Wait')
class WaitActor(Actor):
    pass


@action_actor('Open')
class OpenActor(Actor):
    pass


@action_target('Open')
class OpenTarget(Target):
    pass


@action_actor('Close')
class CloseActor(Actor):
    pass


@action_target('Close')
class CloseTarget(Target):
    pass


@action_actor('Lock')
class LockActor(Actor):
    pass


@action_target('Lock')
class LockTarget(Target):
    pass


@action_actor('Unlock')
class UnlockActor(Actor):
    pass


@action_target('Unlock')
class UnlockTarget(Target):
    pass


@action_actor('Use')
class UseActor(Actor):
    pass


@action_target('Use')
class UseTarget(Target):
    pass


@action_actor('ModifyLogicState')
class ModifyLogicStateActor(Actor):
    pass


@action_target('ModifyLogicState')
class ModifyLogicStateTarget(Target):
    pass


@action_actor('Get', 'Pickup')
class GetActor(Actor):

    #
    # get_valid_count()
    #
    def get_valid_count(self, actor, target, actioncount, itemcount):
        """
        Return the number of entities that an actor can perform an action on.
        
        :param actor: subject actor
        :param target: object target
        :param actioncount: number of target entities desired to act on
        :param itemcount: number of target entities available
        :return: the number of target entities the actor can perform the action on
        """
        validcount = min(actioncount, actor.inventory.spare_capacity // target.weight)
        if itemcount < validcount:
            return 0, 'Selected count is too high.'
        elif validcount == 0:
            if actioncount == 1:
                return validcount, 'You do not have enough inventory space to pick up this.'
            else:
                return validcount, f'You do not have enough inventory space to pick up {actioncount} of these.'
        elif validcount < actioncount:
            return validcount, f'You do not have enough inventory space to pick up {actioncount} of these.'
        else:
            return validcount, None


@action_target('Get', 'Pickup')
class GetTarget(Target):

    #
    # get_valid_count()
    #
    def get_valid_count(self, actor, target, actioncount, itemcount):
        """
        Return how many target entities can have an action performed on them.

        :param actor: subject actor
        :param target: object target
        :param actioncount: number of target entities desired to act on
        :param itemcount: number of target entities available
        :return: the number of target entities the actor can perform the action on
        """
        return actioncount, None


@action_actor('Drop')
class DropActor(Actor):
    pass


@action_target('Drop')
class DropTarget(Target):
    pass


@action_actor('EnableTrait')
class EnableTraitActor(Actor):
    pass


@action_target('EnableTrait')
class EnableTraitTarget(Target):
    pass


@action_actor('SetIntelligenceTask')
class EnableTraitActor(Actor):
    pass


@action_target('SetIntelligenceTask')
class EnableTraitTarget(Target):
    pass


@action_actor('Destroy')
class DestroyActor(Actor):
    pass


@action_target('Destroy')
class DestroyTarget(Target):
    pass
