import libtcodpy as libtcod


class PathFinder:
    """Calculates paths."""
    
    #
    # __init__()
    #
    def __init__(self, map):
        """
        Constructor.
        
        :param map: Map to generate paths on
        """
        self.map = map

    #
    # audio_move_cost()
    #
    def audio_move_cost(self, x0, y0, x1, y1, userdefined):
        """
        Cost of moving between tiles

        :param x0: x-position of start
        :param y0: y-position of start
        :param x1: x-position of end
        :param y1: y-position of end
        :param userdefined: user data
        :return: cost.  Zero means blocked
        """
        if self.map.tiles[x1][y1].blocks_audio:
            return 0.0

        if x0 != x1 and y0 != y1:
            if self.map.tiles[x0][y1].blocks_audio and self.map.tiles[x1][y0].blocks_audio:
                return 0.0

        return 1.0

    def move_cost(self, x0, y0, x1, y1, entity):
        """
        Cost of moving between tiles
        
        :param x0: x-position of start
        :param y0: y-position of start
        :param x1: x-position of end
        :param y1: y-position of end
        :param entity: moving entity
        :return: cost.  Zero means blocked
        """
        if x0 != x1 and y0 != y1:
            targettile = self.map.tiles[x1][y1]
            if targettile.blocks_pathing(entity):
                return 0.0

            targettile = self.map.tiles[x0][y1]
            if targettile.blocks_pathing(entity):
                return 0.0

            targettile = self.map.tiles[x1][y0]
            if targettile.blocks_pathing(entity):
                return 0.0
        elif x0 != x1:
            targettile = self.map.tiles[x1][y0]
            if targettile.blocks_pathing(entity):
                return 0.0
        elif y0 != y1:
            targettile = self.map.tiles[x0][y1]
            if targettile.blocks_pathing(entity):
                return 0.0

        # libtcod already takes diagonals into account
        targettile = self.map.tiles[x1][y1]
        if targettile.blocks_pathing(entity):
            return 0.0
        else:
            return 1.0

    def calculate_path(self, entity, x, y):
        """
        Calculate a path.

        :param entity: entity
        :param x: x-position of end
        :param y: y-position of end
        :return: path
        """
        path = libtcod.path_new_using_function(self.map.sizeX, self.map.sizeY, func=self.move_cost, userdata=entity)
        libtcod.path_compute(path, entity.x, entity.y, x, y)

        nodes = list()
        for i in range(libtcod.path_size(path)):
            nodes.append(libtcod.path_get(path, i))

        libtcod.path_delete(path)
        return nodes

    def calculate_audio_path(self, x0, y0, x1, y1):
        """
        Calculate a path for audio to propagate along.

        :return: path
        """
        path = libtcod.path_new_using_function(self.map.sizeX, self.map.sizeY, func=self.audio_move_cost)
        libtcod.path_compute(path, x0, y0, x1, y1)

        nodes = list()
        for i in range(libtcod.path_size(path)):
            nodes.append(libtcod.path_get(path, i))

        libtcod.path_delete(path)
        return nodes

    def generate_path(self, entity, x, y):
        """
        Calculate a path.
        
        :param entity: entity
        :param x: x-position of end
        :param y: y-position of end
        :return: path
        """
        #  try/finally ensures that the path is deleted if the generator is garbage-collected before it runs out.
        try:
            path = libtcod.path_new_using_function(self.map.sizeX, self.map.sizeY, func=self.move_cost, userdata=entity)
            libtcod.path_compute(path, entity.x, entity.y, x, y)

            pathlen = libtcod.path_size(path)
            if pathlen == 0:
                yield None
            else:
                for i in range(pathlen):
                    yield libtcod.path_get(path, i)
        finally:
            libtcod.path_delete(path)
