"""
Generic area
"""

#
# Imports
#
from collections import namedtuple

from common import *


#
# Area
#
class Area:
    """Area."""

    Rect = namedtuple('Rect', 'x,y,width,height')

    #
    # __init__()
    #
    def __init__(self):
        """
        Area on a map.
        """
        self.rects = []

    #
    # addRect()
    #
    def add_rect(self, rect):
        """
        Add a rectangle.  This converts width and height parameters to positions.

        :param rect: Rect object
        """
        self.rects.append((rect.x, rect.y, rect.x + rect.width, rect.y + rect.height))

    #
    # inArea()
    #
    def in_area(self, x, y):
        """
        Test whether a given point is within this area

        :param x: x position
        :param y: y position
        :return: boolean specifying whether the point is within the area or not
        """
        for rect in self.rects:
            if x >= rect[0] and y >= rect[1] and x < rect[2] and y < rect[3]:
                return True

        return False

    #
    # enteredArea()
    #
    def entered_area(self, x, y, oldX, oldY):
        """
        Test whether something has entered.

        :param x: current x
        :param y: current y
        :param oldX: old x
        :param oldY: old y
        :return: True or not
        """
        # This way around to avoid short-circuiting
        return not self.in_area(oldX, oldY) and self.in_area(x, y)
