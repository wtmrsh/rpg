class EntityProperty:
    def __init__(self, name, required, defaultvalue=None):
        self.name = name
        self.required = required
        self.format = ''
        self.help = ''
        self.value = defaultvalue

    def validate_definition_type(self, value):
        return True

    def convert_to_type(self, value):
        return value

    def get_error_msg(self, entityname):
        return f"Entity property '{self.name}' for entity '{entityname}' requires: {self.format}"

    def parse_from_definition(self, entityname, value):
        if self.validate_definition_type(value):
            self.value = self.convert_to_type(value)
        else:
            raise TypeError(self.get_error_msg(entityname))

    def set_property(self, entity):
        pass

