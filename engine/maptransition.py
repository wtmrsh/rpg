"""Map transitions."""

#
# MapTransition
#
class MapTransition():
    """Transition from one map to another."""

    #
    # __init__()
    #
    def __init__(self, fromMap, toMap):
        """
        Constructor.

        :param fromMap: map that the transition starts on
        :param toMap: map that the transition ends on
        """
        self.fromMap = fromMap
        self.toMap = toMap

    #
    # execute()
    #
    def execute(self, player):
        """
        Perform the transition.

        :param player: player Entity
        :return: new map
        """
        return self.toMap