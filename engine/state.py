import engine.observable as obs
from engine.defines import InputType, ActionResult
from engine.viewmanager import ViewManager
from engine.exceptions import SpawnChildStateException, ExitStateException


class State:

    def __init__(self, name):
        self.name = name
        self.viewmanager = ViewManager(self.name)
        self.model = None
        self.controller = None
        self.suspended = False
        self.debugTurn = 0
        self.playeraction = [None, False]
        self.reset_action()
        self.actionResult = None

    def reset_action(self):
        self.actionResult = ActionResult(
            turntaken=False,
            printmsg=True,
            playermsg=None,
            othermsg=None,
            inputtype=InputType.IMMEDIATE,
            delayedaction=None)

    def enter(self, fromstate, stateindex, *args, **kwargs):
        self.reset_action()

        self.viewmanager.stateIndex = stateindex
        self.viewmanager.update_mouse()

        self.enter_pre()
        self.enter_impl(fromstate, *args, **kwargs)

        self.create_views(self.viewmanager)
        self.setup_model()
        self.intialise_views(self.viewmanager, self.model)

        self.setup_controller()
        self.enter_post()
        
    def setup_model(self):
        pass

    def create_views(self, viewmanager):
        viewmanager.clear()

    def intialise_views(self, viewmanager, model):
        pass

    def setup_controller(self):
        pass

    def enter_pre(self):
        pass

    def enter_post(self):
        pass

    def enter_impl(self, fromstate, *args, **kwargs):
        pass

    def exit(self, *args, **kwargs):
        self.exit_pre()
        self.exit_impl(*args, **kwargs)
        self.exit_post()

        # Clear view
        obs.trigger(None, 'UI.ClearViews')
        self.viewmanager.unregister_events()
        self.viewmanager.clear_cursor()

    def exit_pre(self):
        pass

    def exit_post(self):
        pass

    def exit_impl(self, *args, **kwargs):
        pass

    def change_state(self, nextstate, *args, **kwargs):
        raise SpawnChildStateException(nextstate, *args, **kwargs)

    def exit_state(self):
        raise ExitStateException(None)

    def suspend(self, newstate):
        self.viewmanager.clear_cursor()

        self.suspended = True
        self.suspend_pre()
        self.suspend_impl(newstate)
        self.suspend_post()

    def suspend_pre(self):
        pass

    def suspend_post(self):
        pass

    def suspend_impl(self, newstate):
        pass

    def resume(self, oldstate, playeraction, *args, **kwargs):
        self.suspended = False

        self.resume_pre()
        self.resume_impl(oldstate, *args, **kwargs)
        self.resume_post()

        self.viewmanager.update_mouse()

        # Set up player action
        if playeraction:
            self.playeraction = [playeraction, True]
        else:
            self.reset_action()

        # Trigger view update
        obs.trigger(None, 'UI.RefreshViews')

    def resume_pre(self):
        pass

    def resume_post(self):
        pass

    def resume_impl(self, oldstate, *args, **kwargs):
        pass

    def handle_input(self):
        if not self.playeraction[0]:
            self.playeraction = self.viewmanager.handle_input(self.actionResult, self.controller, self.model)

    def update(self, frametime):
        self.update_pre(frametime)
        self.viewmanager.update(frametime)

        if self.playeraction[0]:
            actionFunc = self.playeraction[0]
            executeNow = self.playeraction[1]
            funcArgs = self.playeraction[2:]

            self.playeraction = [None, False]

            if executeNow:
                if len(funcArgs) > 0:
                    self.actionResult = actionFunc(self.model, funcArgs)
                else:
                    self.actionResult = actionFunc(model=self.model)
            else:
                self.actionResult.turntaken = False
                self.actionResult.delayedaction = actionFunc

            if self.actionResult.printmsg and self.actionResult.playermsg:
                obs.trigger(None, 'Game.Message', self.actionResult.playermsg)
        else:
            self.actionResult = ActionResult(
                turntaken=False,
                playermsg=None,
                printmsg=True,
                othermsg=None,
                inputtype=self.actionResult.inputtype,
                delayedaction=self.actionResult.delayedaction)

        self.update_impl(self.actionResult, frametime)
        self.update_post(frametime)

    def update_pre(self, frametime):
        pass

    def update_post(self, frametime):
        pass

    def update_impl(self, action, frametime):
        pass

    def render(self, isactive):
        self.viewmanager.display(isactive)
