"""
Button for views
"""

#
# Imports
#
from bearlibterminal import terminal

from common import *

#
# Button
#
class Button():
    """Button."""

    #
    # __init__()
    #
    def __init__(self, parent, x, y, height, text, helpText, key, renderBackground, handler):
        """
        Buttons for Views.
        
        :param parent: owning View
        :param x: x position within View
        :param y: y position within View
        :param height: height of button
        :param text: text to show
        :param helpText: text shown on tooltip
        :param key: hotkey
        :param renderBackground: whether or not to render background
        :param handler: callback for when left-clicked
        """
        self.parent = parent
        self.x = x
        self.y = y
        self.text = text
        self.helpText = helpText
        self.key = key
        self.renderBackground = renderBackground
        self.handler = handler
        self.width = len(text)
        self.height = height
        self.show = True
        self.leftPiece = None
        self.rightPiece = None
        self.centrePiece = None
        self.topLeftPiece = None
        self.topPiece = None
        self.topRightPiece = None
        self.bottomLeftPiece = None
        self.bottomPiece = None
        self.bottomRightPiece = None
        self.buttonBackColour = None
        self.buttonColour = None
        self.buttonBackHoveredColour = None
        self.buttonHoveredColour = None
        self.padding = 0

    #
    # offset_x
    #
    @property
    def xOffset(self):
        """Screen x-position"""
        return self.x + self.parent.client_x

    #
    # offset_y
    #
    @property
    def yOffset(self):
        """Screen y-position"""
        return self.y + self.parent.client_y

    #
    # clear()
    #
    def clear(self):
        """Clear button background"""
        pass

    #
    # render_background()
    #
    def render_background(self, hover):
        """
        Render background.

        :param hover: whether or not this button is hovered over.
        """
        # Check for right-align
        if self.x < 0:
            buttonSize = self.width + self.padding * 2
            xPos = self.parent.client_extent_x - buttonSize
        else:
            xPos = self.xOffset

        yPos = self.yOffset

        terminal.layer(self.parent.base_layer)

        if hover:
            terminal.color(self.buttonBackHoveredColour)
        else:
            terminal.color(self.buttonBackColour)

        if self.height == 1:
            terminal.put(xPos, yPos, self.leftPiece)

            for x in range(xPos + 1, xPos + self.width + self.padding * 2 - 1):
                terminal.put(x, yPos, self.centrePiece)

            terminal.put(xPos + self.width + self.padding * 2 - 1, yPos, self.rightPiece)
        elif self.height == 2:
            # Top
            terminal.put(xPos, yPos, self.topLeftPiece)

            for x in range(xPos + 1, xPos + self.width + self.padding * 2 - 1):
                terminal.put(x, yPos, self.topPiece)

            terminal.put(xPos + self.width + self.padding * 2 - 1, yPos, self.topRightPiece)

            # Bottom
            terminal.put(xPos, yPos + 1, self.bottomLeftPiece)

            for x in range(xPos + 1, xPos + self.width + self.padding * 2 - 1):
                terminal.put(x, yPos + 1, self.bottomPiece)

            terminal.put(xPos + self.width + self.padding * 2 - 1, yPos + 1, self.bottomRightPiece)

    #
    # render()
    #
    def render(self, hover):
        """
        Render.

        :param hover: whether or not this button is hovered over.
        """
        if self.x < 0:
            buttonSize = self.width + self.padding * 2
            xPos = self.parent.client_extent_x - buttonSize
        else:
            xPos = self.xOffset

        yPos = self.yOffset

        terminal.layer(self.parent.base_layer + 1)

        if hover:
            terminal.color(self.buttonHoveredColour)
        else:
            terminal.color(self.buttonColour)

        text = self.text
        if self.height == 2:
            text = ('[offset=0,%d]' % (CELL_SIZE // 2)) + text

        terminal.print(xPos + self.padding, yPos, text)
