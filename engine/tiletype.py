from enum import IntEnum, IntFlag, auto


class TileType(IntEnum):
    UNKNOWN = auto()
    WALL = auto()
    FLOOR = auto()
    SPACE = auto()
    WINDOW = auto()
    DOOR = auto()
