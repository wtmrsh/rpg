"""
Base entity properties.
"""

#
# Imports
#
from engine.entity import EntityStateFilter
from engine.entityproperty import EntityProperty
from engine.defines import EntityCategory
from engine.colour import Colour
from engine.gamemodule import get_game_factions_module


#
# NamesEntityProperty
#
class NamesEntityProperty(EntityProperty):
    def __init__(self):
        super().__init__('names', True)
        self.format = 'list of 2 strings'
        self.help = 'The printable names of one entity and multiple entities, respectively'

    def validate_definition_type(self, value):
        return type(value) is list and len(value) == 2 and type(value[0]) is str and type(value[1]) is str

    def set_property(self, entity):
        entity._name = self.value[0]
        entity._nameplural = self.value[1]


#
# CategoryEntityProperty
#
class CategoryEntityProperty(EntityProperty):
    def __init__(self):
        super().__init__('category', True)
        self.format = 'EntityCategory enum name'
        self.help = 'The category of an entity'

    def validate_definition_type(self, value):
        return type(value) is str

    def convert_to_type(self, value):
        return EntityCategory[value]

    def set_property(self, entity):
        entity.category = self.value


#
# ColourEntityProperty
#
class ColourEntityProperty(EntityProperty):
    def __init__(self):
        super().__init__('colour', False, Colour.from_name('White'))
        self.format = 'str'
        self.help = 'Entity render colour'

    def validate_definition_type(self, value):
        return type(value) is str

    def convert_to_type(self, value):
        return Colour.from_string(value)

    def set_property(self, entity):
        entity.rgba = self.value


#
# InventoryTypeEntityProperty
#
class InventoryTypeEntityProperty(EntityProperty):
    def __init__(self):
        super().__init__('inventory-type', True)
        self.format = 'string'
        self.help = 'The user-defined type of entity, with regards to how it is dealt with by inventories'

    def validate_definition_type(self, value):
        return type(value) is str

    def set_property(self, entity):
        entity.inventorytype = self.value


#
# FactionsEntityProperty
#
class FactionsEntityProperty(EntityProperty):
    def __init__(self):
        super().__init__('factions', False, set())
        self.format = 'list of Faction enum names'
        self.help = 'The factions that this entity belongs to, as defined by the user'

    def validate_definition_type(self, value):
        return type(value) is list

    def convert_to_type(self, value):
        factionEnum = getattr(get_game_factions_module(), 'Faction')
        return {factionEnum[f] for f in value}

    def set_property(self, entity):
        entity.factions = self.value


#
# PriorityModifierEntityProperty
#
class PriorityModifierEntityProperty(EntityProperty):
    def __init__(self):
        super().__init__('priority-modifier', False, 0)
        self.format = 'priority modifier in [-99, 99]'
        self.help = 'The modifier to action priority, used to tweak the interactions between entities'

    def validate_definition_type(self, value):
        return type(value) is int

    def set_property(self, entity):
        entity.prioritymodifier = self.value


#
# Factory
#
entityPropertyFactory = {
    'names': NamesEntityProperty,
    'category': CategoryEntityProperty,
    'colour': ColourEntityProperty,
    'inventory-type': InventoryTypeEntityProperty,
    'factions': FactionsEntityProperty,
    'priority-modifier': PriorityModifierEntityProperty
}
