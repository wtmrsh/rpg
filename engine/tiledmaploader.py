"""TiledMapLoader"""

#
# Imports
#
import os
import re
import sys
import ast
import json
import random
import itertools
from functools import partial
from collections import defaultdict, namedtuple

from common import *

import engine.ini
import engine.entity
import engine.gamemodule
import engine.tiledefinitions
import engine.entityfactories
import engine.observable as obs
from engine.settings import GameSettings
from engine.map import Map
from engine.defines import CompassDirection
from engine.tiletype import TileType
from engine.maploader import MapLoader
from engine.exceptions import LevelLoadError
from engine.tile import Tile
from engine.colour import Colour
from engine.area import Area
from engine.triggerarea import TriggerArea
from engine.tiledentityproperties import tiledEntityPropertyFactory
from engine.tiledtraitparsers import tiledTraitParserFactory
from engine.transition import Transition

#
# UseTargetDefinition
#
UseTargetDefinition = namedtuple('UseTargetDefinition', 'name,count,passnext,updateonfailure,actions,states')

#
# LogicStateCallbackDefinition
#
LogicStateCallbackDefinition = namedtuple('LogicStateCallbackDefinition', 'state,verifier,usetargets')

#
# EntitySpecialisation
#
EntitySpecialisation = namedtuple('EntitySpecialisation', 'type,name,classname,count,direction,ownername,variant,states,frame,usetargets,callbacks,instanceoverrides,xoffset,yoffset')

#
# TriggerAreaDefinition
#
TriggerAreaDefinition = namedtuple('TriggerAreaDefinition', 'x,y,width,height,event,params,count,usetargets,suppress,messages,allowedusernames,numusersneeded,userfilterfunc')


#
# TiledMapLoader
#
class TiledMapLoader(MapLoader):
    #
    # __init__()
    #
    def __init__(self, filename, maptype=Map):
        """
        Constructor.

        :param filename: filename to load
        :param mapName:  name of map
        """
        super().__init__(maptype)
        self.filename = filename
        self.requiredTileSize = 0
        self.namedEntities = dict()

    #
    # load()
    #
    def load(self):
        """
        Load map

        :return: created Map
        """
        map = self.maptype()

        self.load_tiled_data(map)
        self.analyse_doors(map)
        map.register_events()

        map.update_vision_map()
        map.update(0)

        return map

    #
    # verify_tiled_json_data()
    #
    def verify_tiled_json_data(self, jsondata):

        # Check format
        if int(jsondata['tilewidth']) != self.requiredTileSize:
            raise LevelLoadError(self.filename, 'Invalid tilewidth {}: does not match cell size.'.format(jsondata['tilewidth']))

        if int(jsondata['tileheight']) != self.requiredTileSize:
            raise LevelLoadError(self.filename, 'Invalid tileheight {}: does not match cell size.'.format(jsondata['tileheight']))

        if jsondata['renderorder'] != 'right-up':
            raise LevelLoadError(self.filename, "Invalid renderorder: should be 'right-up'.")

        if jsondata['orientation'] != 'orthogonal':
            raise LevelLoadError(self.filename, "Invalid orientation: should be 'orthogonal'.")

        # Check layers are all present
        groundLayer = False
        overlayLayer = False
        wallsLayer = False
        decorationLayer = False
        worldLayer = False
        entityLayer = False
        areaLayer = False
        lightLayer = False
        pathLayer = False

        for layer in jsondata['layers']:
            layerName = layer['name']
            if layerName == 'Ground':
                groundLayer = True
            elif layerName == 'Overlay':
                overlayLayer = True
            elif layerName == 'Walls':
                wallsLayer = True
            elif layerName == 'Decoration':
                decorationLayer = True
            elif layerName == 'World Entities':
                worldLayer = True
            elif layerName == 'Entities':
                entityLayer = True
            elif layerName == 'Areas':
                areaLayer = True
            elif layerName == 'Lights':
                lightLayer = True
            elif layerName == 'Paths':
                pathLayer = True

        if not groundLayer:
            raise LevelLoadError(self.filename, "Could not find 'Ground' layer.")
        if not overlayLayer:
            raise LevelLoadError(self.filename, "Could not find 'Overlay' layer.")
        if not wallsLayer:
            raise LevelLoadError(self.filename, "Could not find 'Walls' layer.")
        if not decorationLayer:
            raise LevelLoadError(self.filename, "Could not find 'Decoration' layer.")
        if not worldLayer:
            raise LevelLoadError(self.filename, "Could not find 'World Entities' layer.")
        if not entityLayer:
            raise LevelLoadError(self.filename, "Could not find 'Entities' layer.")
        if not areaLayer:
            raise LevelLoadError(self.filename, "Could not find 'Areas' layer.")
        if not lightLayer:
            raise LevelLoadError(self.filename, "Could not find 'Lights' layer.")
        if not pathLayer:
            raise LevelLoadError(self.filename, "Could not find 'Paths' layer.")

    #
    # load_tilesets()
    #
    def load_tilesets(self, jsondata, map):

        # Get tilesets, and load if not already loaded
        tileoffset = 0
        tilesets = jsondata['tilesets']
        loadedtilesets = dict()
        engine.tiledefinitions.definitions = dict()

        for tileset in tilesets:
            source = tileset['source'].replace('\/', '/ ')

            tilesetbasedir = os.path.split(self.filename)[0]
            tilesetsource = os.path.join(tilesetbasedir, source)

            if tilesetsource not in loadedtilesets:
                tilesetoffset = tileset['firstgid']
                newtileoffset = engine.ini.load_tile_definitions(tilesetsource, tileoffset, engine.tiledefinitions.definitions)

                # Add to lookup: the map file's working offset, and the start and count tiles for the tileset file
                loadedtilesets[tilesetsource] = (tilesetoffset, tileoffset, newtileoffset - tileoffset)
                tileoffset = newtileoffset

        lookup = engine.ini.create_tile_lookup()
        return lookup

    #
    # get_trait_action_prereqs()
    #
    def get_trait_action_prereqs(self, action, actiontype, prereqs, entityclass):
        gamemodule = engine.gamemodule.get_game_prereq_module()
        entitydef = engine.entity.definitions[entityclass]

        if actiontype in entitydef and action in entitydef[actiontype]:
            curprereqs = list(entitydef[actiontype][action])
        else:
            curprereqs = list()

        for prereq in prereqs:
            if prereq.startswith('-'):
                p = getattr(gamemodule, prereq[1:], None) or getattr(sys.modules['engine.traitprereqs'], prereq[1:], None)
                curprereqs.remove(p)
            else:
                p = getattr(gamemodule, prereq, None) or getattr(sys.modules['engine.traitprereqs'], prereq, None)
                curprereqs.append(p)

        return curprereqs

    #
    # parse_nested_string_list
    #
    def parse_nested_string_list(self, string):
        # Format is like a python list but without quotes
        itemexpr = r'[\+\-a-zA-Z]+'
        quoted = re.sub(itemexpr, lambda m: '"{}"'.format(m.group(0)), string)
        return ast.literal_eval(quoted)

    #
    # parse_entity_instance_overrides()
    #
    def parse_entity_instance_overrides(self, x, y, entityname, entitytype, entityclass, properties):
        overrides = defaultdict(dict)
        for property in properties:
            name = property['name']
            value = property['value']

            if name.startswith('Properties.'):
                # Parse instance properties
                # Could be various types, so use a parser
                nameparts = [x.strip() for x in name.split('.')]
                propname = nameparts[1]

                if propname in tiledEntityPropertyFactory:
                    propinst = tiledEntityPropertyFactory[propname]()
                    propinst.parse_from_definition(entityname, value)
                    overrides['Properties'][propname] = propinst
                else:
                    errmsg = f"Entity definition: property override '{propname}' not recognised"
                    raise LevelLoadError(self.filename, errmsg)
            elif name.startswith('Traits.'):
                # Parse traits
                nameparts = [x.strip() for x in name.split('.')]
                traitname = nameparts[1]
                traitsection = nameparts[2]

                if traitsection == 'Parameters':
                    # Parse parameters to be passed into the trait constructor
                    # Regular string
                    traitparamname = nameparts[3]
                    if traitname not in overrides['Traits']:
                        overrides['Traits'][traitname] = dict()
                    if traitsection not in overrides['Traits'][traitname]:
                        overrides['Traits'][traitname][traitsection] = dict()

                    if traitname not in tiledTraitParserFactory:
                        errmsg = f"Entity definition: trait '{traitname}' not recognised"
                        raise LevelLoadError(self.filename, errmsg)

                    parser = tiledTraitParserFactory[traitname]
                    overrides['Traits'][traitname][traitsection][traitparamname] = parser.parse_argument(traitparamname,
                                                                                                         value)
                elif traitsection == 'Actors':
                    # Parse actor prerequisites for an action that this trait exposes
                    # Comma-separated strings, which are resolved to prerequisite function objects
                    traitaction = nameparts[3]
                    if 'Actors' not in overrides:
                        overrides['Actors'] = defaultdict(list)

                    prereqs = [x.strip() for x in value.split(',')]
                    overrides['Actors'][traitaction].extend(self.get_trait_action_prereqs(traitaction,
                                                                                          'Actors',
                                                                                          prereqs,
                                                                                          entityclass))
                elif traitsection == 'Targets':
                    # Parse target prerequisites for an action that this trait exposes
                    # Comma-separated strings, which are resolved to prerequisite function objects
                    traitaction = nameparts[3]
                    if 'Targets' not in overrides:
                        overrides['Targets'] = defaultdict(list)

                    prereqs = [x.strip() for x in value.split(',')]
                    overrides['Targets'][traitaction].extend(self.get_trait_action_prereqs(traitaction,
                                                                                           'Targets',
                                                                                           prereqs,
                                                                                           entityclass))
                elif traitsection == 'Specialisation':
                    # Parse trait specialisation (subclass)
                    # Regular string
                    if traitname not in overrides['Traits']:
                        overrides['Traits'][traitname] = dict()

                    overrides['Traits'][traitname]['Specialisation'] = value
            elif name.startswith('Actors.'):
                # Parse actor prerequisites for the given action
                # Comma-separated strings, which are resolved to prerequisite function objects
                nameparts = [x.strip() for x in name.split('.')]
                actionname = nameparts[1]

                prereqs = [x.strip() for x in value.split(',')]
                overrides['Actors'][actionname].extend(self.get_trait_action_prereqs(actionname,
                                                                                     'Actors',
                                                                                     prereqs,
                                                                                     entityclass))
            elif name.startswith('Targets.'):
                # Parse targets prerequisites for the given action
                # Comma-separated strings, which are resolved to prerequisite function objects
                nameparts = [x.strip() for x in name.split('.')]
                actionname = nameparts[1]

                prereqs = [x.strip() for x in value.split(',')]
                overrides['Targets'][actionname].extend(self.get_trait_action_prereqs(actionname,
                                                                                      'Targets',
                                                                                      prereqs,
                                                                                      entityclass))
            elif name.startswith('StateManagement.'):
                # Parse state handlers
                nameparts = [x.strip() for x in name.split('.')]
                sectiontype = nameparts[1]
                sectionname = nameparts[2]
                handlerdata = self.parse_nested_string_list(value)

                if sectiontype not in overrides['StateManagement']:
                    overrides['StateManagement'][sectiontype] = dict()

                overrides['StateManagement'][sectiontype][sectionname] = handlerdata
            elif name.startswith('Stats.'):
                # Parse stats
                # Evaluate as a string
                nameparts = [x.strip() for x in name.split('.')]
                statname = nameparts[1]

                overrides['Stats'][statname] = eval(str(value))
            elif name.startswith('Core.'):
                # Core properties
                nameparts = [x.strip() for x in name.split('.')]
                propname = nameparts[1]

                if propname == 'InitialStates':
                    parsedvalue = [v.strip() for v in value.split(',')]
                else:
                    parsedvalue = value

                # Do some checking
                if propname == 'Count' and entitytype != 'Item':
                    errmsg = f"Entity definition: property '{name}' cannot be applied to entity because it is not of type '{entitytype}'."
                    raise LevelLoadError(self.filename, errmsg)

                overrides['Core'][propname] = parsedvalue
            else:
                errmsg = f"Entity definition: property '{name}' not recognised."
                raise LevelLoadError(self.filename, errmsg)

        # Remove any duplicates in prerequisite lists
        if 'Actors' in overrides:
            for action in overrides['Actors']:
                overrides['Actors'][action] = list(set(overrides['Actors'][action]))
        if 'Targets' in overrides:
            for action in overrides['Targets']:
                overrides['Targets'][action] = list(set(overrides['Targets'][action]))

        return overrides

    #
    # create_entity_grid()
    #
    def create_entity_grid(self, layer, map, lookup):
        try:
            layername = layer[4]
            entitygrid = [[None for y in range(map.sizeY)] for x in range(map.sizeX)]
            for entityInfo in layer[3]:
                x, xoffset = divmod(int(entityInfo['x']), self.requiredTileSize)
                y, yoffset = divmod(int(entityInfo['y']), self.requiredTileSize)
                if yoffset == 0:
                    y -= 1
                else:
                    yoffset = self.requiredTileSize - yoffset

                globalId = int(entityInfo['gid'])

                # Get entity type
                tileIndex = TILE_CODEPAGE + globalId - 1
                tileDef = lookup[tileIndex]

                entityname = entityInfo.get('name')
                entitytype = tileDef.type
                entityclass = tileDef.properties['Core.Class']

                initialstates = tileDef.properties.get('Core.InitialStates', '')
                entitystates = [v.strip() for v in initialstates.split(',')]
                entitystates = [v for v in entitystates if v.strip() != '']

                try:
                    # Get definition overrides for this entity
                    entityproperties = entityInfo.get('properties', dict())
                    instanceoverrides = self.parse_entity_instance_overrides(x,
                                                                             y,
                                                                             entityname,
                                                                             entitytype,
                                                                             entityclass,
                                                                             entityproperties)

                    usetargets = list()
                    callbacks = list()
                    for entityProp in entityproperties:
                        propname = entityProp['name']
                        propvalue = entityProp['value']
                        if propname.startswith('Core.UseTarget.'):
                            usetarget = self.parse_usetarget_json(propvalue)
                            usetargets.append(usetarget)
                        elif propname.startswith('Core.LogicStateCallback.'):
                            callback = self.parse_logicstatecallback_json(propvalue)
                            callbacks.append(callback)

                    # Get initial states
                    if 'Core' in instanceoverrides and 'InitialStates' in instanceoverrides['Core']:
                        entitystates = instanceoverrides['Core']['InitialStates']

                    # Get count: applicable to items only
                    entitycount = 1
                    if 'Core' in instanceoverrides and 'Count' in instanceoverrides['Core']:
                        entitycount = instanceoverrides['Core']['Count']

                    # Get direction
                    entitydirection = CompassDirection.NONE
                    if 'Core.Direction' in tileDef.properties:
                        entitydirection = CompassDirection[tileDef.properties['Core.Direction']]
                    if 'Core' in instanceoverrides and 'Direction' in instanceoverrides['Core']:
                        entitydirection = CompassDirection[instanceoverrides['Core']['Direction']]

                    # Get owner
                    entityowner = None
                    if 'Core' in instanceoverrides and 'Owner' in instanceoverrides['Core']:
                        if layername != 'Inventory':
                            failedname = entityname or '<unnamed>'
                            errmsg = f"Entity {failedname}:{entityclass}:{entitytype}:({x}, {y}) has an owner set, but is on the {layername} layer.  Inventory entities can only be added to an 'Inventory' layer."
                            raise LevelLoadError(self.filename, errmsg)
                        if entitytype != 'Item':
                            failedname = entityname or '<unnamed>'
                            errmsg = f"Entity {failedname}:{entityclass}:{entitytype}:({x}, {y}) is not an item.  Entities on an 'Inventory' layer on be of type 'Item'."
                            raise LevelLoadError(self.filename, errmsg)
                        entityowner = instanceoverrides['Core']['Owner']

                    spec = EntitySpecialisation(
                        type=entitytype,
                        name=entityname,
                        classname=entityclass,
                        count=entitycount,
                        direction=entitydirection,
                        ownername=entityowner,
                        variant=tileDef.variant,
                        states=entitystates,
                        frame=tileDef.frame,
                        usetargets=usetargets,
                        callbacks=callbacks,
                        instanceoverrides=instanceoverrides,
                        xoffset=xoffset,
                        yoffset=yoffset)

                    entitygrid[x][y] = spec
                except Exception as e:
                    failedname = entityname or '<unnamed>'
                    errmsg = f'Error parsing entity {failedname}:{entityclass}:{entitytype}:({x},{y}).'
                    raise LevelLoadError(self.filename, errmsg) from e

        except Exception as e:
            errmsg = f'Error reading entity at ({x},{y}).'
            raise LevelLoadError(self.filename, errmsg) from e

        return entitygrid

    #
    # parse_usetarget()
    #
    def parse_usetarget(self, definition):
        try:
            target = definition.get('target', None)
            usetype = definition['type']
            onpass = definition['pass']
            updateonfailure = definition.get('updateonfailure', False)
            actions = definition.get('actions', list())
            states = definition.get('states', list())

            # Get count
            if usetype == 'cycle':
                count = -1
            elif usetype == 'once':
                count = 1
            else:
                errMsg = "Error reading UseTarget: type should be one of cycle|once"
                raise LevelLoadError(self.filename, errMsg)

            # Get whether or not we should try the next action if the current one is already in the desired state
            if onpass == 'next':
                passnext = True
            elif onpass == 'break':
                passnext = False
            else:
                errMsg = "Error reading UseTarget: pass should be one of next|break"
                raise LevelLoadError(self.filename, errMsg)

            # Parse states
            for state in states:
                setstates = state.get('set')
                if setstates:
                    state['set'] = [s.strip() for s in setstates.split(',')]
                else:
                    state['set'] = list()

                unsetstates = state.get('unset')
                if unsetstates:
                    state['unset'] = [s.strip() for s in unsetstates.split(',')]
                else:
                    state['unset'] = list()

                togglestates = state.get('toggle')
                if togglestates:
                    state['toggle'] = [s.strip() for s in togglestates.split(',')]
                else:
                    state['toggle'] = list()

        except Exception as e:
            errMsg = 'Could not read UseTarget definition'
            raise LevelLoadError(self.filename, errMsg) from e

        return UseTargetDefinition(name=target,
                                   count=count,
                                   passnext=passnext,
                                   updateonfailure=updateonfailure,
                                   actions=actions,
                                   states=states)

    #
    # parse_usetarget_json()
    #
    def parse_usetarget_json(self, definition):
        try:
            jsonData = json.loads(definition)
            return self.parse_usetarget(jsonData)
        except Exception as e:
            errMsg = 'Could not read UseTarget definition'
            raise LevelLoadError(self.filename, errMsg) from e

    #
    # parse_logicstatecallback()
    #
    def parse_logicstatecallback_json(self, definition):
        try:
            jsonData = json.loads(definition)
            state = jsonData['state']
            verifier = eval('lambda value: ' + jsonData['verifier'])
            usetargetDefs = list()

            for usetarget in jsonData['usetargets']:
                usetargetDef = self.parse_usetarget(usetarget)
                usetargetDefs.append(usetargetDef)
        except Exception as e:
            errMsg = 'Could not read LogicStateCallback definition'
            raise LevelLoadError(self.filename, errMsg) from e

        return LogicStateCallbackDefinition(state, verifier, usetargetDefs)

    #
    # parse_path()
    #
    def parse_path(self, map, pathDef):
        name = pathDef['name']
        try:
            x0 = int(pathDef['x'] // self.requiredTileSize)
            y0 = int(pathDef['y'] // self.requiredTileSize)

            path = []
            for node in pathDef['polyline']:
                x = x0 + int(node['x'] // self.requiredTileSize)
                y = y0 + int(node['y'] // self.requiredTileSize)

                xPos = x
                yPos = map.sizeY - y - 1
                path.append((xPos, yPos))

            map.paths[name] = path
        except Exception as e:
            errMsg = f"Error reading path '{name}'."
            raise LevelLoadError(self.filename, errMsg) from e

    #
    # parse_start_area()
    #
    def parse_start_area(self, map, areaDef, rect):
        (x0, y0, width, height) = rect

        try:
            areaproperties = {d['name']: d['value'] for d in areaDef['properties']} if 'properties' in areaDef else {}
            xPos = x0
            yPos = map.sizeY - y0 - 1

            # Set defaults
            direction = None

            for name in areaproperties:
                value = areaproperties[name]
                if name.startswith('Core.'):
                    # Core properties
                    nameParts = [x.strip() for x in name.split('.')]
                    propName = nameParts[1]

                    if propName == 'Direction':
                        direction = value
                    else:
                        errMsg = f"Area definition: property '{name}' for start area ({x0},{y0},{width},{height}) not recognised."
                        raise LevelLoadError(self.filename, errMsg)
                else:
                    errMsg = f"Area definition: property '{name}' for start area ({x0},{y0},{width},{height}) not recognised."
                    raise LevelLoadError(self.filename, errMsg)

            if direction is None:
                errMsg = f"Area definition: property 'Core.Direction' for trigger area ({x0},{y0},{width},{height}) not set."
                raise LevelLoadError(self.filename, errMsg)

            # Set start position randomly from within area
            startX = random.randint(xPos, xPos + width - 1)
            startY = random.randint(yPos - height + 1, yPos)
            map.entrypoint = (startX, startY, CompassDirection[direction])
        except Exception as e:
            errMsg = f'Error reading start area at offset ({x0},{y0})'
            raise LevelLoadError(self.filename, errMsg) from e

    #
    # parse_trigger_area()
    #
    def parse_trigger_area(self, map, areadef, rect):
        (x0, y0, width, height) = rect

        try:
            areaproperties = {d['name']: d['value'] for d in areadef['properties']} if 'properties' in areadef else {}
            xpos = x0
            ypos = map.sizeY - y0 - 1
            ypos -= (height - 1)

            # Set defaults
            event = None
            count = -1
            funcparams = list()
            usetargets = list()
            suppress = True
            messages = [None, None, None, None]
            allowedusernames = None
            numusersneeded = 1
            userfilterfunc = None

            for name in areaproperties:
                value = areaproperties[name]
                if name.startswith('Core.'):
                    # Core properties
                    nameparts = [x.strip() for x in name.split('.')]
                    propname = nameparts[1]

                    if propname == 'Event':
                        event = value
                    elif propname == 'Count':
                        count = int(value)
                    elif propname.startswith('UseTarget'):
                        usetarget = self.parse_usetarget_json(value)
                        usetargets.append(usetarget)
                    elif propname == 'SuppressMsg':
                        suppress = bool(value)
                    elif propname == 'PlayerUseMsg':
                        messages[0] = value
                    elif propname == 'PlayerFailMsg':
                        messages[1] = value
                    elif propname == 'OtherUseMsg':
                        messages[2] = value
                    elif propname == 'OtherFailMsg':
                        messages[3] = value
                    elif propname == 'AllowedUserNames':
                        allowedusernames = [x.strip() for x in value.split(',')]
                    elif propname == 'NumUsersNeeded':
                        numusersneeded = int(value)
                    elif propname == 'UserFilterFunc':
                        allowedusernames = eval('lambda entity: ' + value)
                    else:
                        errmsg = f"Area definition: property '{name}' for trigger area ({x0},{y0},{width},{height})"\
                                "not recognised."
                        raise LevelLoadError(self.filename, errmsg)
                elif name.startswith('Parameters.'):
                    nameparts = [x.strip() for x in name.split('.')]
                    paramindex = int(nameparts[1]) - 1
                    while len(funcparams) < paramindex:
                        funcparams.append(None)
                    funcparams.append(value)
                else:
                    errmsg = f"Area definition: property '{name}' for trigger area ({x0},{y0},{width},{height})"\
                            "not recognised."
                    raise LevelLoadError(self.filename, errmsg)

            if event is None and len(usetargets) == 0:
                errmsg = f"Area definition: neither property 'Core.Event' nor 'Core.UseTarget' are set for trigger area at ({x0},{y0},{width},{height})"
                raise LevelLoadError(self.filename, errmsg)

            return TriggerAreaDefinition(
                x=xpos,
                y=ypos,
                width=width,
                height=height,
                event=event,
                params=funcparams,
                count=count,
                usetargets=usetargets,
                suppress=suppress,
                messages=messages,
                allowedusernames=allowedusernames,
                numusersneeded=numusersneeded,
                userfilterfunc=userfilterfunc)

        except Exception as e:
            errmsg = f'Error reading trigger area at offset ({x0},{y0}).'
            raise LevelLoadError(self.filename, errmsg) from e

    def parse_light_area(self, map, lightDef, area):
        colourProp = None
        onProp = None
        patternProp = None

        for prop in lightDef['properties']:
            if prop['name'] == 'Colour':
                colourProp = prop['value']
            elif prop['name'] == 'On':
                onProp = prop['value']
            elif prop['name'] == 'Pattern':
                patternProp = prop['value']

        if colourProp is None:
            errMsg = "Error reading light area at offset (%d,%d): 'Colour' property is missing." % (x0, y0)
            raise LevelLoadError(self.filename, errMsg)
        if onProp is None:
            errMsg = "Error reading light area at offset (%d,%d): 'On' property is missing." % (x0, y0)
            raise LevelLoadError(self.filename, errMsg)

        # Convert colour from ARGB to RGB
        colour = '0x' + colourProp[3:5] + colourProp[5:7] + colourProp[7:9]

        if 'ellipse' in lightDef:
            (x0, y0, radius) = area

            try:
                xPos = x0
                yPos = map.sizeY - y0 - 1

                if map.tiles[xPos][yPos].type in [TileType.WALL, TileType.WINDOW, TileType.DOOR]:
                    raise LevelLoadError(self.filename, 'You cannot place a light at %d,%d' % (xPos, yPos))

                lightObj = map.create_circle_light(xPos, yPos, radius, Colour.from_string(colour), patternProp, onProp)
            except Exception as e:
                errMsg = "Error reading light area at offset (%d,%d)." % (x0, y0)
                raise LevelLoadError(self.filename, errMsg) from e

        else:
            (x0, y0, width, height) = area
            try:
                xPos = x0
                yPos = map.sizeY - y0 - 1

                lightObj = map.create_rect_light(xPos, yPos, width, height, Colour.from_string(colour), patternProp, onProp)
            except Exception as e:
                errMsg = "Error reading light area at offset (%d,%d)." % (x0, y0)
                raise LevelLoadError(self.filename, errMsg) from e

        return lightObj

    #
    # get_entity_kwargs()
    #
    def get_entity_kwargs(self, properties):
        return dict((k, v) for k, v in properties.items() if k[0].islower())

    #
    # create_world_entity()
    #
    def create_world_entity(self, map, tile, entitySpec):
        instanceOverrides = entitySpec.instanceoverrides
        if entitySpec.type == 'WallObject':
            entity = map.create_wallobject_on_map(tile,
                                                  entitySpec.classname,
                                                  entitySpec.name,
                                                  entitySpec.states,
                                                  entitySpec.variant,
                                                  entitySpec.frame,
                                                  instanceOverrides,
                                                  entitySpec.direction,
                                                  False,
                                                  entitySpec.usetargets,
                                                  entitySpec.callbacks,
                                                  entitySpec.xoffset,
                                                  entitySpec.yoffset)
        else:
            entity = map.create_world_entity_on_map(tile,
                                                    entitySpec.type,
                                                    entitySpec.classname,
                                                    entitySpec.name,
                                                    entitySpec.states,
                                                    entitySpec.variant,
                                                    entitySpec.frame,
                                                    instanceOverrides,
                                                    False,
                                                    entitySpec.usetargets,
                                                    entitySpec.callbacks,
                                                    entitySpec.xoffset,
                                                    entitySpec.yoffset)

        return entity

    #
    # create_entity()
    #
    def create_entity(self, map, x, y, entityspec):
        instanceoverrides = entityspec.instanceoverrides

        if entityspec.ownername:
            entities = [engine.entityfactories.create_entity(entityspec.classname,
                                                             entityspec.name,
                                                             entityspec.variant,
                                                             entityspec.frame,
                                                             instanceoverrides)]
        else:
            if entityspec.type == 'Entity':
                entities = [map.create_entity_on_map(x,
                                                     y,
                                                     entityspec.direction,
                                                     entityspec.classname,
                                                     entityspec.name,
                                                     entityspec.states,
                                                     entityspec.variant,
                                                     entityspec.frame,
                                                     instanceoverrides,
                                                     False,
                                                     entityspec.usetargets,
                                                     entityspec.callbacks,
                                                     entityspec.xoffset,
                                                     entityspec.yoffset)]
            elif entityspec.type == 'Item':
                entities = map.create_entity_as_inventory(x,
                                                          y,
                                                          entityspec.classname,
                                                          entityspec.name,
                                                          entityspec.states,
                                                          entityspec.variant,
                                                          entityspec.frame,
                                                          instanceoverrides,
                                                          False,
                                                          entityspec.usetargets,
                                                          entityspec.callbacks,
                                                          entityspec.xoffset,
                                                          entityspec.yoffset,
                                                          entityspec.count)
            elif entityspec.type == 'Abstract':
                entities = [map.create_entity_on_map(None,
                                                     None,
                                                     entityspec.direction,
                                                     entityspec.classname,
                                                     entityspec.name,
                                                     entityspec.states,
                                                     entityspec.variant,
                                                     entityspec.frame,
                                                     instanceoverrides,
                                                     False,
                                                     entityspec.usetargets,
                                                     entityspec.callbacks,
                                                     entityspec.xoffset,
                                                     entityspec.yoffset)]
            else:
                raise LevelLoadError(self.filename, f"Unknown entity type '{entityspec.type}'")

        return entities

    #
    # resolve_entity_dependencies()
    #
    # Basic topological sort.  Not hugely efficient, but that's not so important here.
    #
    def resolve_entity_dependencies(self, entitydefinitions, entitydependencies):
        from copy import deepcopy

        entitydefs = list(entitydefinitions)
        entitydeps = deepcopy(entitydependencies)
        sorteddefs = list()

        while len(entitydefs) > 0:
            remove = list()
            for item in entitydefs:
                itemname = item[2].name
                if itemname not in entitydeps:
                    remove.append(item)
                    for ed in entitydeps:
                        if itemname in entitydeps[ed]:
                            entitydeps[ed].remove(itemname)

                    entitydeps = {k: v for k, v in entitydeps.items() if len(v) > 0}

            for r in remove:
                entitydefs.remove(r)
                sorteddefs.append(r)

        return sorteddefs

    #
    # parse_map_properties()
    #
    def parse_map_properties(self, properties, map):
        gamefacilitatormodule = engine.gamemodule.get_game_facilitators_module()

        mainprops = {d['name']: (d['type'], d['value']) for d in properties}
        metaprops = dict()
        globalprops = dict()

        if 'Name' in mainprops:
            map.name = mainprops['Name'][1]
        if 'Script' in mainprops:
            # Load script into module for map
            mm = mainprops['Script'][1]
            scriptmodulename = f'games.{GameSettings.game}.maps.{mm}'
            map.set_script_module(scriptmodulename)

        # Parse properties for type
        for propname in mainprops:
            proptype = mainprops[propname][0]
            propvalue = mainprops[propname][1]

            if proptype == 'string':
                pass
            elif proptype == 'int':
                pass
            elif proptype == 'float':
                pass
            elif proptype == 'file':
                pass
            elif proptype == 'bool':
                pass
            elif proptype == 'object':
                # TODO: this should really be a reference to an Entity
                pass
            elif proptype == 'color':
                propvalue = Colour.from_string(propvalue)

            if propname.startswith('Facilitator'):
                facaction = propname.replace('Facilitator.', '')
                f = getattr(gamefacilitatormodule, propvalue, None)
                map.facilitators[facaction] = f
            elif propname.startswith('Global.'):
                globalname = propname.replace('Global.', '')
                globalprops[globalname] = propvalue
            elif propname.startswith('Meta.'):
                metaname = propname.replace('Meta.', '')
                metaprops[metaname] = propvalue
            else:
                mainprops[propname] = propvalue

        return mainprops, metaprops, globalprops

    #
    # load_tiled_data()
    #
    def load_tiled_data(self, map):

        # Load data
        with open(self.filename) as file:
            contents = file.read()
            jsonData = json.loads(contents)

        self.requiredTileSize = CELL_SIZE * 2

        self.verify_tiled_json_data(jsonData)

        # Parse map properties
        if 'properties' in jsonData:
            map.properties, map.metaproperties, map.globalproperties = self.parse_map_properties(jsonData['properties'], map)
        else:
            map.properties = dict()
            map.metaproperties = dict()

        # Get layer data
        inventoryLayers = list()
        abstractLayers = list()

        for layer in jsonData['layers']:
            layerName = layer['name']
            if layerName == 'Ground':
                groundLayer = (int(layer['x']), int(layer['y']), float(layer['opacity']), layer['data'], layerName)
            elif layerName == 'Overlay':
                overlayLayer = (int(layer['x']), int(layer['y']), float(layer['opacity']), layer['data'], layerName)
            elif layerName == 'Walls':
                wallsLayer = (int(layer['x']), int(layer['y']), float(layer['opacity']), layer['data'], layerName)
            elif layerName == 'Decoration':
                decorationLayer = (int(layer['x']), int(layer['y']), float(layer['opacity']), layer['data'], layerName)
            elif layerName == 'World Entities':
                worldLayer = (int(layer['x']), int(layer['y']), float(layer['opacity']), layer['objects'], layerName)
            elif layerName == 'Entities':
                entityLayer = (int(layer['x']), int(layer['y']), float(layer['opacity']), layer['objects'], layerName)
            elif layerName == 'Inventory':
                invLayer = (int(layer['x']), int(layer['y']), float(layer['opacity']), layer['objects'], layerName)
                inventoryLayers.append(invLayer)
            elif layerName == 'Abstract':
                absLayer = (int(layer['x']), int(layer['y']), float(layer['opacity']), layer['objects'], layerName)
                abstractLayers.append(absLayer)
            elif layerName == 'Areas':
                areaLayer = (int(layer['x']), int(layer['y']), float(layer['opacity']), layer['objects'], layerName)
            elif layerName == 'Lights':
                lightLayer = (int(layer['x']), int(layer['y']), float(layer['opacity']), layer['objects'], layerName)
            elif layerName == 'Paths':
                pathLayer = (int(layer['x']), int(layer['y']), float(layer['opacity']), layer['objects'], layerName)

        # Get tilesets, and load if not already loaded
        tileLookup = self.load_tilesets(jsonData, map)

        # Set map size
        width = int(jsonData['width'])
        height = int(jsonData['height'])

        map.set_size(width, height)

        # Store named entities, for wiring up later
        self.namedEntities = dict()

        # Parse paths
        map.paths.clear()
        for pathDef in pathLayer[3]:
            self.parse_path(map, pathDef)

        # Set up tiles
        for y in range(height):
            for x in range(width):
                offset = y * width + x

                # Ground tile
                baseValue = groundLayer[3][offset - (groundLayer[1] * width + groundLayer[0])]
                if baseValue == 0:
                    raise LevelLoadError(self.filename, f'Tile at ({x},{y}) has no base tile specified.')

                # See if there's a wall tile as well, in which case the wall assumes the role of base.
                wallValue = wallsLayer[3][offset - (wallsLayer[1] * width + wallsLayer[0])]
                if wallValue != 0:
                    baseValue = wallValue

                baseDef = tileLookup[TILE_CODEPAGE + baseValue - 1]

                # Create tile
                xpos = x
                ypos = height - y - 1

                invsize = baseDef.properties['space']
                tile = Tile(owner=map, inventorysize=invsize)

                # Add tile here, as we need to make use of its 'owner' in later setup
                map.set_tile(xpos, ypos, tile)
                if baseDef.type == 'Wall':
                    tile.wallTile = TILE_CODEPAGE + wallValue - 1
                    tile.set_type(TileType.WALL, baseDef.variant, baseDef.state)
                    tile.wallObjects = [None, None, None, None]
                elif baseDef.type == 'Floor':
                    tile.set_type(TileType.FLOOR, baseDef.variant, baseDef.state)
                elif baseDef.type == 'Space':
                    tile.set_type(TileType.SPACE, baseDef.variant, baseDef.state)

                tile._blocksVision = baseDef.properties['blocks-vision']
                tile._blocksMovement = baseDef.properties['blocks-movement']
                tile._blocksAudio = baseDef.properties['blocks-audio']

                # Colour
                if baseDef.colour:
                    tile.ambientLight = baseDef.colour

                # Overlay
                overlayValue = overlayLayer[3][offset - (overlayLayer[1] * width + overlayLayer[0])]
                if overlayValue > 0:
                    tileIndex = TILE_CODEPAGE + overlayValue - 1
                    tile.overlayTile = tileIndex
                    tile.overlayOpacity = overlayLayer[2]

                # Decoration
                decorationValue = decorationLayer[3][offset - (decorationLayer[1] * width + decorationLayer[0])]
                if decorationValue > 0:
                    pass

        # Get entity information
        entityDependencies = defaultdict(list)
        entitySpecialisations = list()
        for layer in itertools.chain(inventoryLayers, [worldLayer], [entityLayer], abstractLayers):
            layerName = layer[4]
            if layerName == 'World Entities':
                layerType = 'World'
            elif layerName == 'Entities':
                layerType = 'Entity'
            elif layerName == 'Inventory':
                layerType = 'Inventory'
            elif layerName == 'Abstract':
                layerType = 'Abstract'
            else:
                raise LevelLoadError(self.filename, f"Unexpected entity layer '{layerName}'.")

            grid = self.create_entity_grid(layer, map, tileLookup)
            for y in range(map.sizeY):
                for x in range(map.sizeX):
                    entitySpec = grid[x][y]
                    if entitySpec:
                        xPos = x
                        yPos = map.sizeY - y - 1
                        entitySpecialisations.append((map.tiles[xPos][yPos], layerType, entitySpec))

                        # Register dependencies: first inventory, then usetargets
                        if entitySpec.ownername:
                            if entitySpec.name == '':
                                failedName = '<unnamed>'
                                errMsg = f"Entity {failedName}:{entitySpec.classname}:{entitySpec.type}:({x}, {y}) has no name, but needs one to be owned by entity '{entitySpec.ownername}'."
                                raise LevelLoadError(self.filename, errMsg)
                            if entitySpec.ownername == '':
                                errMsg = f"Entity {entitySpec.name}:{entitySpec.classname}:{entitySpec.type}:({x}, {y}) has not specified the name of the entity that owns it."
                                raise LevelLoadError(self.filename, errMsg)

                            entityDependencies[entitySpec.ownername].append(entitySpec.name)
                        for usetarget in entitySpec.usetargets:
                            if usetarget.name:
                                entityDependencies[entitySpec.name].append(usetarget.name)

        # Resolve entity dependencies and reorder the list appropriately
        entitySpecialisations = self.resolve_entity_dependencies(entitySpecialisations, entityDependencies)

        # Create entities in required order
        entityInventoryLookup = defaultdict(list)
        for entitySpecialisation in entitySpecialisations:
            tile, entityType, entitySpec = entitySpecialisation
            if entityType == 'World':
                entities = [self.create_world_entity(map, tile, entitySpec)]
            elif entityType == 'Entity':
                entities = self.create_entity(map, tile.x, tile.y, entitySpec)

                # Add any inventory items
                for entity in entities:
                    for item in entityInventoryLookup[entitySpec.name]:
                        entity.inventory.add_entity(item, entitySpec.count, True)
            elif entityType == 'Inventory':
                entities = self.create_entity(map, tile.x, tile.y, entitySpec)
                if entitySpec.ownername:
                    for entity in entities:
                        entityInventoryLookup[entitySpec.ownername].append(entity)
            elif entityType == 'Abstract':
                entities = self.create_entity(map, None, None, entitySpec)
            else:
                errMsg = f"Unknown type '{entityType}' for Entity {entitySpec.name}:{entitySpec.classname}:{entitySpec.type}:({x}, {y})."
                raise LevelLoadError(self.filename, errMsg)

            # Add to lookup for wiring
            for entity in entities:
                if entitySpec.name != '':
                    self.namedEntities[entitySpec.name] = entity

        # Go through areas and set them up
        triggerAreaDefs = list()
        for areaDef in areaLayer[3]:
            # Get the tiles that this area touches
            if 'ellipse' in areaDef:
                raise LevelLoadError(self.filename, 'Ellipses are not supported for general areas.')
            elif 'polygon' in areaDef:
                raise LevelLoadError(self.filename, 'Polygons are not supported for general areas.')
            elif 'polyline' in areaDef:
                raise LevelLoadError(self.filename, 'Polylines are not supported for general areas.')
            elif 'gid' in areaDef:
                raise LevelLoadError(self.filename, 'Tiles are not supported for general areas.')
            elif 'text' in areaDef:
                raise LevelLoadError(self.filename, 'Text is not supported for general areas.')
            else:
                # Rectangle
                rect = (
                    int(areaDef['x'] // self.requiredTileSize),
                    int(areaDef['y'] // self.requiredTileSize),
                    int(areaDef['width'] // self.requiredTileSize),
                    int(areaDef['height'] // self.requiredTileSize))

                areaType = areaDef['type']
                if areaType == 'Start':
                    self.parse_start_area(map, areaDef, rect)
                elif areaType == 'Trigger':
                    triggerAreaDef = self.parse_trigger_area(map, areaDef, rect)
                    triggerAreaDefs.append(triggerAreaDef)

        # Create composite trigger areas
        for triggerAreaDef in triggerAreaDefs:

            # Create a callback funtion
            areacallback = partial(obs.trigger, None, triggerAreaDef.event, *triggerAreaDef.params)

            usetargets = list()
            for usetarget in triggerAreaDef.usetargets:
                usetargets.append((self.namedEntities[usetarget.name],
                                   usetarget.count,
                                   usetarget.passnext,
                                   usetarget.updateonfailure,
                                   usetarget.actions))

            ta = TriggerArea(map,
                             areacallback,
                             triggerAreaDef.count,
                             triggerAreaDef.suppress,
                             triggerAreaDef.messages,
                             usetargets,
                             triggerAreaDef.allowedusernames,
                             triggerAreaDef.numusersneeded,
                             triggerAreaDef.userfilterfunc,
                             Area.Rect(x=triggerAreaDef.x,
                                       y=triggerAreaDef.y,
                                       width=triggerAreaDef.width,
                                       height=triggerAreaDef.height))
            map.triggerareas.append(ta)

        # Go through lights
        for lightDef in lightLayer[3]:
            x = int(lightDef['x'] // self.requiredTileSize)
            y = int(lightDef['y'] // self.requiredTileSize)

            try:
                if 'ellipse' in lightDef:
                    radius = int(lightDef['width'] // self.requiredTileSize) // 2
                    circle = (
                        x + radius,
                        y + radius,
                        radius)

                    self.parse_light_area(map, lightDef, circle)
                else:
                    rect = (
                        x,
                        y,
                        int(lightDef['width'] // self.requiredTileSize),
                        int(lightDef['height'] // self.requiredTileSize))

                    self.parse_light_area(map, lightDef, rect)
            except Exception as e:
                errMsg = "Error reading light area at offset (%d,%d)." % (x, y)
                raise LevelLoadError(self.filename, errMsg) from e

        map.flush_entity_wiring()

    def analyse_doors(self, map):
        """
        Create transitions for doors, to be used as a lookup for pathing intelligence.

        :param map: Map instance.
        """
        map.doortransitions = dict()

        for y in range(map.sizeY):
            for x in range(map.sizeX):
                tile = map.tiles[x][y]

                if tile.type == TileType.DOOR:
                    doorent = tile.door

                    # Check entrances/exits
                    ee = list()
                    for loc in [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]:
                        if 0 <= x < map.sizeX and 0 <= y < map.sizeY:
                            lx, ly = loc
                            t = map.tiles[lx][ly]
                            if t.type == TileType.DOOR:
                                errmsg = f'Door at ({lx}, {ly}) has another door next to it.'
                                raise LevelLoadError(self.filename, errmsg)
                            if not t._blocksMovement:
                                ee.append(loc)

                    if len(ee) != 2:
                        errmsg = f'Door at ({lx}, {ly}) requires exactly 2 entrances/exits.'
                        raise LevelLoadError(self.filename, errmsg)

                    # Check they are opposite each other
                    dx = abs(ee[0][0] - ee[1][0])
                    dy = abs(ee[0][1] - ee[1][1])
                    if (dx == 0 and dy == 2) or (dx == 2 and dy == 0):
                        pass
                    else:
                        errmsg = f'Door at ({lx}, {ly}) has a mismatched entrance/exit.'
                        raise LevelLoadError(self.filename, errmsg)

                    # Create transition
                    tran = Transition(map, doorent, ee)
                    map.doortransitions[doorent] = tran
