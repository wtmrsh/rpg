"""Base class for a view."""


#
# Imports
#
from enum import IntEnum, auto

from bearlibterminal import terminal

import engine.views
import engine.observable as obs
from common import *
from engine.settings import GraphicsSettings, GameSettings
from engine.colour import Colour


class View:
    """
    Base view class.  Contains basic functionality for a view.
    """

    def __init__(self, name, layercount, viewmanager):
        """Create view."""
        self.eventidentifier = -1
        self.parent = None
        self.name = name
        self._baselayer = viewmanager.stateIndex * 10 + 1
        self.layercount = layercount
        self.viewmanager = viewmanager
        self.map = None
        self.buttons = list()
        self.hoverbutton = None
        self.show = True
        self.active = True
        self.dirty = True
        self.hasfocus = False
        self.mousecellx = -1
        self.mousecelly = -1
        self.mousepixelx = -1
        self.mousepixely = -1
        self.cursor = 'Pointer'
        self.tooltiptimer = None
        self.tooltiptext = None

        # Configure
        self.tooltiptimeramount = float(self.viewmanager.get_view_setting(self.name, 'tooltip-timer'))

        transamount = int(self.viewmanager.get_view_setting(self.name, 'transparency') * 255.0)
        transcolour = Colour(255, 255, 255, 255 - transamount)

        self.bordercolour = (Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'border')) * transcolour).to_bgra()
        self.focussedcolour = (Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'focussed')) * transcolour).to_bgra()
        self.backgroundcolour = (Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'background')) * transcolour).to_bgra()
        self.tooltiptextcolour = (Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'tooltip-text')) * transcolour).to_bgra()
        self.tooltipbgcolour = (Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'tooltip-bg')) * transcolour).to_bgra()
        self.hBorderSize = self.viewmanager.get_view_setting(self.name, 'sizes', 'hborder')
        self.vbordersize = self.viewmanager.get_view_setting(self.name, 'sizes', 'vborder')
        self.decorator = self.viewmanager.get_view_setting(self.name, 'decorator')

        # Decorator
        viewdec = GraphicsSettings.viewDecorators[self.decorator]
        codePage = viewdec.offset

        self.topleftpiece = codePage + viewdec.tiles['TopLeft'].offset
        self.toprightpiece = codePage + viewdec.tiles['TopRight'].offset
        self.bottomleftpiece = codePage + viewdec.tiles['BottomLeft'].offset
        self.bottomrightpiece = codePage + viewdec.tiles['BottomRight'].offset
        self.toppiece = codePage + viewdec.tiles['Top'].offset
        self.leftpiece = codePage + viewdec.tiles['Left'].offset
        self.bottompiece = codePage + viewdec.tiles['Bottom'].offset
        self.rightpiece = codePage + viewdec.tiles['Right'].offset
        self.centrepiece = codePage + viewdec.tiles['Centre'].offset
        
        self.tooltipleftpiece = codePage + viewdec.tiles['Tooltip-Left'].offset
        self.tooltiprightpiece = codePage + viewdec.tiles['Tooltip-Right'].offset
        self.tooltipcentrepiece = codePage + viewdec.tiles['Tooltip-Centre'].offset

        obs.on(None, 'UI.RefreshView', self.on_refresh_view)
        obs.on(None, 'UI.ClearView', self.on_clear_view)

    #
    # base_layer
    #
    @property
    def base_layer(self):
        return self._baselayer

    #
    # offset_x
    #
    @property
    def offset_x(self):
        return self.parent.offset_x + self.parent.child_x_offset(self)

    #
    # offset_y
    #
    @property
    def offset_y(self):
        return self.parent.offset_y + self.parent.child_y_offset(self)

    #
    # extent_x
    #
    @property
    def extent_x(self):
        return self.offset_x + self.width

    #
    # extent_y
    #
    @property
    def extent_y(self):
        return self.offset_y + self.height

    #
    # width
    #
    @property
    def width(self):
        return self.get_width()

    #
    # height
    #
    @property
    def height(self):
        return self.get_height()

    @property
    def client_x(self):
        return self.offset_x + self.vbordersize

    @property
    def client_y(self):
        return self.offset_y + self.hBorderSize

    #
    # client_extent_x
    #
    @property
    def client_extent_x(self):
        return self.client_x + self.client_width

    #
    # client_extent_y
    #
    @property
    def client_extent_y(self):
        return self.client_y + self.client_height

    #
    # client_width
    #
    @property
    def client_width(self):
        return self.width - self.vbordersize * 2

    #
    # client_height
    #
    @property
    def client_height(self):
        return self.height - self.hBorderSize * 2

    #
    # get_cursor()
    #
    def get_cursor(self):
        return self.cursor

    #
    # add_button
    #
    def add_button(self, button, padding=None):
        viewDec = GraphicsSettings.viewDecorators[self.decorator]
        codePage = viewDec.offset

        button.leftpiece = codePage + viewDec.tiles['Button-Left'].offset
        button.rightpiece = codePage + viewDec.tiles['Button-Right'].offset
        button.centrepiece = codePage + viewDec.tiles['Button-Centre'].offset

        button.topleftpiece = codePage + viewDec.tiles['Button-TopLeft'].offset
        button.toprightpiece = codePage + viewDec.tiles['Button-TopRight'].offset
        button.toppiece = codePage + viewDec.tiles['Button-Top'].offset

        button.bottomleftpiece = codePage + viewDec.tiles['Button-BottomLeft'].offset
        button.bottomrightpiece = codePage + viewDec.tiles['Button-BottomRight'].offset
        button.bottompiece = codePage + viewDec.tiles['Button-Bottom'].offset

        # No transparency for buttons as it messes up the cell background
        transAmount = 0
        transColour = Colour(255, 255, 255, 255 - transAmount)

        button.buttonBackColour =(Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'button-bg')) * transColour).to_bgra()
        button.buttonColour = (Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'button-text')) * transColour).to_bgra()
        button.buttonBackHoveredColour =(Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'button-bg-hovered')) * transColour).to_bgra()
        button.buttonHoveredColour = (Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'button-text-hovered')) * transColour).to_bgra()

        button.buttonBackColour =(Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'button-bg')) * transColour).to_bgra()
        button.buttonColour = (Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'button-text')) * transColour).to_bgra()
        button.buttonBackHoveredColour =(Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'button-bg-hovered')) * transColour).to_bgra()
        button.buttonHoveredColour = (Colour.from_bgra(self.viewmanager.get_view_setting(self.name, 'colours', 'button-text-hovered')) * transColour).to_bgra()

        button.padding = padding if padding is not None else int(self.viewmanager.get_view_setting(self.name, 'sizes', 'button-padding'))

        self.buttons.append(button)

    def get_width(self):
        return self.viewmanager.get_view_setting(self.name, 'sizes', 'width')

    def get_height(self):
        return self.viewmanager.get_view_setting(self.name, 'sizes', 'height')

    def on_added_to_container(self, container):
        pass

    def on_removed_from_container(self, container):
        pass

    def set_focus(self, focus):
        self.hasfocus = focus

    def change_map(self, map):
        obs.off(None, 'UI.RefreshView', self.on_refresh_view)
        obs.off(None, 'UI.ClearView', self.on_clear_view)

        self.unregister_events()
        self.map = map

        obs.on(None, 'UI.RefreshView', self.on_refresh_view)
        obs.on(None, 'UI.ClearView', self.on_clear_view)
        self.register_events()

    def register_events(self):
        pass

    def unregister_events(self):
        pass

    #
    # crop()
    #
    def crop(self, x = None, y = None, width = None, height = None):
        """
        """
        terminal.crop(x or self.offset_x, y or self.offset_y, width or self.width, height or self.height)

    #
    # uncrop
    #
    def uncrop(self):
        """
        """
        terminal.crop()

    def on_refresh_view(self, source, view):
        self.set_dirty(view)

    def on_clear_view(self, source, view):
        self.clear(view)

    #
    # set_dirty()
    #
    def set_dirty(self, view):
        view.dirty = True

    #
    # clear()
    #
    def clear(self, view):
        terminal.bkcolor(Colour.from_name('black').to_bgra())
        for layer in range(view.base_layer, view.base_layer + view.layercount):
            terminal.layer(layer)
            terminal.clear_area(view.offset_x, view.offset_y, view.width, view.height)

    #
    # mouse_entered()
    #
    def mouse_entered(self, fromView, cellX, cellY, pixelX, pixelY):
        self.mousecellx = cellX
        self.mousecelly = cellY
        self.mousepixelx = pixelX
        self.mousepixely = pixelY
        self.on_mouse_entered(fromView)
        obs.trigger(self, 'UI.View.MouseEntered', self, cellX, cellY, pixelX, pixelY)

    #
    # mouse_exited()
    #
    def mouse_exited(self, toView):
        self.mousecellx = -1
        self.mousecelly = -1
        self.mousepixelx = -1
        self.mousepixely = -1
        self.on_mouse_exited()

        if self.hoverbutton:
            self.hoverbutton = None
            obs.trigger(self, 'UI.RefreshView', self)

        obs.trigger(self, 'UI.View.MouseExited', self)

    #
    # mouse_moved()
    #
    def mouse_moved(self, cellX, cellY, pixelX, pixelY):
        self.mousecellx = cellX
        self.mousecelly = cellY
        self.mousepixelx = pixelX
        self.mousepixely = pixelY

        # Check buttons
        refreshView = False
        hoverbutton = None
        self.cursor = 'Pointer'

        for button in self.buttons:

            if not button.show:
                continue

            buttonSize = button.width + button.padding * 2
            if button.x < 0:
                buttonX = button.parent.client_extent_x - buttonSize
            else:
                buttonX = button.offset_x

            buttonY = button.offset_y
            if self.mousecellx >= buttonX and self.mousecellx < (buttonX + buttonSize) and self.mousecelly >= buttonY and self.mousecelly < (buttonY + button.height):
                hoverbutton = button
                break

        if hoverbutton != self.hoverbutton:
            refreshView = True
            if hoverbutton:
                self.tooltiptimer = 0.0
            else:
                self.tooltiptimer = None

        self.hoverbutton = hoverbutton
        if self.hoverbutton:
            self.cursor = 'Hand'

        if self.on_mouse_moved(self) or refreshView:
            obs.trigger(self, 'UI.RefreshView', self)

        obs.trigger(self, 'UI.View.MouseMoved', self, cellX, cellY, pixelX, pixelY)

    #
    # on_mouse_entered()
    #
    def on_mouse_entered(self, fromView):
        pass

    #
    # on_mouse_exited()
    #
    def on_mouse_exited(self):
        pass

    #
    # on_mouse_exited()
    #
    def on_mouse_moved(self, view):
        return False

    #
    # update()
    #
    def update(self, frametime):
        showToolTip = False
        if self.tooltiptimer is not None:
            self.tooltiptimer += frametime
            if self.tooltiptimer >= self.tooltiptimeramount:
                showToolTip = True

        self.show_tooltip(showToolTip)

    #
    # show_tooltip()
    #
    def show_tooltip(self, show):
        wasShown = self.tooltiptext is not None
        if show:
            self.tooltiptext = self.hoverbutton.helpText
        else:
            self.tooltiptext = None

        if show or wasShown:
            obs.trigger(self, 'UI.RefreshView', self)

    #
    # render_background()
    #
    def render_background(self):
        terminal.layer(self.base_layer)

        if self.client_width == self.width and self.client_height == self.height:
            # No border
            terminal.color(self.backgroundcolour)
            for y in range(self.offset_y, self.extent_y):
                for x in range(self.offset_x, self.extent_x):
                    terminal.put(x, y, self.centrepiece)
        else:
            if self.hasfocus:
                terminal.color(self.focussedcolour)
            else:
                terminal.color(self.bordercolour)

            if self.client_width != self.width and self.client_height != self.height:
                terminal.put(self.client_x - self.hBorderSize, self.client_y - self.vbordersize, self.topleftpiece)
                terminal.put(self.client_extent_x, self.client_y - self.vbordersize, self.toprightpiece)
                terminal.put(self.client_x - self.hBorderSize, self.client_extent_y, self.bottomleftpiece)
                terminal.put(self.client_extent_x, self.client_extent_y, self.bottomrightpiece)

            if self.hBorderSize > 0:
                for x in range(self.client_x, self.client_extent_x):
                    terminal.put(x, self.offset_y, self.toppiece)
                    terminal.put(x, self.client_extent_y, self.bottompiece)

            if self.vbordersize > 0:
                for y in range(self.client_y, self.client_extent_y):
                    terminal.put(self.offset_x, y, self.leftpiece)
                    terminal.put(self.client_extent_x, y, self.rightpiece)

            terminal.color(self.backgroundcolour)
            for y in range(self.client_y, self.client_extent_y):
                for x in range(self.client_x, self.client_extent_x):
                    terminal.put(x, y, self.centrepiece)

    def calculate_tooltip_position(self):
        if self.mousepixelx > (GameSettings.windowWidth / 2):
            xOffset = -1
        else:
            xOffset = 1
        if self.mousepixely > (GameSettings.windowHeight / 2):
            yOffset = -1
        else:
            yOffset = 1

        xPos = self.mousepixelx + xOffset * CELL_SIZE
        yPos = self.mousepixely + yOffset * CELL_SIZE
        if xOffset < 0:
            xPos -= len(self.tooltiptext) * CELL_SIZE
            
        return xPos, yPos
    
    def render_tooltip_background(self):
        terminal.layer(250)
        terminal.clear_area(0, 0, GameSettings.windowWidth, GameSettings.windowHeight)
        if self.tooltiptext is not None:
            xPos, yPos = self.calculate_tooltip_position()
            mouseCellX = xPos // CELL_SIZE
            mouseCellY = yPos // CELL_SIZE

            textSize = terminal.measure(self.tooltiptext, GameSettings.windowWidth, GameSettings.windowHeight)
            bgLength = textSize[0]
            
            terminal.color(self.tooltipbgcolour)
            terminal.put_ext(mouseCellX, mouseCellY, xPos % CELL_SIZE, yPos % CELL_SIZE, self.tooltipleftpiece)
            for x in range(mouseCellX + 1, mouseCellX + bgLength - 1):
                terminal.put_ext(x, mouseCellY, xPos % CELL_SIZE, yPos % CELL_SIZE, self.tooltipcentrepiece)
            terminal.put_ext(mouseCellX + bgLength - 1, mouseCellY, xPos % CELL_SIZE, yPos % CELL_SIZE, self.tooltiprightpiece)

    def render_tooltip(self):
        terminal.layer(251)
        terminal.clear_area(0, 0, GameSettings.windowWidth, GameSettings.windowHeight)
        if self.tooltiptext is not None:
            xPos, yPos = self.calculate_tooltip_position()
            mouseCellX = xPos // CELL_SIZE
            mouseCellY = yPos // CELL_SIZE
            
            tooltipText = '[offset=%d,%d][color=%d]%s' % (xPos % CELL_SIZE, yPos % CELL_SIZE, self.tooltiptextcolour, self.tooltiptext)
            terminal.print(mouseCellX, mouseCellY, tooltipText)                  

    #
    # render()
    #
    def render(self):
        pass

    #
    # display()
    #
    def display(self):
        if self.show and self.dirty:
            self.clear(self)
            
            # View backgrounds
            self.render_background()

            # Button backgrounds
            for button in self.buttons:
                if not button.show:
                    continue

                button.clear()
                if button.renderBackground:
                    button.render_background(button == self.hoverbutton)
            
            # Tooltip backgrounds
            self.render_tooltip_background()
            
            # View
            self.render()

            # Buttons
            for button in self.buttons:
                if not button.show:
                    continue

                button.render(button == self.hoverbutton)

            # Tooltip
            self.render_tooltip()

            self.dirty = False
                