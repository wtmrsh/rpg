"""Exceptions"""


#
# Exit
#
class ExitException(Exception):

    def __init__(self, exitCode = 0):
        super().__init__()
        self.exitCode = exitCode


#
# LogicError
#
class LogicError(Exception):
    """Exception used for control flow in game logic."""
    def __init__(self, msg):
        super().__init__(msg)


#
# LevelLoad
#
class LevelLoadError(Exception):
    """Exception raised when failing to load a level."""
    def __init__(self, filename, msg):
        super().__init__(msg)
        self.filename = filename


#
# SpawnChildState
#
class SpawnChildStateException(Exception):
    """Change game state to a child one, from which it is returnable to the parent."""
    def __init__(self, tostate, *args, **kwargs):
        super().__init__()
        self.tostate = tostate
        self.args = args
        self.kwargs = kwargs


#
# TransitionToState
#
class TransitionToStateException(Exception):
    """Change game state to a new one, ditching the previous."""
    def __init__(self, tostate, popparent=False, *args, **kwargs):
        super().__init__()
        self.tostate = tostate
        self.popparent = popparent
        self.args = args
        self.kwargs = kwargs


#
# ExitState
#
class ExitStateException(Exception):
    """Exit current game state"""
    def __init__(self, playeraction, *args, **kwargs):
        super().__init__()
        self.playeraction = playeraction
        self.args = args
        self.kwargs = kwargs
