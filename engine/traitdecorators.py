"""
Trait system: decorators.

Formatted 16/09
"""

#
# Imports
#
from collections import defaultdict

import engine.observable as obs


#
# base_trait_inst()
#
def base_trait_inst(ent, membertype, memberclass, **memberkwargs):
    """
    Instantiate a base trait.
    
    :param ent: Entity to add to
    :param membertype: type of Trait
    :param memberkwargs: Trait instantiation arguments
    """
    if not hasattr(ent, '_traitInformation'):
        setattr(ent, '_traitInformation', dict())

    membername = membertype[0].lower() + membertype[1:]
    ent._traitInformation[membername] = [membertype, memberclass, memberkwargs]


#
# base_trait()
#
def base_trait(memberType, memberKwVisuals, **memberKwArgs):
    """
    Decorator to wrap base trait instantiation.
    
    :param memberType: type of Trait
    :param memberKwArgs: Trait instantiation arguments
    :return: wrapped class
    """
    def inner(cls):
        oldinit = cls.__init__

        def newinit(self, *args, **kwargs):
            base_trait_inst(self, memberType, memberType, memberKwVisuals, **memberKwArgs)
            oldinit(self, *args, **kwargs)

        cls.__init__ = newinit
        return cls
    return inner
