import engine.observable as obs

eventhandlers = list()


def event_handler(event, source=None):
    def inner(fn):
        eventhandlers.append((source, event, fn))
        obs.on(source, event, fn)
        return fn
    return inner


def remove_script_objects():
    # Event handlers
    for handler in eventhandlers:
        obs.off(handler[0], handler[1], handler[2])

    eventhandlers.clear()
