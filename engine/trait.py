"""
Trait system.

Formatted 20/09
"""

#
# Imports
#
import engine.observable as obs


class Trait:
    """Base class used by traits."""
    
    #
    # __init__()__
    #
    def __init__(self, owner):
        """
        Constructor.
        
        :param owner: Entity that owns the trait.
        """
        self.owner = owner
        self.enabled = True

        # Events
        self.eventhandlers = list()
        self.visualstatuses = dict()

    #
    # register_event()
    #
    def register_event(self, source, event, handler):
        self.eventhandlers.append((source, event, handler))

    #
    # register_events()
    #
    def register_events(self):
        for (source, event, handler) in self.eventhandlers:
            obs.on(source, event, handler)

    #
    # unregister_events()
    #
    def unregister_events(self):
        for (source, event, handler) in self.eventhandlers:
            obs.off(source, event, handler)

    #
    # on_added_to_map()
    #
    def on_added_to_map(self, map):
        self.register_events()

    #
    # on_removed_from_map()
    #
    def on_removed_from_map(self, map):
        self.unregister_events()

    #
    # turn()
    #
    def turn(self, curturn):
        pass