"""Tilesets"""


#
# ImageTileSet
#    
class ImageTileSet:
    """
    """
    
    #
    # Subclass for tile
    #
    class Tile:
        """
        """
        
        #
        # Constructor
        #
        def __init__(self, offset):
            self.offset = offset
            
    #
    # Constructor
    #
    def __init__(self, offset):
        """
        """
        self.tiles = {}
        self.offset = offset

    #
    # Add a tile
    #
    def add_tile(self, name, offset):
        """
        """
        t = ImageTileSet.Tile(offset)
        self.tiles[name] = t
        return t


#
# ImageTileList
#
class ImageTileList:
    """
    """

    #
    # Subclass for tile
    #
    class Tile:
        """
        """

        #
        # Constructor
        #
        def __init__(self, type, properties):
            self.type = type
            self.properties = properties

    #
    # Constructor
    #
    def __init__(self, offset, count):
        """
        """
        self.tiles = [None] * count
        self.offset = offset

    #
    # Add a tile
    #
    def add_tile(self, index, type, properties):
        """
        """
        t = ImageTileList.Tile(type, properties)
        self.tiles[index] = t
        return t
