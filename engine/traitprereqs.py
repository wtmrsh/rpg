"""Trait prerequisites."""

#
# Imports
#
from collections import defaultdict

from engine.defines import ActionRequestResult
from engine.tiletype import TileType

actorPrereqs = defaultdict(set)
targetPrereqs = defaultdict(set)


#
# actor_prereq
#
def actor_prereq(action):
    def inner(fn):
        actorPrereqs[action].add(fn)
        return fn
    return inner


#
# target_prereq
#
def target_prereq(action):
    def inner(fn):
        targetPrereqs[action].add(fn)
        return fn
    return inner


#
# prereq_is_lockable
#
@target_prereq('Lock')
def prereq_is_lockable(target, actor, *args, **kwargs):
    """
    Check if the owning Entity can be locked.

    :param target: owning Entity
    :param actor: acting Entity
    :return: boolean success
    """
    self = target.lockable
    aname = actor.def_name()
    tname = target.def_name()

    if target.has_state('Open'):
        pa = {
            'Action': 'Close',
            'Args': {
            }
        }
        return ActionRequestResult(passed=False,
                                   actionrequired=True,
                                   playermsg=f'{tname} is open.',
                                   othermsg=f'{aname} tries to lock {tname} but it is open.',
                                   prereqactions=[pa])
    elif target.has_state('Locked'):
        return ActionRequestResult(passed=False,
                                   actionrequired=False,
                                   playermsg=f'{tname} is already locked.',
                                   othermsg=f'{aname} tries to lock {tname} but it is already locked.',
                                   prereqactions=list())
    else:
        if len(self.codes) == 0:
            return ActionRequestResult(passed=False,
                                       actionrequired=True,
                                       playermsg=f'{tname} cannot be locked.',
                                       othermsg=f'{aname} tries to lock {tname} but it cannot be lcoked.',
                                       prereqactions=list())
        else:
            actorKeys = set()
            if actor.has_trait('Key'):
                actorKeys |= actor.key.codes
            if actor.has_trait('Inventory'):
                actorKeys |= actor.inventory.get_key_codes()

            if len(actorKeys & self.codes) == 0:
                return ActionRequestResult(passed=False,
                                           actionrequired=True,
                                           playermsg=f'You do not have the right key to lock {tname}.',
                                           othermsg=f'{aname} tries to lock {tname} but does not have the right key.',
                                           prereqactions=list())

    return ActionRequestResult(passed=True,
                               actionrequired=True,
                               playermsg=None,
                               othermsg=None,
                               prereqactions=list())


#
# prereq_is_unlockable
#
@target_prereq('Unlock')
def prereq_is_unlockable(target, actor, *args, **kwargs):
    """
    Check if the owning Entity can be unlocked.

    :param target: owning Entity
    :param actor: acting Entity
    :return: boolean success
    """
    self = target.lockable
    aname = actor.def_name()
    tname = target.def_name()

    if target.has_state('Open'):
        return ActionRequestResult(passed=False,
                                   actionrequired=True,
                                   playermsg=f'{tname} is open.',
                                   othermsg=f'{aname} tries to unlock {tname} but it is open.',
                                   prereqactions=list())
    elif not target.has_state('Locked'):
        return ActionRequestResult(passed=False,
                                   actionrequired=False,
                                   playermsg=f'{tname} is not locked.',
                                   othermsg=f'{aname} tries to unlock {tname} but it is not locked.',
                                   prereqactions=list())

    # If this entity has no codes, then it can't be unlocked.  If it does, then check if the unlocker is itself a key.
    # Also check if it has any keys in inventory.
    if actor.actorType == 'Map':
        return ActionRequestResult(passed=True,
                                   actionrequired=True,
                                   playermsg=None,
                                   othermsg=None,
                                   prereqactions=list())
    elif len(self.codes) > 0:
        actorKeys = set()
        if actor.has_trait('Key'):
            actorKeys |= actor.key.codes
        if actor.has_trait('Inventory'):
            actorKeys |= actor.inventory.get_key_codes()

        if len(actorKeys & self.codes) == 0:
            return ActionRequestResult(passed=False,
                                       actionrequired=True,
                                       playermsg=f'You do not have the right key to unlock {tname}.',
                                       othermsg=f'{aname} tries to unlock {tname} but does not have the right key.',
                                       prereqactions=list())
        else:
            return ActionRequestResult(passed=True,
                                       actionrequired=True,
                                       playermsg=None,
                                       othermsg=None,
                                       prereqactions=list())
    else:
        return ActionRequestResult(passed=False,
                                   actionrequired=True,
                                   playermsg=f'{tname} cannot be unlocked.',
                                   othermsg=f'{aname} tries to unlock {tname} but cannot.',
                                   prereqactions=list())


#
# prereq_is_openable
#
@target_prereq('Open')
def prereq_is_openable(target, actor, *args, **kwargs):
    """
    Check if the owning Entity can be opened.

    :param target: owning Entity
    :param actor: acting Entity
    :return: boolean success
    """
    aname = actor.def_name()
    tname = target.def_name()

    if target.has_state('Open'):
        return ActionRequestResult(passed=False,
                                   actionrequired=False,
                                   playermsg=f'{tname} is already open.',
                                   othermsg=f'{aname} tries to open {tname} but it is already open.',
                                   prereqactions=list())
    elif target.has_state('Locked'):
        return ActionRequestResult(passed=False,
                                   actionrequired=False,
                                   playermsg=f'{tname} is locked.',
                                   othermsg=f'{aname} tries to open {tname} but it is locked.',
                                   prereqactions=['Unlock'])
    else:
        return ActionRequestResult(passed=True,
                                   actionrequired=True,
                                   playermsg=None,
                                   othermsg=None,
                                   prereqactions=list())


#
# prereq_is_closeable
#
@target_prereq('Close')
def prereq_is_closeable(target, actor, *args, **kwargs):
    """
    Check if the owning Entity can be closed.

    :param target: owning Entity
    :param actor: acting Entity
    :return: boolean success
    """
    aname = actor.def_name()
    tname = target.def_name()

    if not target.has_state('Open'):
        return ActionRequestResult(passed=False,
                                   actionrequired=False,
                                   playermsg=f'{tname} is already closed.',
                                   othermsg=f'{aname} tries to close {tname} but it is already closed.',
                                   prereqactions=[])

    # Special case for doors.
    targettile = target.map.tiles[target.x][target.y]
    if targettile.type == TileType.DOOR and target.tile.entity:
        if targettile.entity.is_player:
            return ActionRequestResult(passed=False,
                                       actionrequired=True,
                                       playermsg=f'You are blocking {tname} from being closed.',
                                       othermsg=f'{aname} tries to close {tname} but you are in the way.',
                                       prereqactions=list())
        else:
            return ActionRequestResult(passed=False,
                                       actionrequired=True,
                                       playermsg=f'There are items blocking {tname} from being closed.',
                                       othermsg=f'{aname} tries to close {tname} but there are items in the way.',
                                       prereqactions=list())
    else:
        return ActionRequestResult(passed=True,
                                   actionrequired=True,
                                   playermsg=None,
                                   othermsg=None,
                                   prereqactions=list())
